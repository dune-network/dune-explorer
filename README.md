# DunScan

DunScan is a block explorer for the Dune blockchain.

DunScan has been developed by [OCamlPro](http://www.ocamlpro.com) since
September 2017.

## Issues and Contributions

Check the project on [Gitlab.com](https://gitlab.com/dune-network/dune-explorer)
for the latest sources, issues and contributions:

* Issues: Check the [Issues](https://gitlab.com/dune-network/dune-explorer/issues)
  and submit a new one !
* Contributions: Check the
  [Contributions](https://gitlab.com/dune-network/dune-explorer/merge_requests)
  and submit your MR !

## Copyright and license

Copyright OCamlPro 2017-2019. This code is licensed under the terms
of the GNU Public License version 3 (GPL v3).

## Building

You need OCaml 4.08.1.

Update submodules:

	git submodule init
	git submodule update

### Dependencies

DunScan has multiple OCaml and non OCaml dependencies.
The non OCaml dependencies must be installed first as they are necessary
for the OCaml ones.

#### Non OCaml dependencies

On Debian 9:

	sudo apt install \
    postgresql libsodium-dev libgeoip1 \
    geoip1-database libcurl4-gnutls-dev \
    curl zlib1g-dev libgeoip-dev pkg-config \
    libgmp-dev


On Ubuntu:

	sudo apt install \
    postgresql libsodium-dev libgeoip-dev \
    geoip-database libcurl4-gnutls-dev \
    curl zlib1g-dev libgeoip-dev pkg-config \
    libgmp-dev


On CentOS 7 (utdrmac):

Base packages:

	yum install epel-release
	yum install gcc m4 make libsodium libsodium-devel geoip geoip-devel zlib libcurl libcurl-devel

	rpm -Uvh https://yum.postgresql.org/10/redhat/rhel-7-x86_64/pgdg-centos10-10-2.noarch.rpm
	yum install postgresql10 postgresql10-server postgresql10-libs

check that the version of libsodium is at least 1.0.18

#### OCaml dependencies

Before installing and compiling all dependencies, you need to disable
the sandbox mode of opam. Check the file `~/.opam/config`, and comment
the lines below if there are still there:

	wrap-build-commands:
	  ["%{hooks}%/sandbox.sh" "build"] {os = "linux" | os = "macos"}
	wrap-install-commands:
      ["%{hooks}%/sandbox.sh" "install"] {os = "linux" | os = "macos"}
	wrap-remove-commands:
	  ["%{hooks}%/sandbox.sh" "remove"] {os = "linux" | os = "macos"}

When you finished the compilation, you can reenable the sandboxing
mode of opam.

Then the dependencies can (and should) be installed with opam through

	opam install \
    base64.3.4.0 \
    cohttp-lwt-unix.2.5.4 \
    csv-lwt.2.4 \
    ezjsonm.1.2.0 \
    geoip.0.0.3 \
    js_of_ocaml-lwt.3.8.0 \
    js_of_ocaml-tyxml.3.8.0 \
    lwt.5.3.0 \
    lwt_log.1.1.1 \
    ocplib-endian.1.1 \
    ocurl.0.9.1 \
    omd.1.3.1 \
	tls.0.11.1 \
    ocplib-json-typed-bson.0.7.1 \
    alcotest.1.2.3 \
    pgocaml.4.2.2 \
    extunix.0.2.0 \
    httpaf-lwt-unix.0.7.0 \
    bigstring.0.3 \
    digestif.1.0.0 \
	zarith.1.11 \
	zarith_stubs_js.v0.14.0 \
	stdlib-shims.0.1.0 \
	ezjs_odometer.0.1 \
	ezjs_recaptcha.0.1.1 \
	ezjs_qrcode.0.1 \
	ezjs_push.0.2 \
	ezjs_cleave.0.1.1 \
	ezjs_jquery.0.1 \
	ezjs_cytoscape.0.1 \
	ezjs_blockies.0.1.1

	opam pin add metal-api https://gitlab.com/dune-network/dune-metal.git

### Configuring

#### Configuring Postgresql

For DunScan to compile successfully, you will need your user to be able
to create tables in Postgresql. This is done with the following
commands:

	sudo -i -u postgres
	psql
	CREATE USER <user>;
	ALTER ROLE <user> CREATEDB;
	ALTER ROLE <user> SET search_path to db, public;


where `<user>` should be replaced by your login.


#### Configuring DunScan

You can create a file `config/Makefile.database` with the following options:

	DATABASE=mainnet
	DESTSUBDIR=dunscan
	WITH_VERSION=true
	API_HOST:=localhost
	API_PORT:=8080
	NEED_PARSEXP=true
	BASE64_3:=true


The options are:
* DATABASE: the database to use (you can use different names for other networks
    or tests)
* DESTSUBDIR: only used on the production server (the destination subdir
    in /var/www)
* WITH_VERSION: set to false to avoid recompiling everything everytime
* API_HOST & API_PORT: if you want to use localhost instead of api.dunscan.io
* NEED_PARSEXP: if you use sexplib (>= v0.11), then set it to true
* BASE64_3: if you use base64 (v>=3.0.0), then set it to true

### Building

You can now start the compilation:

	make


If no error happened, you should get :
* A binary called `bin/dunscan-crawler`
* A binary called `bin/dunscan-api-server`
* A directory `www` containing at least an `explorer-main.js` file

## Usage

### Overview

DunScan is composed of the following components:
* A Dune crawler, called `dunscan-crawler`. It regularly connects to a
  Dune node to query new blocks, and register these blocks in a Postgresql
  database;
* An API server, called `dunscan-api-server`. It provides a REST RPC that can
  be used to read information from the Postgresql database. Some of the requests
  are also used to query local Dune nodes;
* A webapp, stored in `www/`. The webapp runs some Javascript code to
  display information in a browser.

There are mostly two ways to contribute to DunScan:
* You can just use the webapp. For that, you just need a local web server.
  By default, it will connect to the official DunScan API server. If you
  do modifications, you can immediately check the results without running
  your own Dune node.
* You can run a full DunScan node: you need to run a Dune node,
  to run DunScan `dunscan-crawler` to fill the database, to run
  `dunscan-api-server` to give access to the database, and to run a
  local web server for the webapp.

### Testing the Interface Locally

You need to run a Web Server, that will give access to the `www/` directory
of the webapp. A simple way is to use the web server included in PHP:

First, install PHP:

	sudo apt-get install php


Go into `www/`:

	cd www
	php -S localhost:8888


and view the webpage:

	xdg-open http://localhost:8888/


If you didn't modify `API_PORT` and `API_HOST` in `Makefile.database`,
the webapp will use the official DunScan API Server. Otherwise, it will
try to connect to your API server.

Note that you edit `static/info.json` (that will be copied to
`www/info.json`) to change some informations on your web server, such
as the URL of the API server, the languages available or the networks
you are tracking.

### Filling the Database with the Crawler

Create a file `config/config.json` containing :

	[
	  {
		"name": "mainnet-main",
		"crawler" : [
          {"url" : "http://dune-node", "port" : 8732}
	    ],
		"api" : [
          {"url" : "http://dune-node", "port" : 8732},
	    ]
	  } ,

	  {
        "name" : "mainnet-extra-data",
		"crawler" : [
          {"url" : "http://dune-node", "port" : 8732}
        ],
		"api" : [ ]
	  }
	]


Start the crawler:

	./bin/dunscan-crawler config.json


It should immediately connect to the Dune node, and rewind the chain
to register blocks from the genesis block to the latest one. Then, it
should wait and connect to the Dune node every 3 seconds to query for new
blocks.

To get extra non-critical data like marketcap data or voting data, you
should run another instance of the crawler with `--extra-data` option


	./bin/dunscan-crawler --extra-data config.json


This instance of the crawler will read the `$NETWORK-extra-data` field
from your config.json

### Running the API Server Locally

You need first to have started the Dune crawler as explained in the
previous section.

Modify file `config/Makefile.database` with:


	API_PORT = 8080
	API_HOST = localhost


and rebuild everything:

	make


Start the API server (on port 8080):

	./bin/dunscan-api-server --node-config config/config.json --api-config config/api-config.json


Note that you might want to change `config/api-config.json` to suit the parameters of your network, if you are running a private Dune network. For example, you can use:

	dune-client -A localhost -p 8132 rpc get /chains/main/blocks/head/context/constants


to get the constants of your network, and modify `api-config.json`
accordingly.

Note that, if you need to modify `static/info.json`, it is usually a good idea to copy them in
`static/local/` and modify these files. They will be copied in `www/`
at every `make` command.
