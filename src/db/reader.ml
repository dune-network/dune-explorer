(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types
open Db_intf

let verbose_counter = ref 0

let search_limit = 20
let debug fmt = Utils.debug !Debug_constants.debug_reader fmt

module Reader_generic (M : Db_intf.MONAD) = struct
  module Monad = M
  open M

  module PGOCaml_old = PGOCaml_generic.Make(M)

  module PGOCaml = struct
    include PGOCaml_old
    let prepare dbh ~name ~query () =
      debug "DB %S PREPARE %s\n%!" name query;
      prepare dbh ~name ~query ()

    let execute_rev dbh ~name ~params () =
      if !Debug_constants.debug_reader then begin
        incr verbose_counter;
        let counter = !verbose_counter in
        Printf.eprintf "DB x%dx begin %s\n%!" counter name;
        bind (execute_rev dbh ~name ~params ())
          (fun rows ->
             Printf.eprintf "DB x%dx end %s\n%!" counter name;
             return rows)
      end else
        execute_rev dbh ~name ~params ()
  end

  let dbh_pool =
    let validate conn =
      PGOCaml.alive conn >>= fun is_alive ->
      debug "[Reader] Validate connection : [%b]\n%!" is_alive ;
      M.return is_alive in
    let check _conn is_ok =
      debug "[Reader] Check connection.\n%!" ;
      is_ok false in
    let dispose conn =
      debug "[Reader] Dispose connection.\n%!" ;
      PGOCaml.close conn in
    M.pool_create ~check ~validate ~dispose 20 (fun () ->
      PGOCaml.connect ~database:DunscanConfig.database ())

  let with_dbh f = M.pool_use dbh_pool f

  let (>>>) f g = f g

  let of_count_pair =
    let opt_check = function
        Some count -> Int64.to_int count
      | None -> 0 in
    function
    | [ a,b ] -> return (opt_check a, opt_check b)
    | _ -> return (0,0)

  let of_dbf f = function
    | [ c ] -> return (f c)
    | _ -> return (f 0L)
  let of_db_optf f = function
    | [ Some c ] -> return (f c)
    | _ -> return (f 0L)
  let of_db = of_dbf (fun x -> x)
  let of_db_opt = of_db_optf (fun x -> x)
  let of_count_opt = of_db_optf Int64.to_int
  let of_count = of_dbf Int64.to_int

  let test_opt = Pg_helper.test_opt
  let test_opti = Pg_helper.test_opti

  let block_successor hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT bl.hash FROM block as bl \
       INNER JOIN block as pred ON pred.hash = bl.predecessor \
       WHERE bl.predecessor = $hash AND bl.hash <> $hash AND \
       bl.distance_level = pred.distance_level"] >>= function
    | [] -> return None
    | h :: _ -> return (Some h)

  let level ?hash () =
    let hash, nohash = test_opti hash in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT level, level_position, cycle, cycle_position, voting_period, \
       voting_period_position \
       FROM block WHERE (($nohash AND distance_level = 0) OR hash = $?hash) \
       ORDER BY level DESC LIMIT 1"] >>= fun rows ->
    match Pg_helper.rows_to_option rows with
    | None -> return None
    | Some ( lvl_level, lvl_level_position,
             lvl_cycle, lvl_cycle_position,
             lvl_voting_period, lvl_voting_period_position ) ->
      let lvl_level = Int64.to_int lvl_level in
      let lvl_level_position = Int64.to_int lvl_level_position in
      let lvl_cycle = Int64.to_int lvl_cycle in
      let lvl_cycle_position = Int64.to_int lvl_cycle_position in
      let lvl_voting_period = Int64.to_int lvl_voting_period in
      let lvl_voting_period_position = Int64.to_int lvl_voting_period_position in
      return (Some { lvl_level; lvl_level_position; lvl_cycle; lvl_cycle_position;
                     lvl_voting_period; lvl_voting_period_position  })

  let current_cycle dbh =
    [%pgsql dbh "SELECT MAX(cycle) FROM block WHERE distance_level = 0"]
    >>= of_db_opt

  let nb_cycle () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT MAX(cycle) + 1 FROM block"] >>= of_count_opt

  let limits page page_size =
    Int64.of_int (page * page_size), Int64.of_int page_size

  let nonces ?(page=0) ?(page_size=20) () =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT b.cycle, array_agg(b.level::bigint), array_agg(s.hash::varchar), \
       array_agg(b.hash::varchar) FROM block AS b \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(b.level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= b.level AND cst.level_end IS NULL) \
       LEFT JOIN (SELECT * FROM seed_nonce_revelation_all WHERE distance_level = 0) AS s ON s.level = b.level \
       WHERE b.distance_level = 0 AND b.level % blocks_per_commitment = 0 \
       GROUP BY b.cycle ORDER BY b.cycle DESC OFFSET $offset LIMIT $limit"]
    >>= fun rows ->
    return @@
    List.rev @@ List.fold_left (fun acc row ->
        match row with
        | (cycle, Some levels, Some op_hashes, Some bl_hashes) ->
          if List.for_all (fun hash -> hash = None) op_hashes then acc
          else
            let nonces =
              List.sort (fun (_, level1, _) (_, level2,_) -> compare level1 level2) @@
              List.mapi (fun i level ->
                  List.nth op_hashes i,
                  Option.fold ~none:0 ~some:Int64.to_int level,
                  Option.value ~default:"" (List.nth bl_hashes i)) levels in
          (Int64.to_int cycle, nonces) :: acc
        | _ -> acc ) [] rows


  let block ?(operations=false) selector =
    with_dbh >>> fun dbh ->
    if operations then begin
      match selector with
      | Hash hash ->
        [%pgsql.object dbh
            "SELECT b.*, array_agg(o.hash::varchar) as op_hash, array_agg(o.op_type) as op_type \
             FROM block AS b \
             INNER JOIN operation AS o ON o.block_hash = b.hash \
             WHERE b.hash = $hash GROUP BY b.hash"]
      | Level level ->
        let level = Int64.of_int level in
        [%pgsql.object dbh
            "SELECT b.*, array_agg(o.hash::varchar) as op_hash, array_agg(o.op_type) as op_type \
             FROM block AS b \
             INNER JOIN operation AS o ON o.block_hash = b.hash \
             WHERE level = $level AND distance_level = 0 GROUP BY b.hash"]
    end >>= fun rows ->
      match Pg_helper.rows_to_option rows with
      | Some row ->
        return @@ Some (Pg_helper.block_with_ops row)
      | None -> return None
    else begin
      match selector with
      | Hash hash ->
        [%pgsql.object dbh "SELECT * FROM block WHERE hash = $hash"]
      | Level level ->
        let level = Int64.of_int level in
        [%pgsql.object dbh "SELECT * FROM block WHERE level = $level AND distance_level = 0"]
    end >|= (fun rows -> Pg_helper.rows_to_option rows |> Option.map Pg_helper.block_noop)

  let blocks ?(page=0) ?(page_size=20) ?(operations=false) () =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    if operations then
      [%pgsql.object dbh
        "SELECT b.*, array_agg(o.hash::varchar) as op_hash, array_agg(o.op_type) as op_type \
         FROM block AS b \
         INNER JOIN operation AS o ON o.block_hash = b.hash \
         WHERE b.distance_level = 0 \
         GROUP BY b.hash ORDER BY level DESC \
         OFFSET $offset LIMIT $limit"] >>= fun rows ->
      return @@ List.map Pg_helper.block_with_ops rows
    else
      [%pgsql.object dbh
        "SELECT * FROM block WHERE distance_level = 0 \
         ORDER BY level DESC OFFSET $offset LIMIT $limit"] >>= fun rows ->
      return @@ List.map Pg_helper.block_noop rows

  let blocks_with_pred_fitness ?(page=0) ?(page_size=20) () =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
        "SELECT b.*, pred.fitness as pred_fitness FROM block as b \
         INNER JOIN block AS pred ON pred.hash = b.predecessor \
         WHERE b.distance_level = 0 \
         ORDER BY b.level DESC \
         OFFSET $offset LIMIT $limit"] >|=
    (List.map Pg_helper.block_with_pred_fitness)

  let snapshot_blocks  ?(page=0) ?(page_size=20) () =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql dbh
      "SELECT sr.cycle, index, rolls_count, \
       (index + 1) * cst.blocks_per_roll_snapshot - 1 + cl.level_start AS sr_level \
       FROM snapshot_rolls AS sr \
       INNER JOIN cycle_limits AS cl0 ON cl0.cycle = sr.cycle \
       INNER JOIN protocol_constants AS cst0 ON cst0.distance_level = 0 AND \
       COALESCE(cl0.level_start BETWEEN cst0.level_start AND cst0.level_end, \
       cst0.level_start <= cl0.level_start AND cst0.level_end IS NULL) \
       INNER JOIN cycle_limits AS cl ON cl.cycle = sr.cycle - (cst0.preserved_cycles +2) \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE sr.cycle >= cst0.preserved_cycles + 2 AND ready \
       ORDER BY sr.cycle DESC \
       OFFSET $offset LIMIT $limit"] >|=
    (List.map (fun (cycle, index, rc, snap_level) ->
         let snap_index = Int32.to_int index in
         let snap_cycle = Int64.to_int cycle in
         let snap_rolls = Int32.to_int rc in
         let snap_level = Option.fold ~none:0 ~some:Int64.to_int snap_level in
         { snap_cycle; snap_index; snap_level; snap_rolls}))

  let nb_snapshot_blocks () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(DISTINCT sr.cycle) FROM snapshot_rolls AS sr \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(sr.cycle BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= sr.cycle AND cst.level_end IS NULL) \
       WHERE sr.cycle >= preserved_cycles + 2 AND ready"]
    >>= of_count_opt

  let snapshot_levels () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT (index + 1) * blocks_per_roll_snapshot - 1 + cl.level_start \
       FROM snapshot_rolls AS sr \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(sr.cycle BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= sr.cycle AND cst.level_end IS NULL) \
       INNER JOIN cycle_limits AS cl ON cl.cycle = sr.cycle - (cst.preserved_cycles +2) \
       WHERE sr.cycle >= cst.preserved_cycles + 2 AND ready"] >|= fun rows ->
    (List.rev @@ List.fold_left (fun acc level -> match level with
         | None -> acc
         | Some level -> Int64.to_int level :: acc) [] rows)

  let head () =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh "SELECT * FROM block WHERE distance_level = 0 ORDER BY level DESC LIMIT 1"]
    >>= fun rows ->
    return
      ( rows
        |> Pg_helper.rows_to_option
        |> Option.map Pg_helper.block_noop )

  let heads ?(page=0) ?(page_size=20) ?level () =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    let level, nolevel = test_opt Int64.of_int level in
    [%pgsql.object dbh
      "SELECT * FROM block \
       WHERE ($nolevel AND distance_level <> 0 AND level <> 0) OR \
       (NOT $nolevel AND level = $?level) \
       ORDER BY level DESC \
       OFFSET $offset LIMIT $limit"]
    >|= (List.map Pg_helper.block_noop)

  let heads_with_pred_fitness ?(page=0) ?(page_size=20) ?level () =
    let offset, limit = limits page page_size in
    let level, nolevel = test_opt Int64.of_int level in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT b.*, pred.fitness as pred_fitness FROM block AS b \
       INNER JOIN block AS pred ON b.predecessor = pred.hash \
       WHERE ($nolevel AND b.distance_level <> 0 AND b.level <> 0) OR \
       (NOT $nolevel AND b.level = $?level) \
       ORDER BY b.level DESC \
       OFFSET $offset LIMIT $limit"] >|=
    (List.map Pg_helper.block_with_pred_fitness)

  let nb_heads () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT count(*) FROM block WHERE distance_level <> 0 AND level <> 0"]
    >>= of_count_opt

  let nb_uncles ~level () =
    with_dbh >>> fun dbh ->
    let lvl = Int64.of_int level in
    if level = 0 then return 0 else
      [%pgsql dbh
        "SELECT count (*) FROM block \
         WHERE distance_level <> 0  AND level = $lvl"]
      >>= of_count_opt

  let nb_operation_block hash filters =
    with_dbh >>> fun dbh ->
    match filters with
    | [] ->
      [%pgsql dbh "SELECT operation_count from block WHERE hash = $hash"] >>= of_count
    | [ "Nonce" ] ->
      [%pgsql dbh
        "SELECT COUNT(*) FROM seed_nonce_revelation_all WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Activation" ] ->
      [%pgsql dbh "SELECT COUNT(*) FROM activation_all WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Transaction" ] ->
      [%pgsql dbh "SELECT COUNT(*) FROM transaction_all WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Delegation" ] ->
      [%pgsql dbh "SELECT COUNT(*) FROM delegation_all WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Origination" ] ->
      [%pgsql dbh "SELECT COUNT(*) FROM origination_all WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Reveal" ] ->
      [%pgsql dbh "SELECT COUNT(*) FROM reveal_all WHERE op_block_hash = $hash"]
         >>= of_count_opt
    | [ "Endorsement" ] ->
      [%pgsql dbh "SELECT COUNT(*) FROM endorsement_all WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Double_baking_evidence" ] ->
      [%pgsql dbh
        "SELECT COUNT(*) FROM double_baking_evidence_all WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Double_endorsement_evidence" ] ->
      [%pgsql dbh
        "SELECT COUNT(*) FROM double_endorsement_evidence_all WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Manage_account" ] ->
      [%pgsql dbh
        "SELECT COUNT(*) FROM manage_account WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Manage_accounts" ] ->
      [%pgsql dbh
        "SELECT COUNT(*) FROM manage_accounts WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | [ "Activate_protocol" ] ->
      [%pgsql dbh
        "SELECT COUNT(*) FROM activate_protocol WHERE op_block_hash = $hash"]
      >>= of_count_opt
    | _ -> return 0

  let nb_operations_block_all hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT operation_count from block WHERE hash = $hash"]
    >>= of_count >>= fun n_tot ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM seed_nonce_revelation_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_seed ->
    [%pgsql dbh "SELECT COUNT(*) FROM activation_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_act ->
    [%pgsql dbh "SELECT COUNT(*) FROM transaction_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_tr ->
    [%pgsql dbh "SELECT COUNT(*) FROM delegation_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_dlg ->
    [%pgsql dbh "SELECT COUNT(*) FROM origination_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_ori ->
    [%pgsql dbh "SELECT COUNT(*) FROM reveal_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_rvl ->
    [%pgsql dbh "SELECT COUNT(*) FROM endorsement_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_end ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM double_baking_evidence_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_dbe ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM double_endorsement_evidence_all WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_dee ->
    [%pgsql dbh "SELECT COUNT(*) FROM manage_account WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_mac ->
    [%pgsql dbh "SELECT COUNT(*) FROM manage_accounts WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_macs ->
    [%pgsql dbh "SELECT COUNT(*) FROM activate_protocol WHERE op_block_hash = $hash"]
    >>= of_count_opt >>= fun n_acp ->
    return (n_tot, n_tr, n_ori, n_dlg, n_rvl, n_end, n_mac, n_macs, n_acp, n_act, n_seed,
            n_dbe, n_dee)

  let nb_operation_account ~delegate hash filters =
    with_dbh >>> fun dbh -> match filters with
      | [ "Activation" ] ->
        [%pgsql dbh "SELECT nb_activation \
                    FROM operation_count_user WHERE dn = $hash"] >>= of_count
      | [ "Transaction" ] ->
        [%pgsql dbh "SELECT nb_transaction_src + nb_transaction_dst \
                    FROM operation_count_user WHERE dn = $hash"] >>= of_count_opt
      | [ "Delegation" ] ->
        [%pgsql dbh "SELECT nb_delegation_src + nb_delegation_dlg \
                    FROM operation_count_user WHERE dn = $hash"] >>= of_count_opt
      | [ "Origination" ] ->
        if not delegate then (
          [%pgsql dbh "SELECT nb_origination_src + nb_origination_man + \
                      nb_origination_kt1 FROM operation_count_user WHERE dn = $hash"]
            >>= of_count_opt)
        else (
          [%pgsql dbh "SELECT nb_origination_dlg \
                      FROM operation_count_user WHERE dn = $hash"] >>= of_count)
      | [ "Reveal" ] ->
        [%pgsql dbh "SELECT nb_reveal FROM operation_count_user WHERE dn = $hash"]
          >>= of_count
      | [ "Endorsement" ] ->
        [%pgsql dbh "SELECT nb_endorsement FROM operation_count_user WHERE dn = $hash"]
          >>= of_count
      | [ "Double_baking_evidence" ] ->
        [%pgsql dbh "SELECT nb_dbe_acc + nb_dbe_bk \
                    FROM operation_count_user WHERE dn = $hash"] >>= of_count_opt
      | [ "Double_endorsement_evidence" ] ->
        [%pgsql dbh "SELECT nb_dee_acc + nb_dee_dn \
                    FROM operation_count_user WHERE dn = $hash"] >>= of_count_opt
      | [ "Nonce" ] ->
        [%pgsql dbh "SELECT nb_nonce FROM operation_count_user WHERE dn = $hash"]
        >>= of_count
      | [ "Token" ] ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM token_operations WHERE distance_level = 0 AND contract = $hash"]
        >>= of_count_opt
      | [ "Manage_account" ] ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM manage_account WHERE distance_level = 0 AND ( \
           source = $hash OR target = $hash)"]
        >>= of_count_opt
      | [ "Manage_accounts" ] ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM manage_accounts WHERE distance_level = 0 AND source = $hash"]
        >>= of_count_opt
      | [ "Activate_protocol" ] ->
        [%pgsql dbh
        "SELECT COUNT(*) FROM activate_protocol WHERE distance_level = 0 AND source = $hash"]
        >>= of_count_opt
      | _ -> return 0

  let nb_operations_account_all hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT nb_transaction_src, nb_transaction_dst, \
       nb_origination_src, nb_origination_man, nb_origination_kt1, \
       nb_origination_dlg, nb_delegation_src, nb_delegation_dlg, \
       nb_reveal, nb_endorsement, nb_activation, nb_nonce, nb_dbe_bk, nb_dbe_acc, \
       nb_dee_dn, nb_dee_acc FROM operation_count_user WHERE dn = $hash"] >>= (function
        | [ x  ] -> return x
        | _ -> return (0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L)) >>=
    fun (nb_tr_src, nb_tr_dst, nb_ori_src, nb_ori_man, nb_ori_kt1, nb_ori_dlg,
         nb_dlg_src, nb_dlg_dlg, nb_rvl, nb_end, nb_act, nb_nonce, nb_dbe_bk,
         nb_dbe_acc, nb_dee_dn, nb_dee_acc) ->
    [%pgsql dbh "SELECT COUNT(*) FROM manage_account WHERE source = $hash OR target = $hash"]
    >>= of_count_opt >>= fun nb_mac ->
    let i = Int64.to_int in
    return (i nb_tr_src, i nb_tr_dst, i nb_ori_src, i nb_ori_man, i nb_ori_kt1,
            i nb_ori_dlg, i nb_dlg_src, i nb_dlg_dlg, i nb_rvl, nb_mac, i nb_end,
            i nb_act, i nb_nonce, i nb_dbe_bk, i nb_dbe_acc, i nb_dee_dn, i nb_dee_acc)

  let nb_operation filters =
    with_dbh >>> fun dbh -> begin match filters with
      | [ "Activation" ] ->
        [%pgsql dbh "SELECT SUM(nb_activation)::bigint FROM cycle_count"]
      | [ "Transaction" ] ->
        [%pgsql dbh "SELECT SUM(nb_transaction)::bigint FROM cycle_count"]
      | [ "Delegation" ] ->
        [%pgsql dbh "SELECT SUM(nb_delegation)::bigint FROM cycle_count"]
      | [ "Origination" ] ->
        [%pgsql dbh "SELECT SUM(nb_origination)::bigint FROM cycle_count"]
      | [ "Reveal" ] ->
        [%pgsql dbh "SELECT SUM(nb_reveal)::bigint FROM cycle_count"]
      | [ "Endorsement" ] ->
        [%pgsql dbh "SELECT SUM(nb_endorsement_op)::bigint FROM cycle_count"]
      | [ "Double_baking_evidence" ] ->
        [%pgsql dbh "SELECT SUM(nb_dbe)::bigint FROM cycle_count"]
      | [ "Double_endorsement_evidence" ] ->
        [%pgsql dbh "SELECT SUM(nb_dee)::bigint FROM cycle_count"]
      | [ "Manage_account" ] ->
        [%pgsql dbh "SELECT COUNT(*) FROM manage_account WHERE distance_level = 0"]
      | [ "Manage_accounts" ] ->
        [%pgsql dbh "SELECT COUNT(*) FROM manage_accounts WHERE distance_level = 0"]
      | [ "Activate_protocol" ] ->
        [%pgsql dbh "SELECT COUNT(*) FROM activate_protocol WHERE distance_level = 0"]
      | _ -> return [ None ] end
    >>= of_count_opt

  let nb_operations ?(delegate=false) ?(filters=[]) selector =
    match selector with
    | Block hash -> nb_operation_block hash filters
    | Account hash -> nb_operation_account ~delegate hash filters
    | Empty -> nb_operation filters

  let nb_pending_operation_empty filters =
    with_dbh >>> fun dbh -> begin match filters with
      | [ "Activation" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT hash) FROM activation_pending"]
      | [ "Seed_nonce_revelation" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT hash) FROM seed_nonce_revelation_pending"]
      | [ "Transaction" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT (hash, counter)) FROM transaction_pending"]
      | [ "Delegation" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT (hash, counter)) FROM delegation_pending"]
      | [ "Origination" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT (hash, counter)) FROM origination_pending"]
      | [ "Reveal" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT (hash, counter)) FROM reveal_pending"]
      | [ "Endorsement" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT hash) FROM endorsement_pending"]
      | _ -> return [ None ] end
    >>= of_count_opt

  let nb_pending_operation_account account filters =
    with_dbh >>> fun dbh -> begin match filters with
      | [ "Transaction" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT (hash, counter)) FROM transaction_pending \
                    WHERE source = $account OR destination = $account"]
      | [ "Delegation" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT (hash, counter)) FROM delegation_pending \
                    WHERE source = $account OR delegate = $account"]
      | [ "Origination" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT (hash, counter)) FROM origination_pending \
                    WHERE source = $account OR delegate = $account \
                    OR kt1 = $account OR manager = $account"]
      | [ "Reveal" ] ->
        [%pgsql dbh "SELECT COUNT(DISTINCT (hash, counter)) FROM reveal_pending \
                    WHERE source = $account"]
      | _ -> return [ None ] end
    >>= of_count_opt

  let nb_pending_operations ?(filters=[]) = function
    | Block _ -> return 0
    | Account account -> nb_pending_operation_account account filters
    | Empty -> nb_pending_operation_empty filters

  (* operation from operation hash *)

  let transaction_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT source, destination, fee, counter, \
       amount, parameters, gas_limit, storage_limit, failed, internal, burn_dun, \
       op_level, timestamp_op as tsp, errors, collect_fee_gas, collect_pk \
       FROM transaction_all WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= (Pg_helper.transaction_from_db)

  let origination_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT source, kt1, fee, counter, manager, delegate, script_code as code, \
       script_storage_type as storage, script_code_hash as code_hash, spendable, \
       delegatable, balance, gas_limit, \
       storage_limit, failed, internal, burn_dun, op_level, timestamp_op as tsp, errors \
       FROM origination_all WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= Pg_helper.origination_from_db

  let delegation_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT source, fee, counter, delegate, gas_limit, storage_limit, \
       failed, internal, op_level, timestamp_op as tsp, errors \
       FROM delegation_all WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= Pg_helper.delegation_from_db

  let reveal_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT source, fee, counter, pubkey, gas_limit, storage_limit, \
       failed, internal, op_level, timestamp_op as tsp, errors \
       FROM reveal_all WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= Pg_helper.reveal_from_db

  let activation_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT pkh, secret, balance, op_level, timestamp_op as tsp \
       FROM activation_all WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= (List.map Pg_helper.activation_from_db)

  let nonce_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT level, nonce, op_level, timestamp_op as tsp FROM seed_nonce_revelation_all \
       WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= (List.map Pg_helper.nonce_from_db)

  let double_endorsement_evidence_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT dee.* FROM double_endorsement_evidence_all AS dee \
       WHERE dee.hash = $op_hash AND op_block_hash = $block_hash"]
    >|= (List.map Pg_helper.dee_from_db)

  let double_baking_evidence_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT bl.signature, dbe.hash, dbe.op_block_hash, dbe.network, accused, denouncer, \
       lost_deposit, lost_rewards, lost_fees, gain_rewards, op_level, dbe.timestamp, \
       h1.level as h1_level, h1.proto as h1_proto, h1.predecessor as h1_predecessor, \
       h1.timestamp as h1_tsp, h1.validation_pass as h1_validation_pass, \
       h1.operations_hash as h1_operations_hash, h1.fitness as h1_fitness, \
       h1.context as h1_context, h1.priority as h1_priority, h1.commited_nonce_hash as h1_commited_nonce_hash, \
       h1.pow_nonce as h1_pow_nonce, h1.signature as h1_signature, \
       h2.level as h2_level, h2.proto as h2_proto, h2.predecessor as h2_predecessor, \
       h2.timestamp as h2_tsp, h2.validation_pass as h2_validation_pass, \
       h2.operations_hash as h2_operations_hash, h2.fitness as h2_fitness, \
       h2.context as h2_context, h2.priority as h2_priority, h2.commited_nonce_hash as h2_commited_nonce_hash, \
       h2.pow_nonce as h2_pow_nonce, h2.signature as h2_signature \
       FROM double_baking_evidence_all AS dbe \
       INNER JOIN header AS h1 ON h1.id = dbe.header1 \
       INNER JOIN header AS h2 ON h2.id = dbe.header2 \
       INNER JOIN block AS bl  ON h1.level = bl.level \
       WHERE dbe.hash = $op_hash AND bl.distance_level = 0 AND op_block_hash = $block_hash"]
    >|= (List.map Pg_helper.dbe_from_db)

  let endorsement_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT source, block_level, block_hash, slots, op_level, priority, \
       timestamp FROM endorsement_all \
       WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= (List.map Pg_helper.endorsement_from_db)

  let manage_account_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT source, fee, counter, target, signature, maxrolls, set_maxrolls, \
       admin, set_admin, white_list, delegation, recovery, set_recovery, actions_name, \
       actions_arg, gas_limit, storage_limit, failed, internal, op_level, \
       timestamp_block as tsp, errors \
       FROM manage_account \
       WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= Pg_helper.manage_account_from_db

  let manage_accounts_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT source, fee, counter, bytes, gas_limit, storage_limit, failed, \
       internal, op_level, timestamp_block as tsp, errors \
       FROM manage_accounts \
       WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= Pg_helper.manage_accounts_from_db

  let activate_protocol_operations op_hash block_hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT source, fee, counter, protocol, parameters, gas_limit, storage_limit, \
       failed, internal, op_level, timestamp_block as tsp, errors \
       FROM activate_protocol \
       WHERE hash = $op_hash AND op_block_hash = $block_hash"]
    >|= Pg_helper.activate_protocol_from_db

  let anon_operation op_hash op_anon_types block_hash =
    let op_anon_types = List.filter_map (fun x -> x) op_anon_types in
    let nonce, activation, dbe, dee = Pg_helper.anon_types op_anon_types in
    (if activation then activation_operations op_hash block_hash else return [])
    >>= fun act_ops ->
    (if nonce then nonce_operations op_hash block_hash else return [])
    >>= fun nonce_ops ->
    (if dbe then double_baking_evidence_operations op_hash block_hash else return [])
    >>= fun dbe_ops ->
    (if dee then double_endorsement_evidence_operations op_hash block_hash else return [])
    >>= fun dee_ops ->
    return @@ Some (Anonymous (act_ops @ nonce_ops @ dbe_ops @ dee_ops))

  let manager_operation op_hash op_manager_types block_hash =
    let op_manager_types = List.filter_map (fun x -> x) op_manager_types in
    let transaction, origination, delegation, reveal =
      Pg_helper.manager_types op_manager_types in
    if not transaction && not origination && not delegation && not reveal then
      return None
    else
      (if transaction then transaction_operations op_hash block_hash else return ("", []))
      >>= fun (tr_src, tr_ops) ->
      (if origination then origination_operations op_hash block_hash else return ("", []))
      >>= fun (ori_src, ori_ops) ->
      (if delegation then delegation_operations op_hash block_hash else return ("", []))
      >>= fun (del_src, del_ops) ->
      (if reveal then reveal_operations op_hash block_hash else return ("", []))
      >>= fun (rvl_src, rvl_ops) ->
      let ops = tr_ops @ ori_ops @ del_ops @ rvl_ops in
      let source =
        if tr_ops <> [] then tr_src
        else if ori_ops <> [] then ori_src
        else if del_ops <> [] then del_src
        else if rvl_ops <> [] then rvl_src
        else "" in
      return @@ Some (Sourced (Manager ("manager", Alias.to_name source, ops)))

  let dune_manager_operation op_hash op_dune_manager_types block_hash =
    let op_dune_manager_types = List.filter_map (fun x -> x) op_dune_manager_types in
    let manage_account, manage_accounts, activate_protocol =
      Pg_helper.dune_manager_types op_dune_manager_types in
    if not manage_account && not manage_accounts && not activate_protocol then
      return None
    else
      (if manage_account then manage_account_operations op_hash block_hash else return ("", []))
      >>= fun (mac_src, mac_ops) ->
      (if manage_accounts then manage_accounts_operations op_hash block_hash else return ("", []))
      >>= fun (macs_src, macs_ops) ->
      (if activate_protocol then activate_protocol_operations op_hash block_hash else return ("", []))
      >>= fun (acp_src, acp_ops) ->
      let ops = mac_ops @ macs_ops @ acp_ops in
      let source =
        if mac_ops <> [] then mac_src
        else if macs_ops <> [] then macs_src
        else if acp_ops <> [] then acp_src
        else "" in
      return @@ Some (Sourced (Dune_manager (Alias.to_name source, ops)))

  let operation ?block_hash op_hash =
    let block_hash, no_block_hash = test_opti block_hash in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT block_hash, o.network, op_type, op_manager_type, op_anon_type \
       FROM operation AS o \
       INNER JOIN block ON block.hash = o.block_hash \
       WHERE o.hash = $op_hash AND \
       (($no_block_hash AND distance_level = 0) OR \
       (NOT $no_block_hash AND block_hash = $?block_hash)) \
       LIMIT 1"] >>= fun rows -> match rows with
    | [ op_block_hash, op_network_hash, "Anonymous", _, anon_types ] -> begin
        anon_operation op_hash anon_types op_block_hash >>= function
        | None -> return None
        | Some op_type ->
          return @@ Some { op_hash; op_block_hash; op_network_hash; op_type }
      end
    | [ op_block_hash, op_network_hash, "Endorsement", _, _ ] -> begin
        endorsement_operations op_hash op_block_hash >>= function
        | op_type :: _ ->
          return @@ Some { op_hash; op_block_hash; op_network_hash; op_type }
        | _ -> return None
      end
    | [ op_block_hash, op_network_hash, "Manager", mana_types, _ ] -> begin
        manager_operation op_hash mana_types op_block_hash >>= function
        | None -> return None
        | Some op_type ->
          return @@ Some { op_hash; op_block_hash; op_network_hash; op_type }
      end
    | [ op_block_hash, op_network_hash, "Dune_manager", mana_types, _ ] -> begin
        dune_manager_operation op_hash mana_types op_block_hash >>= function
        | None -> return None
        | Some op_type ->
          return @@ Some { op_hash; op_block_hash; op_network_hash; op_type }
      end
    | _ -> return None

  (* operation of a block *)

  let nonce_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, level, nonce, op_level, timestamp_block as tsp \
       FROM seed_nonce_revelation_all WHERE op_block_hash = $block_hash \
       ORDER BY level LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.batch_operation Pg_helper.nonce_from_db (fun l -> Anonymous l))


  let activation_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, pkh, secret, balance, \
       op_level, timestamp_block as tsp \
       FROM activation_all WHERE op_block_hash = $block_hash \
       ORDER BY balance, hash, secret LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.batch_operation Pg_helper.activation_from_db (fun l -> Anonymous l))

  let dbe_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT bl.signature, dbe.hash, dbe.op_block_hash, dbe.network, accused, denouncer, \
       lost_deposit, lost_rewards, lost_fees, gain_rewards, op_level, dbe.timestamp, \
       h1.level as h1_level, h1.proto as h1_proto, h1.predecessor as h1_predecessor, \
       h1.timestamp as h1_tsp, h1.validation_pass as h1_validation_pass, \
       h1.operations_hash as h1_operations_hash, h1.fitness as h1_fitness, \
       h1.context as h1_context, h1.priority as h1_priority, h1.commited_nonce_hash as h1_commited_nonce_hash, \
       h1.pow_nonce as h1_pow_nonce, h1.signature as h1_signature, \
       h2.level as h2_level, h2.proto as h2_proto, h2.predecessor as h2_predecessor, \
       h2.timestamp as h2_tsp, h2.validation_pass as h2_validation_pass, \
       h2.operations_hash as h2_operations_hash, h2.fitness as h2_fitness, \
       h2.context as h2_context, h2.priority as h2_priority, h2.commited_nonce_hash as h2_commited_nonce_hash, \
       h2.pow_nonce as h2_pow_nonce, h2.signature as h2_signature \
       FROM double_baking_evidence_all AS dbe \
       INNER JOIN header AS h1 ON h1.id = header1 \
       INNER JOIN header AS h2 ON h2.id = header2 \
       INNER JOIN block AS bl ON h1.level = bl.level AND bl.distance_level = 0 \
       WHERE dbe.op_block_hash = $block_hash \
       ORDER BY dbe.hash LIMIT $limit OFFSET $offset"] >|=
    (List.map (Pg_helper.op_type_from_db (fun r -> Anonymous [Pg_helper.dbe_from_db r])))

  let dee_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT dee.* FROM double_endorsement_evidence_all AS dee \
       WHERE dee.op_block_hash = $block_hash \
       ORDER BY dee.hash LIMIT $limit OFFSET $offset"] >|=
    (List.map (Pg_helper.op_type_from_db (fun r -> Anonymous [Pg_helper.dee_from_db r])))

  let endorsement_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, block_hash, slots, \
       block_level, op_level, priority, timestamp \
       FROM endorsement_all WHERE op_block_hash = $block_hash \
       ORDER BY priority, slots LIMIT $limit OFFSET $offset"] >|=
    (List.map  (Pg_helper.op_type_from_db Pg_helper.endorsement_from_db))

  let transaction_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, destination, fee, counter, \
       amount, parameters, gas_limit, storage_limit, failed, internal, burn_dun, \
       op_level, timestamp_block as tsp, errors, collect_fee_gas, collect_pk \
       FROM transaction_all WHERE op_block_hash = $block_hash \
       ORDER BY amount DESC, hash, counter LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.manager_batch_operation
       (fun r -> Transaction (Pg_helper.transaction_from_db_base r)))

  let delegation_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, \
       delegate, gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM delegation_all WHERE op_block_hash = $block_hash \
       ORDER BY hash, counter LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.manager_batch_operation
       (fun r -> Delegation (Pg_helper.delegation_from_db_base r)))

  let reveal_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, \
       pubkey, gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM reveal_all WHERE op_block_hash = $block_hash \
       ORDER BY hash, counter LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.manager_batch_operation
       (fun r -> Reveal (Pg_helper.reveal_from_db_base r)))

  let origination_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, kt1, fee, counter, \
       manager, delegate, script_code as code, script_storage_type as storage, \
       script_code_hash as code_hash, spendable, \
       delegatable, balance, gas_limit, storage_limit, failed, internal, burn_dun, \
       op_level, timestamp_block as tsp, errors \
       FROM origination_all WHERE op_block_hash = $block_hash \
       ORDER BY balance DESC, hash LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.manager_batch_operation
       (fun r-> Origination (Pg_helper.origination_from_db_base r)))

  let manage_account_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, target, \
       signature, maxrolls, set_maxrolls, admin, set_admin, white_list, delegation, \
       recovery, set_recovery, actions_name, actions_arg, gas_limit, storage_limit, \
       failed, internal, op_level, timestamp_block as tsp, errors \
       FROM manage_account WHERE op_block_hash = $block_hash \
       ORDER BY hash, counter LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.dune_manager_batch_operation
       (fun r -> Manage_account (Pg_helper.manage_account_from_db_base r)))

  let manage_accounts_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, bytes, \
       gas_limit, storage_limit, failed, internal, op_level, timestamp_block as tsp, errors \
       FROM manage_accounts WHERE op_block_hash = $block_hash \
       ORDER BY hash, counter LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.dune_manager_batch_operation
       (fun r -> Manage_accounts (Pg_helper.manage_accounts_from_db_base r)))

  let activate_protocol_from_block dbh limit offset block_hash =
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, protocol, parameters, \
       gas_limit, storage_limit, failed, internal, op_level, timestamp_block as tsp, errors \
       FROM activate_protocol WHERE op_block_hash = $block_hash \
       ORDER BY hash, counter LIMIT $limit OFFSET $offset"] >|=
    (Pg_helper.dune_manager_batch_operation
       (fun r -> Activate_protocol (Pg_helper.activate_protocol_from_db_base r)))

  let operations_from_block filters page page_size block_hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    match filters with
    (* Anonymous *)
    | [ "Nonce" ] -> nonce_from_block dbh limit offset block_hash
    | [ "Activation" ] -> activation_from_block dbh limit offset block_hash
    | [ "Double_baking_evidence" ] -> dbe_from_block dbh limit offset block_hash
    | [ "Double_endorsement_evidence" ] -> dee_from_block dbh limit offset block_hash
    (* Consensus *)
    | [ "Endorsement" ] -> endorsement_from_block dbh limit offset block_hash
    (* Manager *)
    | [ "Transaction" ] -> transaction_from_block dbh limit offset block_hash
    | [ "Delegation" ] -> delegation_from_block dbh limit offset block_hash
    | [ "Reveal" ] -> reveal_from_block dbh limit offset block_hash
    | [ "Origination" ] -> origination_from_block dbh limit offset block_hash
    (* Dune Manager *)
    | [ "Manage_account" ] -> manage_account_from_block dbh limit offset block_hash
    | [ "Manage_accounts" ] -> manage_accounts_from_block dbh limit offset block_hash
    | [ "Activate_protcol" ] -> activate_protocol_from_block dbh limit offset block_hash
    (* Generic *)
    | _filters -> return []


  (* operation of an account *)

  let transaction_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, destination, fee, counter, \
       amount, parameters, gas_limit, storage_limit, failed, internal, \
       burn_dun, op_level, timestamp_block as tsp, errors, collect_fee_gas, collect_pk \
       FROM transaction_all WHERE (source = $hash OR destination = $hash) \
       AND distance_level = 0 \
       ORDER BY op_level DESC, hash, counter LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.transaction_from_db_list

  let transfer_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT transaction, op_block_hash, network, op_level, timestamp as tsp, \
       kind, source, destination, amount, flag \
       FROM token_operations WHERE contract = $hash AND distance_level = 0 \
       ORDER BY op_level DESC, counter DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.token_from_db_list

  let delegation_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, \
       delegate, gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM delegation_all WHERE (source = $hash OR delegate = $hash) \
       AND distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.delegation_from_db_list

  let reveal_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, \
       pubkey, gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM reveal_all WHERE source = $hash \
       AND distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.reveal_from_db_list

  let origination_from_account ~delegate page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, kt1, fee, counter, \
       manager, delegate, script_code as code, script_storage_type as storage, \
       script_code_hash as code_hash, spendable, \
       delegatable, balance, gas_limit, storage_limit, failed, internal, \
       burn_dun, op_level, timestamp_block as tsp, errors \
       FROM origination_all WHERE \
       (($delegate AND delegate = $hash) OR \
       (NOT $delegate AND (source = $hash OR kt1 = $hash OR manager = $hash))) \
       AND distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.origination_from_db_list

  let activation_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
     [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, pkh, secret, balance, \
       op_level, timestamp_block as tsp \
       FROM activation_all WHERE pkh = $hash \
       AND distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.activation_from_db_list

  let endorsement_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql dbh
      "SELECT nb_endorsement FROM operation_count_user \
       WHERE dn = $hash"] >>= of_db >>= fun count ->
    let limit = min limit count in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, block_hash, slots, \
       block_level, op_level, priority, timestamp \
       FROM endorsement_all WHERE distance_level = 0 AND source = $hash \
       ORDER BY block_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.endorsement_from_db_list

  let manage_account_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, target, \
       signature, maxrolls, set_maxrolls, admin, set_admin, white_list, delegation, \
       recovery, set_recovery, actions_name, actions_arg, gas_limit, storage_limit, \
       failed, internal, op_level, timestamp_block as tsp, errors \
       FROM manage_account WHERE (source = $hash OR target = $hash) \
       AND distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.manage_account_from_db_list

  let manage_accounts_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, bytes, \
       gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM manage_accounts WHERE source = $hash \
       AND distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.manage_accounts_from_db_list

  let activate_protocol_from_account page page_size hash =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, protocol, parameters, \
       gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM activate_protocol WHERE source = $hash \
       AND distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.activate_protocol_from_db_list


  (* operation from recent *)

  let transaction_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    begin
      if Int64.add limit offset < Pg_update.Constants.last_transactions then
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, destination, fee, counter, \
           amount, parameters, gas_limit, storage_limit, failed, internal, burn_dun, \
           op_level, timestamp_block as tsp, errors, collect_fee_gas, collect_pk \
           FROM transaction_last WHERE distance_level = 0 \
           ORDER BY op_level DESC, hash, counter LIMIT $limit OFFSET $offset"]
      else
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, destination, fee, counter, \
           amount, parameters, gas_limit, storage_limit, failed, internal, burn_dun, \
           op_level, timestamp_block as tsp, errors, collect_fee_gas, collect_pk \
           FROM transaction_all WHERE distance_level = 0 \
           ORDER BY op_level DESC, hash, counter LIMIT $limit OFFSET $offset"]
    end
    >|= Pg_helper.transaction_from_db_list

  let activation_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    begin
      if Int64.add limit offset < Pg_update.Constants.last_activations then
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, pkh, secret, balance, \
           op_level, timestamp_block as tsp \
           FROM activation_last WHERE distance_level = 0 \
           ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
      else
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, pkh, secret, balance, \
           op_level, timestamp_block as tsp \
           FROM activation_all WHERE distance_level = 0 \
           ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    end
    >|= Pg_helper.activation_from_db_list

  let delegation_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    begin
      if Int64.add limit offset < Pg_update.Constants.last_delegations then
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, fee, counter, \
           delegate, gas_limit, storage_limit, failed, internal, \
           op_level, timestamp_block as tsp, errors \
           FROM delegation_last WHERE distance_level = 0 \
           ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
      else
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, fee, counter, \
           delegate, gas_limit, storage_limit, failed, internal, \
           op_level, timestamp_block as tsp, errors \
           FROM delegation_all WHERE distance_level = 0 \
           ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    end
    >|= Pg_helper.delegation_from_db_list

  let reveal_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    begin
      if Int64.add limit offset < Pg_update.Constants.last_reveals then
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, fee, counter, \
           pubkey, gas_limit, storage_limit, failed, internal, \
           op_level, timestamp_block as tsp, errors \
           FROM reveal_last WHERE distance_level = 0 \
           ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
      else
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, fee, counter, \
           pubkey, gas_limit, storage_limit, failed, internal, \
           op_level, timestamp_block as tsp, errors \
           FROM reveal_all WHERE distance_level = 0 \
           ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    end
    >|= Pg_helper.reveal_from_db_list

  let endorsement_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    begin
      if Int64.add offset limit > Pg_update.Constants.last_endorsements then
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, block_hash, slots, \
           block_level, op_level, priority, timestamp \
           FROM endorsement_all where distance_level = 0 \
           ORDER BY block_level DESC, slots DESC LIMIT $limit OFFSET $offset"]
      else
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, block_hash, slots, \
           block_level, op_level, priority, timestamp \
           FROM endorsement_last where distance_level = 0 \
           ORDER BY block_level DESC, slots DESC LIMIT $limit OFFSET $offset"]
    end
    >|= Pg_helper.endorsement_from_db_list

  let origination_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    begin
      if Int64.add limit offset < Pg_update.Constants.last_originations then
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, kt1, fee, counter, \
           manager, delegate, script_code as code, script_storage_type as storage, \
           script_code_hash as code_hash, spendable, \
           delegatable, balance, gas_limit, storage_limit, failed, internal, burn_dun, \
           op_level, timestamp_block as tsp, errors \
           FROM origination_last WHERE distance_level = 0 \
           ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
      else
        [%pgsql.object dbh
          "SELECT hash, op_block_hash, network, source, kt1, fee, counter, \
           manager, delegate, script_code as code, script_storage_type as storage, \
           script_code_hash as code_hash, spendable, \
           delegatable, balance, gas_limit, storage_limit, failed, internal, burn_dun, \
           op_level, timestamp_block as tsp, errors \
           FROM origination_all WHERE distance_level = 0 \
           ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    end
    >|= Pg_helper.origination_from_db_list

  let double_baking_evidence_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT bl.signature, dbe.hash, dbe.op_block_hash, dbe.network, accused, denouncer, \
       lost_deposit, lost_rewards, lost_fees, gain_rewards, op_level, dbe.timestamp,  \
       h1.level as h1_level, h1.proto as h1_proto, h1.predecessor as h1_predecessor, \
       h1.timestamp as h1_tsp, h1.validation_pass as h1_validation_pass, \
       h1.operations_hash as h1_operations_hash, h1.fitness as h1_fitness, \
       h1.context as h1_context, h1.priority as h1_priority, h1.commited_nonce_hash as h1_commited_nonce_hash, \
       h1.pow_nonce as h1_pow_nonce, h1.signature as h1_signature, \
       h2.level as h2_level, h2.proto as h2_proto, h2.predecessor as h2_predecessor, \
       h2.timestamp as h2_tsp, h2.validation_pass as h2_validation_pass, \
       h2.operations_hash as h2_operations_hash, h2.fitness as h2_fitness, \
       h2.context as h2_context, h2.priority as h2_priority, h2.commited_nonce_hash as h2_commited_nonce_hash, \
       h2.pow_nonce as h2_pow_nonce, h2.signature as h2_signature \
       FROM double_baking_evidence_all AS dbe \
       INNER JOIN header AS h1 ON h1.id = dbe.header1 \
       INNER JOIN header AS h2 ON h2.id = dbe.header2 \
       INNER JOIN block AS bl ON h1.level = bl.level \
       WHERE dbe.distance_level = 0 AND bl.distance_level = 0 \
       ORDER BY dbe.op_level DESC, dbe.hash LIMIT $limit OFFSET $offset"] >|=
    (List.map (fun r ->
         {op_hash = r#hash; op_block_hash = r#op_block_hash; op_network_hash = r#network;
          op_type = Anonymous [Pg_helper.dbe_from_db r]}))

  let double_endorsement_evidence_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT dee.* FROM double_endorsement_evidence_all AS dee \
       WHERE dee.distance_level = 0 \
       ORDER BY dee.op_level DESC, dee.hash LIMIT $limit OFFSET $offset"] >|=
    (List.map (fun r ->
         {op_hash = r#hash; op_block_hash = r#op_block_hash; op_network_hash = r#network;
          op_type = Anonymous [Pg_helper.dee_from_db r]}))

  let manage_account_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, target, \
       signature, maxrolls, set_maxrolls, admin, set_admin, white_list, delegation, \
       recovery, set_recovery, actions_name, actions_arg, gas_limit, storage_limit, \
       failed, internal, op_level, timestamp_block as tsp, errors \
       FROM manage_account WHERE distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.manage_account_from_db_list

  let manage_accounts_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, bytes, \
       gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM manage_accounts WHERE distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.manage_accounts_from_db_list

  let activate_protocol_from_recent page page_size =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, protocol, parameters, \
       gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM activate_protocol WHERE distance_level = 0 \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.activate_protocol_from_db_list


  let operations ?(delegate=false) ?(filters=[])
      ?(page=0) ?(page_size=20) selector =
    match selector with
    | Block hash ->
      operations_from_block filters page page_size hash

    | Account hash ->
      begin match filters with
        | [ "Transaction" ] -> transaction_from_account page page_size hash
        | [ "Delegation" ] -> delegation_from_account page page_size hash
        | [ "Endorsement" ] -> endorsement_from_account page page_size hash
        | [ "Origination" ] -> origination_from_account ~delegate page page_size hash
        | [ "Reveal" ] -> reveal_from_account page page_size hash
        | [ "Activation" ] -> activation_from_account page page_size hash
        | [ "Token" ] -> transfer_from_account page page_size hash
        | [ "Manage_account" ] -> manage_account_from_account page page_size hash
        | [ "Manage_accounts" ] -> manage_accounts_from_account page page_size hash
        | [ "Activate_protocol" ] -> activate_protocol_from_account page page_size hash
        | _ -> assert false
      end

    | Empty ->
      begin match filters with
        | [ "Transaction" ] -> transaction_from_recent page page_size
        | [ "Delegation" ] -> delegation_from_recent page page_size
        | [ "Endorsement" ] -> endorsement_from_recent page page_size
        | [ "Origination" ] -> origination_from_recent page page_size
        | [ "Reveal" ] -> reveal_from_recent page page_size
        | [ "Activation" ] -> activation_from_recent page page_size
        | [ "Double_baking_evidence" ] ->
          double_baking_evidence_from_recent page page_size
        | [ "Double_endorsement_evidence" ] ->
          double_endorsement_evidence_from_recent page page_size
        | [ "Manage_account" ] -> manage_account_from_recent page page_size
        | [ "Manage_accounts" ] -> manage_accounts_from_recent page page_size
        | [ "Activate_protocol" ] -> activate_protocol_from_recent page page_size
        | _ -> assert false
      end

  let pending_operations_empty page page_size filters =
    let offset, limit = limits page page_size in
    let return_map f l = return (List.map f l) in
    with_dbh >>> fun dbh -> match filters with
    | [ "Seed_nonce_revelation" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, level, nonce \
         FROM seed_nonce_revelation_pending \
         ORDER BY timestamp_op DESC, hash OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_seed_nonce_revelation_from_db
    | [ "Activation" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, pkh, secret \
         FROM activation_pending \
         ORDER BY timestamp_op DESC, hash OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_activation_from_db
    | [ "Endorsement" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, block_level, priority \
         FROM endorsement_pending \
         ORDER BY timestamp_op DESC, hash OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_endorsement_from_db
    | [ "Transaction" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, source, counter, fee, \
         gas_limit, storage_limit, destination, amount, parameters, collect_fee_gas, \
         collect_pk FROM transaction_pending \
         ORDER BY timestamp_op DESC, hash, counter DESC OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_transaction_from_db
    | [ "Reveal" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, source, counter, fee, \
         gas_limit, storage_limit, pubkey FROM reveal_pending \
         ORDER BY timestamp_op DESC, hash, counter DESC OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_reveal_from_db
    | [ "Delegation" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, source, counter, fee, \
         gas_limit, storage_limit, delegate FROM delegation_pending \
         ORDER BY timestamp_op DESC, hash, counter DESC OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_delegation_from_db
    | [ "Origination" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, source, counter, fee, \
         gas_limit, storage_limit, kt1, manager, delegate, script_code as code, \
         script_storage_type as storage, script_code_hash as code_hash, spendable, \
         delegatable, balance \
         FROM origination_pending \
         ORDER BY timestamp_op DESC, hash, counter DESC OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_origination_from_db
    | _ -> return []

  let pending_operations_account page page_size account filters =
    let offset, limit = limits page page_size in
    let return_map f l = return (List.map f l) in
    with_dbh >>> fun dbh -> match filters with
    | [ "Transaction" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, source, counter, fee, \
         gas_limit, storage_limit, destination, amount, parameters, collect_fee_gas, \
         collect_pk FROM transaction_pending \
         WHERE source = $account OR destination = $account \
         ORDER BY timestamp_op DESC, hash, counter DESC OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_transaction_from_db
    | [ "Reveal" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, source, counter, fee, \
         gas_limit, storage_limit, pubkey FROM reveal_pending \
         WHERE source = $account \
         ORDER BY timestamp_op DESC, hash, counter DESC OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_reveal_from_db
    | [ "Delegation" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, source, counter, fee, \
         gas_limit, storage_limit, delegate FROM delegation_pending \
         WHERE source = $account OR delegate = $account \
         ORDER BY timestamp_op DESC, hash, counter DESC OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_delegation_from_db
    | [ "Origination" ] ->
      [%pgsql.object dbh
        "SELECT hash, branch, status, timestamp_op, errors, source, counter, fee, \
         gas_limit, storage_limit, kt1, manager, delegate, script_code as code, \
         script_storage_type as storage, script_code_hash as code_hash, spendable, \
         delegatable, balance \
         FROM origination_pending \
         WHERE source = $account OR delegate = $account OR kt1 = $account \
         OR manager = $account \
         ORDER BY timestamp_op DESC, hash, counter DESC OFFSET $offset LIMIT $limit"]
      >>= return_map Pg_helper.pending_origination_from_db
    | _ -> return []

  let pending_operations ?(filters=[]) ?(page=0) ?(page_size=20) = function
    | Block _ -> assert false
    | Account account -> pending_operations_account page page_size account filters
    | Empty -> pending_operations_empty page page_size filters

  (* Temporary *)
  let head_level dbh =
    [%pgsql dbh
      "SELECT level FROM block \
       WHERE distance_level = 0 ORDER BY level DESC LIMIT 1"]
    >>= of_db

  let head_cycle () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT MAX(cycle) FROM block WHERE distance_level = 0"]
    >>= of_count_opt

  (* Baking Section *)

  let nb_bakings ?(rights=true) ?cycle hash =
    with_dbh >>> fun dbh ->
    let cycle, nocycle = test_opt Int64.of_int cycle in
    [%pgsql dbh
      "SELECT array_sum(nb_baking, 1) + (CASE WHEN $rights THEN nb_miss_baking ELSE 0 END) \
       FROM cycle_count_baker \
       WHERE dn = $hash AND ($nocycle or cycle = $?cycle)"]
    >>= of_count_opt

  let bakings ?(page=0) ?(page_size=20) ?cycle hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let offset, limit = limits page page_size in
    let cycle, nocycle = test_opt Int64.of_int cycle in
    [%pgsql dbh
      "SELECT b.hash, b.baker, b.level, b.cycle, b.priority, \
       lr.bakers_priority[array_position(lr.bakers, $hash)], \
       b.distance_level, b.fees, \
       bake_time(b.timestamp - pred.timestamp, b.priority, \
       b.endorsements_included, cst.time_between_blocks, \
       cst.delay_per_missing_endorsement, cst.initial_endorsers), \
       b.baker = $hash, b.timestamp, b.endorsements_included \
       FROM block AS b \
       INNER JOIN block AS pred ON b.predecessor = pred.hash \
       INNER JOIN level_rights AS lr ON b.level = lr.level \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(b.level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= b.level AND cst.level_end IS NULL) \
       WHERE (b.baker = $hash OR \
       lr.bakers_priority[array_position(lr.bakers, $hash)] <= b.priority) \
       AND ($nocycle OR b.cycle = $?cycle) AND lr.level <= $head_lvl AND ready \
       ORDER BY b.level DESC, b.distance_level \
       NULLS FIRST LIMIT $limit OFFSET $offset"]
    >>= fun rows ->
    return @@ Pg_helper.bakings_from_db_list rows

  let nb_cycle_bakings hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun head_cycle ->
    [%pgsql dbh "SELECT COUNT(*) FROM cycle_count_baker \
                WHERE dn = $hash AND cycle <= $head_cycle"]
    >>= of_count_opt

  let cycle_bakings offset limit hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun head_cycle ->
    (* assume rewards and deposits are not change during a cycle *)
    [%pgsql dbh
      "SELECT ccb.cycle, array_sum(nb_baking,1), array_sum(nb_baking,2), \
       nb_miss_baking, array_wavg(nb_baking,1), fees, time, block_rewards, \
       block_security_deposit \
       FROM cycle_count_baker AS ccb \
       INNER JOIN cycle_limits AS cl ON cl.cycle = ccb.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE dn = $hash AND ccb.cycle <= $head_cycle \
       ORDER BY ccb.cycle DESC LIMIT $limit OFFSET $offset"]
    >>= fun rows ->
    return (Pg_helper.cycle_bakings_from_db_list (Int64.to_int head_cycle) rows)

  let cycle_bakings_sv ?(page=0) ?(page_size=20) hash =
    let offset, limit = limits page page_size in
    cycle_bakings offset limit hash

  let total_bakings hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun head_cycle ->
    (* assume rewards and deposits are not change during a cycle *)
    [%pgsql dbh
      "SELECT COUNT( * ), SUM(array_sum(nb_baking,1))::bigint, \
       SUM(array_sum(nb_baking,2))::bigint, SUM(nb_miss_baking)::bigint, \
       SUM(array_wavg(nb_baking,1)), SUM(fees)::bigint, SUM(time), \
       SUM(block_rewards)::bigint, \
       SUM(array_sum(nb_baking,1) * block_security_deposit)::bigint \
       FROM cycle_count_baker AS ccb \
       INNER JOIN cycle_limits AS cl ON ccb.cycle = cl.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE dn = $hash AND ccb.cycle <= $head_cycle"]
    >>= function
    | [ (Some a, b, c, Some d, Some e, Some f, Some g, rewards, deposits) ] ->
      let count_float = (Int64.to_float a) in
      (let row = Pg_helper.cycle_bakings_from_db_list 0
           [0L, b, c, d, Some (e /. count_float), f, g, 0L, 0L ] in
       match rewards, deposits, row with
       | Some rewards, Some deposits, [total_bk] ->
         return [{total_bk with cbk_dun = {
             dun_fee = total_bk.cbk_dun.dun_fee;
             dun_reward = rewards;
             dun_deposit = deposits}}]
       | _, _, _ -> return [])
    | _ -> return []

  let nb_baker_rights ?cycle hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let cycle, nocycle = test_opt Int64.of_int cycle in
    [%pgsql dbh
      "SELECT COUNT (level) FROM level_rights \
       WHERE $hash = ANY (bakers) AND level > $head_lvl AND \
       ($nocycle OR cycle = $?cycle) AND ready"]
    >>= of_count_opt

  let baker_rights ?cycle ?(page=0) ?(page_size=20) hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let offset, limit = limits page page_size in
    let cycle, nocycle = test_opt Int64.of_int cycle in
    [%pgsql dbh
      "SELECT level, cycle, bakers_priority[ARRAY_POSITION(bakers, $hash)], \
       level - $head_lvl \
       FROM level_rights \
       WHERE $hash = ANY (bakers) AND level > $head_lvl AND \
       ($nocycle OR cycle = $?cycle) AND ready \
       ORDER BY level ASC NULLS FIRST LIMIT $limit OFFSET $offset"]
    >>= fun rows ->
    return @@ Pg_helper.baker_rights_from_db_list rows


  let cycle_all_rights ?cycle ?(prio=0) hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let (cycle, nocycle) = test_opt Int64.of_int cycle in
    let prio = Int32.of_int prio in
    [%pgsql dbh
      "SELECT COUNT(CASE WHEN ARRAY_POSITION(bakers, $hash) = $prio THEN 1 ELSE 0 END), \
       COUNT(CASE WHEN ARRAY_POSITION(endorsers, $hash) = $prio THEN 1 ELSE 0 END) \
       FROM level_rights \
       WHERE $hash = ANY (bakers) AND level > $head_lvl AND \
       ($nocycle OR cycle = $?cycle) AND ready"]
    >>= of_count_pair


  let nb_cycle_rewards ?(only_future=false) hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun current_cycle ->
    [%pgsql dbh
      "SELECT COUNT(sr.cycle) FROM snapshot_owner AS so \
       INNER JOIN snapshot_rolls AS sr ON sr.id = so.id \
       WHERE so.dn1 = $hash AND ready AND (NOT $only_future OR (sr.cycle >= $current_cycle))"] >>= of_count_opt

  let cycle_baker_rights hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    current_cycle dbh >>= fun current_cycle ->
    [%pgsql dbh
       "WITH t(cycle, prio) AS ( \
        SELECT cycle, bakers_priority[ARRAY_POSITION(bakers, $hash)] \
        FROM level_rights \
        WHERE $hash = ANY (bakers)  and level > $head_lvl AND ready), \
        lr(cycle, cnt_prio0, avg_prio) AS ( \
        SELECT cycle, SUM(CASE WHEN prio = 0 THEN 1 ELSE 0 END), \
        AVG(prio)::float FROM t \
        GROUP BY cycle), \
        ro(cycle) AS ( \
        SELECT sr.cycle FROM snapshot_owner AS so \
        INNER JOIN snapshot_rolls AS sr ON sr.id = so.id \
        WHERE so.dn1 = $hash AND sr.cycle >= $current_cycle AND ready) \
        SELECT ro.cycle, cnt_prio0, avg_prio \
        FROM ro LEFT JOIN lr ON lr.cycle = ro.cycle \
        ORDER BY ro.cycle DESC"]
    >>= fun rows ->
    return @@
    List.map (fun (cr_cycle, cr_nblocks, cr_priority) ->
        {cr_cycle = Int64.to_int cr_cycle;
         cr_nblocks = Option.fold ~none:(-1) ~some:Int64.to_int cr_nblocks;
         cr_priority = Option.value ~default:0. cr_priority}) rows

  let nb_bakings_history hash =
    nb_cycle_rewards ~only_future:true hash >>= fun nb_rights ->
    nb_cycle_bakings hash >>= fun nb_cycle_bakings ->
    if nb_rights + nb_cycle_bakings = 0 then return 0
    else return (1 + nb_rights + nb_cycle_bakings)

  let bakings_history ?(page=0) ?(page_size=20) hash =
    total_bakings hash >>= fun tot_row ->
    cycle_baker_rights hash >>= fun rights_rows ->
    let offset = Int64.of_int @@
      if page = 0 then 0 else page * page_size - List.length rights_rows in
    let limit = Int64.of_int @@
      if page = 0 then page_size - List.length rights_rows else page_size  in
    if offset < 0L then return (tot_row, rights_rows, []) else
      cycle_bakings offset limit hash >>= fun rows ->
      return (tot_row, (if page = 0 then rights_rows else []), rows)

  (* Baking Endorsements Section *)

  let nb_bakings_endorsement ?cycle hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let cycle, nocycle = test_opt Int64.of_int cycle in
    [%pgsql dbh
      "SELECT count(*) FROM level_rights \
       WHERE $hash = any(endorsers) AND ($nocycle or cycle = $?cycle) \
       AND level < $head_lvl AND ready"]
    >>= of_count_opt

  let bakings_endorsement ?(page=0) ?(page_size=20) ?cycle hash =
    let offset, limit = limits page page_size in
    let cycle, nocycle = test_opt Int64.of_int cycle in
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    (* assume rewards and deposits are not change during a cycle *)
    [%pgsql dbh
      "nullable-results"
      "WITH lr(level, nslot) AS ( \
       SELECT level, slots[array_position(endorsers, $hash)] \
       FROM level_rights AS lv_r \
       INNER JOIN cycle_limits AS cl ON cl.cycle = $?cycle \
       WHERE $hash = any(endorsers) AND \
       ($nocycle OR (level + 1 BETWEEN cl.level_start AND cl.level_end)) \
       AND level < $head_lvl AND ready), \
       p(level, hash, endorser, cycle, priority, distance_level, slots, tsp) AS ( \
       SELECT block_level, op_block_hash, source, op_cycle, priority, \
       distance_level, slots, timestamp \
       FROM endorsement_all \
       WHERE source = $hash AND ( $nocycle OR op_cycle = $?cycle ) \
       AND distance_level = 0) \
       SELECT lr.level, lr.nslot, p.hash, p.endorser, p.cycle, p.priority, \
       p.distance_level, p.slots, p.tsp \
       FROM lr AS lr LEFT JOIN p AS p ON lr.level = p.level \
       ORDER BY lr.level DESC, p.distance_level LIMIT $limit OFFSET $offset"]
    >>= fun rows ->
    return @@ Pg_helper.bakings_endorsement_from_db_list rows

  let nb_cycle_endorsements hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun head_cycle ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM cycle_count_baker \
       WHERE dn = $hash AND cycle <= $head_cycle"]
    >>= of_count_opt

  let cycle_endorsements offset limit hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun head_cycle ->
    (* assume rewards and deposits are not change during a cycle *)
    [%pgsql dbh
      "SELECT ccb.cycle, array_sum(nb_endorsement,1), nb_miss_endorsement, \
       array_wavg(nb_endorsement, 1), endorsement_rewards, \
       endorsement_security_deposit \
       FROM cycle_count_baker AS ccb \
       INNER JOIN cycle_limits AS cl ON cl.cycle = ccb.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE dn = $hash AND ccb.cycle <= $head_cycle \
       ORDER BY ccb.cycle DESC LIMIT $limit OFFSET $offset"]
    >>= fun rows ->
    return (Pg_helper.cycle_endorsements_from_db_list (Int64.to_int head_cycle) rows)

  let cycle_endorsements_sv ?(page=0) ?(page_size=20) hash =
    let offset, limit = limits page page_size in
    cycle_endorsements offset limit hash

  let total_endorsements hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun head_cycle ->
    (* assume rewards and deposits are not change during a cycle *)
    [%pgsql dbh
      "SELECT COUNT( * ), SUM(array_sum(nb_endorsement,1))::bigint, \
       SUM(nb_miss_endorsement)::bigint, SUM(array_wavg(nb_endorsement,1)), \
       SUM(endorsement_rewards)::bigint, \
       SUM(array_sum(nb_endorsement,1) * endorsement_security_deposit)::bigint \
       FROM cycle_count_baker AS ccb \
       INNER JOIN cycle_limits AS cl ON cl.cycle = ccb.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE dn = $hash AND ccb.cycle <= $head_cycle"]
    >>= function
    | [ (Some a, b, Some c, Some d, Some e, Some deposits) ] ->
      let count_float = Int64.to_float a in
      (let row = Pg_helper.cycle_endorsements_from_db_list 0
           [0L, b, c, Some (d /. count_float), e, 0L] in
       match row with
       | [ total_ed ] ->
         return
           [ {total_ed with
              ced_dun =
                { dun_fee=0L; dun_reward = total_ed.ced_dun.dun_reward;
                  dun_deposit = deposits} } ]
       | _ -> return [])
    | _ -> return []

  let nb_endorser_rights ?cycle hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let cycle, nocycle = test_opt Int64.of_int cycle in
    [%pgsql dbh
      "SELECT COUNT (level) FROM level_rights \
       WHERE $hash = ANY (endorsers) AND level >= $head_lvl AND \
       ($nocycle OR cycle = $?cycle) AND ready"]
    >>= of_count_opt

  let endorser_rights ?cycle ?(page=0) ?(page_size=20) hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let offset, limit = limits page page_size in
    let cycle, nocycle = test_opt Int64.of_int cycle in
    [%pgsql dbh
      "SELECT level, cycle, slots[ARRAY_POSITION(endorsers, $hash)], \
       level - $head_lvl FROM level_rights \
       WHERE $hash = ANY (endorsers)  AND level >= $head_lvl AND \
       ($nocycle OR cycle = $?cycle) AND ready \
       ORDER BY level ASC NULLS FIRST LIMIT $limit OFFSET $offset"]
    >>= fun rows ->
    return @@ Pg_helper.endorser_rights_from_db_list rows

  let cycle_endorser_rights hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun current_cycle ->
    head_level dbh >>= fun head_lvl ->
    [%pgsql dbh
      "WITH lr(cycle, n, nslot) AS ( \
       SELECT cycle, COUNT(level), \
       SUM( slots[ARRAY_POSITION(endorsers, $hash)]) \
       FROM level_rights \
       WHERE $hash = ANY (endorsers)  and level > $head_lvl AND ready \
       GROUP BY cycle), \
       ro(cycle) AS ( \
       SELECT sr.cycle FROM snapshot_owner AS so \
       INNER JOIN snapshot_rolls AS sr ON sr.id = so.id \
       WHERE so.dn1 = $hash AND sr.cycle >= $current_cycle AND ready) \
       SELECT ro.cycle, n, nslot \
       FROM ro LEFT JOIN lr ON lr.cycle = ro.cycle \
       ORDER BY ro.cycle DESC"]
    >>= fun rows ->
    return @@ List.map
      (fun (cr_cycle, cr_nblocks, cr_priority) ->
         {cr_cycle = Int64.to_int cr_cycle;
          cr_nblocks = Option.fold ~none:0 ~some:Int64.to_int cr_nblocks;
          cr_priority = Option.fold ~none:0. ~some:Int64.to_float cr_priority}) rows

  let nb_endorsements_history hash =
    nb_cycle_rewards ~only_future:true hash >>= fun nb_rights ->
    nb_cycle_endorsements hash >>= fun nb_cycle_endorsements ->
    if nb_rights + nb_cycle_endorsements = 0 then return 0
    else return (1 + nb_rights + nb_cycle_endorsements)

  let endorsements_history ?(page=0) ?(page_size=20) hash =
    total_endorsements hash >>= fun tot_row ->
    cycle_endorser_rights hash >>= fun rights_rows ->
    let offset = Int64.of_int @@
      if page = 0 then 0 else page * page_size - List.length rights_rows in
    let limit = Int64.of_int @@
      if page = 0 then page_size - List.length rights_rows else page_size in
    if offset < 0L then return (tot_row, rights_rows, []) else
      cycle_endorsements offset limit hash >>= fun rows ->
      return (tot_row, (if page = 0 then rights_rows else []), rows)

  let nb_cycle_rights ?(future=true) ?filter () =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let filter, nofilter = test_opti filter in
    let level, notlevel =
      test_opti
        (match filter with None -> None | Some filter -> Int64.of_string_opt filter) in
    [%pgsql dbh
      "SELECT COUNT(level) FROM level_rights \
       WHERE ($future AND level > $head_lvl \
       OR NOT $future AND level <= $head_lvl) \
       AND ($nofilter OR ( NOT $notlevel AND level = $?level \
       OR $notlevel AND bakers_priority[ARRAY_POSITION(bakers, $?filter)] < 4)) \
       AND ready"]
    >>= of_count_opt

  let cycle_rights ?(future=true) ?filter ?(page=0) ?(page_size=20) () =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    let offset, limit = limits page page_size in
    let filter, nofilter = test_opti filter in
    let level, notlevel =
      test_opti
        (match filter with None -> None | Some filter -> Int64.of_string_opt filter) in
    [%pgsql dbh
      "nullable-results"
      "SELECT lr.level, bakers, endorsers, bakers_priority, bl.priority, bl.baker \
       FROM level_rights AS lr \
       LEFT JOIN block AS bl ON (bl.level = lr.level AND bl.distance_level = 0) \
       WHERE ($future AND lr.level > $head_lvl \
       OR NOT $future AND lr.level <= $head_lvl) \
       AND ($nofilter OR ( NOT $notlevel AND lr.level = $?level \
       OR $notlevel AND bakers_priority[ARRAY_POSITION(bakers, $?filter)] < 4)) \
       AND ready
       ORDER BY \
       CASE WHEN $future THEN lr.level END ASC, \
       CASE WHEN NOT $future THEN lr.level END DESC \
       LIMIT $limit OFFSET $offset"]
    >>= fun rows ->
    return @@ Pg_helper.rights_from_db_list rows

  let last_baking_and_endorsement hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    [%pgsql dbh
      "SELECT hash, baker, level, cycle, priority, 0, distance_level, \
       fees, 0::float, true, timestamp, endorsements_included \
       FROM block \
       WHERE baker = $hash AND distance_level = 0 \
       ORDER BY level DESC LIMIT 1"]
    >>= fun last_baking ->
    [%pgsql dbh
      "nullable-results"
      "SELECT block_level, array_length(slots, 1), op_block_hash, source, \
       op_cycle, priority, 0, slots, timestamp \
       FROM endorsement_all \
       WHERE source = $hash AND distance_level = 0 \
       ORDER BY block_level DESC LIMIT 1"]
    >>= fun last_endorsement ->
    [%pgsql dbh
      "SELECT lr.cycle, lr.level, b.timestamp
       FROM block AS b \
       INNER JOIN level_rights AS lr ON b.level = lr.level \
       WHERE lr.bakers_priority[array_position(lr.bakers, $hash)] <= b.priority \
       AND lr.level <= $head_lvl AND ready ORDER by lr.level DESC LIMIT 1"]
    >>= (function
        | (cycle, level, tsp) :: _ ->
          return (Int64.to_int cycle, Int64.to_int level, Pg_helper.string_of_cal tsp)
        | _ -> return (0, 0, "1970-01-01T00:00:00Z"))
    >>= fun last_baking_right ->
    [%pgsql dbh
      "SELECT lr.cycle, lr.level, b.timestamp FROM block as b \
       INNER JOIN level_rights AS lr ON lr.level = b.level \
       WHERE $hash = ANY(endorsers) AND lr.level < $head_lvl AND ready \
       ORDER by lr.level DESC LIMIT 1"]
    >>= (function
        | (cycle, level, tsp) :: _ ->
          return (Int64.to_int cycle, Int64.to_int level, Pg_helper.string_of_cal tsp)
        | _ -> return (0, 0, "1970-01-01T00:00:00Z"))
    >>= fun last_endorsement_right ->
    let last_baking = Pg_helper.bakings_from_db_list last_baking in
    let last_endorsement = Pg_helper.bakings_endorsement_from_db_list last_endorsement in
    return (last_baking, last_endorsement, last_baking_right, last_endorsement_right)

  let next_baking_and_endorsement hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT b.cycle, b.level, b.timestamp FROM block AS b \
       WHERE distance_level = 0 ORDER BY level DESC LIMIT 1"]
    >>= begin function
      | [ head_cycle, head_level, head_tsp ] ->
        return (head_cycle, head_level,
                Pg_helper.string_of_cal head_tsp)
      | _ -> return (0L, 0L, "1970-01-01T00:00:00Z") end
    >>= fun (head_cycle, head_lvl, head_tsp) ->
    [%pgsql dbh
      "SELECT cycle, level FROM level_rights WHERE level > $head_lvl AND \
       $hash = ANY(bakers) AND bakers_priority[array_position(bakers, $hash)] = 0 \
       AND ready ORDER BY level ASC LIMIT 1"]
    >>= (function
        | (cycle, level) :: _ -> return (Int64.to_int cycle, Int64.to_int level)
        | _ -> return (0, 0))
    >>= fun next_baking ->
    [%pgsql dbh
      "SELECT cycle, level FROM level_rights WHERE level >= $head_lvl AND \
       $hash = ANY(endorsers) AND ready ORDER BY level ASC LIMIT 1"]
    >>= (function
        | (cycle, level) :: _ -> return (Int64.to_int cycle, Int64.to_int level)
        | _ -> return (0, 0))
    >>= fun next_endorsement ->
    return (Int64.to_int head_cycle, Int64.to_int head_lvl, next_baking, next_endorsement, head_tsp)

  let required_balance hash =
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    current_cycle dbh >>= fun current_cycle ->
    [%pgsql dbh
      "WITH rights AS \
       (SELECT lr.cycle, \
       SUM(CASE WHEN \
       bakers_priority[ARRAY_POSITION(bakers, $hash)] = 0 \
       THEN 1 ELSE 0 END) AS bcount, \
       SUM(slots[ARRAY_POSITION(endorsers, $hash)]) AS ecount \
       FROM level_rights AS lr \
       WHERE (lr.level > $head_lvl) AND \
       ($hash = ANY (bakers) OR $hash = ANY (endorsers)) AND ready \
       GROUP BY lr.cycle ORDER BY lr.cycle ASC) \
       SELECT r.cycle, r.bcount * block_security_deposit + r.ecount * endorsement_security_deposit, \
       COALESCE((SELECT count FROM snapshot_owner WHERE dn1 = $hash \
       AND id = (SELECT id FROM snapshot_rolls AS sr WHERE sr.cycle = r.cycle AND ready))), \
       COALESCE((SELECT rolls_count FROM snapshot_rolls AS sr WHERE sr.cycle = r.cycle AND ready)) \
       FROM rights AS r \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE($head_lvl BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= $head_lvl AND cst.level_end IS NULL)"]
    >>= fun rights_rows ->
    let cst = Infos.constants ~level:(Int64.to_int head_lvl) in
    let allowed_fork = cst.Dune_types.preserved_cycles in
    cycle_bakings 0L (Int64.of_int (allowed_fork + 3)) hash >>= fun cbks ->
    let cbks = List.map
        (fun cbk ->
           cbk.cbk_cycle, List.fold_left Int64.add 0L
             [cbk.cbk_dun.dun_deposit; cbk.cbk_dun.dun_reward; cbk.cbk_dun.dun_fee]) cbks in
    cycle_endorsements 0L (Int64.of_int (allowed_fork + 3)) hash >>= fun ceds ->
    let ceds = List.map
        (fun ced ->
           ced.ced_cycle, List.fold_left Int64.add 0L
             [ced.ced_dun.dun_deposit; ced.ced_dun.dun_reward; ced.ced_dun.dun_fee]) ceds in
    let _, l = List.fold_left
        (fun (s, acc) (cycle, deposit, roll, total) ->
           let cyclei = Int64.to_int cycle in
           let roll = Option.fold ~none:(-1) ~some:Int32.to_int roll in
           let deposit = Option.value ~default:0L deposit in
           let total = Option.fold ~none:0 ~some:Int32.to_int total in
           let back =
             if cycle = current_cycle then 0L
             else (
               let back_bk = List.fold_left (fun acc cbk ->
                   if fst cbk = cyclei - allowed_fork - 1 then Int64.add acc (snd cbk)
                   else acc ) 0L cbks in
               let back_end = List.fold_left (fun acc ced ->
                   if fst ced = cyclei - allowed_fork - 1 then Int64.add acc (snd ced)
                   else acc ) 0L ceds in
               Int64.add back_bk back_end) in
           let s = Int64.(add (sub s back) deposit) in
           (s, (cyclei, deposit, back, s, roll, total) :: acc))
        (0L, []) rights_rows in
    return @@ List.rev l

  let endorsements selector =
    with_dbh >>> fun dbh ->
    begin
      match selector with
      | Hash block_hash ->
        [%pgsql dbh
          "SELECT hash, op_block_hash, network, source, slots, block_hash, \
           block_level, op_level, priority, timestamp FROM endorsement_all \
           WHERE block_hash = $block_hash \
           ORDER BY priority, slots DESC"]
      | Level level ->
        let level64 = Int64.of_int level in
        [%pgsql dbh
          "SELECT hash, op_block_hash, network, source, slots, block_hash, \
           block_level, op_level, priority, timestamp FROM endorsement_all \
           WHERE block_level = $level64 AND \
           op_block_hash <> 'Orphan' \
           ORDER BY priority, slots DESC"]
    end
    >>= fun rows ->
    return @@ List.map
      (fun (op_hash, op_block_hash, op_network_hash, endorse_src, endorse_slot,
            endorse_block_hash, endorse_block_level, endorse_op_level,
            endorse_priority, endorse_timestamp) ->
        let endorse_slot = List.filter_map (Option.map Int32.to_int) endorse_slot in
        let endorse_block_level = Int64.to_int endorse_block_level in
        let endorse_op_level = Int64.to_int endorse_op_level in
        let endorse_priority = Int32.to_int endorse_priority in
        let endorse_src = Alias.to_name endorse_src in
        let endorse_timestamp = Pg_helper.string_of_cal endorse_timestamp in
        let endorse =
          { endorse_src; endorse_block_hash; endorse_slot;
            endorse_block_level; endorse_op_level; endorse_priority;
            endorse_timestamp } in
        { op_hash; op_block_hash; op_network_hash;
          op_type = Sourced (Endorsement endorse) }) rows

  let protocol protocol_hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT * from protocol WHERE hash = $protocol_hash"] >>= function
    | [] -> return None
    | [ (proto_hash, proto_name) ] -> return @@ Some { proto_hash; proto_name }
    | rows ->
      begin
        debug
          "[Reader] [protocol] Primary key constraint failed ? %S has %d entries\n%!"
          protocol_hash
          (List.length rows);
        try assert false with e -> fail e
      end

  let account_status hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT rev.hash FROM reveal_all AS rev \
       WHERE rev.pubkey = $hash \
       ORDER BY rev.timestamp_block ASC LIMIT 1"] >>= fun reveal ->
    [%pgsql dbh
      "SELECT ori.hash FROM origination_all AS ori \
       WHERE ori.kt1 = $hash LIMIT 1"] >>= fun origin ->
    (match reveal, origin with
       rev :: _, ori :: _ ->
       { account_status_hash = Alias.to_name hash;
         account_status_revelation = Some rev;
         account_status_origination = Some ori }
     | rev :: _, [] ->
       { account_status_hash = Alias.to_name hash;
         account_status_revelation = Some rev;
         account_status_origination = None }
     | [], ori :: _ ->
       { account_status_hash = Alias.to_name hash;
         account_status_revelation = None;
         account_status_origination = Some ori }
     | [], [] ->
       { account_status_hash = Alias.to_name hash;
         account_status_revelation = None;
         account_status_origination = None }) |> return

  let account selector =
    match selector with
    | Account hash ->
      begin
        with_dbh >>> fun dbh ->
        [%pgsql dbh
          "nullable-results"
          "SELECT t.hash, t.alias, i.manager, i.delegate, i.spendable, i.delegatable, \
           i.origination, s.spendable_balance, s.frozen, s.rewards, s.fees, s.deposits \
           FROM dune_user AS t \
           INNER JOIN account_info AS i ON i.hash = t.hash \
           INNER JOIN balance_snapshot AS s ON s.hash = t.hash \
           WHERE t.hash = $hash"]
        >>= function
        | [ row ] -> return (Some (Pg_helper.account_from_db row))
        | _ -> return None end
    | _ -> return None

  let accounts ?(page=0) ?(page_size=20) ?contract () =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    let contract, nocontract = test_opti contract in
    [%pgsql dbh
      "nullable-results"
      "SELECT t.hash, t.alias, i.manager, i.delegate, i.spendable, i.delegatable, \
       i.origination, s.spendable_balance, s.frozen, s.rewards, s.fees, s.deposits \
       FROM dune_user AS t \
       INNER JOIN account_info AS i ON i.hash = t.hash \
       LEFT JOIN balance_snapshot AS s ON s.hash = t.hash \
       WHERE ($nocontract OR (NOT $nocontract AND t.contract = $?contract)) \
       AND t.hash <> 'God' AND t.hash <> '' \
       ORDER BY t.id DESC LIMIT $limit OFFSET $offset"]
    >>= fun rows ->
    return @@ List.map Pg_helper.account_from_db rows

  let nb_accounts ?contract () =
    let contract, nocontract = test_opti contract in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "nullable-results"
      "SELECT COUNT(t.hash) \
       FROM dune_user AS t \
       INNER JOIN account_info AS i ON i.hash = t.hash \
       LEFT JOIN balance_snapshot AS s ON s.hash = t.hash \
       WHERE ($nocontract OR (NOT $nocontract AND t.contract = $?contract)) \
       AND t.hash <> 'God' AND t.hash <> ''"]
    >>= of_count_opt

  let to_peer point_id = point_id

  let nb_network_peers ?state () =
    with_dbh >>> fun dbh ->
    let state, nostate = test_opti state in
    [%pgsql dbh "SELECT COUNT(*) FROM peers WHERE ($nostate OR state = $?state)"]
    >>= of_count_opt

  let network_stats ?state ?(page=0) ?(page_size=20) () =
    let open Dune_types in
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    let state, nostate = test_opti state in
    [%pgsql dbh
      "SELECT * FROM peers \
       WHERE ($nostate OR state = $?state) \
       ORDER BY state DESC, total_sent + total_received DESC \
       OFFSET $offset LIMIT $limit"]
    >>= function
    | [] -> return []
    | results ->
      return @@
      List.map (fun (_, peer_id, country_name, country_code, point_id, trusted, score, state,
                     total_sent, total_recv, current_inflow, current_outflow,
                     last_failed_connection_point, last_failed_connection_date,
                     last_rejected_connection_point, last_rejected_connection_date,
                     last_established_connection_point, last_established_connection_date,
                     last_disconnection_point, last_disconnection_date,
                     last_seen_point, last_seen_date,
                     last_miss_point, last_miss_date) ->
                 let state =
                   match state with
                   | "accepted" -> Accepted
                   | "running"  -> Running
                   | "disconnected" -> Disconnected
                   | _ -> assert false in
                 let to_peer = function None -> "" | Some point_id -> to_peer point_id in
                 let id_point = Some (to_peer point_id) in
                 let to_date = function None -> "" | Some date -> date in
                 let last_failed_connection =
                   Some (to_peer last_failed_connection_point, to_date last_failed_connection_date) in
                 let last_rejected_connection =
                   Some (to_peer last_rejected_connection_point, to_date last_rejected_connection_date) in
                 let last_established_connection =
                   Some (to_peer last_established_connection_point, to_date last_established_connection_date) in
                 let last_disconnection =
                   Some (to_peer last_disconnection_point, to_date last_disconnection_date) in
                 let last_seen = Some (to_peer last_seen_point, to_date last_seen_date) in
                 let last_miss = Some (to_peer last_miss_point, to_date last_miss_date) in
                 let country = country_name, country_code in
                 { peer_id; country; score ; trusted ; conn_metadata = None ;
                   state ; id_point ;
                   stat = { total_sent; total_recv ;
                            current_inflow = Int64.to_int current_inflow ;
                            current_outflow = Int64.to_int current_outflow } ;
                   last_failed_connection ; last_rejected_connection ;
                   last_established_connection ; last_disconnection ;
                   last_seen; last_miss } )
        results

  let country_stats ?state () =
    with_dbh >>> fun dbh ->
    begin match state with
      | Some "running" ->
        [%pgsql dbh
          "SELECT country_name, country_code, COUNT(*) FROM peers \
           WHERE state = 'running' GROUP BY (country_name, country_code)"]
      | _ ->
        [%pgsql dbh
          "SELECT country_name, country_code, COUNT(*) FROM peers \
           GROUP BY (country_name, country_code)"]
    end  >>= fun rows ->
    return @@
    List.map (fun (country_name, country_code, total) ->
        let total = match total with None -> 0 | Some t -> Int64.to_int t in
        {country_name; country_code; total})
      rows

  let baker_stats ?cycle hash =
    let cycle, nocycle = test_opt Int64.of_int cycle in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "WITH tmp0 AS ( \
       SELECT baker, volume, fees, pow_nonce FROM block \
       WHERE distance_level = 0 AND ($nocycle OR cycle = $?cycle) AND baker = $hash \
       ORDER BY level DESC), \
       tmp1(volume, fees) AS ( \
       SELECT SUM(volume)::bigint, SUM(fees)::bigint FROM tmp0), \
       tmp2(pow_nonce) AS (SELECT DISTINCT ON (baker) pow_nonce FROM tmp0), \
       tmp3(nb_block, nb_endorsement, rewards) AS ( \
       SELECT SUM(array_sum(nb_baking, 1))::bigint, \
       SUM(array_sum(nb_endorsement, 1))::bigint, \
       SUM(block_rewards)::bigint + SUM(endorsement_rewards)::bigint \
       FROM cycle_count_baker WHERE ($nocycle OR cycle = $?cycle) AND dn = $hash)
       SELECT nb_block, nb_endorsement, rewards, volume, fees, pow_nonce
       FROM tmp1, tmp2, tmp3"]
    >|= function
    | [ Some nbb, Some nbe, vol_opt, fees_opt, rewards_opt, pow_nonce ] ->
      let baker_hash = Alias.to_name hash in
      let nb_blocks = Int64.to_int nbb in
      let nb_endorsements = Int64.to_int nbe in
      let volume_total = Option.value ~default:0L vol_opt in
      let fees_total = Option.value ~default:0L fees_opt in
      let rewards_total = Option.value ~default:0L rewards_opt in
      let baker_version = Infos.baker_version pow_nonce in
      { baker_hash; nb_blocks; nb_endorsements;
        volume_total; fees_total; rewards_total; baker_version }
    | _ ->
      { baker_hash = Alias.to_name hash; nb_blocks = 0; volume_total = 0L;
        fees_total = 0L; rewards_total = 0L; nb_endorsements = 0; baker_version = "" }

  let bakers_stats ?cycle () =
    let cycle, nocycle = test_opt Int64.of_int cycle in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "WITH tmp0 AS ( \
       SELECT baker, volume, fees, pow_nonce FROM block \
       WHERE distance_level = 0 AND ($nocycle OR cycle = $?cycle) \
       ORDER BY level DESC), \
       tmp1(dn, volume, fees) AS ( \
       SELECT baker, SUM(volume)::bigint, SUM(fees)::bigint FROM tmp0 \
       GROUP BY baker), \
       tmp2(dn, pow_nonce) AS ( \
       SELECT DISTINCT ON (baker) baker, pow_nonce FROM tmp0), \
       tmp3(dn, nb_block, nb_endorsement, rewards) AS ( \
       SELECT dn, SUM(array_sum(nb_baking, 1))::bigint AS nb_block, \
       SUM(array_sum(nb_endorsement, 1))::bigint, \
       SUM(block_rewards)::bigint + SUM(endorsement_rewards)::bigint \
       FROM cycle_count_baker WHERE ($nocycle OR cycle = $?cycle) \
       GROUP BY dn)
       SELECT tmp1.dn, nb_block, nb_endorsement, volume, fees, rewards, pow_nonce
       FROM tmp1 \
       INNER JOIN tmp2 ON tmp1.dn = tmp2.dn
       INNER JOIN tmp3 ON tmp1.dn = tmp3.dn \
       ORDER BY nb_block DESC, tmp1.dn"]
    >|=
    (List.map (fun (baker_hash, nbb, nbe, vol_opt, fees_opt, rewards_opt, pow_nonce) ->
         let baker_hash = Alias.to_name baker_hash in
         let nb_blocks = Option.fold ~none:0 ~some:Int64.to_int nbb in
         let nb_endorsements = Option.fold ~none:0 ~some:Int64.to_int nbe in
         let volume_total = Option.value ~default:0L vol_opt in
         let fees_total = Option.value ~default:0L fees_opt in
         let rewards_total = Option.value ~default:0L rewards_opt in
         let baker_version = Infos.baker_version pow_nonce in
         {baker_hash; nb_blocks; nb_endorsements; volume_total;
          fees_total; rewards_total; baker_version}))

  let health_stats cycle =
    with_dbh >>> fun dbh ->
    let cycle = Int64.of_int cycle in
    [%pgsql dbh
      "SELECT endorsers_per_block, blocks_per_cycle, blocks_per_commitment \
       FROM cycle_limits AS cl \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE cl.cycle = $cycle"]
    >>= (function
        | (endorsers_per_block, blocks_per_cycle, blocks_per_commitment) :: _ ->
          return (Int32.to_int endorsers_per_block, Int32.to_int blocks_per_cycle,
                  Int32.to_int blocks_per_commitment)
        | _ -> return (0,0,0))
    >>= fun (endorsers_per_block, blocks_per_cycle, blocks_per_commitment) ->
    [%pgsql dbh
      "SELECT AVG(priority)::float, SUM(volume)::bigint, SUM(fees)::bigint, \
       AVG(LEAST(priority, 16)^2)::float \
       FROM block WHERE distance_level = 0 AND cycle = $cycle"]
    >>= fun cycle_stats ->
    [%pgsql dbh
      "SELECT switch_count, longest_alt_chain \
       FROM switch WHERE cycle = $cycle"]
    >>= fun switch_stats ->
    [%pgsql dbh
      "SELECT level, EXTRACT(YEAR FROM timestamp), EXTRACT(MONTH FROM timestamp), \
       EXTRACT(DAY FROM timestamp) FROM block \
       WHERE cycle = $cycle AND \
       cycle_position = 0 AND level <> 0"]
    >>= fun cycle_start ->
    [%pgsql dbh
      "SELECT level, EXTRACT(YEAR FROM timestamp), EXTRACT(MONTH FROM timestamp), \
       EXTRACT(DAY FROM timestamp) FROM block \
       WHERE cycle = $cycle \
       ORDER BY level DESC LIMIT 1"]
    >>= fun cycle_end ->
    [%pgsql dbh
      "SELECT COUNT( * ) FROM cycle_count_baker \
       WHERE cycle = $cycle AND nb_baking <> '{}'"]
    >>= of_count_opt >>= fun cycle_bakers ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM cycle_count_baker \
       WHERE cycle = $cycle AND nb_endorsement <> '{}'"]
    >>= of_count_opt >>= fun cycle_endorsers ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM block \
       WHERE distance_level <> 0 AND \
       cycle = $cycle"]
    >>= of_count_opt >>= fun alternative_heads_number ->
    [%pgsql dbh
      "SELECT COUNT(s.level) FROM seed_nonce_revelation_all AS s \
       INNER JOIN cycle_limits AS cl ON cl.cycle = $cycle \
       WHERE s.op_level BETWEEN cl.level_start AND cl.level_end \
       AND s.distance_level = 0"]
    >>= of_count_opt >>= fun cycle_revelations_number ->
    [%pgsql dbh
      "SELECT SUM(CASE WHEN distance_level = 0 THEN array_length(slots, 1) ELSE 0 END), \
       SUM(CASE WHEN distance_level <> 0 THEN array_length(slots, 1) ELSE 0 END) \
       FROM endorsement_all WHERE op_cycle = $cycle"]
    >>= fun cycle_endorsements ->
    [%pgsql dbh
      "SELECT hash, level, volume \
       FROM block WHERE \
       volume = (SELECT max(volume) FROM block WHERE cycle = $cycle) \
       AND distance_level = 0 \
       AND cycle = $cycle LIMIT 1"] >>= fun biggest_block_volume ->
    [%pgsql dbh
      "SELECT hash, level, fees \
       FROM block WHERE \
       fees = (SELECT max(fees) FROM block WHERE cycle = $cycle) \
       AND distance_level = 0 \
       AND cycle = $cycle LIMIT 1"] >>= fun biggest_block_fees ->
    [%pgsql dbh
      "SELECT baker, COUNT(*) FROM block WHERE \
       cycle = $cycle GROUP BY baker \
       ORDER BY COUNT(*) DESC, baker LIMIT 1"] >>= fun top_baker ->
    [%pgsql dbh
      "SELECT SUM(nb_miss_baking), SUM(nb_miss_endorsement) FROM cycle_count_baker \
       WHERE cycle = $cycle"] >|= fun missed ->
    let slot_number = float_of_int endorsers_per_block in
    let cycle_start_level, year_start, month_start, day_start  = match cycle_start with
      | (level, Some year, Some month, Some day) :: _
        -> Int64.to_int level, int_of_float year, int_of_float month, int_of_float day
      | _ -> -1, -1, -1, -1 in
    let cycle_end_level, year_end, month_end, day_end = match cycle_end with
       | (level, Some year, Some month, Some day) :: _
      -> Int64.to_int level, int_of_float year, int_of_float month, int_of_float day
      | _ -> -1, -1, -1, -1 in
    let mean_priority, cycle_volume, cycle_fees, prio2 = match cycle_stats with
      | [ Some priority, Some volume, Some fees, Some prio2 ] ->
        priority, volume, fees, prio2
      | _ -> 0., 0L, 0L, 0. in
    let cycle_position =
      float_of_int @@ (cycle_end_level - cycle_start_level) + 1 in
    let total_endorsements, main_endorsements_rate,
        alt_endorsements_rate = match cycle_endorsements with
      | [main_endo, alt_endo] ->
        let main_endo = Option.fold ~none:0. ~some:Int64.to_float main_endo in
        let alt_endo = Option.fold ~none:0. ~some:Int64.to_float alt_endo in
        int_of_float (main_endo +. alt_endo),
        main_endo /. (alt_endo +. main_endo) *. 100.,
        (alt_endo /. (cycle_position *. slot_number)) *. 100.
      | _ -> 0, 0., 0. in
    let endorsements_rate =
      ((float total_endorsements) /. (cycle_position *. slot_number)) *. 100. in
    let empty_endorsements_rate = 100. -. endorsements_rate in
    let main_revelation_rate =
      ((float_of_int cycle_revelations_number) /.
       (float_of_int @@ blocks_per_cycle / blocks_per_commitment)) *. 100. in
    let double_endorsements = 0 in
    let switch_number, longest_switch_depth = match switch_stats with
      | [ i, j ] -> Int64.to_int i, Int64.to_int j
      | _ -> -1, -1 in
    let score_priority = 100. -. prio2 /. (16. ** 2.) *. 100. in
    let biggest_block_volume = match biggest_block_volume with
      | ( hash, level, _ ) :: _ -> hash, Int64.to_int level
      | _ -> "No one", -1 in
    let biggest_block_fees = match biggest_block_fees with
      | ( hash, level, _ ) :: _ -> hash, Int64.to_int level
      | _ -> "No one", -1 in
    let top_baker = match top_baker with
      | ( top_baker, _ ) :: _ -> top_baker
      | _ -> "No one" in
    let missed_blocks, missed_endorsements = match missed with
      | (Some b, Some e) :: _ -> int_of_string b, int_of_string e
      | _ -> 0, 0 in
    {
      cycle_start_level ;
      cycle_end_level ;
      cycle_volume ;
      cycle_fees ;
      cycle_bakers ;
      cycle_endorsers ;
      cycle_date_start = (year_start, month_start, day_start) ;
      cycle_date_end = (year_end, month_end, day_end) ;

      endorsements_rate ;
      main_endorsements_rate ;
      alt_endorsements_rate ;
      empty_endorsements_rate ;
      double_endorsements ;

      main_revelation_rate ;

      alternative_heads_number ;
      switch_number ;
      longest_switch_depth ;

      mean_priority ;
      score_priority ;

      biggest_block_volume ;
      biggest_block_fees ;
      top_baker = Alias.to_name top_baker ;

      missed_blocks;
      missed_endorsements;
    }

  let context_days () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT day FROM day_context ORDER BY day DESC"] >>= fun days ->
    return @@
    List.map (fun d ->
        CalendarLib.Printer.Calendar.sprint "%Y-%m-%d" d) days

  let context_stats day =
    let day_before = CalendarLib.Calendar.prev day `Day in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT id FROM day_context WHERE day = $day"] >>= function
    | [ id ] ->
      [%pgsql dbh
        "SELECT * FROM context_totals WHERE id = $id"] >>= begin function
        | [ _id, Some context_hash, _context_period, _context_period_kind,
            Some context_addresses, Some context_keys, Some context_revealed,
            Some context_originated, Some context_contracts, Some context_roll_owners,
            Some context_rolls, Some context_delegated, Some context_delegators,
            Some context_deleguees, Some context_self_delegates,
            Some context_multi_deleguees, Some context_current_balances,
            Some context_full_balances, Some context_staking_balances,
            Some context_frozen_balances, Some context_frozen_deposits,
            Some context_frozen_rewards, Some context_frozen_fees,
            Some context_paid_bytes, Some context_used_bytes ] ->
          level ~hash:context_hash () >>= fun context_level ->
          [%pgsql dbh
            "SELECT id FROM day_context WHERE day = $day_before"] >>= begin function
            | [ id_before ] ->
              [%pgsql dbh
                "SELECT * FROM context_totals WHERE id = $id_before"] >>= begin function
                | [ _id, _context_hash2, _context_period2, _context_period_kind2,
                    Some context_addresses2, Some context_keys2, Some context_revealed2,
                    Some context_originated2, Some context_contracts2, Some context_roll_owners2,
                    Some context_rolls2, Some context_delegated2, Some context_delegators2,
                    Some context_deleguees2, Some context_self_delegates2,
                    Some context_multi_deleguees2, Some context_current_balances2,
                    Some context_full_balances2, Some context_staking_balances2,
                    Some context_frozen_balances2, Some context_frozen_deposits2,
                    Some context_frozen_rewards2, Some context_frozen_fees2,
                    Some context_paid_bytes2, Some context_used_bytes2 ] ->
                  return @@
                  Some {
                    context_level ;

                    context_addresses =
                      Int32.to_int context_addresses ;
                    context_addresses_diff =
                      if context_addresses = 0l then 0.
                      else
                        ((Int32.to_float context_addresses) -.
                         (Int32.to_float context_addresses2)) /.
                        (Int32.to_float context_addresses) *. 100. ;
                    context_keys =
                      Int32.to_int context_keys ;
                    context_keys_diff =
                      if context_keys = 0l then 0.
                      else
                        ((Int32.to_float context_keys) -.
                         (Int32.to_float context_keys2)) /.
                        (Int32.to_float context_keys) *. 100. ;
                    context_revealed =
                      Int32.to_int context_revealed ;
                    context_revealed_diff =
                      if context_revealed = 0l then 0.
                      else
                        ((Int32.to_float context_revealed) -.
                         (Int32.to_float context_revealed2)) /.
                        (Int32.to_float context_revealed) *. 100. ;
                    context_originated =
                      Int32.to_int context_originated ;
                    context_originated_diff =
                      if context_originated = 0l then 0.
                      else
                        ((Int32.to_float context_originated) -.
                         (Int32.to_float context_originated2)) /.
                        (Int32.to_float context_originated) *. 100. ;
                    context_contracts =
                      Int32.to_int context_contracts ;
                    context_contracts_diff =
                      if context_contracts = 0l then 0.
                      else
                        ((Int32.to_float context_contracts) -.
                         (Int32.to_float context_contracts2)) /.
                        (Int32.to_float context_contracts) *. 100. ;
                    context_roll_owners =
                      Int32.to_int context_roll_owners ;
                    context_roll_owners_diff =
                      if context_roll_owners = 0l then 0.
                      else
                        ((Int32.to_float context_roll_owners) -.
                         (Int32.to_float context_roll_owners2)) /.
                        (Int32.to_float context_roll_owners) *. 100. ;
                    context_rolls =
                      Int32.to_int context_rolls ;
                    context_rolls_diff =
                      if context_rolls = 0l then 0.
                      else
                        ((Int32.to_float context_rolls) -.
                         (Int32.to_float context_rolls2)) /.
                        (Int32.to_float context_rolls) *. 100. ;
                    context_delegated ;
                    context_delegated_diff =
                      if context_delegated = 0L then 0.
                      else
                        ((Int64.to_float context_delegated) -.
                         (Int64.to_float context_delegated2)) /.
                        (Int64.to_float context_delegated) *. 100. ;
                    context_delegators =
                      Int32.to_int context_delegators ;
                    context_delegators_diff =
                      if context_delegators = 0l then 0.
                      else
                        ((Int32.to_float context_delegators) -.
                         (Int32.to_float context_delegators2)) /.
                        (Int32.to_float context_delegators) *. 100. ;
                    context_deleguees =
                      Int32.to_int context_deleguees ;
                    context_deleguees_diff =
                      if context_deleguees = 0l then 0.
                      else
                        ((Int32.to_float context_deleguees) -.
                         (Int32.to_float context_deleguees2)) /.
                        (Int32.to_float context_deleguees) *. 100. ;
                    context_self_delegates =
                      Int32.to_int context_self_delegates ;
                    context_self_delegates_diff =
                      if context_self_delegates = 0l then 0.
                      else
                        ((Int32.to_float context_self_delegates) -.
                         (Int32.to_float context_self_delegates2)) /.
                        (Int32.to_float context_self_delegates) *. 100. ;
                    context_multi_deleguees =
                      Int32.to_int context_multi_deleguees ;
                    context_multi_deleguees_diff =
                      if context_multi_deleguees = 0l then 0.
                      else
                        ((Int32.to_float context_multi_deleguees) -.
                         (Int32.to_float context_multi_deleguees2)) /.
                        (Int32.to_float context_multi_deleguees) *. 100. ;
                    context_current_balances ;
                    context_current_balances_diff =
                      if context_current_balances = 0L then 0.
                      else
                        ((Int64.to_float context_current_balances) -.
                         (Int64.to_float context_current_balances2)) /.
                        (Int64.to_float context_current_balances) *. 100. ;
                    context_full_balances ;
                    context_full_balances_diff =
                      if context_full_balances = 0L then 0.
                      else
                        ((Int64.to_float context_full_balances) -.
                         (Int64.to_float context_full_balances2)) /.
                        (Int64.to_float context_full_balances) *. 100. ;
                    context_staking_balances ;
                    context_staking_balances_diff =
                      if context_staking_balances = 0L then 0.
                      else
                        ((Int64.to_float context_staking_balances) -.
                         (Int64.to_float context_staking_balances2)) /.
                        (Int64.to_float context_staking_balances) *. 100. ;
                    context_frozen_balances ;
                    context_frozen_balances_diff =
                      if context_frozen_balances = 0L then 0.
                      else
                        ((Int64.to_float context_frozen_balances) -.
                         (Int64.to_float context_frozen_balances2)) /.
                        (Int64.to_float context_frozen_balances) *. 100. ;
                    context_frozen_deposits ;
                    context_frozen_deposits_diff =
                      if context_frozen_deposits = 0L then 0.
                      else
                        ((Int64.to_float context_frozen_deposits) -.
                         (Int64.to_float context_frozen_deposits2)) /.
                        (Int64.to_float context_frozen_deposits) *. 100. ;
                    context_frozen_rewards ;
                    context_frozen_rewards_diff =
                      if context_frozen_rewards = 0L then 0.
                      else
                        ((Int64.to_float context_frozen_rewards) -.
                         (Int64.to_float context_frozen_rewards2)) /.
                        (Int64.to_float context_frozen_rewards) *. 100. ;
                    context_frozen_fees ;
                    context_frozen_fees_diff =
                      if context_frozen_fees = 0L then 0.
                      else
                        ((Int64.to_float context_frozen_fees) -.
                         (Int64.to_float context_frozen_fees2)) /.
                        (Int64.to_float context_frozen_fees) *. 100. ;
                    context_paid_bytes ;
                    context_paid_bytes_diff =
                      if context_paid_bytes = 0L then 0.
                      else
                        ((Int64.to_float context_paid_bytes) -.
                         (Int64.to_float context_paid_bytes2)) /.
                        (Int64.to_float context_paid_bytes) *. 100. ;
                    context_used_bytes ;
                    context_used_bytes_diff =
                      if context_used_bytes = 0L then 0.
                      else
                        ((Int64.to_float context_used_bytes) -.
                         (Int64.to_float context_used_bytes2)) /.
                        (Int64.to_float context_used_bytes) *. 100. ;
                  }
                | _ ->
                  return @@
                  Some {
                    context_level ;
                    context_addresses = Int32.to_int context_addresses ;
                    context_addresses_diff = 100. ;
                    context_keys = Int32.to_int context_keys ;
                    context_keys_diff = 100. ;
                    context_revealed = Int32.to_int context_revealed ;
                    context_revealed_diff = 100. ;
                    context_originated = Int32.to_int context_originated ;
                    context_originated_diff = 100. ;
                    context_contracts = Int32.to_int context_contracts ;
                    context_contracts_diff = 100. ;
                    context_roll_owners = Int32.to_int context_roll_owners ;
                    context_roll_owners_diff = 100. ;
                    context_rolls = Int32.to_int context_rolls ;
                    context_rolls_diff = 100. ;
                    context_delegated ;
                    context_delegated_diff = 100. ;
                    context_delegators = Int32.to_int context_delegators ;
                    context_delegators_diff = 100. ;
                    context_deleguees = Int32.to_int context_deleguees ;
                    context_deleguees_diff = 100. ;
                    context_self_delegates = Int32.to_int context_self_delegates ;
                    context_self_delegates_diff = 100. ;
                    context_multi_deleguees = Int32.to_int context_multi_deleguees ;
                    context_multi_deleguees_diff = 100. ;
                    context_current_balances ;
                    context_current_balances_diff = 100. ;
                    context_full_balances ;
                    context_full_balances_diff = 100. ;
                    context_staking_balances ;
                    context_staking_balances_diff = 100. ;
                    context_frozen_balances ;
                    context_frozen_balances_diff = 100. ;
                    context_frozen_deposits ;
                    context_frozen_deposits_diff = 100. ;
                    context_frozen_rewards ;
                    context_frozen_rewards_diff = 100. ;
                    context_frozen_fees ;
                    context_frozen_fees_diff = 100. ;
                    context_paid_bytes ;
                    context_paid_bytes_diff = 100. ;
                    context_used_bytes ;
                    context_used_bytes_diff = 100. ;
                  }
              end
            | _ ->
              return @@
              Some {
                context_level ;
                context_addresses = Int32.to_int context_addresses ;
                context_addresses_diff = 100. ;
                context_keys = Int32.to_int context_keys ;
                context_keys_diff = 100. ;
                context_revealed = Int32.to_int context_revealed ;
                context_revealed_diff = 100. ;
                context_originated = Int32.to_int context_originated ;
                context_originated_diff = 100. ;
                context_contracts = Int32.to_int context_contracts ;
                context_contracts_diff = 100. ;
                context_roll_owners = Int32.to_int context_roll_owners ;
                context_roll_owners_diff = 100. ;
                context_rolls = Int32.to_int context_rolls ;
                context_rolls_diff = 100. ;
                context_delegated ;
                context_delegated_diff = 100. ;
                context_delegators = Int32.to_int context_delegators ;
                context_delegators_diff = 100. ;
                context_deleguees = Int32.to_int context_deleguees ;
                context_deleguees_diff = 100. ;
                context_self_delegates = Int32.to_int context_self_delegates ;
                context_self_delegates_diff = 100. ;
                context_multi_deleguees = Int32.to_int context_multi_deleguees ;
                context_multi_deleguees_diff = 100. ;
                context_current_balances ;
                context_current_balances_diff = 100. ;
                context_full_balances ;
                context_full_balances_diff = 100. ;
                context_staking_balances ;
                context_staking_balances_diff = 100. ;
                context_frozen_balances ;
                context_frozen_balances_diff = 100. ;
                context_frozen_deposits ;
                context_frozen_deposits_diff = 100. ;
                context_frozen_rewards ;
                context_frozen_rewards_diff = 100. ;
                context_frozen_fees ;
                context_frozen_fees_diff = 100. ;
                context_paid_bytes ;
                context_paid_bytes_diff = 100. ;
                context_used_bytes ;
                context_used_bytes_diff = 100. ;
              }
          end
        | _ -> return None
      end
    | _ -> return None

  let nb_tops ?(kind=Balances) () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT id FROM day_context \
       WHERE day = (SELECT MAX(day) FROM day_context)"] >>= begin function
      | [ id ] -> return id
      | _ -> return Int64.minus_one
    end >>= fun id ->
    begin match kind with
      | Balances ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_balances \
           WHERE id = $id"]
      | Frozen_balances ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_frozen_balances \
           WHERE id = $id"]
      | Frozen_deposits ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_frozen_deposits \
           WHERE id = $id"]
      | Frozen_rewards ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_frozen_rewards \
           WHERE id = $id"]
      | Paid_bytes ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_paid_bytes \
           WHERE id = $id"]
      | Staking_balances ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_staking_balances \
           WHERE id = $id"]
      | Total_balances ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_total_balances \
           WHERE id = $id"]
      | Total_delegated ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_total_delegated \
           WHERE id = $id"]
      | Total_delegators ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_total_delegators \
           WHERE id = $id"]
      | Frozen_fees ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_total_frozen_fees \
           WHERE id = $id"]
      | Used_bytes ->
        [%pgsql dbh
          "SELECT COUNT(hash) FROM top_used_bytes \
           WHERE id = $id"]
    end >>= of_count_opt

  let tops ?(page=0) ?(page_size=20) ?(kind=Balances) () =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT id FROM day_context \
       WHERE day = (SELECT MAX(day) FROM day_context)"] >>= begin function
      | [ id ] -> return id
      | _ -> return Int64.minus_one
    end >>= fun id ->
    [%pgsql dbh
      "SELECT hash, period, period_kind FROM context_totals \
       WHERE id = $id"] >>= begin function
      | [ Some top_hash, Some top_period, Some top_kind ] ->
        return (top_hash, top_period, top_kind)
      | _ -> return ("", "", "")
    end >>= fun (top_hash, top_period, top_kind) ->
    begin match kind with
      | Balances ->
        [%pgsql dbh
          "SELECT hash, balance FROM top_balances \
           WHERE id = $id ORDER BY balance DESC, hash OFFSET $offset LIMIT $limit"]
      | Frozen_balances ->
        [%pgsql dbh
          "SELECT hash, frozen_balance FROM top_frozen_balances \
           WHERE id = $id ORDER BY frozen_balance DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Frozen_deposits ->
        [%pgsql dbh
          "SELECT hash, frozen_deposits FROM top_frozen_deposits \
           WHERE id = $id ORDER BY frozen_deposits DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Frozen_rewards ->
        [%pgsql dbh
          "SELECT hash, frozen_rewards FROM top_frozen_rewards \
           WHERE id = $id ORDER BY frozen_rewards DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Paid_bytes ->
        [%pgsql dbh
          "SELECT hash, paid_bytes FROM top_paid_bytes \
           WHERE id = $id ORDER BY paid_bytes DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Staking_balances ->
        [%pgsql dbh
          "SELECT hash, staking_balance FROM top_staking_balances \
           WHERE id = $id ORDER BY staking_balance DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Total_balances ->
        [%pgsql dbh
          "SELECT hash, total_balance FROM top_total_balances \
           WHERE id = $id ORDER BY total_balance DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Total_delegated ->
        [%pgsql dbh
          "SELECT hash, total_delegated FROM top_total_delegated \
           WHERE id = $id ORDER BY total_delegated DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Total_delegators ->
        [%pgsql dbh
          "SELECT hash, total_delegators FROM top_total_delegators \
           WHERE id = $id ORDER BY total_delegators DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Frozen_fees ->
        [%pgsql dbh
          "SELECT hash, total_frozen_fees FROM top_total_frozen_fees \
           WHERE id = $id ORDER BY total_frozen_fees DESC, hash \
           OFFSET $offset LIMIT $limit"]
      | Used_bytes ->
        [%pgsql dbh
          "SELECT hash, used_bytes FROM top_used_bytes \
           WHERE id = $id ORDER BY used_bytes DESC, hash \
           OFFSET $offset LIMIT $limit"]
    end >>= fun top_list ->
    let top_list = List.map (fun (dn, value) ->
        Alias.to_name dn, value
      ) top_list in
    return {
      top_hash ;
      top_period ;
      top_kind ;
      top_list
    }

  let account_bonds_rewards hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun current_cycle ->
    (* assume rewards and deposits are not change during a cycle *)
    let cst = Infos.last_constants () in
    let preserved_cycles = Int64.of_int cst.Dune_types.preserved_cycles in
    let cycle_limit =
      if current_cycle < preserved_cycles then 0L
      else Int64.sub current_cycle preserved_cycles in
    [%pgsql dbh
      "SELECT SUM(block_rewards)::bigint, \
       SUM(block_security_deposit * array_sum(nb_baking,1))::bigint, \
       SUM(fees)::bigint \
       FROM cycle_count_baker AS ccb \
       INNER JOIN cycle_limits AS cl ON cl.cycle = ccb.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE ccb.cycle >= $cycle_limit AND dn = $hash"]
    >>= fun block_res ->
    [%pgsql dbh
      "SELECT SUM(end_rewards_array(endorsement_reward, nb_endorsement))::bigint, \
       SUM(endorsement_security_deposit * array_sum(nb_endorsement,1))::bigint \
       FROM cycle_count_baker AS ccb \
       INNER JOIN cycle_limits AS cl ON cl.cycle = ccb.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE dn = $hash AND ccb.cycle >= $cycle_limit"]
    >>= fun endorsement_res ->
    let acc_b_rewards, acc_b_deposits, acc_fees = match block_res with
      | [ Some acc_b_rewards, Some acc_b_deposits, Some acc_fees ] ->
        acc_b_rewards, acc_b_deposits, acc_fees
      | _ -> 0L, 0L, 0L in
    let acc_e_rewards, acc_e_deposits = match endorsement_res with
       | [ Some acc_e_rewards, Some acc_e_deposits ] ->
        acc_e_rewards, acc_e_deposits
       | _ -> 0L, 0L in
    return {acc_b_rewards; acc_b_deposits; acc_fees; acc_e_rewards; acc_e_deposits}

  let extra_bonds_rewards hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun current_cycle ->
    let cst = Infos.last_constants () in
    let preserved_cycles = Int64.of_int cst.Dune_types.preserved_cycles in
    let cycle_limit =
      if current_cycle < preserved_cycles then 0L
      else Int64.sub current_cycle preserved_cycles in
    [%pgsql dbh
      "SELECT \
       SUM(CASE WHEN dbe.denouncer = $hash THEN dbe.gain_rewards ELSE 0 END)::bigint, \
       - SUM(CASE WHEN dbe.accused = $hash THEN dbe.lost_deposit ELSE 0 END)::bigint, \
       - SUM(CASE WHEN dbe.accused = $hash THEN dbe.lost_rewards ELSE 0 END)::bigint, \
       - SUM(CASE WHEN dbe.accused = $hash THEN dbe.lost_fees ELSE 0 END)::bigint \
       FROM double_baking_evidence_all AS dbe \
       WHERE (dbe.denouncer = $hash OR dbe.accused = $hash) \
       AND dbe.op_cycle >= $cycle_limit AND dbe.distance_level = 0"]
    >>= fun dbe ->
    [%pgsql dbh
      "SELECT \
       SUM(CASE WHEN dee.denouncer = $hash THEN dee.gain_rewards ELSE 0 END)::bigint, \
       - SUM(CASE WHEN dee.accused = $hash THEN dee.lost_deposit ELSE 0 END)::bigint, \
       - SUM(CASE WHEN dee.accused = $hash THEN dee.lost_rewards ELSE 0 END)::bigint, \
       - SUM(CASE WHEN dee.accused = $hash THEN dee.lost_fees ELSE 0 END)::bigint \
       FROM double_endorsement_evidence_all AS dee \
       WHERE (dee.denouncer = $hash OR dee.accused = $hash) \
       AND dee.op_cycle >= $cycle_limit AND dee.distance_level = 0"]
    >>= fun dee ->
    [%pgsql dbh
      "SELECT SUM(seed_nonce_revelation_tip)::bigint FROM seed_nonce_revelation_all AS sn \
       INNER JOIN cycle_limits AS cl ON cl.cycle = $cycle_limit \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(sn.op_level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= sn.op_level AND cst.level_end IS NULL) \
       WHERE baker = $hash AND sn.distance_level = 0 AND \
       sn.op_level >= cl.level_start"]
    >>= of_db_opt >>= fun acc_rv_rewards ->
    [%pgsql dbh
      "SELECT \
       SUM(CASE WHEN s.hash IS NULL AND bl.cycle < $current_cycle \
       THEN block_reward_f(block_reward, priority, endorsements_included, \
       endorsers_per_block, initial_endorsers IS NOT NULL) \
       ELSE 0::bigint END)::bigint, \
       SUM(CASE WHEN s.hash IS NULL AND bl.cycle < $current_cycle \
       THEN bl.fees ELSE 0 END)::bigint \
       FROM block AS bl \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(bl.level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= bl.level AND cst.level_end IS NULL) \
       LEFT JOIN (SELECT * FROM seed_nonce_revelation_all WHERE distance_level = 0) AS s ON s.level = bl.level \
       WHERE bl.baker = $hash AND bl.level % blocks_per_commitment = 0 \
       AND bl.cycle >= $cycle_limit AND bl.distance_level = 0"]
    >>= fun lost_revelation ->
    let acc_dnb_gain, acc_dnb_deposit, acc_dnb_rewards, acc_dnb_fees =
      match dbe with
      | [ acc_dnb_gain, acc_dnb_deposit, acc_dnb_rewards, acc_dnb_fees ] ->
        Option.value ~default:0L acc_dnb_gain, Option.value ~default:0L acc_dnb_deposit,
        Option.value ~default:0L acc_dnb_rewards, Option.value ~default:0L acc_dnb_fees
      | _ -> 0L, 0L, 0L, 0L in
    let acc_dne_gain, acc_dne_deposit, acc_dne_rewards, acc_dne_fees =
      match dee with
      | [ acc_dne_gain, acc_dne_deposit, acc_dne_rewards, acc_dne_fees ] ->
        Option.value ~default:0L acc_dne_gain, Option.value ~default:0L acc_dne_deposit,
        Option.value ~default:0L acc_dne_rewards, Option.value ~default:0L acc_dne_fees
      | _ -> 0L, 0L, 0L, 0L in
    let acc_rv_lost_rewards, acc_rv_lost_fees =
      match lost_revelation with
      | [ acc_rv_lost_rewards, acc_rv_lost_fees ] ->
        Option.value ~default:0L acc_rv_lost_rewards,
        Option.value ~default:0L acc_rv_lost_fees
      | _ -> 0L, 0L in
    return { acc_dnb_gain; acc_dnb_deposit; acc_dnb_rewards; acc_dnb_fees;
             acc_dne_gain; acc_dne_deposit; acc_dne_rewards; acc_dne_fees;
             acc_rv_rewards; acc_rv_lost_rewards; acc_rv_lost_fees }

  let max_roll_cycle () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT MAX(cycle) FROM snapshot_rolls WHERE ready"] >>= of_count_opt

  let rolls_distribution cycle =
    let cycle = Int64.of_int cycle in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT dn1, count FROM snapshot_owner AS so \
       INNER JOIN snapshot_rolls AS sr ON sr.id = so.id \
       WHERE sr.cycle = $cycle AND ready ORDER BY count DESC, dn1"] >>= fun rows ->
    return @@
    List.map (fun (dn, rolls) -> Alias.to_name dn, Int32.to_int rolls) rows

  let roll_number account_hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT count FROM snapshot_owner AS so \
       INNER JOIN \
       (SELECT id FROM snapshot_rolls AS sr WHERE ready ORDER BY sr.cycle DESC LIMIT 1) \
       AS max_id ON max_id.id = so.id \
       WHERE dn1 = $account_hash"] >>= function
    | [ count ] -> return @@ Int32.to_int count
    | _ -> return 0

  let rolls_history ?(page=0) ?(page_size=20) account_hash =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "nullable-results"
      "SELECT sr.cycle, COALESCE(so.count, 0), COALESCE(sr.rolls_count, 0) FROM snapshot_rolls AS sr \
       LEFT JOIN snapshot_owner AS so ON sr.id = so.id \
       WHERE so.dn1 = $account_hash AND ready \
       ORDER BY sr.cycle DESC OFFSET $offset LIMIT $limit"] >>= fun list ->
    return @@
    List.map (fun (cycle, rolls, rolls_total) ->
        (match cycle with None -> 0L | Some c -> c),
        (match rolls with None -> 0l | Some r -> r),
        (match rolls_total with None -> 0l | Some r -> r))
      list

  let all_deleguees_count_by_cycle_count () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(DISTINCT id) FROM snapshot_deleguees"] >>= of_count_opt

  let all_deleguees_count_by_cycle ?(page=0) ?(page_size=20) () =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT sr.cycle, COUNT(deleguee) FROM snapshot_deleguees AS sd \
       INNER JOIN snapshot_rolls AS sr ON sd.id = sr.id WHERE ready
       GROUP BY sr.cycle ORDER BY sr.cycle DESC OFFSET $offset LIMIT $limit"]
    >>= fun list ->
    return @@
    List.rev @@
    List.fold_left (fun acc (cycle, count_opt) ->
        match count_opt with
        | None -> acc
        | Some c -> (cycle, c) :: acc) [] list

  let deleguees_count_by_cycle_count account_hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(DISTINCT id) FROM snapshot_deleguees AS sd \
       WHERE sd.dn1 = $account_hash"] >>= of_count_opt

  let deleguees_count_by_cycle ?(page=0) ?(page_size=20) account_hash =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT sr.cycle, COUNT(deleguee) FROM snapshot_deleguees AS sd \
       INNER JOIN snapshot_rolls AS sr ON sd.id = sr.id \
       WHERE sd.dn1 = $account_hash AND ready \
       GROUP BY sr.cycle ORDER BY sr.cycle DESC OFFSET $offset LIMIT $limit"]
    >>= fun list ->
    return @@
    List.rev @@
    List.fold_left (fun acc (cycle, count_opt) ->
        match count_opt with
        | None -> acc
        | Some c -> (cycle, c) :: acc) [] list

  let deleguees_count account_hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(deleguee) FROM snapshot_deleguees AS sd \
       INNER JOIN \
       (SELECT id FROM snapshot_rolls AS sr WHERE ready ORDER BY sr.cycle DESC LIMIT 1) \
       AS max_id ON max_id.id = sd.id \
       WHERE dn1 = $account_hash"] >>= of_count_opt

  let deleguees ?(page=0) ?(page_size=20) account_hash =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT deleguee FROM snapshot_deleguees AS sd \
       INNER JOIN \
       (SELECT id FROM snapshot_rolls AS sr WHERE ready ORDER BY sr.cycle DESC LIMIT 1) \
       AS max_id ON max_id.id = sd.id \
       WHERE dn1 = $account_hash OFFSET $offset LIMIT $limit"]

  let nb_delegators ?cycle hash =
    with_dbh >>> fun dbh ->
    (match cycle with
     | None -> current_cycle dbh
     | Some cycle -> return (Int64.of_int cycle)) >>= fun cycle ->
    [%pgsql dbh
      "SELECT count(*) \
       FROM snapshot_deleguees AS sd \
       INNER JOIN snapshot_rolls AS sr ON sd.id = sr.id
       WHERE dn1 = $hash AND sr.cycle = $cycle AND ready"] >>= of_count_opt

  let delegate_rewards_split_cycles ?(page=0) ?(page_size=20) hash =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    (* assume endorsed_level = endorsing_level - 1 for rights *)
    (* assume rewards and deposits are not changed during a cycle *)
    current_cycle dbh >>= fun current_cycle ->
    [%pgsql dbh
      "nullable-results"
      "WITH baking(cycle, fees, bk_rew, end_rew) AS \
       (SELECT cycle, fees, block_rewards, endorsement_rewards
       FROM cycle_count_baker WHERE dn = $hash), \
       delegators(id, dn1, count) AS \
       (SELECT sd.id, sd.dn1, count(*) \
       FROM snapshot_deleguees AS sd WHERE dn1 = $hash GROUP BY id, dn1), \
       bk_rights(cycle, nbak) AS \
       (SELECT cycle, COUNT(level) \
       FROM level_rights WHERE bakers_priority[ARRAY_POSITION(bakers, $hash)] = 0 \
       AND level > $head_lvl AND ready \
       GROUP BY cycle ORDER BY cycle ASC), \
       endo_rights(cycle, reward) AS \
       (SELECT cl.cycle, SUM(endorsement_reward * \
       slots[ARRAY_POSITION(endorsers, $hash)]) \
       FROM level_rights \
       INNER JOIN cycle_limits AS cl ON \
       level + 1 BETWEEN cl.level_start AND cl.level_end
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(level + 1 BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= level + 1 AND cst.level_end IS NULL) \
       WHERE $hash = ANY(endorsers) AND level > $head_lvl AND ready \
       GROUP BY cl.cycle ORDER BY cl.cycle ASC), \
       baking_gain_rewards AS \
       (SELECT dbe.op_cycle as cycle, \
       SUM(CASE WHEN dbe.denouncer = $hash THEN dbe.gain_rewards ELSE 0 END) AS gain, \
       SUM(CASE WHEN dbe.accused = $hash THEN - dbe.lost_deposit ELSE 0 END) AS lost_deposits, \
       SUM(CASE WHEN dbe.accused = $hash THEN - dbe.lost_rewards ELSE 0 END) AS lost_rewards, \
       SUM(CASE WHEN dbe.accused = $hash THEN - dbe.lost_fees ELSE 0 END) AS lost_fees \
       FROM double_baking_evidence_all as dbe \
       WHERE (dbe.denouncer = $hash OR dbe.accused = $hash) \
       AND dbe.distance_level = 0 GROUP BY dbe.op_cycle), \
       end_gain_rewards AS \
       (SELECT dee.op_cycle as cycle, \
       SUM(CASE WHEN dee.denouncer = $hash THEN dee.gain_rewards ELSE 0 END) AS gain, \
       SUM(CASE WHEN dee.accused = $hash THEN - dee.lost_deposit ELSE 0 END) AS lost_deposits, \
       SUM(CASE WHEN dee.accused = $hash THEN - dee.lost_rewards ELSE 0 END) AS lost_rewards, \
       SUM(CASE WHEN dee.accused = $hash THEN - dee.lost_fees ELSE 0 END) AS lost_fees \
       FROM double_endorsement_evidence_all as dee \
       WHERE (dee.denouncer = $hash OR dee.accused = $hash) \
       AND dee.distance_level = 0 GROUP BY dee.op_cycle), \
       lost_revelation(cycle, lost_reward, lost_fees) AS ( \
       SELECT bl.cycle, \
       SUM(CASE WHEN s.hash IS NULL AND bl.cycle < $current_cycle \
       THEN block_reward_f(block_reward, priority, endorsements_included, \
       endorsers_per_block, initial_endorsers IS NOT NULL) ELSE 0 END)::bigint, \
       SUM(CASE WHEN s.hash IS NULL AND bl.cycle < $current_cycle \
       THEN bl.fees ELSE 0 END)::bigint \
       FROM block AS bl \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(bl.level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= bl.level AND cst.level_end IS NULL) \
       LEFT JOIN (SELECT * FROM seed_nonce_revelation_all AS sn WHERE sn.distance_level = 0) AS s ON s.level = bl.level \
       WHERE bl.baker = $hash AND bl.level % blocks_per_commitment = 0 \
       AND bl.distance_level = 0 \
       GROUP BY bl.cycle), \
       revelation_rewards(cycle, reward) AS ( \
       SELECT cl.cycle, \
       SUM(seed_nonce_revelation_tip)::bigint FROM seed_nonce_revelation_all AS sn \
       INNER JOIN cycle_limits AS cl ON sn.op_level BETWEEN cl.level_start AND cl.level_end \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(sn.op_level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= sn.op_level AND cst.level_end IS NULL) \
       WHERE baker = $hash AND sn.distance_level = 0 \
       GROUP BY cl.cycle) \
       SELECT sr.cycle, so.staking_balance, so.delegated_balance, \
       d.count, bak.bk_rew, bak.fees, bak.end_rew, \
       bkr.nbak * block_reward, endr.reward::bigint, \
       denb.gain::bigint, denb.lost_deposits::bigint, denb.lost_rewards::bigint, \
       denb.lost_fees::bigint, dene.gain::bigint, dene.lost_deposits::bigint, \
       dene.lost_rewards::bigint, dene.lost_fees::bigint, \
       rv.reward, lsn.lost_reward, lsn.lost_fees \
       FROM snapshot_owner AS so \
       INNER JOIN snapshot_rolls AS sr ON sr.id = so.id \
       INNER JOIN cycle_limits AS cl ON cl.cycle = sr.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       LEFT JOIN delegators AS d ON d.id = sr.id \
       LEFT JOIN baking AS bak ON bak.cycle = sr.cycle \
       LEFT JOIN baking_gain_rewards AS denb ON denb.cycle = sr.cycle \
       LEFT JOIN end_gain_rewards AS dene ON dene.cycle = sr.cycle \
       LEFT JOIN bk_rights AS bkr ON bkr.cycle = sr.cycle \
       LEFT JOIN endo_rights AS endr ON endr.cycle = sr.cycle \
       LEFT JOIN lost_revelation AS lsn ON lsn.cycle = sr.cycle \
       LEFT JOIN revelation_rewards AS rv ON rv.cycle = sr.cycle \
       WHERE so.dn1 = $hash AND ready ORDER BY sr.cycle DESC \
       OFFSET $offset LIMIT $limit"] >>= fun row ->
    return @@ List.rev @@ List.fold_left
      (fun acc ars -> match ars with
           (Some ars_cycle, Some staking_balance, Some delegated_balance,
            nb_del, brewards, ars_fees, erewards,
            ars_baking_rights, ars_endorsing_rights, b_gain,
            b_lost_deposit, b_lost_rewards, b_lost_fees, e_gain,
            e_lost_deposit, e_lost_rewards, e_lost_fees,
            rv_reward, lost_rv_reward, lost_rv_fees) ->
           let ars_cycle = Int64.to_int ars_cycle in
           let ars_delegate_staking_balance = staking_balance in
           let ars_delegate_delegated_balance = delegated_balance in
           let ars_delegators_nb = Option.fold ~none:0 ~some:Int64.to_int nb_del in
           let ars_block_rewards = Option.value ~default:0L brewards in
           let ars_fees = Option.value ~default:0L ars_fees in
           let ars_gain_from_denounciation_b = Option.value ~default:0L b_gain in
           let ars_lost_deposit_b = Option.value ~default:0L b_lost_deposit in
           let ars_lost_rewards_b = Option.value ~default:0L b_lost_rewards in
           let ars_lost_fees_b = Option.value ~default:0L b_lost_fees in
           let ars_gain_from_denounciation_e = Option.value ~default:0L e_gain in
           let ars_lost_deposit_e = Option.value ~default:0L e_lost_deposit in
           let ars_lost_rewards_e = Option.value ~default:0L e_lost_rewards in
           let ars_lost_fees_e = Option.value ~default:0L e_lost_fees in
           let ars_endorsement_rewards = Option.value ~default:0L erewards in
           let ars_baking_rights_rewards = Option.value ~default:0L ars_baking_rights in
           let ars_endorsing_rights_rewards = Option.value ~default:0L ars_endorsing_rights in
           let ars_rv_rewards = Option.value ~default:0L rv_reward in
           let ars_rv_lost_rewards = Option.value ~default:0L lost_rv_reward in
           let ars_rv_lost_fees = Option.value ~default:0L lost_rv_fees in
           let current_cycle = Int64.to_int current_cycle in
           let cst = Infos.last_constants () in
           let unfrozen_cycle_offset = current_cycle - cst.Dune_types.preserved_cycles in
           let ars_status = match ars_cycle - current_cycle with
             | 0 -> Cycle_in_progress
             | diff when diff > 0 -> Cycle_pending
             | diff
               when diff < 0 && ars_cycle >= unfrozen_cycle_offset ->
               Rewards_pending
             | diff
               when diff < 0 && ars_cycle < unfrozen_cycle_offset ->
               Rewards_delivered
             | _ -> assert false (* Cannot happen *) in
           { ars_cycle ;
             ars_delegate_staking_balance ;
             ars_delegators_nb ;
             ars_delegate_delegated_balance ;
             ars_block_rewards ; ars_fees ; ars_endorsement_rewards ;
             ars_baking_rights_rewards ; ars_endorsing_rights_rewards ;
             ars_status ;
             ars_gain_from_denounciation_b ;
             ars_lost_deposit_b ; ars_lost_rewards_b ; ars_lost_fees_b ;
             ars_gain_from_denounciation_e ;
             ars_lost_deposit_e ; ars_lost_rewards_e ; ars_lost_fees_e ;
             ars_rv_rewards; ars_rv_lost_rewards; ars_rv_lost_fees
           } :: acc
         | _ -> acc) [] row

  let delegate_rewards_split ?(page=0) ?(page_size=20) ?cycle hash =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun current_cycle ->
    let cycle = match cycle with
      | None -> current_cycle
      | Some cycle -> Int64.of_int cycle in
    [%pgsql dbh
      "SELECT block_reward, endorsement_reward, seed_nonce_revelation_tip, \
       blocks_per_commitment, endorsers_per_block, initial_endorsers \
       FROM cycle_limits AS cl \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE cl.cycle = $cycle"]
    >>= (function
        | (br, er, rr, bpc, epb, ie) :: _ ->
          return (br, er, rr, Int64.of_int32 bpc, epb, ie <> None)
        | _ -> return (0L,0L,0L,1L,0l,true))
    >>= fun (b_reward, e_reward, r_reward, blocks_per_commitment,
             endorsers_per_block, emmyp) ->
    head_level dbh >>= fun head_lvl ->
    [%pgsql dbh
      "SELECT block_rewards, fees \
       FROM cycle_count_baker \
       WHERE cycle = $cycle AND dn = $hash"]
    >>= fun block_res ->
    [%pgsql dbh
      "SELECT endorsement_rewards \
       FROM cycle_count_baker \
       WHERE dn = $hash AND cycle = $cycle"]
    >>= of_db >>= fun rs_endorsement_rewards ->
    [%pgsql dbh
      "SELECT SUM($b_reward::bigint)::bigint \
       FROM level_rights WHERE bakers_priority[ARRAY_POSITION(bakers, $hash)] = 0 \
       AND cycle = $cycle AND level > $head_lvl AND ready"]
    >>= of_db_opt >>= fun rs_baking_rights_rewards ->
    [%pgsql dbh
      "SELECT SUM($e_reward * slots[ARRAY_POSITION(endorsers, $hash)]::bigint)::bigint \
       FROM level_rights \
       INNER JOIN cycle_limits AS cl ON level + 1 BETWEEN cl.level_start AND cl.level_end \
       WHERE $hash = ANY(endorsers) AND cl.cycle = $cycle \
       AND level > $head_lvl AND ready"]
    >>= of_db_opt >>= fun rs_endorsing_rights_rewards ->
    [%pgsql dbh
      "SELECT COUNT(deleguee) FROM snapshot_deleguees AS sd \
       WHERE sd.dn1 = $hash AND \
       sd.id = (SELECT id FROM snapshot_rolls WHERE cycle = $cycle AND ready)"]
    >>= of_count_opt >>= fun rs_delegators_nb ->
    [%pgsql dbh
      "SELECT deleguee, balance FROM snapshot_deleguees AS sd \
       WHERE sd.dn1 = $hash \
       AND sd.id = (SELECT id FROM snapshot_rolls WHERE cycle = $cycle AND ready) \
       ORDER BY balance DESC, deleguee OFFSET $offset LIMIT $limit"]
    >>= fun rs_delegators_balance ->
    [%pgsql dbh
      "SELECT staking_balance FROM snapshot_owner AS so \
       WHERE so.dn1 = $hash \
       AND so.id = (SELECT id FROM snapshot_rolls WHERE cycle = $cycle AND ready)"]
    >>= of_db >>= fun rs_delegate_staking_balance ->
    [%pgsql dbh
      "SELECT \
       CAST(SUM(CASE WHEN dbe.denouncer = $hash THEN \
       dbe.gain_rewards ELSE 0 END) AS bigint) AS gain, \
       CAST(SUM(CASE WHEN dbe.accused = $hash THEN \
       - dbe.lost_deposit ELSE 0 END) AS bigint) AS lost_deposits, \
       CAST(SUM(CASE WHEN dbe.accused = $hash THEN \
       - dbe.lost_rewards ELSE 0 END) AS bigint) AS lost_rewards, \
       CAST(SUM(CASE WHEN dbe.accused = $hash THEN \
       - dbe.lost_fees ELSE 0 END) AS bigint) AS lost_fees \
       FROM double_baking_evidence_all as dbe \
       WHERE (dbe.denouncer = $hash OR dbe.accused = $hash) \
       AND op_cycle = $cycle AND distance_level = 0"]
    >>= fun dbe_res ->
    [%pgsql dbh
      "SELECT \
       CAST(SUM(CASE WHEN dee.denouncer = $hash THEN \
       dee.gain_rewards ELSE 0 END) AS bigint) AS gain, \
       CAST(SUM(CASE WHEN dee.accused = $hash THEN \
       - dee.lost_deposit ELSE 0 END) AS bigint) AS lost_deposits, \
       CAST(SUM(CASE WHEN dee.accused = $hash THEN \
       - dee.lost_rewards ELSE 0 END) AS bigint) AS lost_rewards, \
       CAST(SUM(CASE WHEN dee.accused = $hash THEN \
       - dee.lost_fees ELSE 0 END) AS bigint) AS lost_fees \
       FROM double_endorsement_evidence_all as dee \
       WHERE (dee.denouncer = $hash OR dee.accused = $hash) \
       AND op_cycle = $cycle AND distance_level = 0"]
    >>= fun dee_res ->
    [%pgsql dbh
      "SELECT \
       SUM(CASE WHEN s.hash IS NULL AND cycle < $current_cycle \
       THEN block_reward_f($b_reward, bl.priority, bl.endorsements_included, \
       $endorsers_per_block, $emmyp) ELSE 0::bigint END)::bigint, \
       SUM(CASE WHEN s.hash IS NULL AND cycle < $current_cycle \
       THEN fees ELSE 0 END)::bigint \
       FROM block AS bl \
       LEFT JOIN (SELECT * FROM seed_nonce_revelation_all WHERE distance_level = 0) AS s ON s.level = bl.level \
       WHERE bl.baker = $hash AND bl.level % $blocks_per_commitment = 0 \
       AND cycle = $cycle AND bl.distance_level = 0"]
    >>= fun lost_rv ->
    [%pgsql dbh
      "SELECT COUNT(sn.hash) FROM seed_nonce_revelation_all AS sn \
       INNER JOIN cycle_limits AS cl ON cl.cycle = $cycle \
       WHERE baker = $hash AND \
       sn.op_level BETWEEN cl.level_start AND cl.level_end \
       AND distance_level = 0"]
    >>= of_db_opt >>= fun n_nonce_revelation ->
    let rs_block_rewards, rs_fees = match block_res with
      | [ b_rewards, fees ] -> b_rewards, fees
      | _ -> 0L, 0L in
    let rs_delegators_balance =
      List.map (fun (del, bal) ->
          Alias.to_name del, bal) rs_delegators_balance in
    let ( rs_gain_from_denounciation_b, rs_lost_deposit_b,
          rs_lost_rewards_b, rs_lost_fees_b ) = match dbe_res with
      | [ Some gain, Some ldepo, Some lrew, Some lfees ] -> gain, ldepo, lrew, lfees
      | _ -> 0L, 0L, 0L, 0L in
    let ( rs_gain_from_denounciation_e, rs_lost_deposit_e,
          rs_lost_rewards_e, rs_lost_fees_e ) = match dee_res with
      | [ Some gain, Some ldepo, Some lrew, Some lfees ] -> gain, ldepo, lrew, lfees
      | _ -> 0L, 0L, 0L, 0L in
    let rs_rv_lost_rewards, rs_rv_lost_fees = match lost_rv with
      | [ Some rv_lost_reward, Some rv_lost_fees ] ->
        rv_lost_reward, rv_lost_fees
      | _ -> 0L, 0L in
    let rs_rv_rewards = Int64.mul n_nonce_revelation r_reward in
    return
      { rs_delegate_staking_balance ; rs_delegators_nb ; rs_delegators_balance ;
        rs_block_rewards ; rs_fees ; rs_endorsement_rewards ;
        rs_baking_rights_rewards ; rs_endorsing_rights_rewards ;
        rs_gain_from_denounciation_b ; rs_lost_deposit_b ; rs_lost_rewards_b; rs_lost_fees_b;
        rs_gain_from_denounciation_e ; rs_lost_deposit_e ; rs_lost_rewards_e; rs_lost_fees_e;
        rs_rv_rewards; rs_rv_lost_rewards; rs_rv_lost_fees
      }

  let delegate_rewards_split_fast ?(page=0) ?(page_size=20) ?cycle hash =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    (match cycle with
     | None -> current_cycle dbh
     | Some cycle -> return (Int64.of_int cycle)) >>= fun cycle ->
    [%pgsql dbh
      "SELECT deleguee, balance FROM snapshot_deleguees AS sd \
       WHERE sd.dn1 = $hash \
       AND sd.id = (SELECT id FROM snapshot_rolls WHERE cycle = $cycle AND ready) \
       ORDER BY balance DESC, deleguee OFFSET $offset LIMIT $limit"]
    >>= fun rs_delegators_balance ->
    return  @@ List.map (fun (del, bal) ->
        Alias.to_name del, bal) rs_delegators_balance

  let nb_cycle_delegator_rewards hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(cycle) FROM snapshot_rolls AS sr \
       INNER JOIN snapshot_deleguees AS sd ON sd.id = sr.id \
       WHERE sd.deleguee = $hash AND ready"] >>= of_count_opt

  let delegator_rewards_with_details ?(page=0) ?(page_size=20) hash =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    head_level dbh >>= fun head_lvl ->
    current_cycle dbh >>= fun current_cycle ->
    [%pgsql dbh
      "nullable-results"
      "WITH delegate AS \
       (SELECT sr.cycle, dn1, sd.id, balance \
       FROM snapshot_deleguees AS sd \
       INNER JOIN snapshot_rolls AS sr ON sr.id = sd.id \
       WHERE deleguee = $hash AND ready), \
       baking(cycle, fees, bk_rew, end_rew) AS \
       (SELECT ccb.cycle, fees, block_rewards, endorsement_rewards \
       FROM cycle_count_baker AS ccb \
       INNER JOIN delegate AS dl ON dl.dn1 = ccb.dn AND dl.cycle = ccb.cycle), \
       bk_rights(cycle, nbak) AS \
       (SELECT lr.cycle, COUNT(level) \
       FROM level_rights AS lr
       INNER JOIN delegate AS dl ON dl.cycle = lr.cycle AND \
       dl.dn1 = ANY (bakers) \
       WHERE bakers_priority[ARRAY_POSITION(bakers::varchar[], dl.dn1::varchar)] = 0 \
       AND level > $head_lvl AND ready \
       GROUP BY lr.cycle ORDER BY lr.cycle ASC), \
       endo_rights(cycle, rewards) AS \
       (SELECT cl.cycle, \
       SUM(endorsement_reward * slots[ARRAY_POSITION(endorsers::varchar[], dl.dn1::varchar)]) \
       FROM level_rights AS lr \
       INNER JOIN cycle_limits AS cl ON \
       level + 1 BETWEEN cl.level_start AND cl.level_end
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       INNER JOIN delegate AS dl ON dl.cycle = cl.cycle \
       AND dl.dn1 = ANY (endorsers) AND ready \
       WHERE level > $head_lvl
       GROUP BY cl.cycle ORDER BY cl.cycle ASC), \
       dbe_rewards(cycle, gain, lost_deposits, lost_rewards, lost_fees) AS \
       (SELECT dbe.op_cycle, \
       SUM(CASE WHEN dbe.denouncer = dl.dn1 THEN dbe.gain_rewards ELSE 0 END)::bigint, \
       SUM(CASE WHEN dbe.accused = dl.dn1 THEN - dbe.lost_deposit ELSE 0 END)::bigint, \
       SUM(CASE WHEN dbe.accused = dl.dn1 THEN - dbe.lost_rewards ELSE 0 END)::bigint, \
       SUM(CASE WHEN dbe.accused = dl.dn1 THEN - dbe.lost_fees ELSE 0 END)::bigint \
       FROM double_baking_evidence_all as dbe \
       INNER JOIN delegate AS dl ON dl.cycle = dbe.op_cycle AND \
       (dl.dn1 = dbe.denouncer OR dl.dn1 = dbe.accused) \
       AND dbe.distance_level = 0 \
       GROUP BY dbe.op_cycle), \
       dee_rewards(cycle, gain, lost_deposits, lost_rewards, lost_fees) AS \
       (SELECT dee.op_cycle, \
       SUM(CASE WHEN dee.denouncer = dl.dn1 THEN dee.gain_rewards ELSE 0 END)::bigint, \
       SUM(CASE WHEN dee.accused = dl.dn1 THEN - dee.lost_deposit ELSE 0 END)::bigint, \
       SUM(CASE WHEN dee.accused = dl.dn1 THEN - dee.lost_rewards ELSE 0 END)::bigint, \
       SUM(CASE WHEN dee.accused = dl.dn1 THEN - dee.lost_fees ELSE 0 END)::bigint \
       FROM double_endorsement_evidence_all as dee \
       INNER JOIN delegate AS dl ON dl.cycle = dee.op_cycle AND \
       (dl.dn1 = dee.denouncer OR dl.dn1 = dee.accused) \
       AND dee.distance_level = 0 \
       GROUP BY dee.op_cycle), \
       lost_revelation_rewards(cycle, lost_reward, lost_fees) AS ( \
       SELECT bl.cycle, \
       SUM(CASE WHEN s.hash IS NULL AND bl.cycle < $current_cycle \
       THEN block_reward_f(block_reward, priority, endorsements_included, \
       endorsers_per_block, initial_endorsers IS NOT NULL) ELSE 0::bigint END)::bigint, \
       SUM(CASE WHEN s.hash IS NULL AND bl.cycle < $current_cycle \
       THEN bl.fees ELSE 0 END)::bigint \
       FROM block AS bl \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(bl.level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= bl.level AND cst.level_end IS NULL) \
       INNER JOIN delegate AS dl ON dl.cycle = bl.cycle \
       LEFT JOIN (SELECT * FROM seed_nonce_revelation_all WHERE distance_level = 0) AS s ON s.level = bl.level \
       WHERE bl.baker = dl.dn1 AND bl.level % blocks_per_commitment = 0 \
       AND bl.distance_level = 0 \
       GROUP BY bl.cycle),  \
       revelation_rewards(cycle, reward) AS ( \
       SELECT cl.cycle, \
       SUM(seed_nonce_revelation_tip)::bigint FROM seed_nonce_revelation_all AS sn \
       INNER JOIN cycle_limits AS cl ON sn.op_level BETWEEN cl.level_start AND cl.level_end \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(sn.op_level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= sn.op_level AND cst.level_end IS NULL) \
       INNER JOIN delegate AS dl ON dl.cycle = cl.cycle  \
       WHERE sn.baker = dl.dn1 AND sn.distance_level = 0 \
       GROUP BY cl.cycle) \
       SELECT dl.cycle, dl.dn1, dl.balance, so.staking_balance, \
       bak.bk_rew, bak.fees, bak.end_rew, \
       bkr.nbak * block_reward, endr.rewards::bigint, \
       dbe.gain, dbe.lost_deposits, dbe.lost_rewards, \
       dbe.lost_fees, dee.gain, dee.lost_deposits, dee.lost_rewards, \
       dee.lost_fees, rv.reward, lsn.lost_reward, lsn.lost_fees \
       FROM delegate AS dl \
       INNER JOIN snapshot_owner AS so ON (so.id = dl.id AND so.dn1 = dl.dn1) \
       INNER JOIN cycle_limits AS cl ON cl.cycle = dl.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       LEFT JOIN baking AS bak ON bak.cycle = dl.cycle \
       LEFT JOIN bk_rights AS bkr ON bkr.cycle = dl.cycle \
       LEFT JOIN endo_rights AS endr ON endr.cycle = dl.cycle \
       LEFT JOIN dbe_rewards AS dbe ON dbe.cycle = dl.cycle \
       LEFT JOIN dee_rewards AS dee ON dee.cycle = dl.cycle \
       LEFT JOIN lost_revelation_rewards AS lsn ON lsn.cycle = dl.cycle \
       LEFT JOIN revelation_rewards AS rv ON rv.cycle = dl.cycle \
       ORDER BY dl.cycle DESC \
       OFFSET $offset LIMIT $limit"]
    >>= fun rows ->
    return @@ List.rev @@ List.fold_left
      (fun acc dor -> match dor with
         | (Some dor_cycle, Some dor_delegate, Some dor_balance,
            Some dor_staking_balance, brewards, dor_fees, erewards, brewards_rights,
            erewards_rights,
            dor_dnb_gain, dor_dnb_lost_deposit, dor_dnb_lost_rewards, dor_dnb_lost_fees,
            dor_dne_gain, dor_dne_lost_deposit, dor_dne_lost_rewards, dor_dne_lost_fees,
            dor_rv_rewards, dor_rv_lost_rewards, dor_rv_lost_fees) ->
           let dor_cycle = Int64.to_int dor_cycle in
           let dor_delegate = Alias.to_name dor_delegate in
           let (brewards, brewards_rights, erewards, erewards_rights, dor_fees,
                dor_dnb_gain, dor_dnb_lost_deposit, dor_dnb_lost_rewards, dor_dnb_lost_fees,
                dor_dne_gain, dor_dne_lost_deposit, dor_dne_lost_rewards, dor_dne_lost_fees,
                dor_rv_rewards, dor_rv_lost_rewards, dor_rv_lost_fees) =
             (Option.value ~default:0L brewards, Option.value ~default:0L brewards_rights,
              Option.value ~default:0L erewards, Option.value ~default:0L erewards_rights,
              Option.value ~default:0L dor_fees, Option.value ~default:0L dor_dnb_gain,
              Option.value ~default:0L dor_dnb_lost_deposit, Option.value ~default:0L dor_dnb_lost_rewards,
              Option.value ~default:0L dor_dnb_lost_fees, Option.value ~default:0L dor_dne_gain,
              Option.value ~default:0L dor_dne_lost_deposit, Option.value ~default:0L dor_dne_lost_rewards,
              Option.value ~default:0L dor_dne_lost_fees, Option.value ~default:0L dor_rv_rewards,
              Option.value ~default:0L dor_rv_lost_rewards, Option.value ~default:0L dor_rv_lost_fees) in
           let dor_block_rewards = Int64.add brewards brewards_rights in
           let dor_end_rewards = Int64.add erewards erewards_rights in
           let dor_rewards =
             List.fold_left Int64.add 0L [ dor_block_rewards; dor_end_rewards; dor_fees ] in
           let dor_extra_rewards =
             Int64.(add (add dor_dnb_gain dor_dne_gain) dor_rv_rewards) in
           let dor_losses =
             List.fold_left Int64.add 0L [
               dor_dnb_lost_deposit; dor_dnb_lost_rewards; dor_dnb_lost_fees;
               dor_dne_lost_deposit; dor_dne_lost_rewards; dor_dne_lost_fees;
               dor_rv_lost_rewards; dor_rv_lost_fees ] in
           let current_cycle = Int64.to_int current_cycle in
           let cst = Infos.last_constants () in
           let unfrozen_cycle_offset = current_cycle - cst.Dune_types.preserved_cycles in
           let dor_status = match dor_cycle - current_cycle with
             | 0 -> Cycle_in_progress
             | diff when diff > 0 -> Cycle_pending
             | diff when diff < 0 && dor_cycle >= unfrozen_cycle_offset ->
               Rewards_pending
             | diff when diff < 0 && dor_cycle < unfrozen_cycle_offset ->
               Rewards_delivered
             | _ -> assert false (* Cannot happen *) in
           ({ dor_cycle; dor_delegate; dor_staking_balance; dor_balance;
              dor_rewards; dor_extra_rewards; dor_losses; dor_status },
            { dor_block_rewards; dor_end_rewards; dor_fees; dor_rv_rewards;
              dor_dnb_gain; dor_dne_gain; dor_rv_lost_rewards; dor_rv_lost_fees;
              dor_dnb_lost_deposit; dor_dnb_lost_rewards; dor_dnb_lost_fees;
              dor_dne_lost_deposit; dor_dne_lost_rewards; dor_dne_lost_fees})
           :: acc
         | _ -> acc
      ) [] rows

  let delegator_rewards ?(page=0) ?(page_size=20) hash =
    delegator_rewards_with_details ~page ~page_size hash >>= fun l ->
    return (List.map fst l)

  let search_block ?(limit=20) str =
    let limit = Int64.of_int (min limit search_limit) in
    let str2 = Printf.sprintf "%s%%" str in
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT b.hash FROM block AS b WHERE b.hash LIKE $str2 LIMIT $limit"]

  let search_operation ?(limit=20) str =
    let limit = Int64.of_int (min limit search_limit) in
    let str2 = Printf.sprintf "%s%%" str in
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT o.hash FROM operation AS o WHERE o.hash LIKE $str2 LIMIT $limit"]

  let search_account ?(limit=20) str =
    let limit = Int64.of_int (min limit search_limit) in
     let str2 =
       Printf.sprintf "(tz1|tz2|tz3|kt1|dn1|dn2|dn3){0,1}%s%%" (String.lowercase_ascii str) in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT t.hash, alias, (CASE WHEN lower(t.hash) SIMILAR TO $str2 \
       THEN 'account' ELSE 'alias' END) FROM dune_user AS t \
       INNER JOIN balance_snapshot AS b ON b.hash = t.hash \
       WHERE (lower(t.hash) SIMILAR TO $str2 OR lower(alias) SIMILAR TO $str2) \
       AND spendable_balance > 1000000 LIMIT $limit"]
    >>= fun rows ->
    return @@ List.fold_left (fun acc row -> match row with
        | (pkh, alias, Some kind) -> ({pkh; alias}, kind) :: acc
        | _ -> acc) [] rows

  let nb_search_block str =
    let str2 = Printf.sprintf "%s%%" str in
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT COUNT(b.hash) FROM block AS b WHERE b.hash LIKE $str2"]
    >>= of_count_opt

  let nb_search_operation str =
    let str2 = Printf.sprintf "%s%%" str in
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT COUNT(o.hash) FROM operation AS o WHERE o.hash LIKE $str2"]
    >>= of_count_opt

  let nb_search_account str =
    let str2 =
      Printf.sprintf "(tz1|tz2|tz3|kt1|dn1|dn2|dn3){0,1}%s%%" (String.lowercase_ascii str) in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM dune_user AS t \
       INNER JOIN balance_snapshot AS b ON b.hash = t.hash \
       WHERE (lower(t.hash) SIMILAR TO $str2 OR lower(alias) SIMILAR TO $str2) \
       AND spendable_balance > 1000000"]
    >>= of_count_opt

  let activated_balances () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT SUM(balance)::bigint FROM activation_all \
                WHERE distance_level = 0"] >>= of_db_opt

  let h_activated_balances hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT balance FROM activation_all \
                WHERE pkh = $hash AND distance_level = 0"] >>= of_db_opt

  let unfrozen_rewards dbh cycle_limit =
    [%pgsql dbh
      "SELECT SUM(block_rewards + endorsement_rewards)::bigint \
       FROM cycle_count AS cc \
       INNER JOIN cycle_limits AS cl ON cl.cycle = cc.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE cc.cycle < $cycle_limit"] >>= function
    | [ Some rewards ] -> return rewards
    | _ -> return 0L

  let h_unfrozen_rewards dbh cycle_limit hash =
    [%pgsql dbh
      "SELECT SUM(block_rewards + endorsement_rewards)::bigint, \
       SUM(fees)::bigint FROM cycle_count_baker AS cc \
       INNER JOIN cycle_limits AS cl ON cl.cycle = cc.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE cc.cycle < $cycle_limit AND dn = $hash"] >>= function
    | [ Some rewards, Some fees ] -> return (rewards, fees)
    | [ Some rewards, _ ] -> return (rewards, 0L)
    | [ _, Some fees ] -> return (0L, fees)
    | _ -> return (0L, 0L)

  let revelations dbh cycle_limit =
    [%pgsql dbh
      "SELECT SUM(nb_nonce * seed_nonce_revelation_tip)::bigint FROM cycle_count AS cc \
       INNER JOIN cycle_limits AS cl ON cl.cycle = cc.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE cc.cycle < $cycle_limit::bigint"]
    >>= begin function
      | [ Some rewards ] -> return rewards
      | _ -> return 0L end
    >>= fun revelations_rewards ->
    [%pgsql dbh
      "SELECT COUNT(b.fees), SUM(b.fees + seed_nonce_revelation_tip)::bigint FROM block AS b \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(b.level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= b.level AND cst.level_end IS NULL) \
       LEFT JOIN (SELECT * FROM seed_nonce_revelation_all WHERE distance_level = 0) AS s ON s.level = b.level \
       WHERE b.level > 0 AND b.level % blocks_per_commitment = 0 \
       AND b.cycle < $cycle_limit AND \
       b.distance_level = 0 AND s.hash IS NULL"]
    >>= function
    | [ Some nb_missed, Some burn ] ->
      return (revelations_rewards, Int64.to_int nb_missed, burn)
    | _ -> return (revelations_rewards, 0, 0L)

  let h_revelations dbh cycle_limit hash =
    [%pgsql dbh
      "SELECT SUM(seed_nonce_revelation_tip)::bigint FROM seed_nonce_revelation_all AS s \
       INNER JOIN cycle_limits AS cl ON s.op_level BETWEEN cl.level_start AND cl.level_end \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(s.op_level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= s.op_level AND cst.level_end IS NULL) \
       WHERE cl.cycle < $cycle_limit AND s.baker = $hash \
       AND s.distance_level = 0"]   >>= begin function
      | [ Some rewards ] -> return rewards
      | _ -> return 0L end >>= fun revelations_rewards ->
    [%pgsql dbh
      "SELECT COUNT(fees), SUM(fees + seed_nonce_revelation_tip)::bigint FROM block AS b \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(b.level BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= b.level AND cst.level_end IS NULL) \
       LEFT JOIN (SELECT * FROM seed_nonce_revelation_all WHERE distance_level = 0) AS s ON s.level = b.level \
       WHERE b.level > 0 AND b.level % blocks_per_commitment = 0 \
       AND b.cycle < $cycle_limit AND s.hash IS NULL AND b.baker = $hash \
       AND b.distance_level = 0"]
    >>= function
    | [ Some nb_missed, Some burn ] ->
      return (revelations_rewards, Int64.to_int nb_missed, burn)
    | _ -> return (revelations_rewards, 0, 0L)

  let burned_ori_dun ?level dbh =
    let level, nolevel = test_opt Int64.of_int level in
    [%pgsql dbh "SELECT SUM(burn_dun)::bigint FROM origination_all WHERE \
                 (NOT failed) AND distance_level = 0 AND ($nolevel OR op_level <= $?level)"]
    >>= of_db_opt

  let ori_dun dbh hash =
    [%pgsql dbh "SELECT SUM(burn_dun)::bigint FROM origination_all \
                WHERE (NOT failed) AND source = $hash AND distance_level = 0"]
    >>= of_db_opt >>= fun burn ->
    [%pgsql dbh "SELECT SUM(balance + fee)::bigint FROM origination_all \
                WHERE (NOT failed) AND source = $hash AND distance_level = 0"]
    >>= of_db_opt >>= fun send ->
    [%pgsql dbh "SELECT SUM(fee)::bigint FROM origination_all \
                WHERE source = $hash AND failed AND distance_level = 0"]
    >>= of_db_opt >>= fun fees ->
    [%pgsql dbh "SELECT SUM(balance)::bigint FROM origination_all \
                WHERE NOT failed AND kt1 = $hash AND distance_level = 0"]
    >>= of_db_opt >>= fun balances ->
    return (burn, Int64.add fees send, balances)

  let burned_tr_dun ?level dbh =
    let level, nolevel = test_opt Int64.of_int level in
    [%pgsql dbh "SELECT SUM(burn_dun)::bigint FROM transaction_all WHERE \
                (NOT failed) AND distance_level = 0 AND ($nolevel OR op_level <= $?level)"]
    >>= of_db_opt

  let tr_dun dbh hash =
    [%pgsql dbh "SELECT SUM(burn_dun)::bigint FROM transaction_all \
                WHERE (NOT failed) AND source = $hash AND distance_level = 0"]
    >>= of_db_opt >>= fun burn ->
    [%pgsql dbh "SELECT SUM(amount + fee)::bigint FROM transaction_all \
                WHERE NOT failed AND source = $hash AND distance_level = 0"]
    >>= of_db_opt >>= fun send ->
    [%pgsql dbh "SELECT SUM(fee)::bigint FROM transaction_all \
                WHERE source = $hash AND failed AND distance_level = 0"]
    >>= of_db_opt >>= fun fees ->
    [%pgsql dbh "SELECT SUM(amount)::bigint FROM transaction_all \
                WHERE NOT failed AND destination = $hash AND distance_level = 0"]
    >>= of_db_opt >>= fun amount ->
    return (burn, Int64.add fees send, amount)

  let burned_double_baking ?level dbh cycle_limit =
    [%pgsql dbh
      "SELECT CAST(SUM(lost_rewards) AS bigint), \
       CAST(SUM(lost_deposit) AS bigint), \
       CAST(SUM(lost_fees) AS bigint) \
       FROM double_baking_evidence_all AS dbe \
       INNER JOIN header AS h ON h.id = dbe.header1 \
       INNER JOIN block AS bl ON bl.level = h.level \
       WHERE bl.cycle < $cycle_limit AND dbe.distance_level = 0"]
    >>= begin function
      | [ Some lost_rewards, Some lost_deposit, Some lost_fees] ->
        return @@
        Int64.(Int64.abs
                 (add lost_rewards
                    (div (add lost_deposit lost_fees) 2L)))
      | _ -> return 0L
    end >>= fun burn ->
    let level, nolevel = test_opt Int64.of_int level in
    [%pgsql dbh
      "SELECT CAST(SUM(lost_deposit) AS bigint), \
       CAST(SUM(lost_fees) AS bigint) \
       FROM double_baking_evidence_all AS dbe \
       INNER JOIN header AS h ON h.id = dbe.header1 \
       INNER JOIN block AS bl ON bl.level = h.level \
       WHERE bl.cycle >= $cycle_limit AND dbe.distance_level = 0 AND \
       ($nolevel OR dbe.op_level <= $?level)"] >>= function
    | [ Some lost_deposit, Some lost_fees] ->
      return @@
      Int64.(sub burn (add lost_deposit lost_fees))
    | _ -> return burn

  let burned_double_endorsement ?level dbh cycle_limit =
    [%pgsql dbh
      "SELECT CAST(SUM(lost_rewards) AS bigint), \
       CAST(SUM(lost_deposit) AS bigint), \
       CAST(SUM(lost_fees) AS bigint) \
       FROM double_endorsement_evidence_all AS dee \
       INNER JOIN block AS bl ON bl.hash = dee.block_hash1 \
       WHERE bl.cycle < $cycle_limit AND dee.distance_level = 0"]
    >>= begin function
      | [ Some lost_rewards, Some lost_deposit, Some lost_fees] ->
        return @@
        Int64.(Int64.abs
                 (add lost_rewards
                    (div (add lost_deposit lost_fees) 2L)))
      | _ -> return 0L
    end >>= fun burn ->
    let level, nolevel = test_opt Int64.of_int level in
    [%pgsql dbh
      "SELECT CAST(SUM(lost_deposit) AS bigint), \
       CAST(SUM(lost_fees) AS bigint) \
       FROM double_endorsement_evidence_all AS dee \
       INNER JOIN block AS bl ON bl.hash = dee.block_hash1 \
       WHERE bl.cycle >= $cycle_limit AND dee.distance_level = 0 AND \
       ($nolevel OR dee.op_level <= $?level)"] >>= function
    | [ Some lost_deposit, Some lost_fees] ->
      return @@
      Int64.(sub burn (add lost_deposit lost_fees))
    | _ -> return burn

  let double_baking dbh cycle_limit hash =
    [%pgsql dbh
      "SELECT CAST(SUM(CASE WHEN bl.cycle < $cycle_limit THEN lost_rewards ELSE 0 END) AS bigint), \
       CAST(SUM(lost_deposit) AS bigint), \
       CAST(SUM(CASE WHEN bl.cycle < $cycle_limit THEN lost_fees ELSE 0 END) AS bigint) \
       FROM double_baking_evidence_all AS dbe \
       INNER JOIN header AS h ON h.id = dbe.header1 \
       INNER JOIN block AS bl ON bl.level = h.level \
       WHERE accused = $hash AND dbe.distance_level = 0"]
    >>= begin function
      | [ Some lost_rewards, Some lost_deposit, Some lost_fees] ->
        return @@
        Int64.(Int64.abs
                 (add lost_rewards
                    (add lost_deposit lost_fees)))
      | _ -> return 0L
    end >>= fun burn ->
    [%pgsql dbh
      "SELECT CAST(SUM(gain_rewards) AS bigint) \
       FROM double_baking_evidence_all AS dbe \
       WHERE dbe.op_cycle < $cycle_limit AND denouncer = $hash \
       AND dbe.distance_level = 0"]
    >>= function
    | [ Some gains] ->
      return (burn, gains)
    | _ -> return (burn, 0L)

  let double_endorsement dbh cycle_limit hash =
    [%pgsql dbh
      "SELECT CAST(SUM(CASE WHEN bl.cycle < $cycle_limit THEN lost_rewards ELSE 0 END) AS bigint), \
       CAST(SUM(lost_deposit) AS bigint), \
       CAST(SUM(CASE WHEN bl.cycle < $cycle_limit THEN lost_fees ELSE 0 END) AS bigint) \
       FROM double_endorsement_evidence_all AS dee \
       INNER JOIN block AS bl ON bl.hash = dee.block_hash1 \
       WHERE accused = $hash AND dee.distance_level = 0"]
    >>= begin function
      | [ Some lost_rewards, Some lost_deposit, Some lost_fees] ->
        return @@
        Int64.(Int64.abs
                 (add lost_rewards
                    (add lost_deposit lost_fees)))
      | _ -> return 0L
    end >>= fun burn ->
    [%pgsql dbh
      "SELECT CAST(SUM(gain_rewards) AS bigint) \
       FROM double_endorsement_evidence_all AS dee \
       WHERE dee.op_cycle < $cycle_limit AND denouncer = $hash \
       AND dee.distance_level = 0"]
    >>= function
    | [ Some gains] ->
      return (burn, gains)
    | _ -> return (burn, 0L)

  let del_dun dbh hash =
    [%pgsql dbh "SELECT SUM(fee)::bigint FROM delegation_all WHERE source = $hash \
               AND distance_level = 0"]
    >>= of_db_opt

  let rvl_dun dbh hash =
    [%pgsql dbh "SELECT SUM(fee)::bigint FROM reveal_all WHERE source = $hash \
                AND distance_level = 0"]
    >>= of_db_opt

  let cycle_from_level dbh level =
    let level = Int64.of_int level in
    [%pgsql dbh "SELECT cycle FROM cycle_limits \
                 WHERE $level BETWEEN level_start AND level_end"] >|= function
    | cycle :: _ -> cycle
    | _ -> assert false

  let supply ?level () =
    let ico = Infos.api.api_config.conf_ico in
    let foundation = ico.ico_foundation_tokens in
    let early_bakers = ico.ico_early_tokens in
    let contributors = ico.ico_contributors_tokens in
    let validator_program = ico.ico_validator_program in
    let protocol_burn = match level with
      | None ->
        List.fold_left (fun acc (_, burn_amount) -> acc ++ burn_amount) 0L
          ico.ico_protocol_burn
      | Some level ->
        List.fold_left (fun acc (burn_level, burn_amount) ->
            if level >= burn_level then acc ++ burn_amount
            else acc) 0L ico.ico_protocol_burn in
    with_dbh >>> fun dbh ->
    (match level with
     | None -> current_cycle dbh
     | Some level -> cycle_from_level dbh level) >>= fun cycle ->
    let cst = Infos.last_constants () in
    let allowed_fork64 = Int64.of_int cst.Dune_types.preserved_cycles in
    let cycle_limit =
      if cycle < allowed_fork64 then 0L
      else cycle -- allowed_fork64 in
    unfrozen_rewards dbh cycle_limit >>= fun unfrozen_rewards ->
    revelations dbh cycle_limit >>=
    fun (revelation_rewards, missing_revelations, burned_dun_revelation) ->
    burned_ori_dun ?level dbh >>= fun burned_dun_origination ->
    burned_tr_dun ?level dbh >>= fun burned_dun_transaction ->
    burned_double_baking ?level dbh cycle_limit >>= fun burned_dun_double_baking ->
    burned_double_endorsement ?level dbh cycle_limit >>= fun burned_dun_double_endorsement ->
    let total_supply_ico = foundation ++ early_bakers ++
                           contributors ++ validator_program in
    let burned_total =
      burned_dun_revelation ++ burned_dun_origination ++
      burned_dun_transaction ++ burned_dun_double_baking ++
      burned_dun_double_endorsement in
    let current_circulating_supply =
      total_supply_ico ++ unfrozen_rewards ++ revelation_rewards -- burned_total
      -- protocol_burn -- validator_program in
    return
      { foundation ; early_bakers ; contributors ;
        unfrozen_rewards ;
        missing_revelations ;
        revelation_rewards ; burned_dun_revelation ; burned_dun_transaction ;
        burned_dun_origination ; burned_dun_double_baking ;
        burned_dun_double_endorsement; protocol_burn;
        total_supply_ico ; current_circulating_supply;
        locked_dun = ico.ico_locked; validator_program }

  let market_info ?(marketcap=false) ?origin () =
    let of_array = function
      | [ Some gk_usd; Some gk_btc ] -> {gk_usd; gk_btc}
      | _ -> { gk_usd = 0.; gk_btc = 0. } in
    let origin, no_origin = test_opti origin in
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT last_updated, price, volume, change_1h, change_24h, change_7d \
                FROM price WHERE $no_origin OR origin = $?origin \
                ORDER BY last_updated DESC LIMIT 1"] >>= function
    | [] -> return None
    | (last_updated, price, volume, change_1h, change_24h, change_7d) :: _ ->
      let gk_price = of_array price in
      (if not marketcap then return None
       else
         supply () >|= fun {current_circulating_supply; _} ->
         let supply = Int64.(to_float @@ div current_circulating_supply Infos.dun_units) in
         Some {gk_usd = gk_price.gk_usd *. supply; gk_btc = gk_price.gk_btc *. supply})
      >|= fun gk_marketcap ->
      Some { gk_last_updated = Pg_helper.string_of_cal last_updated; gk_tickers = [];
             gk_market_data = {
               gk_price;
               gk_market_volume = of_array volume;
               gk_1h = of_array change_1h;
               gk_24h = of_array change_24h;
               gk_7d = of_array change_7d;
               gk_marketcap
             } }

  let balance_break_down hash =
    with_dbh >>> fun dbh ->
    current_cycle dbh >>= fun current_cycle ->
    let cst = Infos.last_constants () in
    let allowed_fork64 = Int64.of_int cst.Dune_types.preserved_cycles in
    let cycle_limit =
      if current_cycle < allowed_fork64 then 0L
      else Int64.sub current_cycle allowed_fork64 in
    h_activated_balances hash >>= fun h_activated_balance ->
    h_unfrozen_rewards dbh cycle_limit hash >>= fun (h_unfrozen_rewards, fees) ->
    h_revelations dbh cycle_limit hash >>=
    fun (h_revelation_rewards, h_missing_revelations, h_burned_dun_revelation) ->
    ori_dun dbh hash >>= fun (h_burned_dun_origination, h_dun_origination_send, h_dun_origination_recv) ->
    tr_dun dbh hash >>= fun (h_burned_dun_transaction, h_dun_transaction_send, h_dun_transaction_recv) ->
    double_baking dbh cycle_limit hash >>= fun (h_burned_dun_double_baking, h_dun_dbe_rewards) ->
    double_endorsement dbh cycle_limit hash >>= fun (h_burned_dun_double_endorsement, h_dun_dee_rewards) ->
    del_dun dbh hash >>= fun h_del_fees ->
    rvl_dun dbh hash >>= fun h_rvl_fees ->
    let plus_total =
      List.fold_left Int64.add 0L [
        h_activated_balance; h_unfrozen_rewards; fees; h_revelation_rewards;
        h_dun_origination_recv; h_dun_transaction_recv; h_dun_dbe_rewards;
        h_dun_dee_rewards] in
    let minus_total =
      List.fold_left Int64.add 0L [
        h_burned_dun_revelation; h_dun_origination_send; h_burned_dun_origination;
        h_burned_dun_transaction; h_dun_transaction_send; h_del_fees;
        h_rvl_fees; h_burned_dun_double_baking; h_burned_dun_double_endorsement ] in
    let h_total = Int64.sub plus_total minus_total in
    return
      {
        h_activated_balance ;
        h_unfrozen_rewards ;
        h_revelation_rewards ;
        h_missing_revelations ;
        h_burned_dun_revelation ;
        h_burned_dun_origination ;
        h_dun_origination_recv ;
        h_dun_origination_send ;
        h_burned_dun_transaction ;
        h_dun_transaction_recv ;
        h_dun_transaction_send ;
        h_burned_dun_double_baking ;
        h_burned_dun_double_endorsement ;
        h_dun_dbe_rewards ;
        h_total ;
      }

  let h24_stats () =
    (* To speed up the query a bit *)
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT( DISTINCT b.hash ), \
       SUM(CASE WHEN b.priority = 0 THEN 1 else 0 END), \
       SUM(block_reward_f(block_reward, b.priority, b.endorsements_included, \
       endorsers_per_block, initial_endorsers IS NOT NULL))::bigint \
       FROM block AS b \
       INNER JOIN cycle_limits AS cl ON cl.cycle = b.cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE timestamp >= NOW() - '24 hours'::INTERVAL AND \
       b.distance_level = 0"]
    >>= fun blocks_res ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM transaction_all \
       WHERE timestamp_op >= NOW() - '24 hours'::INTERVAL \
       AND distance_level = 0"]
    >>= of_count_opt >>= fun tr_count ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM origination_all \
       WHERE timestamp_op >= NOW() - '24 hours'::INTERVAL \
       AND distance_level = 0"]
    >>= of_count_opt >>= fun or_count ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM delegation_all \
       WHERE timestamp_op >= NOW() - '24 hours'::INTERVAL \
       AND distance_level = 0"]
    >>= of_count_opt >>= fun del_count ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM activation_all \
       WHERE timestamp_op >= NOW() - '24 hours'::INTERVAL \
       AND distance_level = 0"]
    >>= of_count_opt >>= fun act_count ->
    [%pgsql dbh
      "SELECT SUM(array_length(slots, 1)), \
       SUM(array_length(slots, 1) * endorsement_reward /( 1 + priority))::bigint \
       FROM endorsement_all AS e \
       INNER JOIN cycle_limits AS cl ON cl.cycle = op_cycle \
       INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
       COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
       cst.level_start <= cl.level_start AND cst.level_end IS NULL) \
       WHERE timestamp >= NOW() - '24 hours'::INTERVAL \
       AND e.distance_level = 0"]
   >>= fun end_res ->
    let cst = Infos.last_constants () in
    let blocks, blocks0, block_rewards = match blocks_res with
      | [ Some blocks_count, Some blocks0_count, Some block_rewards ] ->
        Int64.to_int blocks_count, Int64.to_int blocks0_count,
        block_rewards
      | _ -> 0, 0, 0L in
    let end_count, end_rewards = match end_res with
      | [ Some end_count, Some end_rewards ] ->
        Int64.to_int end_count, end_rewards
      | _ -> 0, 0L in
    let h24_rewards = block_rewards ++ end_rewards in
    let h24_max_end = blocks * cst.Dune_types.endorsers_per_block in
    let h24_end_rate = (float end_count) /. (float h24_max_end) *. 100. in
    let h24_block_0_rate = (float blocks0) /. (float blocks) *. 100. in
    [%pgsql dbh "SELECT staking_balances FROM context_totals \
                      ORDER BY period DESC LIMIT 1"]
    >>= of_db_opt >>= fun h24_baking_rate ->
    begin [%pgsql dbh "select cycle, index from snapshot_rolls WHERE ready
                      ORDER BY cycle DESC LIMIT 1"]
    >>= function
    | [ cycle, index ] -> return (Int64.to_int cycle, Int32.to_int index)
    | _ -> return (0, 0) end >>= fun (cycle, _index) ->
    [%pgsql dbh
      "SELECT current_balances + frozen_fees + frozen_deposits \
       FROM context_totals ORDER BY period DESC LIMIT 1"] >>= begin function
      | [ Some supply ] ->
        return (Int64.to_float supply /. (Int64.to_float Infos.dun_units))
      | _ -> return 0. end >>= fun circulating_supply ->
    let h24_baking_rate =
      if circulating_supply = 0. then 0. else
        ((Int64.to_float h24_baking_rate /. Int64.to_float Infos.dun_units) /.
         circulating_supply) *. 100. in
    let cycle64 = Int64.of_int cycle in
    [%pgsql dbh
      "SELECT COUNT(DISTINCT dn1) FROM snapshot_owner AS so \
       INNER JOIN snapshot_rolls AS sr ON sr.id = so.id \
       WHERE sr.cycle = $cycle64 AND ready"]
    >>= of_count_opt >>= fun h24_active_baker ->
    return { h24_end_rate ;
             h24_block_0_rate ;
             h24_transactions = tr_count ;
             h24_originations = or_count ;
             h24_delegations = del_count ;
             h24_activations = act_count ;
             h24_endorsements = end_count ;
             h24_baking_rate ;
             h24_active_baker;
             h24_rewards}

  let crawler_activity () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT * FROM crawler_activity"] >>= fun list ->
    return @@
    List.map (fun (crawler_name, crawler_timestamp, crawler_delay ) ->
        let crawler_delay = Int32.to_int crawler_delay in
        { crawler_name ; crawler_timestamp ; crawler_delay })
      list

  let volume_per_day () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT date(timestamp) as d, SUM(volume)::bigint FROM block \
       WHERE volume <> 0 AND distance_level = 0 \
       GROUP BY d ORDER BY d DESC"]
    >|= Pg_helper.per_day_rows (fun i -> i)

  let fees_per_day () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT date(timestamp) as d, SUM(fees)::bigint FROM block \
       WHERE volume <> 0 AND distance_level = 0 \
       GROUP BY d ORDER BY d DESC"]
    >|= Pg_helper.per_day_rows (fun i -> i)

  let blocks_per_day () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT date(timestamp) as d, COUNT(*) FROM block \
       WHERE distance_level = 0 \
       GROUP BY d ORDER BY d DESC"]
    >|= Pg_helper.per_day_rows Int64.to_int

  let bakers_per_day () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT date(timestamp) as d, COUNT(DISTINCT baker) FROM block \
       WHERE distance_level = 0 \
       GROUP BY d ORDER BY d DESC"]
    >|= Pg_helper.per_day_rows Int64.to_int

  let operations_per_day () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT date(timestamp) as d, SUM(operation_count)::bigint FROM block \
       WHERE distance_level = 0 \
       GROUP BY d ORDER BY d DESC"]
    >|= Pg_helper.per_day_rows Int64.to_int

  let operations_per_block_per_day () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT date(timestamp) as d, SUM(operation_count)::bigint, COUNT(*) FROM block \
       WHERE distance_level = 0 \
       GROUP BY d ORDER BY d DESC"] >|=
    Pg_helper.per_day_rows2 (fun i1 i2 -> (Int64.to_float i1) /. (Int64.to_float i2))

  let priorities_per_day () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT date(timestamp) as d, SUM(priority)::bigint, COUNT(*) FROM block \
       WHERE distance_level = 0 \
       GROUP BY d ORDER BY d DESC"] >|=
    Pg_helper.per_day_rows2 (fun i1 i2 -> (Int64.to_float i1) /. (Int64.to_float i2))


  let alias account_hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT alias FROM user_alias WHERE dn = $account_hash"]
    >>= function
    | [ ] -> return None
    | alias :: _  -> return (Some alias)

  let account_from_alias alias =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT dn FROM user_alias WHERE alias = $alias"]
    >>= function
    | [] -> return None
    | account_hash :: _ -> return (Some account_hash)

  let all_aliases () =
    with_dbh >>> fun dbh -> [%pgsql dbh "SELECT dn, alias FROM user_alias"]

  let nb_protocol () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT COUNT(*) FROM protocol_constants WHERE distance_level = 0"]
    >>= of_count_opt

  let protocols ?(page=0) ?(page_size=20) () =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "nullable-results"
      "SELECT cst.protocol, level_start, level_end, a.hash \
       FROM protocol_constants AS cst \
       LEFT JOIN activate_protocol AS a ON cst.block_hash = a.op_block_hash \
       WHERE cst.distance_level = 0 AND (a.distance_level IS NULL OR a.distance_level = 0) \
       ORDER BY level_start DESC OFFSET $offset LIMIT $limit"]
    >>= fun rows ->
    let n = List.length rows in
    return @@
    (List.rev @@ snd @@ List.fold_left (fun (i, acc) row -> match row with
         | (Some prt_hash, Some prt_start, prt_end, prt_activate) ->
           let prt_index = page * page_size + n - i - 1 in
           i+1, {prt_index; prt_hash;
                 prt_start = Int64.to_int prt_start ;
                 prt_end = Option.map Int64.to_int prt_end;
                 prt_activate} :: acc
        | _ -> i, acc) (0,[]) rows)

  let market_prices ?origin () =
    let origin, no_origin = test_opti origin in
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT date(last_updated) AS day, avg(price[1]), \
                avg(price[2]) FROM price \
                WHERE $no_origin OR origin = $?origin \
                GROUP BY day ORDER BY day DESC"]
    >>= fun rows ->
    return @@ List.fold_left (fun acc row -> match row with
        | (Some day, Some usd, Some btc) ->
          (CalendarLib.Printer.Date.sprint "%Y/%m/%d" day,
           [|"price_usd", usd; "price_btc", btc |]) :: acc
        | _ -> acc) [] rows

  let nb_balance_updates ?(from=0) ?up_to account_hash =
    let from = Int64.of_int from in
    with_dbh >>> fun dbh ->
    match up_to with
      Some d -> let d = Int64.of_int d in
      [%pgsql dbh
        "SELECT COUNT(*) FROM balance_updates AS bu \
         INNER JOIN cycle_limits AS cl_from ON cl_from.cycle = $from \
         INNER JOIN cycle_limits AS cl_d ON cl_d.cycle = $d \
         WHERE hash=$account_hash AND bu.distance_level=0 AND \
         level BETWEEN cl_from.level_start AND cl_d.level_start - 1"]
      >>= of_count_opt
    | None ->
      [%pgsql dbh
        "SELECT COUNT(*) FROM balance_updates AS bu \
         INNER JOIN cycle_limits AS cl_from ON cl_from.cycle = $from \
         WHERE hash=$account_hash AND level >= cl_from.level_start \
         AND bu.distance_level=0"]
       >>= of_count_opt

  let make_balance_update_info
      (bu_account, bu_block_hash, bu_diff, bu_date, bu_update_type, bu_op_type,
       bu_internal, bu_level, bu_frozen, bu_burn, bu_cycle) =
    { bu_account; bu_block_hash; bu_diff; bu_date = Pg_helper.date_of_cal bu_date;
      bu_update_type; bu_op_type; bu_internal; bu_level = Int32.to_int bu_level; bu_frozen; bu_burn;
      bu_cycle = Some (Int64.to_int bu_cycle)}

  let balance_updates
      ?(page=0) ?(page_size=20) ?(from=0) ?up_to account_hash : balance_update_info list Monad.t =
    let offset, limit = limits page page_size in
    let from = Int64.of_int from in
    with_dbh >>> fun dbh ->
    begin
      match up_to with
      | Some d -> let d = Int64.of_int d in
        [%pgsql dbh
          "SELECT hash, bu.block_hash, diff, date, update_type, operation_type, \
           internal, level, frozen, burn, cl.cycle \
           FROM balance_updates AS bu \
           INNER JOIN cycle_limits AS cl ON level BETWEEN cl.level_start AND cl.level_end \
           INNER JOIN cycle_limits AS cl_from ON cl_from.cycle = $from \
           INNER JOIN cycle_limits AS cl_d ON cl_d.cycle = $d \
           WHERE hash=$account_hash AND bu.distance_level = 0 AND \
           level BETWEEN cl_from.level_start AND cl_d.level_start - 1 \
           ORDER BY level DESC, diff DESC, hash OFFSET $offset LIMIT $limit"]
      | None ->
        [%pgsql dbh
          "SELECT hash, bu.block_hash, diff, date, update_type, operation_type, \
           internal, level, frozen, burn, cl.cycle \
           FROM balance_updates AS bu \
           INNER JOIN cycle_limits AS cl ON level BETWEEN cl.level_start AND cl.level_end \
           INNER JOIN cycle_limits AS cl_from ON cl_from.cycle = $from \
           WHERE hash=$account_hash AND level >= cl_from.level_start \
           AND bu.distance_level=0 \
           ORDER BY level DESC, diff DESC, hash OFFSET $offset LIMIT $limit"]
    end >>= fun rows ->
    return @@ List.map make_balance_update_info rows

  let cycle_frozen cycle account_hash =
    let cycle = Int64.of_int cycle in
    with_dbh >>> fun dbh ->
    ([%pgsql dbh "SELECT level_start, level_end FROM cycle_limits WHERE cycle = $cycle"] >>= function
      | (min, max) :: _ -> return (min, max)
      | _ -> return (0L, 0L)) >>= fun (theoretical_min, max) ->
    [%pgsql dbh "WITH hd AS (SELECT header1 FROM double_baking_evidence_all WHERE accused=$account_hash AND distance_level = 0) \
                SELECT MAX(level) FROM header WHERE level>=$theoretical_min AND id IN (SELECT * FROM hd)"]
    >>= fun min ->
    let min32 : int32 = match min with
      | [] | None :: _ -> Int64.to_int32 theoretical_min
      | Some hd :: _ -> Int64.to_int32 hd in
    let max32 = Int64.to_int32 max in
    [%pgsql dbh
      "SELECT \
       SUM(CASE WHEN distance_level=0 AND update_type='Reward' AND (level!=$max32-1 OR diff>0) THEN diff ELSE 0 END), \
       SUM(CASE WHEN distance_level=0 AND update_type='Fee' AND (level!=$max32-1 OR diff>0) THEN diff ELSE 0 END), \
       SUM(CASE WHEN distance_level=0 AND update_type='Deposit' AND (level!=$max32-1 OR diff>0) THEN diff ELSE 0 END) \
       FROM balance_updates \
       WHERE level>=$min32 AND level<=$max32 AND hash=$account_hash"] >>=
    function
    | (Some b, Some c, Some d) :: _ ->
      let b_rewards = Int64.of_string b and b_fees = Int64.of_string c and b_deposits = Int64.of_string d
      in
      return
        {b_spendable = Int64.zero;
         b_frozen = Int64.(add b_rewards @@ add b_fees b_deposits);
         b_rewards;
         b_fees;
         b_deposits}
    | _ ->
      return
        {b_spendable = Int64.zero;
         b_frozen =  Int64.zero;
         b_rewards =  Int64.zero;
         b_fees =  Int64.zero;
         b_deposits =  Int64.zero}

  let active_balance_updates cycle account_hash =
    let cycle = Int64.of_int cycle in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT hash, block_hash, diff, date, update_type, operation_type, internal, \
       level, frozen, burn, cl.cycle \
       FROM balance_updates \
       INNER JOIN cycle_limits AS cl ON cl.cycle = $cycle
       WHERE hash=$account_hash \
       AND level < cl.level_end AND level >= level_start \
       AND ((frozen='false' AND operation_type='Transaction') \
       OR (frozen='true' AND diff < 0)) \
       AND distance_level=0"] >>= fun list ->
    return @@ List.fold_left
      (fun (acc : balance_update_info list) res ->
         (make_balance_update_info res) :: acc) [] list

  let balance account_hash : int64 Monad.t =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT spendable_balance \
       FROM balance_snapshot \
       WHERE hash=$account_hash"] >>= function
    | [] -> return Int64.zero
    | elt :: _ -> return elt

  let balance_from_balance_updates account_hash : balance Monad.t =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT spendable_balance,frozen,rewards,fees,deposits \
       FROM balance_snapshot \
       WHERE hash=$account_hash"] >>= function
    | [] ->
      return
        {b_spendable = Int64.zero;
         b_frozen =  Int64.zero;
         b_rewards =  Int64.zero;
         b_fees =  Int64.zero;
         b_deposits =  Int64.zero}
    | (b_spendable, b_frozen, b_rewards, b_fees, b_deposits) :: _ ->
      return {b_spendable; b_frozen; b_rewards; b_fees; b_deposits}

  let balance_history current_cycle account_hash : (Int32.t * balance) list Monad.t =
    let current_cycle = Int32.of_int current_cycle in
    let fill_history history =
      let rec __fill_history result (cpt : Int32.t) remaining_history last_used_balance =
        if cpt > current_cycle then result
        else
          match remaining_history with
            [] ->
            __fill_history
              ((cpt,last_used_balance) :: result)
              (Int32.add cpt Int32.one)
              []
              last_used_balance
          | (cycle,b_spendable,b_frozen,b_rewards,b_fees,b_deposits) :: tl ->
            if cycle <> cpt
            then __fill_history ((cpt,last_used_balance) :: result) (Int32.add cpt Int32.one) remaining_history last_used_balance
            else (* cycle=cpt *)
              let last_balance = {b_spendable;b_frozen;b_rewards;b_fees;b_deposits}
              in
              __fill_history
                ((cycle,last_balance) :: result)
                (Int32.add cpt Int32.one)
                tl
                last_balance
      in
      let first_cycle,first_balance,rest_of_history =
        match history with
          [] -> assert false
        | (c,b_spendable,b_frozen,b_rewards,b_fees,b_deposits) :: tl ->
          (c,
           {b_spendable;b_frozen;b_rewards;b_fees;b_deposits},
           tl)
      in
      __fill_history
        [first_cycle,first_balance]
        (Int32.add first_cycle Int32.one)
        rest_of_history
        first_balance
    in
    let snapshot =
      with_dbh >>> fun dbh ->
      [%pgsql dbh
        "SELECT spendable_balance,frozen,rewards,fees,deposits \
         FROM balance_snapshot \
         WHERE hash=$account_hash"] in
    snapshot >>=
    (function
        [] ->
        return []
      | _  :: _ :: _ -> (* hash is a primary key, so this case never happens *)
        assert false

      | (b_spendable,b_frozen,b_rewards,b_fees,b_deposits) :: [] ->

        let curr_balance =
          {b_spendable;
           b_frozen;
           b_rewards;
           b_fees;
           b_deposits} in

        let hist = with_dbh >>> fun dbh ->
          [%pgsql dbh
            "SELECT cycle,spendable_balance,frozen,rewards,fees,deposits \
             FROM balance_from_balance_updates \
             WHERE hash=$account_hash ORDER BY cycle ASC"] in
        hist >>=
        (fun bal_list ->
           match bal_list with
             [] -> assert false (* If there is an element in snapshot, there is an element *)
           | _ ->
             let result= fill_history bal_list in
             return @@ ((Int32.add current_cycle Int32.one, curr_balance) :: result)
        )
    )

  let nb_balance ?threshold ?(kind=Balances) () =
    let threshold = match kind with
      | Frozen_fees -> Option.value ~default:0L threshold
      | _ -> Option.value ~default:1_000_000L threshold in
    with_dbh >>> fun dbh ->
    begin match kind with
      | Balances ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM balance_snapshot WHERE spendable_balance > $threshold"]
      | Frozen_balances ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM balance_snapshot WHERE frozen > $threshold"]
      | Frozen_deposits ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM balance_snapshot WHERE deposits > $threshold"]
      | Frozen_rewards ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM balance_snapshot WHERE rewards > $threshold"]
      | Frozen_fees ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM balance_snapshot WHERE fees > $threshold"]
      | Total_balances ->
        [%pgsql dbh
          "SELECT COUNT(*) FROM balance_snapshot WHERE spendable_balance + frozen > $threshold"]
      | _ -> return [ Some 0L ]
    end
    >>= of_count_opt

  let balance_ranking ?threshold ?(kind=Balances)
      ?(page=0) ?(page_size=20) () =
    let threshold = match kind with
      | Frozen_fees -> Option.value ~default:0L threshold
      | _ -> Option.value ~default:1_000_000L threshold in
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    begin match kind with
      | Balances ->
        [%pgsql dbh
          "SELECT hash, spendable_balance \
           FROM balance_snapshot WHERE spendable_balance > $threshold \
           ORDER BY spendable_balance DESC, hash OFFSET $offset LIMIT $limit"]
      | Frozen_balances ->
        [%pgsql dbh
          "SELECT hash, frozen \
           FROM balance_snapshot WHERE frozen > $threshold \
           ORDER BY frozen DESC, hash OFFSET $offset LIMIT $limit"]
      | Frozen_deposits ->
        [%pgsql dbh
          "SELECT hash, deposits \
           FROM balance_snapshot WHERE deposits > $threshold \
           ORDER BY deposits DESC, hash OFFSET $offset LIMIT $limit"]
      | Frozen_rewards ->
        [%pgsql dbh
          "SELECT hash, rewards \
           FROM balance_snapshot WHERE rewards > $threshold \
           ORDER BY rewards DESC, hash OFFSET $offset LIMIT $limit"]
      | Frozen_fees ->
        [%pgsql dbh
          "SELECT hash, fees \
           FROM balance_snapshot WHERE fees > $threshold \
           ORDER BY fees DESC, hash OFFSET $offset LIMIT $limit"]
      | Total_balances ->
        [%pgsql dbh
          "SELECT hash, spendable_balance + frozen \
           FROM balance_snapshot WHERE spendable_balance + frozen > $threshold \
           ORDER BY spendable_balance + frozen DESC, hash OFFSET $offset LIMIT $limit"]
        >>= fun l -> return @@ List.map (fun (s, b) -> s, Option.value ~default:0L b) l
      | _ -> return []
    end
    >>= fun l ->
    return @@ List.mapi (fun i (str,v) -> (Int64.to_int offset + i + 1),(Alias.to_name str),v) l

  let nb_exchange () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT COUNT(DISTINCT name) FROM coingecko_exchange"] >>= of_count_opt

  let exchange_info ?(page=0) ?(page_size=10) () =
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "WITH tmp(name, total_volume) AS ( \
       SELECT name, SUM(volume) as total_volume FROM coingecko_exchange \
       WHERE NOW() - TO_TIMESTAMP(timestamp,'YYYY-MM-DD HH24:MI:SS') < '2 days'::INTERVAL \
       GROUP BY name ORDER BY total_volume DESC \
       OFFSET $offset LIMIT $limit ) \
       SELECT tmp.name, tmp.total_volume, base, target, volume, conversion, \
       price_usd, timestamp \
       FROM coingecko_exchange AS ce \
       INNER JOIN tmp on tmp.name = ce.name \
       ORDER BY total_volume, name"]
    >>= fun rows ->
    return @@
    List.fold_left (fun acc exc ->
        match exc with
        | (ex_name, Some ex_total_volume, ex_base, ex_target,
           ex_volume, ex_conversion, ex_price_usd, ex_tsp) ->
          let ticker = {ex_base; ex_target; ex_volume;
                        ex_conversion; ex_price_usd; ex_tsp} in
          if acc = [] || (List.hd acc).ex_name <> ex_name then
            {ex_name; ex_total_volume; ex_tickers = [ticker]} :: acc
          else
            {ex_name; ex_total_volume;
             ex_tickers = ticker :: (List.hd acc).ex_tickers} :: (List.tl acc)
        | _ -> acc )
      [] rows

  let get_services () =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh "SELECT * FROM services ORDER BY kind"]
    >>= fun rows ->
    return @@ List.map Pg_helper.service_from_db rows

  let get_active_aliases () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT UNNEST(addresses), UNNEST(aliases) FROM services \
       WHERE sponsored > NOW ()"]
    >>= fun l ->
    return @@
    List.fold_left (fun acc (pkh, alias) -> match pkh, alias with
        | Some pkh, alias -> {pkh; alias} :: acc
        | _ -> acc) [] l

  let stats_time () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT SUM(CASE WHEN NOW() - timestamp < '1 hour'::INTERVAL THEN 1 ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '6 hours'::INTERVAL THEN 1 ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '12  hours'::INTERVAL THEN 1 ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 day'::INTERVAL THEN 1 ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '7 days'::INTERVAL THEN 1 ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 month'::INTERVAL THEN 1 ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 hour'::INTERVAL THEN operation_count ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '6 hours'::INTERVAL THEN operation_count ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '12  hours'::INTERVAL THEN operation_count ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 day'::INTERVAL THEN operation_count ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '7 days'::INTERVAL THEN operation_count ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 month'::INTERVAL THEN operation_count ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 hour'::INTERVAL THEN volume ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '6 hours'::INTERVAL THEN volume ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '12  hours'::INTERVAL THEN volume ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 day'::INTERVAL THEN volume ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '7 days'::INTERVAL THEN volume ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 month'::INTERVAL THEN volume ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 hour'::INTERVAL THEN fees ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '6 hours'::INTERVAL THEN fees ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '12  hours'::INTERVAL THEN fees ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 day'::INTERVAL THEN fees ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '7 days'::INTERVAL THEN fees ELSE 0 END)::BIGINT,
       SUM(CASE WHEN NOW() - timestamp < '1 month'::INTERVAL THEN fees ELSE 0 END)::BIGINT
       FROM block WHERE distance_level = 0"] >>=
    function
    | [ (Some nbb1, Some nbb2, Some nbb3, Some nbb4, Some nbb5, Some nbb6,
         Some nbo1, Some nbo2, Some nbo3, Some nbo4, Some nbo5, Some nbo6,
         Some vol1, Some vol2, Some vol3, Some vol4, Some vol5, Some vol6,
         Some fee1, Some fee2, Some fee3, Some fee4, Some fee5, Some fee6) ] ->
    return
      { ms_period = [| "1 hour"; "6 hours"; "12 hours"; "1 day"; "7 days"; "1 month" |];
        ms_nhours = [| 1; 6; 12; 24; 168; 720 |];
        ms_nblocks = [| Int64.to_int nbb1; Int64.to_int nbb2; Int64.to_int nbb3;
                        Int64.to_int nbb4; Int64.to_int nbb5; Int64.to_int nbb6 |];
        ms_nops = [| Int64.to_int nbo1; Int64.to_int nbo2; Int64.to_int nbo3;
                     Int64.to_int nbo4; Int64.to_int nbo5; Int64.to_int nbo6 |];
        ms_volume = [| vol1; vol2; vol3; vol4; vol5; vol6 |];
        ms_fees = [| fee1; fee2; fee3; fee4; fee5; fee6 |] }
    | _ ->
      return {ms_period = [||]; ms_nhours = [||]; ms_nblocks = [||]; ms_nops = [||];
              ms_volume = [||]; ms_fees = [||]}

  let delegators_live hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh "SELECT hash FROM account_info WHERE delegate = $hash"] >>= fun rows ->
    return @@ List.map Alias.to_name rows

  let staking_balance hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT SUM(bs.spendable_balance)::bigint FROM balance_snapshot AS bs \
       INNER JOIN account_info AS ai ON ai.hash = bs.hash \
       WHERE ai.delegate = $hash"] >>= of_db_opt

  let constants ?(current=true) ?level ?protocol ?limit () =
    with_dbh >>> fun dbh ->
    (match level, protocol, limit with
     | Some level, _, _ -> let level = Int64.of_int level in
       [%pgsql dbh
         "SELECT * FROM protocol_constants WHERE distance_level = 0 AND \
          COALESCE($level BETWEEN level_start AND level_end, \
          level_start <= $level AND level_end IS NULL)"]
     | _, Some protocol, _ ->
       [%pgsql dbh
         "SELECT * FROM protocol_constants WHERE distance_level = 0 \
          AND protocol = $protocol"]
     | _, _, Some limit -> let limit = Int64.of_int limit in
       [%pgsql dbh "SELECT * FROM protocol_constants \
                   WHERE distance_level = 0 \
                   ORDER BY level_start DESC limit $limit"]
     | _ when current ->
       head_level dbh >>= fun level ->
       [%pgsql dbh
         "SELECT * FROM protocol_constants WHERE distance_level = 0 AND \
          COALESCE($level BETWEEN level_start AND level_end, \
          level_start <= $level AND level_end IS NULL)"]
     | _ ->
       [%pgsql dbh
         "SELECT * FROM protocol_constants WHERE distance_level = 0 \
          ORDER BY level_start"])
    >>= fun constants ->
    return @@ List.map Pg_helper.constants_from_db constants

  let account_tokens account =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT DISTINCT ti.contract, name, symbol, total_supply, decimals, version, \
       language \
       FROM token_info AS ti \
       INNER JOIN token_balances AS tb ON tb.contract = ti.contract \
       WHERE tb.account = $account ORDER BY symbol"] >>= fun rows ->
    return (List.map Pg_helper.token_info_from_db rows)

  let token_balance ?contract account =
    let contract, nocontract = test_opti contract in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT DISTINCT contract, balance FROM token_balances \
       WHERE account = $account \
       AND ($nocontract OR contract = $?contract)"]

  let nb_token_operations ?contract account =
    let contract, nocontract = test_opti contract in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM token_operations \
       WHERE distance_level = 0 AND
       (source = $account OR destination = $account) AND \
       ($nocontract OR contract = $?contract)"]
    >>= of_count_opt

  let token_operations ?contract ?(page=0) ?(page_size=20) account =
    with_dbh >>> fun dbh ->
    let offset, limit = limits page page_size in
    let contract, nocontract = test_opti contract in
    [%pgsql.object dbh
      "SELECT transaction, op_block_hash, network, op_level, timestamp as tsp, \
       kind, source, destination, amount, flag \
       FROM token_operations WHERE distance_level = 0 AND
       (source = $account OR destination = $account) AND \
       ($nocontract OR contract = $?contract) \
       ORDER BY op_level DESC, counter DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.token_from_db_list

  let account_info hash =
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "nullable-results"
      "SELECT ai.hash, delegate, origination, maxrolls, admin, white_list, \
       delegation, recovery, spendable_balance, code, storage, code_hash, reveal, \
       activation, entrypoints \
       FROM account_info AS ai \
       LEFT JOIN balance_snapshot AS ba ON ba.hash = ai.hash \
       WHERE ai.hash = $hash"]
    >|= function
    | [ x ] -> Some (Pg_helper.account_info_from_db x)
    | _ -> None

  let level_constraint ~cycle ~level =
    with_dbh >>> fun dbh ->
    (match cycle.cstt_l with
     | None -> return (Option.map Int64.of_int level.cstt_l)
     | Some cycle_l ->
       let cycle_l = Int64.of_int cycle_l in
       [%pgsql dbh
         "SELECT level_end FROM cycle_limits WHERE cycle = $cycle_l"] >>= function
       | [ level_l ] ->
         return (Some (max level_l (Option.fold ~none:0L ~some:Int64.of_int level.cstt_l)))
       | _ -> return None) >>= fun cstt_l ->
    (match cycle.cstt_g with
     | None -> return (Option.map Int64.of_int level.cstt_g)
     | Some cycle_g -> let cycle_g = Int64.of_int cycle_g in
       [%pgsql dbh
         "SELECT level_start FROM cycle_limits WHERE cycle = $cycle_g"] >>= function
       | [ level_g ] ->
         return (Some ((min level_g (Option.fold ~none:Int64.max_int ~some:Int64.of_int level.cstt_g))))
       | _ -> return None) >>= fun cstt_g ->
    match cycle.cstt_e with
    | None -> return {cstt_l; cstt_g; cstt_e = Option.map Int64.of_int level.cstt_e}
    | Some cycle_e -> let cycle_e = Int64.of_int cycle_e in
       [%pgsql dbh
         "SELECT level_start, level_end FROM cycle_limits WHERE cycle = $cycle_e"]
       >>= function
       | [ level_le, level_ge ] ->
         let cstt_l = match cstt_l with
           | None -> Some level_le
           | Some level_l -> Some (max level_l level_le) in
         let cstt_g = match cstt_g with
           | None -> Some level_ge
           | Some level_g -> Some (min level_g level_ge) in
         return {cstt_l; cstt_g; cstt_e = None}
       | _ -> return {cstt_l; cstt_g; cstt_e = None}

  let nb_blocks_search ?(distance_level=0)
    ~hash ~pred ~baker ~network ~protocol ~signature ~fitness ~volume ~fees
    ~level ~cycle ~votingp ~priority ~nop ~tsp ~valpass ~cycle_pos ~nendo =
    let distance_level = Int64.of_int distance_level in
    let
      (hash, hashd, pred, predd, baker, bakerd, network, networkd,
       protocol, protocold, signature, signatured,
       fitness_e, fitness_g, fitness_l, volume_e, volume_g, volume_l, fees_e, fees_g, fees_l,
       level_e, level_g, level_l, cycle_e, cycle_g, cycle_l, votingp_e, votingp_g, votingp_l,
       priority_e, priority_g, priority_l, nop_e, nop_g, nop_l, tsp_e, tsp_g, tsp_l,
       valpass_e, valpass_g, valpass_l, cycle_pos_e, cycle_pos_g, cycle_pos_l,
       nendo_e, nendo_g, nendo_l),
      (nohash, nohashd, nopred, nopredd, nobaker, nobakerd, nonetwork, nonetworkd,
       noprotocol, noprotocold, nosignature, nosignatured,
       nofitness_e, nofitness_g, nofitness_l, novolume_e, novolume_g, novolume_l,
       nofees_e, nofees_g, nofees_l, nolevel_e, nolevel_g, nolevel_l,
       nocycle_e, nocycle_g, nocycle_l, novotingp_e, novotingp_g, novotingp_l,
       nopriority_e, nopriority_g, nopriority_l, nonop_e, nonop_g, nonop_l,
       notsp_e, notsp_g, notsp_l, novalpass_e, novalpass_g, novalpass_l,
       nocycle_pos_e, nocycle_pos_g, nocycle_pos_l, nonendo_e, nonendo_g, nonendo_l) =
      Pg_helper.boolean_for_blocks_search
        ~hash ~pred ~baker ~network ~protocol ~signature ~fitness ~volume ~fees
        ~level ~cycle ~votingp ~priority ~nop ~tsp ~valpass ~cycle_pos ~nendo in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
        "SELECT COUNT(hash) FROM block as b \
         WHERE ($nohash OR b.hash SIMILAR TO $?hash) \
         AND ($nopred OR b.predecessor SIMILAR TO $?pred) \
         AND ($nobaker OR b.baker SIMILAR TO $?baker) \
         AND ($nohashd OR b.hash <> $?hashd) \
         AND ($nopredd OR b.predecessor <> $?predd) \
         AND ($nobakerd OR b.baker <> $?bakerd) \
         AND ($nolevel_e OR level = $?level_e) \
         AND ($nolevel_g OR level > $?level_g) \
         AND ($nolevel_l OR level < $?level_l) \
         AND ($nocycle_e OR cycle = $?cycle_e) \
         AND ($nocycle_g OR cycle > $?cycle_g) \
         AND ($nocycle_l OR cycle < $?cycle_l) \
         AND ($novotingp_e OR voting_period = $?votingp_e) \
         AND ($novotingp_g OR voting_period > $?votingp_g) \
         AND ($novotingp_l OR voting_period < $?votingp_l) \
         AND ($nofitness_e OR fitness = $?fitness_e) \
         AND ($nofitness_g OR fitness > $?fitness_g) \
         AND ($nofitness_l OR fitness < $?fitness_l) \
         AND ($novolume_e OR volume = $?volume_e) \
         AND ($novolume_g OR volume > $?volume_g) \
         AND ($novolume_l OR volume < $?volume_l) \
         AND ($nofees_e OR fees = $?fees_e) \
         AND ($nofees_g OR fees > $?fees_g) \
         AND ($nofees_l OR fees < $?fees_l) \
         AND ($nonop_e OR operation_count = $?nop_e) \
         AND ($nonop_g OR operation_count > $?nop_g) \
         AND ($nonop_l OR operation_count < $?nop_l) \
         AND ($nopriority_e OR priority = $?priority_e) \
         AND ($nopriority_g OR priority > $?priority_g) \
         AND ($nopriority_l OR priority < $?priority_l) \
         AND ($notsp_e OR timestamp = $?tsp_e) \
         AND ($notsp_g OR timestamp > $?tsp_g) \
         AND ($notsp_l OR timestamp < $?tsp_l) \
         AND ($novalpass_e OR b.validation_pass = $?valpass_e) \
         AND ($novalpass_g OR b.validation_pass > $?valpass_g) \
         AND ($novalpass_l OR b.validation_pass < $?valpass_l) \
         AND ($nocycle_pos_e OR b.cycle_position = $?cycle_pos_e) \
         AND ($nocycle_pos_g OR b.cycle_position > $?cycle_pos_g) \
         AND ($nocycle_pos_l OR b.cycle_position < $?cycle_pos_l) \
         AND ($nonendo_e OR b.endorsements_included = $?nendo_e) \
         AND ($nonendo_g OR b.endorsements_included > $?nendo_g) \
         AND ($nonendo_l OR b.endorsements_included < $?nendo_l) \
         AND ($nonetwork OR b.network SIMILAR TO $?network) \
         AND ($noprotocol OR b.protocol SIMILAR TO $?protocol) \
         AND ($nosignature OR b.signature SIMILAR TO $?signature) \
         AND ($nonetworkd OR b.network <> $?networkd) \
         AND ($noprotocold OR b.protocol <> $?protocold) \
         AND ($nosignatured OR b.signature <> $?signatured) \
         AND (distance_level = $distance_level)"]
    >>= of_count_opt

  let blocks_search ?(page=0) ?(page_size=20) ?(distance_level=0)
    ~hash ~pred ~baker ~network ~protocol ~signature ~fitness ~volume ~fees
    ~level ~cycle ~votingp ~priority ~nop ~tsp ~valpass ~cycle_pos ~nendo =
    let offset, limit = limits page page_size in
    let distance_level = Int64.of_int distance_level in
    let
      (hash, hashd, pred, predd, baker, bakerd, network, networkd,
       protocol, protocold, signature, signatured,
       fitness_e, fitness_g, fitness_l, volume_e, volume_g, volume_l, fees_e, fees_g, fees_l,
       level_e, level_g, level_l, cycle_e, cycle_g, cycle_l, votingp_e, votingp_g, votingp_l,
       priority_e, priority_g, priority_l, nop_e, nop_g, nop_l, tsp_e, tsp_g, tsp_l,
       valpass_e, valpass_g, valpass_l, cycle_pos_e, cycle_pos_g, cycle_pos_l,
       nendo_e, nendo_g, nendo_l),
      (nohash, nohashd, nopred, nopredd, nobaker, nobakerd, nonetwork, nonetworkd,
       noprotocol, noprotocold, nosignature, nosignatured,
       nofitness_e, nofitness_g, nofitness_l, novolume_e, novolume_g, novolume_l,
       nofees_e, nofees_g, nofees_l, nolevel_e, nolevel_g, nolevel_l,
       nocycle_e, nocycle_g, nocycle_l, novotingp_e, novotingp_g, novotingp_l,
       nopriority_e, nopriority_g, nopriority_l, nonop_e, nonop_g, nonop_l,
       notsp_e, notsp_g, notsp_l, novalpass_e, novalpass_g, novalpass_l,
       nocycle_pos_e, nocycle_pos_g, nocycle_pos_l, nonendo_e, nonendo_g, nonendo_l) =
      Pg_helper.boolean_for_blocks_search
        ~hash ~pred ~baker ~network ~protocol ~signature ~fitness ~volume ~fees
        ~level ~cycle ~votingp ~priority ~nop ~tsp ~valpass ~cycle_pos ~nendo in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
        "SELECT b.*, pred.fitness as pred_fitness FROM block as b \
         INNER JOIN block AS pred ON b.predecessor = pred.hash \
         WHERE ($nohash OR b.hash SIMILAR TO $?hash) \
         AND ($nopred OR b.predecessor SIMILAR TO $?pred) \
         AND ($nobaker OR b.baker SIMILAR TO $?baker) \
         AND ($nohashd OR b.hash <> $?hashd) \
         AND ($nopredd OR b.predecessor <> $?predd) \
         AND ($nobakerd OR b.baker <> $?bakerd) \
         AND ($nolevel_e OR b.level = $?level_e) \
         AND ($nolevel_g OR b.level > $?level_g) \
         AND ($nolevel_l OR b.level < $?level_l) \
         AND ($nocycle_e OR b.cycle = $?cycle_e) \
         AND ($nocycle_g OR b.cycle > $?cycle_g) \
         AND ($nocycle_l OR b.cycle < $?cycle_l) \
         AND ($novotingp_e OR b.voting_period = $?votingp_e) \
         AND ($novotingp_g OR b.voting_period > $?votingp_g) \
         AND ($novotingp_l OR b.voting_period < $?votingp_l) \
         AND ($nofitness_e OR b.fitness = $?fitness_e) \
         AND ($nofitness_g OR b.fitness > $?fitness_g) \
         AND ($nofitness_l OR b.fitness < $?fitness_l) \
         AND ($novolume_e OR b.volume = $?volume_e) \
         AND ($novolume_g OR b.volume > $?volume_g) \
         AND ($novolume_l OR b.volume < $?volume_l) \
         AND ($nofees_e OR b.fees = $?fees_e) \
         AND ($nofees_g OR b.fees > $?fees_g) \
         AND ($nofees_l OR b.fees < $?fees_l) \
         AND ($nonop_e OR b.operation_count = $?nop_e) \
         AND ($nonop_g OR b.operation_count > $?nop_g) \
         AND ($nonop_l OR b.operation_count < $?nop_l) \
         AND ($nopriority_e OR b.priority = $?priority_e) \
         AND ($nopriority_g OR b.priority > $?priority_g) \
         AND ($nopriority_l OR b.priority < $?priority_l) \
         AND ($notsp_e OR b.timestamp = $?tsp_e) \
         AND ($notsp_g OR b.timestamp > $?tsp_g) \
         AND ($notsp_l OR b.timestamp < $?tsp_l) \
         AND ($novalpass_e OR b.validation_pass = $?valpass_e) \
         AND ($novalpass_g OR b.validation_pass > $?valpass_g) \
         AND ($novalpass_l OR b.validation_pass < $?valpass_l) \
         AND ($nocycle_pos_e OR b.cycle_position = $?cycle_pos_e) \
         AND ($nocycle_pos_g OR b.cycle_position > $?cycle_pos_g) \
         AND ($nocycle_pos_l OR b.cycle_position < $?cycle_pos_l) \
         AND ($nonendo_e OR b.endorsements_included = $?nendo_e) \
         AND ($nonendo_g OR b.endorsements_included > $?nendo_g) \
         AND ($nonendo_l OR b.endorsements_included < $?nendo_l) \
         AND ($nonetwork OR b.network SIMILAR TO $?network) \
         AND ($noprotocol OR b.protocol SIMILAR TO $?protocol) \
         AND ($nosignature OR b.signature SIMILAR TO $?signature) \
         AND ($nonetworkd OR b.network <> $?networkd) \
         AND ($noprotocold OR b.protocol <> $?protocold) \
         AND ($nosignatured OR b.signature <> $?signatured) \
         AND (b.distance_level = $distance_level) \
         ORDER BY b.level DESC \
         OFFSET $offset LIMIT $limit"] >>= fun rows ->
    return @@ List.map Pg_helper.block_with_pred_fitness rows

  let nb_transactions_search ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dst
      ~amount ~burn ~hasparam ~collect_call =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l, internal, failed,
       fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
       dst, dstd, amount_e, amount_g, amount_l, burn_e, burn_g, burn_l, hasparam,
       collect_call),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l, nofailed, nointernal,
       nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
       nodst, nodstd, noamount_e, noamount_g, noamount_l, noburn_e, noburn_g, noburn_l,
       nohasparam, nocollect_call) =
      Pg_helper.boolean_for_transactions_search
        ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dst ~amount
        ~burn ~hasparam ~collect_call in
    let distance_level = Int32.of_int distance_level in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM transaction_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nodst OR destination SIMILAR TO $?dst) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nodstd OR destination <> $?dstd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nofee_e OR fee = $?fee_e) \
       AND ($nofee_g OR fee > $?fee_g) \
       AND ($nofee_l OR fee < $?fee_l) \
       AND ($noamount_e OR amount = $?amount_e) \
       AND ($noamount_g OR amount > $?amount_g) \
       AND ($noamount_l OR amount < $?amount_l) \
       AND ($nocounter_e OR counter = $?counter_e) \
       AND ($nocounter_g OR counter > $?counter_g) \
       AND ($nocounter_l OR counter < $?counter_l) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND ($nointernal OR internal = $?internal) \
       AND ($nofailed OR failed = $?failed) \
       AND ($noburn_e OR burn_dun = $?burn_e) \
       AND ($noburn_g OR burn_dun > $?burn_g) \
       AND ($noburn_l OR burn_dun < $?burn_l) \
       AND ($nohasparam OR (parameters IS NOT NULL) = $?hasparam) \
       AND ($nocollect_call OR (collect_fee_gas IS NOT NULL) = $?collect_call) \
       AND distance_level = $distance_level"]
    >>= of_count_opt

  let transactions_search ?(page=0) ?(page_size=20) ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dst
      ~amount ~burn ~hasparam ~collect_call =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l, internal, failed,
       fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
       dst, dstd, amount_e, amount_g, amount_l, burn_e, burn_g, burn_l, hasparam,
       collect_call),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l, nofailed, nointernal,
       nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
       nodst, nodstd, noamount_e, noamount_g, noamount_l, noburn_e, noburn_g, noburn_l,
       nohasparam, nocollect_call) =
      Pg_helper.boolean_for_transactions_search
        ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dst ~amount
        ~burn ~hasparam ~collect_call in
    let offset, limit = limits page page_size in
    let distance_level = Int32.of_int distance_level in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, destination, fee, counter, \
       amount, parameters, gas_limit, storage_limit, failed, internal, burn_dun, \
       op_level, timestamp_block as tsp, errors, collect_fee_gas, collect_pk \
       FROM transaction_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nodst OR destination SIMILAR TO $?dst) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nodstd OR destination <> $?dstd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nofee_e OR fee = $?fee_e) \
       AND ($nofee_g OR fee > $?fee_g) \
       AND ($nofee_l OR fee < $?fee_l) \
       AND ($noamount_e OR amount = $?amount_e) \
       AND ($noamount_g OR amount > $?amount_g) \
       AND ($noamount_l OR amount < $?amount_l) \
       AND ($nocounter_e OR counter = $?counter_e) \
       AND ($nocounter_g OR counter > $?counter_g) \
       AND ($nocounter_l OR counter < $?counter_l) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND ($nofailed OR failed = $?failed) \
       AND ($nointernal OR internal = $?internal) \
       AND ($noburn_e OR burn_dun = $?burn_e) \
       AND ($noburn_g OR burn_dun > $?burn_g) \
       AND ($noburn_l OR burn_dun < $?burn_l) \
       AND ($nohasparam OR (parameters IS NOT NULL) = $?hasparam) \
       AND ($nocollect_call OR (collect_fee_gas IS NOT NULL) = $?collect_call) \
       AND distance_level = $distance_level \
       ORDER BY op_level DESC, hash, counter LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.transaction_from_db_list

  let nb_activations_search ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, _srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l),
      (nohash, nohashd, nosrc, _nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l), _ =
      Pg_helper.boolean_for_global_operations_search
        ~hash ~src ~bhash ~level ~tsp ?cycle:None in
    let distance_level = Int32.of_int distance_level in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM activation_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nosrc OR pkh SIMILAR TO $?src) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND distance_level = $distance_level"]
    >>= of_count_opt

  let activations_search ?(page=0) ?(page_size=20) ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, _srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l),
      (nohash, nohashd, nosrc, _nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l), _ =
      Pg_helper.boolean_for_global_operations_search
        ~hash ~src ~bhash ~level ~tsp ?cycle:None in
    let distance_level = Int32.of_int distance_level in
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, pkh, secret, balance, \
       op_level, timestamp_block as tsp \
       FROM activation_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR pkh SIMILAR TO $?src) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND distance_level = $distance_level \
       ORDER BY op_level DESC LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.activation_from_db_list

  let nb_reveals_search ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l, failed, internal,
       fee_e, fee_g, fee_l, counter_e, counter_g, counter_l),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l, nofailed, nointernal,
       nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l) =
      Pg_helper.boolean_for_manager_operations_search
        ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter in
    let distance_level = Int32.of_int distance_level in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM reveal_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nofee_e OR fee = $?fee_e) \
       AND ($nofee_g OR fee > $?fee_g) \
       AND ($nofee_l OR fee < $?fee_l) \
       AND ($nocounter_e OR counter = $?counter_e) \
       AND ($nocounter_g OR counter > $?counter_g) \
       AND ($nocounter_l OR counter < $?counter_l) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND ($nointernal OR internal = $?internal) \
       AND ($nofailed OR failed = $?failed) \
       AND distance_level = $distance_level"]
    >>= of_count_opt

  let reveals_search ?(page=0) ?(page_size=20) ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l, failed, internal,
       fee_e, fee_g, fee_l, counter_e, counter_g, counter_l),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l, nofailed, nointernal,
       nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l) =
      Pg_helper.boolean_for_manager_operations_search
        ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter in
    let distance_level = Int32.of_int distance_level in
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, \
       pubkey, gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM reveal_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nofee_e OR fee = $?fee_e) \
       AND ($nofee_g OR fee > $?fee_g) \
       AND ($nofee_l OR fee < $?fee_l) \
       AND ($nocounter_e OR counter = $?counter_e) \
       AND ($nocounter_g OR counter > $?counter_g) \
       AND ($nocounter_l OR counter < $?counter_l) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND ($nointernal OR internal = $?internal) \
       AND ($nofailed OR failed = $?failed) \
       AND distance_level = $distance_level \
       ORDER BY op_level DESC, hash, counter LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.reveal_from_db_list

  let nb_delegations_search ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dlg =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l, failed, internal,
       fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
       dlg, dlgd),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l, nofailed, nointernal,
       nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
       nodlg, nodlgd) =
      Pg_helper.boolean_for_delegations_search
        ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dlg in
    let distance_level = Int32.of_int distance_level in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM delegation_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nodlg OR source SIMILAR TO $?dlg) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nodlgd OR source <> $?dlgd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nofee_e OR fee = $?fee_e) \
       AND ($nofee_g OR fee > $?fee_g) \
       AND ($nofee_l OR fee < $?fee_l) \
       AND ($nocounter_e OR counter = $?counter_e) \
       AND ($nocounter_g OR counter > $?counter_g) \
       AND ($nocounter_l OR counter < $?counter_l) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND ($nointernal OR internal = $?internal) \
       AND ($nofailed OR failed = $?failed) \
       AND distance_level = $distance_level"]
    >>= of_count_opt

  let delegations_search ?(page=0) ?(page_size=20) ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dlg =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l, failed, internal,
       fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
       dlg, dlgd),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l, nofailed, nointernal,
       nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
       nodlg, nodlgd) =
      Pg_helper.boolean_for_delegations_search
        ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dlg in
    let distance_level = Int32.of_int distance_level in
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, fee, counter, \
       delegate, gas_limit, storage_limit, failed, internal, \
       op_level, timestamp_block as tsp, errors \
       FROM delegation_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nodlg OR source SIMILAR TO $?dlg) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nodlgd OR source <> $?dlgd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nofee_e OR fee = $?fee_e) \
       AND ($nofee_g OR fee > $?fee_g) \
       AND ($nofee_l OR fee < $?fee_l) \
       AND ($nocounter_e OR counter = $?counter_e) \
       AND ($nocounter_g OR counter > $?counter_g) \
       AND ($nocounter_l OR counter < $?counter_l) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND ($nointernal OR internal = $?internal) \
       AND ($nofailed OR failed = $?failed) \
       AND distance_level = $distance_level \
       ORDER BY op_level DESC, hash, counter LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.delegation_from_db_list

  let nb_endorsements_search ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp =
    let level = { cstt_l = Option.map Int64.of_int level.cstt_l;
                  cstt_g = Option.map Int64.of_int level.cstt_g;
                  cstt_e = Option.map Int64.of_int level.cstt_e } in
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l,
       notsp_e, notsp_g, notsp_l),
      cycle_constraints =
      Pg_helper.boolean_for_global_operations_search
        ~hash ~src ~bhash ~level ~cycle ~tsp in
    let distance_level = Int32.of_int distance_level in
    let cycle_e, cycle_g, cycle_l, nocycle_e, nocycle_g, nocycle_l = match cycle_constraints with
      | None -> None, None, None, false, false, false
      | Some x -> x in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM endorsement_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($nocycle_e OR op_cycle = $?cycle_e) \
       AND ($nocycle_g OR op_cycle > $?cycle_g) \
       AND ($nocycle_l OR op_cycle < $?cycle_l) \
       AND ($notsp_e OR timestamp = $?tsp_e) \
       AND ($notsp_g OR timestamp > $?tsp_g) \
       AND ($notsp_l OR timestamp < $?tsp_l) \
       AND distance_level = $distance_level"]
    >>= of_count_opt

  let endorsements_search ?(page=0) ?(page_size=20) ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp =
    let level = { cstt_l = Option.map Int64.of_int level.cstt_l;
                  cstt_g = Option.map Int64.of_int level.cstt_g;
                  cstt_e = Option.map Int64.of_int level.cstt_e } in
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l,
       notsp_e, notsp_g, notsp_l),
      cycle_constraints =
      Pg_helper.boolean_for_global_operations_search
        ~hash ~src ~bhash ~level ~cycle ~tsp in
    let distance_level = Int32.of_int distance_level in
    let offset, limit = limits page page_size in
    let cycle_e, cycle_g, cycle_l, nocycle_e, nocycle_g, nocycle_l = match cycle_constraints with
      | None -> None, None, None, false, false, false
      | Some x -> x in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, block_hash, slots, \
       block_level, op_level, priority, timestamp \
       FROM endorsement_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($nocycle_e OR op_cycle = $?cycle_e) \
       AND ($nocycle_g OR op_cycle > $?cycle_g) \
       AND ($nocycle_l OR op_cycle < $?cycle_l) \
       AND ($notsp_e OR timestamp = $?tsp_e) \
       AND ($notsp_g OR timestamp > $?tsp_g) \
       AND ($notsp_l OR timestamp < $?tsp_l) \
       AND distance_level = $distance_level \
       ORDER BY op_level DESC, hash LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.endorsement_from_db_list

  let nb_originations_search ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dlg ~manager
      ~orikt1 ~balance ~burn =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l, failed, internal,
       fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
       dlg, dlgd, manager, orikt1, balance_e, balance_g, balance_l, burn_e, burn_g, burn_l),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l, nofailed, nointernal,
       nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
       nodlg, nodlgd, nomanager, noorikt1, nobalance_e, nobalance_g, nobalance_l,
       noburn_e, noburn_g, noburn_l) =
      Pg_helper.boolean_for_originations_search
        ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dlg ~manager
        ~orikt1 ~balance ~burn in
    let distance_level = Int32.of_int distance_level in
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(*) FROM origination_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nodlg OR delegate SIMILAR TO $?dlg) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nodlgd OR delegate <> $?dlgd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nomanager OR manager = $?manager) \
       AND ($noorikt1 OR kt1 = $?orikt1) \
       AND ($nofee_e OR fee = $?fee_e) \
       AND ($nofee_g OR fee > $?fee_g) \
       AND ($nofee_l OR fee < $?fee_l) \
       AND ($nocounter_e OR counter = $?counter_e) \
       AND ($nocounter_g OR counter > $?counter_g) \
       AND ($nocounter_l OR counter < $?counter_l) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND ($nobalance_e OR balance = $?balance_e) \
       AND ($nobalance_g OR balance > $?balance_g) \
       AND ($nobalance_l OR balance < $?balance_l) \
       AND ($nointernal OR internal = $?internal) \
       AND ($nofailed OR failed = $?failed) \
       AND ($noburn_e OR burn_dun = $?burn_e) \
       AND ($noburn_g OR burn_dun > $?burn_g) \
       AND ($noburn_l OR burn_dun < $?burn_l) \
       AND distance_level = $distance_level"]
    >>= of_count_opt

  let originations_search ?(page=0) ?(page_size=20) ?(distance_level=0)
      ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dlg ~manager
      ~orikt1 ~balance ~burn =
    level_constraint ~cycle ~level >>= fun level ->
    let
      (hash, hashd, src, srcd, bhash, bhashd,
       level_e, level_g, level_l, tsp_e, tsp_g, tsp_l, failed, internal,
       fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
       dlg, dlgd, manager, orikt1, balance_e, balance_g, balance_l,
       burn_e, burn_g, burn_l),
      (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
       nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l, nofailed, nointernal,
       nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
       nodlg, nodlgd, nomanager, noorikt1, nobalance_e, nobalance_g, nobalance_l,
       noburn_e, noburn_g, noburn_l) =
      Pg_helper.boolean_for_originations_search
        ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dlg ~manager
        ~orikt1 ~balance ~burn in
    let distance_level = Int32.of_int distance_level in
    let offset, limit = limits page page_size in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
      "SELECT hash, op_block_hash, network, source, kt1, fee, counter, \
       manager, delegate, script_code as code, script_storage_type as storage, \
       script_code_hash as code_hash, spendable, \
       delegatable, balance, gas_limit, storage_limit, failed, internal, burn_dun, \
       op_level, timestamp_block as tsp, errors \
       FROM origination_all WHERE \
       ($nohash OR hash SIMILAR TO $?hash) \
       AND ($nosrc OR source SIMILAR TO $?src) \
       AND ($nodlg OR delegate SIMILAR TO $?dlg) \
       AND ($nobhash OR op_block_hash SIMILAR TO $?bhash) \
       AND ($nohashd OR hash <> $?hashd) \
       AND ($nosrcd OR source <> $?srcd) \
       AND ($nodlgd OR delegate <> $?dlgd) \
       AND ($nobhashd OR op_block_hash <> $?bhashd) \
       AND ($nomanager OR manager = $?manager) \
       AND ($noorikt1 OR kt1 = $?orikt1) \
       AND ($nofee_e OR fee = $?fee_e) \
       AND ($nofee_g OR fee > $?fee_g) \
       AND ($nofee_l OR fee < $?fee_l) \
       AND ($nocounter_e OR counter = $?counter_e) \
       AND ($nocounter_g OR counter > $?counter_g) \
       AND ($nocounter_l OR counter < $?counter_l) \
       AND ($nolevel_e OR op_level = $?level_e) \
       AND ($nolevel_g OR op_level > $?level_g) \
       AND ($nolevel_l OR op_level < $?level_l) \
       AND ($notsp_e OR timestamp_op = $?tsp_e) \
       AND ($notsp_g OR timestamp_op > $?tsp_g) \
       AND ($notsp_l OR timestamp_op < $?tsp_l) \
       AND ($nobalance_e OR balance = $?balance_e) \
       AND ($nobalance_g OR balance > $?balance_g) \
       AND ($nobalance_l OR balance < $?balance_l) \
       AND ($nointernal OR internal = $?internal) \
       AND ($nofailed OR failed = $?failed) \
       AND ($noburn_e OR burn_dun = $?burn_e) \
       AND ($noburn_g OR burn_dun > $?burn_g) \
       AND ($noburn_l OR burn_dun < $?burn_l) \
       AND distance_level = $distance_level \
       ORDER BY op_level DESC, hash, counter LIMIT $limit OFFSET $offset"]
    >|= Pg_helper.origination_from_db_list

  let nb_accounts_search
      ~hash ~dlg ~bblock ~baked ~balance ~sbalance ~fbalance ~rewards ~fees
      ~deposits ~delegated ~revealed ~originated =
    let
      (hash, hashd, dlg, dlgd, bblock, bblockd,
       baked_e, baked_g, baked_l,
       balance_e, balance_g, balance_l, sbalance_e, sbalance_g, sbalance_l,
       fbalance_e, fbalance_g, fbalance_l, rewards_e, rewards_g, rewards_l,
       fees_e, fees_g, fees_l, deposits_e, deposits_g, deposits_l,
       delegated, revealed, originated),
      (nohash, nohashd, nodlg, nodlgd, nobblock, nobblockd,
       nobaked_e, nobaked_g, nobaked_l,
       nobalance_e, nobalance_g, nobalance_l, nosbalance_e, nosbalance_g, nosbalance_l,
       nofbalance_e, nofbalance_g, nofbalance_l, norewards_e, norewards_g, norewards_l,
       nofees_e, nofees_g, nofees_l, nodeposits_e, nodeposits_g, nodeposits_l,
       nodelegated, norevealed, nooriginated) =
      Pg_helper.boolean_for_accounts_search
        ~hash ~dlg ~bblock ~baked ~balance ~sbalance ~fbalance ~rewards ~fees
        ~deposits ~delegated ~revealed ~originated in
    ignore(sbalance_e, sbalance_g, sbalance_l, nosbalance_e, nosbalance_g, nosbalance_l);
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT COUNT(DISTINCT t.hash) \
       FROM dune_user AS t \
       INNER JOIN balance_snapshot AS bs ON t.hash = bs.hash \
       INNER JOIN account_info AS ai ON t.hash = ai.hash \
       INNER JOIN block AS b ON (($nobblock AND $nobblockd AND baker = 'God') OR \
       (b.baker = t.hash AND b.hash SIMILAR TO $?bblock) OR\
       (b.baker = t.hash AND b.hash <> $?bblockd)) \
       WHERE ($nohash OR t.hash SIMILAR TO $?hash) \
       AND ($nodlg OR ai.delegate SIMILAR TO $?dlg) \
       AND ($nohashd OR t.hash <> $?hashd) \
       AND ($nodlgd OR ai.delegate <> $?dlgd) \
       AND ($nobaked_e OR b.level = $?baked_e) \
       AND ($nobaked_g OR b.level > $?baked_g) \
       AND ($nobaked_l OR b.level < $?baked_l) \
       AND ($nobalance_e OR spendable_balance = $?balance_e) \
       AND ($nobalance_g OR spendable_balance > $?balance_g) \
       AND ($nobalance_l OR spendable_balance < $?balance_l) \
       AND ($nofbalance_e OR frozen = $?fbalance_e) \
       AND ($nofbalance_g OR frozen > $?fbalance_g) \
       AND ($nofbalance_l OR frozen < $?fbalance_l) \
       AND ($norewards_e OR rewards = $?rewards_e) \
       AND ($norewards_g OR rewards > $?rewards_g) \
       AND ($norewards_l OR rewards < $?rewards_l) \
       AND ($nofees_e OR bs.fees = $?fees_e) \
       AND ($nofees_g OR bs.fees > $?fees_g) \
       AND ($nofees_l OR bs.fees < $?fees_l) \
       AND ($nodeposits_e OR deposits = $?deposits_e) \
       AND ($nodeposits_g OR deposits > $?deposits_g) \
       AND ($nodeposits_l OR deposits < $?deposits_l) \
       AND ($nodelegated OR (ai.delegate IS NOT NULL) = $?delegated) \
       AND ($norevealed OR (ai.reveal IS NOT NULL) = $?revealed) \
       AND ($nooriginated OR (ai.origination IS NOT NULL) = $?originated)"]
    >>= of_count_opt

  let accounts_search ?(page=0) ?(page_size=0)
      ~hash ~dlg ~bblock ~baked ~balance ~sbalance ~fbalance ~rewards ~fees
      ~deposits ~delegated ~revealed ~originated =
    let
      (hash, hashd, dlg, dlgd, bblock, bblockd,
       baked_e, baked_g, baked_l,
       balance_e, balance_g, balance_l, sbalance_e, sbalance_g, sbalance_l,
       fbalance_e, fbalance_g, fbalance_l, rewards_e, rewards_g, rewards_l,
       fees_e, fees_g, fees_l, deposits_e, deposits_g, deposits_l,
       delegated, revealed, originated),
      (nohash, nohashd, nodlg, nodlgd, nobblock, nobblockd,
       nobaked_e, nobaked_g, nobaked_l,
       nobalance_e, nobalance_g, nobalance_l, nosbalance_e, nosbalance_g, nosbalance_l,
       nofbalance_e, nofbalance_g, nofbalance_l, norewards_e, norewards_g, norewards_l,
       nofees_e, nofees_g, nofees_l, nodeposits_e, nodeposits_g, nodeposits_l,
       nodelegated, norevealed, nooriginated) =
      Pg_helper.boolean_for_accounts_search
        ~hash ~dlg ~bblock ~baked ~balance ~sbalance ~fbalance ~rewards ~fees
        ~deposits ~delegated ~revealed ~originated in
    let offset, limit = limits page page_size in
    ignore(sbalance_e, sbalance_g, sbalance_l, nosbalance_e, nosbalance_g, nosbalance_l);
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "nullable-results"
      "SELECT DISTINCT t.hash, alias, manager, delegate, spendable, delegatable, \
       origination, spendable_balance, frozen, rewards, bs.fees, deposits \
       FROM dune_user AS t \
       INNER JOIN balance_snapshot AS bs ON t.hash = bs.hash \
       INNER JOIN block AS b ON (($nobblock AND $nobblockd AND baker = 'God') OR \
       (b.baker = t.hash AND b.hash SIMILAR TO $?bblock) OR\
       (b.baker = t.hash AND b.hash <> $?bblockd)) \
       LEFT JOIN account_info AS ai ON t.hash = ai.hash \
       WHERE ($nohash OR t.hash SIMILAR TO $?hash) \
       AND ($nodlg OR ai.delegate SIMILAR TO $?dlg) \
       AND ($nohashd OR t.hash <> $?hashd) \
       AND ($nodlgd OR ai.delegate <> $?dlgd) \
       AND ($nobaked_e OR b.level = $?baked_e) \
       AND ($nobaked_g OR b.level > $?baked_g) \
       AND ($nobaked_l OR b.level < $?baked_l) \
       AND ($nobalance_e OR spendable_balance = $?balance_e) \
       AND ($nobalance_g OR spendable_balance > $?balance_g) \
       AND ($nobalance_l OR spendable_balance < $?balance_l) \
       AND ($nofbalance_e OR frozen = $?fbalance_e) \
       AND ($nofbalance_g OR frozen > $?fbalance_g) \
       AND ($nofbalance_l OR frozen < $?fbalance_l) \
       AND ($norewards_e OR rewards = $?rewards_e) \
       AND ($norewards_g OR rewards > $?rewards_g) \
       AND ($norewards_l OR rewards < $?rewards_l) \
       AND ($nofees_e OR bs.fees = $?fees_e) \
       AND ($nofees_g OR bs.fees > $?fees_g) \
       AND ($nofees_l OR bs.fees < $?fees_l) \
       AND ($nodeposits_e OR deposits = $?deposits_e) \
       AND ($nodeposits_g OR deposits > $?deposits_g) \
       AND ($nodeposits_l OR deposits < $?deposits_l) \
       AND ($nodelegated OR (ai.delegate IS NOT NULL) = $?delegated) \
       AND ($norevealed OR (ai.reveal IS NOT NULL) = $?revealed) \
       AND ($nooriginated OR (ai.origination IS NOT NULL) = $?originated) \
       ORDER BY spendable_balance DESC, t.hash OFFSET $offset LIMIT $limit"]
    >>= fun rows ->
    return @@ List.map Pg_helper.account_from_db rows

  let token_info contract =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT contract, name, symbol, total_supply, decimals, version, language \
       FROM token_info WHERE contract = $contract"] >>= function
    | [ row ] -> return (Some (Pg_helper.token_info_from_db row))
    | _ -> return None

  let tokens () =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT contract, name, symbol, total_supply, decimals, version, language \
       FROM token_info ORDER BY symbol"] >>= fun rows ->
    return (List.map Pg_helper.token_info_from_db rows)

  let account_baker_version hash =
    with_dbh >>> fun dbh ->
    [%pgsql dbh
      "SELECT pow_nonce FROM block WHERE baker = $hash ORDER BY level DESC LIMIT 1"]
    >|= function
    | [ nonce ] -> Some (Infos.baker_version nonce)
    | _ -> None

  let chain ?(length=5) level =
    let d, u = Int64.of_int @@ level - length / 2, Int64.of_int @@ level + length / 2 in
    with_dbh >>> fun dbh ->
    [%pgsql.object dbh
        "SELECT * FROM block WHERE level >= $d AND level <= $u ORDER BY level DESC"] >|= fun rows ->
    List.map Pg_helper.block_noop rows

    (* let level_acc, acc = List.fold_left (fun (acc_level, acc) b ->
     *     match acc_level with
     *     | None -> Some (b.level, [ b.hash, b.priority, b.endorsements_included, b.distance_level ]), acc
     *     | Some (level, l) when b.level = level ->
     *       Some (level, (b.hash, b.priority, b.endorsements_included, b.distance_level) :: l), acc
     *     | Some x ->
     *       Some (b.level, [ b.hash, b.priority, b.endorsements_included, b.distance_level ]),
     *       x :: acc) (None, []) l in
     * match level_acc with
     * | None -> return acc
     * | Some x -> return (x :: acc) *)


end
