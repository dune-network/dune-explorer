(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types
open Language

let debug fmt = Utils.debug !Debug_constants.debug_writer fmt

let dbh = PGOCaml.connect ~database:DunscanConfig.database ()

let () = EzPG.upgrade_database dbh ~upgrades: Pg_update.upgrades

let pg_lock_r = ref false
let pg_lock f =
  if !pg_lock_r
  then f ()
  else
    begin
      pg_lock_r := true;
      [%pgsql dbh "BEGIN"];
      f ();
      [%pgsql dbh "COMMIT"];
      pg_lock_r := false
    end

let included s =
  match Hashtbl.find_opt Config.crawler_flags s with
  | None -> false
  | Some flag -> flag

let head () =
  [%pgsql.object dbh
      "SELECT * FROM block \
       WHERE distance_level = 0 ORDER BY level DESC LIMIT 1"]
  |> Pg_helper.rows_to_option
  |> Option.map Pg_helper.block_noop

let register_cycle_limits ?(force=false) cycle level_start level_end =
  match [%pgsql dbh "SELECT cycle FROM cycle_limits WHERE cycle = $cycle"] with
  | [] ->
    [%pgsql dbh
      "INSERT INTO cycle_limits(cycle, level_start, level_end) \
       VALUES($cycle, $level_start, $level_end)"]
  | _ when force ->
    [%pgsql dbh
      "UPDATE cycle_limits SET level_start = $level_start, level_end = $level_end \
       WHERE cycle = $cycle"]
  | _ -> ()

let cycle_from_level level =
  match
    [%pgsql dbh "SELECT cycle FROM cycle_limits \
                WHERE $level BETWEEN level_start AND level_end"] with
  | cycle :: _ -> cycle
  | _ -> assert false

let is_block_registered hash =
  let row = [%pgsql dbh "SELECT hash FROM block WHERE hash = $hash"] in
  match row with
  | [ _hash ] -> true
  | _ -> false

let is_new_protocol = function
  | None -> false
  | Some protocol ->
    match [%pgsql dbh "SELECT hash FROM protocol WHERE hash = $protocol"] with
    | [] -> true
    | _ -> false

let register_dune_user hash =
  let insert hash contract =
    [%pgsql dbh "INSERT INTO dune_user (hash, contract) \
                 VALUES ($hash, $contract) ON CONFLICT (hash) DO NOTHING"];
    [%pgsql dbh "INSERT INTO operation_count_user(dn) VALUES($hash) \
                 ON CONFLICT DO NOTHING"];
    if included "account_info" then
      [%pgsql dbh "INSERT INTO account_info(hash, manager) VALUES($hash, $hash) \
                   ON CONFLICT DO NOTHING"] in
  let update hash edpk contract =
    [%pgsql dbh "INSERT INTO dune_user (hash,contract,edpk) \
                VALUES ($hash,$contract,$edpk) ON CONFLICT (hash) \
                DO UPDATE SET hash=$hash,contract=$contract,edpk=$edpk"] in
  debug "[Writer] Registering user %s\n%!" hash ;
  if hash = "" || String.length hash = 1 then insert hash false (* HACK *)
  else
    begin match String.sub hash 0 2 with
      | "tz" | "dn" -> insert hash false
      | "TZ" | "KT" -> insert hash true
      | "ed" -> update (Crypto.pk_to_dn1 hash) hash false
      | _ -> debug "[Writer] invalid user hash %S\n%!" hash end

let register_cycle_count_baker cycle hash =
  if included "counts" then
    if [%pgsql dbh "SELECT cycle, dn FROM cycle_count_baker WHERE cycle = $cycle \
                    AND dn = $hash"] = [] then
      [%pgsql dbh "INSERT INTO cycle_count_baker(cycle, dn) VALUES($cycle, $hash)"]

let update_originated_contract ?origination kt1 delegate manager spendable delegatable =
  if included "account_info" then
    [%pgsql dbh
        "UPDATE account_info SET \
         delegate = $?delegate, manager = $manager, spendable = $spendable, \
         delegatable = $delegatable, origination = $?origination \
         WHERE hash = $kt1"]

let register_header header =
  let shell = header.header_shell in
  let level = Int64.of_int shell.shell_level in
  let proto = Int64.of_int shell.shell_proto in
  let predecessor = shell.shell_predecessor in
  let timestamp = Pg_helper.cal_of_date_dune shell.shell_timestamp in
  let validation_pass = Int64.of_int shell.shell_validation_pass in
  let operations_hash = shell.shell_operations_hash in
  let fitness = shell.shell_fitness in
  let context = shell.shell_context in
  let priority = Int32.of_int header.header_priority in
  let seed = header.header_seed_nonce_hash in
  let nonce = header.header_proof_of_work_nonce in
  let signature = header.header_signature in
  let row =
    [%pgsql dbh
      "SELECT * FROM header WHERE \
       level = $level AND \
       priority = $priority AND \
       fitness = $fitness AND \
       context = $context AND \
       signature = $signature"] in
  match row with
  | [] ->
    [%pgsql dbh
      "INSERT INTO header \
       (level, proto, predecessor, timestamp, validation_pass, \
       operations_hash, fitness, context, priority, \
       commited_nonce_hash, pow_nonce, signature) \
       VALUES \
       ($level, $proto, $predecessor, $timestamp, $validation_pass, \
       $operations_hash, $fitness, $context, $priority, \
       $seed, $nonce, $signature) \
       ON CONFLICT DO NOTHING"]
  | _ -> ()

let register_constants ?(distance=(-1)) ?protocol ?block_hash level
    { proof_of_work_nonce_size; nonce_length; max_revelations_per_block;
      max_operation_data_length; preserved_cycles; blocks_per_cycle;
      blocks_per_commitment; blocks_per_roll_snapshot;
      blocks_per_voting_period; time_between_blocks; endorsers_per_block;
      hard_gas_limit_per_operation; hard_gas_limit_per_block;
      proof_of_work_threshold; tokens_per_roll; michelson_maximum_type_size;
      seed_nonce_revelation_tip; origination_burn; block_security_deposit;
      endorsement_security_deposit; block_reward; endorsement_reward;
      cost_per_byte; hard_storage_limit_per_operation;
      max_proposals_per_delegate; test_chain_duration;
      hard_gas_limit_to_pay_fees; max_operation_ttl; protocol_revision;
      frozen_account_cycles; allow_collect_call;
      quorum_min; quorum_max; min_proposal_quorum;
      initial_endorsers; delay_per_missing_endorsement } =
  match block_hash with
  | Some block_hash ->
    let distance = Int32.of_int distance in
    let level = Int64.of_int level in
    let proof_of_work_nonce_size = Int32.of_int proof_of_work_nonce_size in
    let nonce_length = Int32.of_int nonce_length in
    let max_revelations_per_block = Int32.of_int max_revelations_per_block in
    let max_operation_data_length = Int32.of_int max_operation_data_length in
    let preserved_cycles = Int32.of_int preserved_cycles in
    let blocks_per_cycle = Int32.of_int blocks_per_cycle in
    let blocks_per_commitment = Int32.of_int blocks_per_commitment in
    let blocks_per_roll_snapshot = Int32.of_int blocks_per_roll_snapshot in
    let blocks_per_voting_period = Int32.of_int blocks_per_voting_period in
    let endorsers_per_block = Int32.of_int endorsers_per_block in
    let michelson_maximum_type_size = Int32.of_int michelson_maximum_type_size in
    let max_proposals_per_delegate = Option.map Int32.of_int max_proposals_per_delegate in
    let time_between_blocks = List.map (fun i -> Some (Int32.of_int i)) time_between_blocks in
    let max_operation_ttl = Int32.of_int max_operation_ttl in
    let protocol_revision = Option.map Int32.of_int protocol_revision in
    let frozen_account_cycles = Option.map Int32.of_int frozen_account_cycles in
    let quorum_min = Option.map Int32.of_int quorum_min in
    let quorum_max = Option.map Int32.of_int quorum_max in
    let min_proposal_quorum = Option.map Int32.of_int min_proposal_quorum in
    let initial_endorsers = Option.map Int32.of_int initial_endorsers in
    let delay_per_missing_endorsement = Option.map Int32.of_int delay_per_missing_endorsement in
    [%pgsql dbh
      "INSERT INTO protocol_constants ( \
       level_start, protocol, block_hash, distance_level, \
       proof_of_work_nonce_size, nonce_length, \
       max_revelations_per_block, max_operation_data_length, preserved_cycles, \
       blocks_per_cycle, blocks_per_commitment, blocks_per_roll_snapshot, \
       blocks_per_voting_period, time_between_blocks, endorsers_per_block, \
       hard_gas_limit_per_operation, hard_gas_limit_per_block, proof_of_work_threshold, \
       tokens_per_roll, michelson_maximum_type_size, seed_nonce_revelation_tip, \
       origination_burn, block_security_deposit, endorsement_security_deposit, \
       block_reward, endorsement_reward, cost_per_byte, hard_storage_limit_per_operation, \
       max_proposals_per_delegate, test_chain_duration, hard_gas_limit_to_pay_fees, \
       max_operation_ttl, protocol_revision, frozen_account_cycles, allow_collect_call, \
       quorum_min, quorum_max, min_proposal_quorum, \
       initial_endorsers, delay_per_missing_endorsement) \
       VALUES($level, $?protocol, $block_hash, $distance, \
       $proof_of_work_nonce_size, $nonce_length, \
       $max_revelations_per_block, $max_operation_data_length, \
       $preserved_cycles, $blocks_per_cycle, $blocks_per_commitment, \
       $blocks_per_roll_snapshot, $blocks_per_voting_period, \
       $time_between_blocks, $endorsers_per_block, \
       $hard_gas_limit_per_operation, $hard_gas_limit_per_block, \
       $proof_of_work_threshold, $tokens_per_roll, \
       $michelson_maximum_type_size, $seed_nonce_revelation_tip, \
       $origination_burn, $block_security_deposit, \
       $endorsement_security_deposit, $block_reward, $endorsement_reward, \
       $cost_per_byte, $hard_storage_limit_per_operation, \
       $?max_proposals_per_delegate, $?test_chain_duration, \
       $hard_gas_limit_to_pay_fees, $max_operation_ttl, $?protocol_revision, \
       $?frozen_account_cycles, $?allow_collect_call, $?quorum_min, $?quorum_max, \
       $?min_proposal_quorum, $?initial_endorsers, $?delay_per_missing_endorsement) \
       ON CONFLICT DO NOTHING"]
  | _ -> ()

let register_protocol ?(distance=(-1L)) ?constants ?block_hash level proto =
  let new_proto =
    match [%pgsql dbh "SELECT hash FROM protocol WHERE hash = $proto"] with
    | [] -> true
    | _ -> false in
  if new_proto then (
    [%pgsql dbh "INSERT INTO protocol (hash, name) VALUES ($proto, $proto)"];
    (match [%pgsql dbh "SELECT level_start FROM protocol_constants \
                       WHERE distance_level = 0 \
                       ORDER BY level_start DESC LIMIT 1"] with
    | [] -> ()
    | previous_level_start :: _ ->
      let level = Int64.of_int level in
      [%pgsql dbh "UPDATE protocol_constants SET level_end = $level - 1::bigint \
                  WHERE level_start = $previous_level_start AND distance_level = 0"]);
    match constants with
    | None -> ()
    | Some constants ->
      constants level (fun constants ->
          let distance = Int64.to_int distance in
          register_constants ~distance ~protocol:proto ?block_hash level constants))

(* blocks *)

let register_block ?constants
    block level distance operation_count volume fees endorsements_included =
  let hash = block.node_hash in
  let header = block.node_header in
  let shell = header.header_shell in
  let predecessor_hash = shell.shell_predecessor in
  let fitness = shell.shell_fitness in
  let baker = block.node_metadata.meta_header.header_meta_baker in
  let timestamp = Pg_helper.cal_of_date_dune shell.shell_timestamp in
  let validation_pass = shell.shell_validation_pass in
  let protocol = block.node_protocol in
  let test_protocol = block.node_protocol in
  let network = block.node_chain_id in
  let test_network, test_network_expiration =
    match block.node_metadata.meta_test_chain_status with
    | CSNot_running -> "Not running", "Not running"
    | CSForking { protocol ; expiration } ->
      Printf.sprintf "Forking %S" protocol,
      expiration
    | CSRunning { chain_id ; genesis ; protocol ; expiration } ->
      Printf.sprintf "Running [%S][%S][%S]" chain_id genesis protocol,
      expiration in
  let priority  = Int32.of_int header.header_priority  in
  let commited_nonce_hash  = header.header_seed_nonce_hash in
  let pow_nonce = header.header_proof_of_work_nonce in
  let proto = shell.shell_proto in
  let data = "--" in            (* TODO remove data field *)
  let signature = header.header_signature in
  let protocol_hash = protocol in
  let test_protocol_hash = protocol in
  let validation_pass = Int64.of_int validation_pass in
  let proto = Int64.of_int proto in
  let lvl_level = Int64.of_int level.node_lvl_level in
  let lvl_level_position = Int64.of_int level.node_lvl_level_position in
  let lvl_cycle = Int64.of_int level.node_lvl_cycle in
  let lvl_cycle_position = Int64.of_int level.node_lvl_cycle_position in
  let lvl_voting_period = Int64.of_int level.node_lvl_voting_period in
  let lvl_voting_period_position = Int64.of_int level.node_lvl_voting_period_position in
  let voting_period_kind =
    Dune_utils.string_of_voting_period_kind
      block.node_metadata.meta_header.header_meta_voting_period_kind in
  let endorsements_included = Int32.of_int endorsements_included in
  (match lvl_cycle_position, constants with
   | (0L, Some constants) ->
     constants level.node_lvl_level (fun cst ->
         let blocks_per_cycle = Int64.of_int cst.blocks_per_cycle in
         let level_end = Int64.(pred (add lvl_level blocks_per_cycle)) in
         register_cycle_limits ~force:true lvl_cycle lvl_level level_end)
   | _ -> ());
  register_dune_user baker ;
  register_cycle_count_baker lvl_cycle baker;
  register_protocol ~distance ?constants ~block_hash:hash level.node_lvl_level protocol ;
  register_protocol ~distance ~block_hash:hash level.node_lvl_level test_protocol;
  [%pgsql dbh
    "INSERT INTO block \
     (hash, predecessor, fitness, baker, timestamp, validation_pass, \
     proto, data, signature, \
     protocol, test_protocol, \
     network, test_network, test_network_expiration, level, level_position, \
     priority, cycle, cycle_position, voting_period, voting_period_position, \
     commited_nonce_hash, pow_nonce, distance_level, operation_count, \
     volume, fees, voting_period_kind, endorsements_included) \
     VALUES \
     ($hash, $predecessor_hash, $fitness, $baker, $timestamp, $validation_pass, \
     $proto, $data, $signature, \
     $protocol_hash, \
     $test_protocol_hash, $network, $test_network, $test_network_expiration, \
     $lvl_level, $lvl_level_position, $priority, $lvl_cycle, \
     $lvl_cycle_position, $lvl_voting_period, $lvl_voting_period_position, \
     $commited_nonce_hash, $pow_nonce, $distance, $operation_count, \
     $volume, $fees, $voting_period_kind, $endorsements_included) \
     ON CONFLICT (hash) DO NOTHING"];
  (* factor 2 is to take some space for endorsements and transactions of alternative
     chains that we don't read in operation_recent *)
  let endorsement_cut = match [%pgsql dbh "SELECT MAX(id) FROM endorsement_last"] with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_endorsements))
    | _ -> 0L in
  let transaction_cut = match [%pgsql dbh "SELECT MAX(id) FROM transaction_last"] with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_transactions))
    | _ -> 0L in
  let origination_cut = match [%pgsql dbh "SELECT MAX(id) FROM origination_last"] with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_originations))
    | _ -> 0L in
  let reveal_cut = match [%pgsql dbh "SELECT MAX(id) FROM reveal_last"] with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_reveals))
    | _ -> 0L in
  let delegation_cut = match [%pgsql dbh "SELECT MAX(id) FROM delegation_last"] with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_delegations))
    | _ -> 0L in
  let activation_cut = match [%pgsql dbh "SELECT MAX(id) FROM activation_last"] with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_activations))
    | _ -> 0L in
  [%pgsql dbh "DELETE FROM endorsement_last WHERE id < $endorsement_cut"];
  [%pgsql dbh "DELETE FROM transaction_last WHERE id < $transaction_cut"];
  [%pgsql dbh "DELETE FROM origination_last WHERE id < $origination_cut"];
  [%pgsql dbh "DELETE FROM delegation_last WHERE id < $delegation_cut"];
  [%pgsql dbh "DELETE FROM activation_last WHERE id < $activation_cut"];
  [%pgsql dbh "DELETE FROM reveal_last WHERE id < $reveal_cut"]

let register_genesis genesis =
  let dummy_level = {
    node_lvl_level = 0 ;
    node_lvl_level_position = 0 ;
    node_lvl_cycle = 0 ;
    node_lvl_cycle_position = 0 ;
    node_lvl_voting_period = 0 ;
    node_lvl_voting_period_position = 0 ;
    node_lvl_expected_commitment = false ;
  } in
  let orphan = { genesis with
                 node_hash = Utils.orphan_block_hash;
                 node_header = {
                   genesis.node_header with
                   header_shell = {
                     genesis.node_header.header_shell with
                     shell_predecessor = Utils.orphan_block_hash } } } in
  (* Special user for genesis and first block *)
  [%pgsql dbh "INSERT INTO dune_user (hash,contract) \
              VALUES ('God','false')"];
  register_block genesis dummy_level 0L 0L 0L 0L 0;
  register_block orphan dummy_level Int64.minus_one 0L 0L 0L 0

(* operations *)

let get_status_from_manager_metadata = function
  | None -> false (* Pending *)
  | Some meta ->
    match meta.manager_meta_operation_result with
    | None -> assert false
    | Some meta ->
      match meta.meta_op_status with
      | None -> assert false
      | Some status -> status <> "applied"

let get_burn = function
  | None -> 0L
  | Some meta -> match meta.manager_meta_operation_result with
    | None -> 0L
    | Some meta ->
      match meta.meta_op_balance_updates with
      | None -> 0L
      | Some l_bu ->
        Int64.neg @@
        List.fold_left (fun acc bu -> match bu with
            | Contract (_, change) -> Int64.add acc change
            | _ -> acc) 0L l_bu

let get_internal_burn = function
  | None -> 0L
  | Some meta ->
    List.fold_left (fun acc -> function
        | NTransaction transaction ->
          Int64.add acc (get_burn transaction.node_tr_metadata)
        | NOrigination origination ->
          Int64.add acc (get_burn origination.node_or_metadata)
        | _ -> acc)
      0L meta.manager_meta_internal_operation_results

let get_total_burn meta = Int64.add (get_burn meta) (get_internal_burn meta)

let format_error {node_err_id; node_err_kind; node_err_info} =
  [ Some node_err_id; Some node_err_kind; Some node_err_info ]

let format_errors l = List.flatten @@ List.map format_error l

let get_manager_errors = function
  | None -> None
  | Some m -> match m.manager_meta_operation_result with
    | None -> None
    | Some m -> Some (format_errors m.meta_op_errors)

let get_denounciation_info meta =
  match meta with
  | None -> "", "", 0L, 0L, 0L, 0L
  | Some metadata ->
    let balance_updates =
      match metadata.meta_op_balance_updates with None -> [] | Some l -> l in
    List.fold_left
      (fun (accused, denouncer, lost_deposit,
            lost_rewards, lost_fees, gain_rewards) b ->
        match b with
        (* | Debited _ | Credited _ *)
        | Contract _ ->
          accused, denouncer, lost_deposit,
          lost_rewards, lost_fees, gain_rewards
        | Rewards (dn1, _cycle, amount) ->
          if amount < 0L then (* accused *)
            (dn1, denouncer,
             lost_deposit, amount, lost_fees, gain_rewards)
          else      (* denouncer *)
            (accused, dn1,
             lost_deposit, lost_rewards, lost_fees, amount)
        | Fees (dn1, _cycle, fees) ->
          (dn1, denouncer,
           lost_deposit, lost_rewards, fees, gain_rewards)
        | Deposits (dn1, _cycle, amount) ->
          (dn1, denouncer,
           amount, lost_rewards, lost_fees, gain_rewards))
      ("","", 0L, 0L, 0L, 0L) balance_updates

let get_header_id header =
  let h1_level =
    Int64.of_int header.header_shell.shell_level in
  let h1_priority = Int32.of_int header.header_priority in
  let h1_fitness = header.header_shell.shell_fitness in
  let h1_context = header.header_shell.shell_context in
  let h1_signature = header.header_signature in
  match
    [%pgsql dbh
      "SELECT id FROM header AS h \
       WHERE \
       h.level = $h1_level AND \
       h.priority = $h1_priority AND \
       h.fitness = $h1_fitness AND \
       h.context = $h1_context AND \
       h.signature = $h1_signature"] with
  | [ id ] -> Int64.to_int32 id
  | _ -> debug "[Writer] Header not found\n%!"; assert false


let register_token_info contract storage code =
  if included "token" then
    match Token_standard.token_info storage code with
    | Some (name, symbol, total_supply, decimals, version, language) ->
      [%pgsql dbh
          "INSERT INTO token_info(contract, name, symbol, total_supply, \
           decimals, version, language) \
           VALUES($contract, $name, $symbol, $total_supply, $decimals, $version, $language) \
           ON CONFLICT DO NOTHING"]
    | _ -> ()

let register_token_account tsp account contract =
  if included "token" then
    [%pgsql dbh
        "INSERT INTO token_balances(account, contract, last_modified) \
         VALUES($account, $contract, $tsp) \
         ON CONFLICT DO NOTHING"]

(* Todo : format Love big maps *)
let format_big_map_diff = function
  | None -> [], []
  | Some meta -> match meta.manager_meta_operation_result with
    | None -> [], []
    | Some meta -> match meta.meta_op_big_map_diff with
      | Some l ->
        List.fold_left (fun (accounts, balances) {bmd_key; bmd_value; _} ->
            match bmd_key, bmd_value with
            | Some (Micheline (Bytes h)), Some (Micheline (Int balance))
              ->
              Some (Crypto.hex_to_account h) :: accounts,
              Some (Z.to_string balance) :: balances
            | Some (Micheline (Bytes h)), None ->
              Some (Crypto.hex_to_account h) :: accounts,
              Some "0" :: balances
            | Some (DuneExpr (LoveExpr (LoveValue (Love_value.Value.VAddress account)))),
              Some (DuneExpr (LoveExpr (LoveValue (Love_value.Value.VNat balance)))) ->
              Some account :: accounts,
              Some (Z.to_string balance) :: balances
            | Some (DuneExpr (LoveExpr (LoveValue (Love_value.Value.VAddress account)))),
              None ->
              Some account :: accounts,
              Some "0" :: balances
            | _ -> accounts, balances) ([],[]) l
      | _ -> [], []

let register_token_operation hash tsp level block_hash network transaction =
  if included "token" then
    let open Data_types in
    let src = transaction.node_tr_src in
    let contract = transaction.node_tr_dst in
    let level = Int64.of_int level in
    match transaction.node_tr_parameters with
    | None -> ()
    | Some parameters ->
      List.iter (fun {ts_dst; ts_amount; ts_flag; ts_kind; _} ->
          let kind = Token_standard.token_op_to_str ts_kind in
          debug "[Writer] token_operation %s %s\n%!" kind hash;
          let dst = Option.map (fun x -> x.pkh) ts_dst in
          let ts_amount = Z.to_string ts_amount in
          let balance_accounts, balances =
            format_big_map_diff transaction.node_tr_metadata in
          register_token_account tsp src contract;
          (match dst with | Some dst -> register_token_account tsp dst contract | None -> ());
          [%pgsql dbh
              "INSERT INTO token_operations( \
               transaction, contract, kind, source, destination, amount, flag, \
               balance_accounts, balances, \
               op_level, op_block_hash, timestamp, network) \
               VALUES($hash, $contract, $kind, $src, $?dst, $ts_amount, $ts_flag, \
               $balance_accounts, $balances, \
               $level, $block_hash, $tsp, $network) ON CONFLICT DO NOTHING"])
        (Token_standard.decode src parameters)

let block_info block =
  Int64.of_int block.node_header.header_shell.shell_level,
  block.node_hash, block.node_chain_id,
  Pg_helper.cal_of_date_dune block.node_header.header_shell.shell_timestamp,
  block.node_metadata.meta_header.header_meta_baker,
  Int32.of_int block.node_header.header_priority

let register_seed_nonce_revelation ?block hash tsp snr =
  let level = Int64.of_int snr.node_seed_level in
  let nonce = snr.node_seed_nonce in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, baker, _ = block_info block in
    [%pgsql dbh
      "INSERT INTO seed_nonce_revelation_all \
       (hash, level, nonce, timestamp_op, op_level, op_block_hash, \
       distance_level, network, timestamp_block, baker) \
       VALUES($hash, $level, $nonce, $tsp, $op_level, \
       $op_block_hash, -1, $network, $timestamp_block, $baker)"]
  | None -> ()

let register_double_baking_evidence ?block hash dbe =
  register_header dbe.node_double_bh1 ;
  register_header dbe.node_double_bh2 ;
  let h1 = get_header_id dbe.node_double_bh1 in
  let h2 = get_header_id dbe.node_double_bh2 in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let op_cycle = cycle_from_level op_level in
    let accused, denouncer, lost_deposit,
        lost_rewards, lost_fees, gain_rewards =
      get_denounciation_info dbe.node_double_bh_metadata in
    [%pgsql dbh
      "INSERT INTO double_baking_evidence_all \
       (hash, header1, header2, accused, denouncer, lost_deposit, lost_rewards, \
       lost_fees, gain_rewards, op_level, op_cycle, op_block_hash, \
       distance_level, network, timestamp) \
       VALUES($hash, $h1, $h2, $accused, $denouncer, $lost_deposit, $lost_rewards, \
       $lost_fees, $gain_rewards, $op_level, $op_cycle, \
       $op_block_hash, -1, $network, $timestamp_block)"]
  | None -> ()

let register_double_endorsement_evidence ?block hash dee =
  let endorse1 = dee.node_double_endorsement1 in
  let endorse2 = dee.node_double_endorsement2 in
  let block1 = endorse1.ndee_branch in (* TODO *)
  let level1 = Int64.of_int endorse1.ndee_level in
  let block2 = endorse2.ndee_branch in
  let level2 = Int64.of_int endorse2.ndee_level in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let op_cycle = cycle_from_level op_level in
    let accused, denouncer, lost_deposit,
        lost_rewards, lost_fees, gain_rewards =
      get_denounciation_info dee.node_double_endorsement_metadata in
    [%pgsql dbh
      "INSERT INTO double_endorsement_evidence_all \
       (hash, block_hash1, level1, block_hash2, level2, accused, denouncer, \
       lost_deposit, lost_rewards, lost_fees, gain_rewards, op_level, \
       op_cycle, op_block_hash, distance_level, network, timestamp) \
       VALUES($hash, $block1, $level1, $block2, $level2, $accused, $denouncer, \
       $lost_deposit, $lost_rewards, $lost_fees, $gain_rewards, $op_level, \
       $op_cycle, $op_block_hash, -1, $network, $timestamp_block)"]
  | None -> ()

let register_activation ?block hash tsp act =
  let pkh = act.node_act_pkh in
  let secret = act.node_act_secret in
  register_dune_user pkh;
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let balance = match act.node_act_metadata with
      | None -> None
      | Some meta -> match meta.meta_op_balance_updates with
        | None -> Some 0L
        | Some [ bu ] ->  begin match bu with
            | Contract (acc, amount) when acc = pkh -> Some amount
            | _ -> debug "[Writer] Weird balance_updates type for activation %S\n%!" hash;
              Some 0L
          end
        | Some l ->
          debug "[Writer] Unexpected balance_updates length for activation %S - %S\n%!"
            hash @@ String.concat " " (List.map Dune_utils.string_of_balance_update l);
          None in
    [%pgsql dbh
      "INSERT INTO activation_last \
       (hash, pkh, secret, balance, timestamp_op, op_level, op_block_hash, \
       distance_level, network, timestamp_block) \
       VALUES($hash, $pkh, $secret, $?balance, $tsp, $op_level, \
       $op_block_hash, -1, $network, $timestamp_block)"];
    [%pgsql dbh
      "INSERT INTO activation_all \
       (hash, pkh, secret, balance, timestamp_op, op_level, op_block_hash, \
       distance_level, network, timestamp_block) \
       VALUES($hash, $pkh, $secret, $?balance, $tsp, $op_level, \
       $op_block_hash, -1, $network, $timestamp_block)"]
  | None -> ()

let register_endorsement ?block ?branch hash endo =
  match block, branch with
  | Some block, Some branch ->
    let op_level, op_block_hash, network, timestamp_block, _, priority =
      block_info block in
    let op_cycle = cycle_from_level op_level in
    let level = Int64.of_int endo.node_endorse_block_level in
    let priority =
      match [%pgsql dbh
              "SELECT CASE WHEN initial_endorsers IS NULL THEN priority \
               ELSE $priority END FROM block \
               INNER JOIN cycle_limits AS cl ON cl.cycle = block.cycle \
               INNER JOIN protocol_constants AS cst ON cst.distance_level = 0 AND \
               COALESCE(cl.level_start BETWEEN cst.level_start AND cst.level_end, \
               cst.level_start <= cl.level_start AND cst.level_end IS NULL)\
               WHERE hash = $branch"] with
      | [ Some p ] -> p
      | _ -> assert false in
    let src, slots =
      match endo.node_endorse_metadata with
      | None -> "", []
      | Some metadata ->
        Option.value ~default:"" metadata.meta_op_delegate,
        Option.fold ~none:[] ~some:(List.map (fun s -> Some (Int32.of_int s)))
          metadata.meta_op_slots in
    register_dune_user src;
    register_cycle_count_baker op_cycle src;
    [%pgsql dbh
      "INSERT INTO endorsement_last \
       (hash, source, block_hash, block_level, slots, priority, \
       op_block_hash, op_level, op_cycle, distance_level, network, timestamp) \
       VALUES \
       ($hash, $src, $branch, $level, $slots, $priority, \
       $op_block_hash, $op_level, $op_cycle, -1, $network, $timestamp_block) \
       ON CONFLICT DO NOTHING"];
    [%pgsql dbh
      "INSERT INTO endorsement_all \
       (hash, source, block_hash, block_level, slots, priority, \
       op_block_hash, op_level, op_cycle, distance_level, network, timestamp) \
       VALUES \
       ($hash, $src, $branch, $level, $slots, $priority, \
       $op_block_hash, $op_level, $op_cycle, -1, $network, $timestamp_block) \
       ON CONFLICT DO NOTHING"]
  | _ -> ()

let register_activate_protocol ?constants ?block ?(internal=false) hash acp =
  let source = acp.node_acp_src in
  register_dune_user source;
  let counter = Z.to_int64 acp.node_acp_counter in
  let fee = acp.node_acp_fee in
  let gas_limit = Z.to_int64 acp.node_acp_gas_limit in
  let storage_limit = Z.to_int64 acp.node_acp_storage_limit in
  let level = Int32.of_int acp.node_acp_level in
  let protocol = acp.node_acp_protocol in
  let parameters = match acp.node_acp_parameters with
    | None -> None
    | Some p -> Some (
        EzEncoding.construct Dune_encoding_min.Protocol_param.encoding p) in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let op_level_int = Some (Int64.to_int op_level) in
    let failed = get_status_from_manager_metadata acp.node_acp_metadata in
    let errors = get_manager_errors acp.node_acp_metadata in
    if not (is_new_protocol protocol) then
      Pg_helper.merge_constants_params ?constants
        acp.node_acp_parameters op_level_int (fun constants ->
            register_constants ?protocol ~block_hash:op_block_hash (acp.node_acp_level+1) constants);
    [%pgsql dbh
      "INSERT INTO activate_protocol \
       (hash, source, fee, counter,  gas_limit, storage_limit, \
       level, protocol, parameters, failed, internal, \
       op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $gas_limit, \
       $storage_limit, $level, $?protocol, $?parameters, $failed, $internal, \
       $op_level, $op_block_hash, -1, $network, \
       $timestamp_block, $?errors)"]
  | None -> ()

let register_manage_accounts ?block ?(internal=false) hash macs =
  let source = macs.node_macs_src in
  register_dune_user source;
  let counter = Z.to_int64 macs.node_macs_counter in
  let fee = macs.node_macs_fee in
  let gas_limit = Z.to_int64 macs.node_macs_gas_limit in
  let storage_limit = Z.to_int64 macs.node_macs_storage_limit in
  let bytes = Hex.show macs.node_macs_bytes in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let failed = get_status_from_manager_metadata macs.node_macs_metadata in
    let errors = get_manager_errors macs.node_macs_metadata in
    [%pgsql dbh
      "INSERT INTO manage_accounts \
       (hash, source, fee, counter,  gas_limit, storage_limit, \
       bytes, failed, internal, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $gas_limit, \
       $storage_limit, $bytes, $failed, $internal, \
       $op_level, $op_block_hash, -1, $network, \
       $timestamp_block, $?errors)"]
  | None -> ()

let register_manage_account ?block ?(internal=false) hash mac =
  let source = mac.node_mac_src in
  register_dune_user source;
  let counter = Z.to_int64 mac.node_mac_counter in
  let fee = mac.node_mac_fee in
  let gas_limit = Z.to_int64 mac.node_mac_gas_limit in
  let storage_limit = Z.to_int64 mac.node_mac_storage_limit in
  let target, signature = match mac.node_mac_target with
    | None -> None, None
    | Some (target, signature) -> Some target, signature in
  let maxrolls, set_maxrolls = match mac.node_mac_options.node_mao_maxrolls with
    | None -> None, false
    | Some x -> Option.map Int32.of_int x, true in
  let admin, set_admin = match mac.node_mac_options.node_mao_admin with
    | None -> None, false
    | Some x -> x, true in
  (match admin with (Some admin) -> register_dune_user admin | _ -> ());
  let white_list = Option.map (fun l -> List.map (fun x -> Some x) l)
      mac.node_mac_options.node_mao_white_list in
  let delegation = mac.node_mac_options.node_mao_delegation in
  let recovery, set_recovery = match mac.node_mac_options.node_mao_recovery with
    | None -> None, false
    | Some x -> x, true in
  let actions_name, actions_arg = match mac.node_mac_options.node_mao_actions with
    | [] -> None, None
    | l -> let names, args = List.split l in
      Some (List.map (fun x -> Some x) names), Some (List.map (fun x -> Some x) args) in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let failed = get_status_from_manager_metadata mac.node_mac_metadata in
    let errors = get_manager_errors mac.node_mac_metadata in
    [%pgsql dbh
      "INSERT INTO manage_account \
       (hash, source, fee, counter,  gas_limit, storage_limit, \
       target, signature, maxrolls, set_maxrolls, admin, set_admin, white_list, \
       delegation, recovery, set_recovery, actions_name, actions_arg, failed, internal, op_level, \
       op_block_hash, distance_level, network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $gas_limit, \
       $storage_limit, $?target, $?signature, $?maxrolls, $set_maxrolls, $?admin, \
       $set_admin, $?white_list, $?delegation, $?recovery, $set_recovery, \
       $?actions_name, $?actions_arg, $failed, $internal, $op_level, $op_block_hash, \
       -1, $network, $timestamp_block, $?errors)"]
  | None -> ()

let register_delegation ?block ?(internal=false) hash tsp dlg =
  let source = dlg.node_del_src in
  let counter = Z.to_int64 dlg.node_del_counter in
  let fee = dlg.node_del_fee in
  let gas_limit = Z.to_int64 dlg.node_del_gas_limit in
  let storage_limit = Z.to_int64 dlg.node_del_storage_limit in
  debug "[Writer]  delegation %s\n%!" hash;
  register_dune_user source;
  let delegate = dlg.node_del_delegate in
  register_dune_user delegate;
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let failed = get_status_from_manager_metadata dlg.node_del_metadata in
    let errors = get_manager_errors dlg.node_del_metadata in
    [%pgsql dbh
      "INSERT INTO delegation_last \
       (hash, source, fee, counter, delegate, gas_limit, storage_limit, \
       failed, internal, timestamp_op, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $delegate, $gas_limit, \
       $storage_limit, $failed, $internal, $tsp, \
       $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)"];
    [%pgsql dbh
      "INSERT INTO delegation_all \
       (hash, source, fee, counter, delegate, gas_limit, storage_limit, \
       failed, internal, timestamp_op, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $delegate, $gas_limit, \
       $storage_limit, $failed, $internal, $tsp, \
       $op_level, $op_block_hash, -1, $network, \
       $timestamp_block, $?errors)"]
  | None -> ()

let register_reveal ?block ?(internal=false) hash tsp rvl =
  let source = rvl.node_rvl_src in
  let counter = Z.to_int64 rvl.node_rvl_counter in
  let fee = rvl.node_rvl_fee in
  let gas_limit = Z.to_int64 rvl.node_rvl_gas_limit in
  let storage_limit = Z.to_int64 rvl.node_rvl_storage_limit in
  debug "[Writer]  reveal %s\n%!" hash;
  register_dune_user source;
  let pubkey = rvl.node_rvl_pubkey in
  register_dune_user pubkey;
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let failed = get_status_from_manager_metadata rvl.node_rvl_metadata in
    let errors = get_manager_errors rvl.node_rvl_metadata in
    [%pgsql dbh
      "INSERT INTO reveal_last \
       (hash, source, fee, counter, pubkey, gas_limit, storage_limit, \
       failed, internal, timestamp_op, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $pubkey, $gas_limit, \
       $storage_limit, $failed, $internal, $tsp, \
       $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)"];
    [%pgsql dbh
      "INSERT INTO reveal_all \
       (hash, source, fee, counter, pubkey, gas_limit, storage_limit, \
       failed, internal, timestamp_op, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $pubkey, $gas_limit, \
       $storage_limit, $failed, $internal, $tsp, \
       $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)"]
  | None -> ()

let rec register_transaction ?block ?(internal=false) hash tsp tr =
  debug "[Writer]  transaction %s\n%!" hash;
  let source = tr.node_tr_src in
  let counter = Z.to_int64 tr.node_tr_counter in
  let fee = tr.node_tr_fee in
  let gas_limit = Z.to_int64 tr.node_tr_gas_limit in
  let storage_limit = Z.to_int64 tr.node_tr_storage_limit in
  register_dune_user source;
  let dst = tr.node_tr_dst in
  register_dune_user dst;
  let amount = tr.node_tr_amount in
  let parameters = tr.node_tr_parameters in
  let collect_fee_gas = Option.map Z.to_int64 tr.node_tr_collect_fee_gas in
  let collect_pk = tr.node_tr_collect_pk in
  let nonce = Option.map Int32.of_int tr.node_tr_nonce in
  begin match block with
    | Some block ->
      let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
      let failed = get_status_from_manager_metadata tr.node_tr_metadata in
      let errors = get_manager_errors tr.node_tr_metadata in
      let burn = if internal then 0L else get_total_burn tr.node_tr_metadata in
      if not failed then
        register_token_operation hash tsp (Int64.to_int op_level)
          op_block_hash network tr;
      [%pgsql dbh
        "INSERT INTO transaction_all \
         (hash, source, destination, fee, counter, amount, parameters, \
         gas_limit, storage_limit, failed, internal, timestamp_op, burn_dun, \
         op_level, op_block_hash, distance_level, network, timestamp_block, \
         errors, collect_fee_gas, collect_pk, nonce) \
         VALUES \
         ($hash, $source, $dst, $fee, $counter, $amount, $?parameters, \
         $gas_limit, $storage_limit, $failed, $internal, $tsp, $burn, \
         $op_level, $op_block_hash, -1, $network, $timestamp_block, \
         $?errors, $?collect_fee_gas, $?collect_pk, $?nonce)"];
      [%pgsql dbh
        "INSERT INTO transaction_last \
         (hash, source, destination, fee, counter, amount, parameters, \
         gas_limit, storage_limit, failed, internal, timestamp_op, burn_dun, \
         op_level, op_block_hash, distance_level, network, timestamp_block, \
         errors, collect_fee_gas, collect_pk, nonce) \
         VALUES \
         ($hash, $source, $dst, $fee, $counter, $amount, $?parameters, \
         $gas_limit, $storage_limit, $failed, $internal, $tsp, $burn, \
         $op_level, $op_block_hash, -1, $network, $timestamp_block, \
         $?errors, $?collect_fee_gas, $?collect_pk, $?nonce)"]
    | None -> ()
  end;
  match tr.node_tr_metadata with
  | None -> ()
  | Some metadata ->
    register_operation_type ?block ~internal:true hash tsp
      metadata.manager_meta_internal_operation_results

and register_origination ?block ?(internal=false) hash tsp ori =
  debug "[Writer]  origination %s\n%!" hash;
  let source = ori.node_or_src in
  let counter = Z.to_int64 ori.node_or_counter in
  let fee = ori.node_or_fee in
  let gas_limit = Z.to_int64 ori.node_or_gas_limit in
  let storage_limit = Z.to_int64 ori.node_or_storage_limit in
  register_dune_user source;
  let manager = ori.node_or_manager in
  register_dune_user manager;
  let delegate = match ori.node_or_delegate with
    | None -> "" | Some delegate -> delegate in
  register_dune_user delegate ;
  let balance = ori.node_or_balance in
  let spendable = match ori.node_or_spendable with
    | Some spendable -> spendable
    | None -> match ori.node_or_script with
      | Some _ -> false
      | _ -> true in
  let sc_code, sc_storage, sc_code_hash = match ori.node_or_script with
    | None -> None, None, None
    | Some sc -> sc.sc_code, Some sc.sc_storage, sc.sc_code_hash in
  begin match block with
    | Some block ->
      let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
      let kt1 = match ori.node_or_metadata with
        | None -> Crypto.op_to_KT1 hash
        | Some meta ->
          match meta.manager_meta_operation_result with
          | None -> assert false
          | Some meta ->
            match meta.meta_op_originated_contracts with
            | None | Some [] -> Crypto.op_to_KT1 hash
            | Some (hd :: _ ) -> hd in
      debug "[Writer] kt1 %S originated by %S\n%!" kt1 source ;
      begin match sc_storage with
        | None -> ()
        | Some storage -> register_token_info kt1 storage sc_code end;
      let delegatable = match ori.node_or_delegatable with
        | Some delegatable -> delegatable
        | None -> if String.length kt1 < 2 then false
          else match String.sub kt1 0 2 with
            | "KT" -> true
            | _ -> false in
      register_dune_user kt1;
      let failed = get_status_from_manager_metadata ori.node_or_metadata in
      let errors = get_manager_errors ori.node_or_metadata in
      let burn = if internal then 0L else get_total_burn ori.node_or_metadata in
      [%pgsql dbh
        "INSERT INTO origination_last \
         (hash, source, kt1, fee, counter, manager, delegate, script_code, \
         script_storage_type, script_code_hash, spendable, delegatable, balance, gas_limit, \
         storage_limit, failed, internal, burn_dun, timestamp_op, \
         op_level, op_block_hash, distance_level, network, timestamp_block, errors) \
         VALUES($hash, $source, $kt1, $fee, $counter, $manager, $delegate, \
         $?sc_code, $?sc_storage, $?sc_code_hash, $spendable, $delegatable, $balance, $gas_limit, \
         $storage_limit, $failed, $internal, $burn, $tsp, \
         $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)"];
      [%pgsql dbh
        "INSERT INTO origination_all \
         (hash, source, kt1, fee, counter, manager, delegate, script_code, \
         script_storage_type, script_code_hash, spendable, delegatable, balance, gas_limit, \
         storage_limit, failed, internal, burn_dun, timestamp_op, \
         op_level, op_block_hash, distance_level, network, timestamp_block, errors) \
         VALUES($hash, $source, $kt1, $fee, $counter, $manager, $delegate, \
         $?sc_code, $?sc_storage, $?sc_code_hash, $spendable, $delegatable, $balance, $gas_limit, \
         $storage_limit, $failed, $internal, $burn, $tsp, \
         $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)"]
    | None -> ()
  end;
  match ori.node_or_metadata with
  | None -> ()
  | Some metadata ->
    register_operation_type ?block ~internal:true hash tsp
      metadata.manager_meta_internal_operation_results

and register_operation_type ?constants ?block ?branch ?internal hash tsp operations =
  let len = List.length operations in
  List.iteri (fun i op ->
      debug "[Writer] register_operation_type %S %d / %d\n%!" hash (i + 1) len ;
      match op with
      | NSeed_nonce_revelation snr when included "seed_nonce_revelation" ->
        register_seed_nonce_revelation ?block hash tsp snr
      | NDouble_baking_evidence dbe when included "double_baking_evidence" ->
        register_double_baking_evidence ?block hash dbe
      | NDouble_endorsement_evidence dee when included "double_endorsement_evidence" ->
        register_double_endorsement_evidence ?block hash dee
      | NActivation act when included "activation" ->
        register_activation ?block hash tsp act
      | NEndorsement endo when included "endorsement" ->
        register_endorsement ?block ?branch hash endo
      | NActivate_protocol acp when included "activate_protocol" ->
        register_activate_protocol ?constants ?block ?internal hash acp
      | NManage_accounts mac when included "manage_accounts" ->
        register_manage_accounts ?block ?internal hash mac
      | NManage_account mac when included "manage_account" ->
        register_manage_account ?block ?internal hash mac
      | NDelegation dlg when included "delegation" ->
        register_delegation ?block ?internal hash tsp dlg
      | NReveal rvl when included "reveal" ->
        register_reveal ?block ?internal hash tsp rvl
      | NTransaction tr when included "transaction" ->
        register_transaction ?block ?internal hash tsp tr
      | NOrigination ori when included "origination" ->
        register_origination ?block ?internal hash tsp ori
      | _ -> ())
    operations

let register_block_operation block op =
  let block_hash = block.node_hash in
  let network = block.node_chain_id in
  let tsp = Pg_helper.cal_of_date_dune block.node_header.header_shell.shell_timestamp in
  let op_hash = op.node_op_hash in
  let op_type = Dune_utils.string_of_op_contents (List.hd op.node_op_contents) in
  let op_anon_type =
    Utils.opt_list @@ Dune_utils.string_of_anonymous_op_contents op.node_op_contents in
  let op_manager_type =
    Utils.opt_list @@ Dune_utils.string_of_manager_op_contents op.node_op_contents in
  debug "[Writer] register_block_operation %S %S\n%!" block_hash op_hash;
  [%pgsql dbh
      "INSERT INTO operation (hash, op_type, op_anon_type, op_manager_type, timestamp, block_hash, network) \
       VALUES ($op_hash, $op_type, $op_anon_type, $op_manager_type, $tsp, $block_hash, $network)"]

let register_operations ?constants block ops =
  let tsp = Pg_helper.cal_of_date_dune block.node_header.header_shell.shell_timestamp in
  let len = List.length ops in
  let include_operations = included "operation" in
  let include_block_operations = included "block_operation" in
  List.iteri (fun i op ->
      if include_block_operations then (
        debug "[Writer] register_operation %S %d / %d\n%!" block.node_hash (i + 1) len;
        register_block_operation block op);
      if include_operations then
        register_operation_type ?constants ~block ~branch:op.node_op_branch op.node_op_hash
          tsp op.node_op_contents) ops

let string_operation (op : node_operation_type option) : string =
  match op with
  | None -> "Header"
  | Some op -> match op with
      NTransaction _ -> "Transaction"
    | NOrigination _ -> "Origination"
    | NReveal _ -> "Reveal"
    | NDelegation _ -> "Delegation"
    | NSeed_nonce_revelation _ -> "Seed nonce revelation"
    | NActivation _ -> "Activation"
    | NDouble_endorsement_evidence _ -> "Double endorsement evidence"
    | NDouble_baking_evidence _ -> "Double baking evidence"
    | NEndorsement _ -> "Endorsement"
    | NProposals _ -> "Proposals"
    | NBallot _ -> "Ballot"
    | NActivate -> "Activate"
    | NActivate_testnet -> "Activate testnet"
    | NActivate_protocol _ -> "Activate protocol"
    | NManage_accounts _ -> "Manage accounts"
    | NManage_account _ -> "Manage account"

(* Balance updates *)

let bu_to_bu_info ?op may_burn bu_internal bu_level bu_date bu bu_block_hash =
  let open Data_types in
  let bu_op_type = string_operation op in
  match bu with
    Contract (bu_account, bu_diff) ->
    Some
      {bu_account;
       bu_block_hash;
       bu_diff;
       bu_date;
       bu_update_type = "Contract";
       bu_op_type;
       bu_internal;
       bu_level = Int32.to_int bu_level;
       bu_frozen = false;
       bu_burn=may_burn; bu_cycle = None} (* Contracts in originations/transactions may burn dun *)
  | Rewards (bu_account,_,bu_diff)->
    Some
      {bu_account;
       bu_block_hash;
       bu_diff;
       bu_date;
       bu_update_type = "Reward";
       bu_op_type;
       bu_internal;
       bu_level = Int32.to_int bu_level;
       bu_frozen = true;
       bu_burn=false; bu_cycle = None}
  | Fees (bu_account,_,bu_diff)->
    Some
      {bu_account;
       bu_block_hash;
       bu_diff;
       bu_date;
       bu_update_type = "Fee";
       bu_op_type;
       bu_internal;
       bu_level = Int32.to_int bu_level;
       bu_frozen = true;
       bu_burn=false; bu_cycle = None}
  | Deposits (bu_account,_,bu_diff) ->
    Some
      {bu_account;
       bu_block_hash;
       bu_diff;
       bu_date;
       bu_update_type = "Deposit";
       bu_op_type;
       bu_internal;
       bu_level = Int32.to_int bu_level;
       bu_frozen = true;
       bu_burn=false; bu_cycle = None}

let get_op_bal_update = function
  | Some {meta_op_status;meta_op_balance_updates = Some l;_} ->
    begin match meta_op_status with Some "applied" | None -> l | _ -> [] end
  | _ -> []

let rec get_man_bal_update ?op header_bu_infos man_mtdt is_orig internal date level block_hash =
  let b_u = match man_mtdt.manager_meta_balance_updates with
      Some b_u -> b_u
    | None -> []
  in
  let bu_info_internal : Data_types.balance_update_info list =
    List.flatten
      (List.map
         (get_balance_update_info ~header_bu_infos date level true block_hash)
         man_mtdt.manager_meta_internal_operation_results) in
  let all_bu =
    b_u
    @ (get_op_bal_update man_mtdt.manager_meta_operation_result) in

  bu_info_internal @
  (
    List.fold_left
      (fun acc bu ->
         let bu_info =
           match op with
             None -> bu_to_bu_info is_orig internal level date bu block_hash
           | Some op -> bu_to_bu_info ~op is_orig internal level date bu block_hash in

         match bu_info with
           None -> acc
         | Some bu -> bu :: acc) [] all_bu)

and get_balance_update_info
    ?(header_bu_infos=[])
    (date : Data_types.Date.t)
    (level : int32)
    (internal:bool)
    (block_hash : block_hash)
    (op : node_operation_type) =
  let open Data_types in
  match op with
    NTransaction {node_tr_metadata = Some man_mtdt; _ }
  | NReveal {node_rvl_metadata = Some man_mtdt; _ }
  | NDelegation {node_del_metadata = Some man_mtdt; _ }
  | NActivate_protocol {node_acp_metadata = Some man_mtdt; _}
  | NManage_accounts {node_macs_metadata = Some man_mtdt; _}
  | NManage_account {node_mac_metadata = Some man_mtdt; _} ->
    get_man_bal_update ~op header_bu_infos man_mtdt false internal date level block_hash

  | NOrigination {node_or_metadata = Some man_mtdt; node_or_manager = manager; _} ->
     (* Burns dun *)
    let (bu_info : balance_update_info list) =
      get_man_bal_update ~op header_bu_infos man_mtdt true internal date level block_hash in
    (* Burned dun appear as contracts. For usual operations, contracts go by pairs
         (one is debited, the other credited).
         Burnt dun are the only unpaired contracts.*)
    let () =
      List.iter
        (fun (info : balance_update_info) ->
           if not info.bu_burn || not (String.equal info.bu_account manager)
           then () (* It has either been treated, or is not a burn contract *)
           else
             let opp_val = Int64.neg info.bu_diff in
             let is_not_burn : bool =
               List.exists
                 (fun (i:balance_update_info) ->
                    if Int64.equal opp_val i.bu_diff
                    then let () = i.bu_burn <- false in true
                    else false
                 )
                 bu_info
             in
             info.bu_burn <- not is_not_burn
        )
        (bu_info@header_bu_infos)
    in bu_info

  | NSeed_nonce_revelation {node_seed_metadata = op_mtdt ; _ }
  | NActivation {node_act_metadata = op_mtdt ; _ }
  | NDouble_endorsement_evidence {node_double_endorsement_metadata = op_mtdt ; _ }
  | NDouble_baking_evidence {node_double_bh_metadata = op_mtdt ; _ }
  | NEndorsement {node_endorse_metadata = op_mtdt ; _ }
  | NProposals {node_prop_metadata = op_mtdt ; _ }
  | NBallot {node_ballot_metadata = op_mtdt ; _ } ->
    List.fold_left
      (fun acc bu ->
         match bu_to_bu_info ~op false internal level date bu block_hash with
           None -> acc
         | Some bu -> bu :: acc)
      []
      (get_op_bal_update op_mtdt)
  | _ -> []

let insert_bu_info bu =
  let open Data_types in
  let (hash : string) = bu.bu_account in
  let (diff : int64) = bu.bu_diff in
  let (date : CalendarLib.Calendar.t) = Pg_helper.cal_of_date bu.bu_date in
  let (update_type : string) = bu.bu_update_type in
  let (internal : bool) = bu.bu_internal in
  let (level : int32) = Int32.of_int bu.bu_level in
  let (frozen: bool) = bu.bu_frozen in
  let (burn : bool) = bu.bu_burn in
  let (block_hash: block_hash) = bu.bu_block_hash in
  let (op_type : string) = bu.bu_op_type in
  [%pgsql dbh
    "INSERT INTO balance_updates \
     (hash, block_hash, diff, date, update_type, operation_type, internal, level, frozen, burn) \
     VALUES \
     ($hash, $block_hash, $diff, $date, $update_type, $op_type, $internal, $level, $frozen, $burn)"]

let bu_infos_from_block block =
  let date = block.node_header.header_shell.shell_timestamp in
  let ops = List.flatten block.node_operations in
  let level = Int32.of_int block.node_header.header_shell.shell_level in
  let block_hash = block.node_hash in
  let header_bu_infos =
    match block.node_metadata.meta_header.header_meta_balance_updates with
      None -> []
    | Some l ->
      List.fold_left
        (fun acc bu ->
           match bu_to_bu_info true true level date bu block_hash with
             None -> acc
           | Some bu_info -> bu_info :: acc) [] l in
    List.fold_left
      (fun acc op ->
         List.flatten (
             List.map
               (get_balance_update_info ~header_bu_infos date level false block_hash)
               op.node_op_contents) @ acc)
      header_bu_infos
      ops

let register_balance_updates_info balance_update_infos =
  List.iter insert_bu_info balance_update_infos

module StrMap =
    Map.Make(
    struct
      type t = string
      let compare =
        String.compare end)

let update_new_cycle cycle =
  (* New cycle for balance history *)
  [%pgsql dbh
    "INSERT INTO balance_from_balance_updates (hash,spendable_balance,frozen,rewards,fees,deposits,cycle) \
     SELECT old.hash,old.spendable_balance,old.frozen,old.rewards,old.fees,old.deposits,$cycle \
     FROM balance_snapshot AS old WHERE old.modified=true ON CONFLICT(hash,cycle) DO NOTHING"];
  [%pgsql dbh
    "UPDATE balance_snapshot SET modified=false"];
  (* There are too much balance updates for the db to be efficient.
     Every 5 cycle, sanitize the table balance_updates. *)
  let cycle = Int64.of_int32 cycle in
  let cycle_limit = Int64.of_int (5 + 1) in
  if cycle < cycle_limit then () (* Do nothing *)
  else (* sanitize *)
    match [%pgsql dbh "SELECT level_start, level_end FROM cycle_limits \
                      WHERE cycle = $cycle - ($cycle_limit + 1::bigint)"] with
    | (level_start, level_end) :: _ ->
      let level_start, level_end = Int64.(to_int32 level_start, to_int32 level_end) in
      [%pgsql dbh
        "DELETE FROM balance_updates \
         WHERE level BETWEEN $level_start AND $level_end"]
    | _ -> ()

let update_balance_from_balance_updates
    ?(force_init=false) cycle hash total_diff =
  let open Data_types in
    let old_balances =
      [%pgsql dbh
        "SELECT spendable_balance,frozen,rewards,fees,deposits \
         FROM balance_snapshot WHERE hash=$hash"] in

    match old_balances with
    | [] ->
      let s_diff = total_diff.b_spendable
      and f_diff = total_diff.b_frozen
      and rew_diff = total_diff.b_rewards
      and fee_diff = total_diff.b_fees
      and dep_diff = total_diff.b_deposits in
      begin
        if force_init
        then
          [%pgsql dbh
            "INSERT INTO balance_from_balance_updates \
             (hash,spendable_balance,frozen,rewards,fees,deposits,cycle) \
             VALUES \
             ($hash,$s_diff,$f_diff,$rew_diff, $fee_diff,$dep_diff,$cycle)"]
        else
          [%pgsql dbh
            "INSERT INTO balance_from_balance_updates \
             (hash,spendable_balance,frozen,rewards,fees,deposits,cycle) \
             VALUES \
             ($hash,0,0,0,0,0,$cycle)"]
      end;
      [%pgsql dbh
        "INSERT INTO balance_snapshot \
         (hash,spendable_balance,frozen,rewards,fees,deposits,modified) \
         VALUES \
         ($hash,$s_diff,$f_diff,$rew_diff, $fee_diff,$dep_diff,true)"]

    | (sb,frz,rew,fees,deps) :: _ ->
      let new_spd_bal =
        Int64.add sb total_diff.b_spendable
      in
      let new_frz_bal =
        Int64.add frz total_diff.b_frozen
      in
      let new_rw_bal =
        Int64.add rew total_diff.b_rewards
      in
      let new_fee_bal =
        Int64.add fees total_diff.b_fees
      in
      let new_dep_bal =
        Int64.add deps total_diff.b_deposits
      in
      [%pgsql dbh
        "UPDATE balance_snapshot \
         SET \
         spendable_balance=$new_spd_bal, \
         frozen=$new_frz_bal, \
         rewards=$new_rw_bal, \
         fees=$new_fee_bal, \
         deposits=$new_dep_bal, \
         modified=true \
         WHERE \
         hash=$hash"]

let register_balance_from_balance_updates level (bu_list : (account_hash * int64 * bool * string) list) =
  let update_map acc bu  =
    let open Data_types in
    let hash,diff,frz,update_type = bu in
    StrMap.update
      hash
      (fun old ->
         let diff_balance =
           {b_spendable = if frz then Int64.zero else diff;
            b_frozen = if frz then diff else Int64.zero;
            b_rewards = if update_type = "Reward" then diff else Int64.zero;
            b_fees = if update_type = "Fee" then diff else Int64.zero;
            b_deposits = if update_type = "Deposit" then diff else Int64.zero;
           }
         in
          match old with
            None -> Some diff_balance
          | Some bal ->

            let (+) = Int64.add in

            Some (
              {b_spendable = bal.b_spendable + diff_balance.b_spendable;
               b_frozen = bal.b_frozen + diff_balance.b_frozen;
               b_rewards =  bal.b_rewards + diff_balance.b_rewards;
               b_fees =  bal.b_fees + diff_balance.b_fees;
               b_deposits =  bal.b_deposits + diff_balance.b_deposits;
           }))
      acc in
  let bal_map = List.fold_left update_map StrMap.empty bu_list in
  let level = Int64.of_int32 level in
  let cycle, first = match
      [%pgsql dbh "SELECT cycle, $level = level_start FROM cycle_limits \
                  WHERE $level BETWEEN level_start AND level_end"] with
  | (cycle, Some first) :: _ -> Int64.to_int32 cycle, first
  | _ -> 0l, false in
  StrMap.iter (update_balance_from_balance_updates cycle) bal_map;
  if first then update_new_cycle cycle

let register_init_balance hash init date level =
  let open Data_types in
  let bu_info =
    {
      bu_account = hash;
      bu_block_hash = "";
      bu_diff = init;
      bu_date = date;
      bu_update_type = "Initialization";
      bu_op_type = "";
      bu_internal = true;
      bu_level = level;
      bu_frozen = false;
      bu_burn = false;
      bu_cycle = None}
  in
  insert_bu_info bu_info;
  update_balance_from_balance_updates
    ~force_init:true
    Int32.zero
    hash
    {b_spendable = init;
     b_frozen = Int64.zero;
     b_rewards = Int64.zero;
     b_fees = Int64.zero;
     b_deposits =Int64.zero}

let register_balances_updates block =
  if included "balance_updates" then
    register_balance_updates_info @@ bu_infos_from_block block

let compute_volume_fees ops =
  List.fold_left (fun (acc_vol, acc_fee) op ->
      List.fold_left (fun (acc_vol, acc_fee) op -> match op with
          | NTransaction tr ->
            let amount = tr.node_tr_amount in
            let fee = tr.node_tr_fee in
            let amount =
              if get_status_from_manager_metadata tr.node_tr_metadata then
                acc_vol
              else
                Int64.add amount acc_vol in
            amount, Int64.add fee acc_fee
          | NOrigination ori ->
            let balance = ori.node_or_balance in
            let fee = ori.node_or_fee in
            let balance =
              if get_status_from_manager_metadata ori.node_or_metadata then
                acc_vol
              else
                Int64.add balance acc_vol in
            balance, Int64.add fee acc_fee
          | NDelegation {node_del_fee = fee; _}
          | NReveal {node_rvl_fee = fee; _}
          | NActivate_protocol {node_acp_fee = fee; _}
          | NManage_accounts {node_macs_fee = fee; _}
          | NManage_account {node_mac_fee = fee; _} ->
            acc_vol, Int64.add fee acc_fee
          | _ -> acc_vol, acc_fee)
        (acc_vol, acc_fee) op.node_op_contents)
    (0L, 0L) ops

let endorsements_included ops =
  List.fold_left (fun acc opl ->
      let n =
        List.fold_left (fun acc2 op ->
            match op with
            | NEndorsement ed -> begin
                match ed.node_endorse_metadata with
                | None -> acc2
                | Some metadata ->
                  match metadata.meta_op_slots with
                  | None -> acc2
                  | Some slots -> acc2 + List.length slots end
            | _ -> acc2
          ) 0 opl.node_op_contents in
      acc + n) 0 ops

let register_all ?constants block lvl ops =
  let volume, fees = compute_volume_fees ops in
  let operation_count = Int64.of_int @@ List.length ops in
  let endorsements_included = endorsements_included ops in
  pg_lock (fun () ->
      register_header block.node_header;
      register_block ?constants block lvl (-1L) operation_count volume fees endorsements_included;
      register_operations ?constants block ops;
      register_balances_updates block
    )

let update_count_info ?(force=false) hash level info =
  match [%pgsql dbh "SELECT level FROM count_info WHERE info = $info"] with
  | [ level0 ] ->
    if (info = "lowest" && level0 > level) ||
       (info = "highest" && level0 < level) || force then
      [%pgsql dbh
        "UPDATE count_info SET hash = $hash, level = $level WHERE info = $info"]
  | _ ->
    [%pgsql dbh "INSERT INTO count_info (hash, level, info) \
                VALUES($hash, $level, $info)"]

let update_counts ?(force=false) hash sign =
  if included "counts" then
    let alt = if sign = 1L then 0L else 1L in
    match [%pgsql dbh
        "SELECT bl.cycle, bl.level, bl.priority, bl.fees, bl.baker, \
         bake_time(bl.timestamp - pred.timestamp, bl.priority, bl.endorsements_included, \
         time_between_blocks, delay_per_missing_endorsement, initial_endorsers), \
         time_between_blocks[1], bl.endorsements_included, block_reward, \
         endorsement_reward, endorsers_per_block, initial_endorsers \
         FROM block AS bl INNER JOIN block AS pred ON \
         pred.hash = bl.predecessor \
         INNER JOIN protocol_constants AS cst ON \
         COALESCE(bl.level BETWEEN cst.level_start AND cst.level_end, \
         cst.level_start <= bl.level AND cst.level_end IS NULL) \
         WHERE bl.hash = $hash AND cst.distance_level = 0"] with
    | [ cycle, level, priority, fees, baker, bktime, t0, endorsements_included,
        block_reward, endorsement_reward, endorsers_per_block, initial_endorsers ] ->
      let emmyp = initial_endorsers <> None in
      debug "[Writer] [update_counts_main] \
             Update counts for block %s at level %Ld with factor %Ld\n%!" hash level sign;
      let bktime = match bktime, t0 with
        | Some bktime, _ -> bktime
        | _, Some t0 ->
          debug "[Writer] [update_counts_main] \
                 Cannot compute bake time, setting it to time_between_blocks[0]\n%!";
          Int32.to_float t0
        | _ ->
          debug "[Writer] [update_counts_main] \
                 Cannot get time constants, setting bake time to 60s\n%!";
          60. in
      let bktime = Int64.to_float sign *. bktime in
      begin match [%pgsql dbh "SELECT cycle FROM cycle_count WHERE cycle = $cycle"] with
        | [] -> [%pgsql dbh "INSERT INTO cycle_count(cycle) VALUES($cycle)"]
        | _ -> () end;
      [%pgsql dbh (* cycle counts *)
          "WITH tr(nb) AS ( \
           SELECT COUNT( * ) FROM transaction_all WHERE op_block_hash = $hash), \
           dlg(nb) AS ( \
           SELECT COUNT( * ) FROM delegation_all WHERE op_block_hash = $hash), \
           ori(nb) AS ( \
           SELECT COUNT( * ) FROM origination_all WHERE op_block_hash = $hash), \
           act(nb) AS ( \
           SELECT COUNT( * ) FROM activation_all WHERE op_block_hash = $hash), \
           rvl(nb) AS ( \
           SELECT COUNT( * ) FROM reveal_all WHERE op_block_hash = $hash), \
           dbe(nb) AS ( \
           SELECT COUNT( * ) FROM double_baking_evidence_all WHERE op_block_hash = $hash), \
           dee(nb) AS ( \
           SELECT COUNT( * ) FROM double_endorsement_evidence_all WHERE op_block_hash = $hash), \
           nonce(nb) AS ( \
           SELECT COUNT( * ) FROM seed_nonce_revelation_all WHERE op_block_hash = $hash), \
           endo(nba, nb_op) AS ( \
           SELECT agg_prio(priority, array_length(slots, 1), true), count(*) \
           FROM endorsement_all WHERE op_block_hash = $hash) \
           UPDATE cycle_count SET \
           nb_transaction = nb_transaction + $sign * tr.nb, \
           nb_delegation = nb_delegation + $sign * dlg.nb, \
           nb_origination = nb_origination + $sign * ori.nb, \
           nb_activation = nb_activation + $sign * act.nb, \
           nb_reveal = nb_reveal + $sign * rvl.nb, \
           nb_dbe = nb_dbe + $sign * dbe.nb, \
           nb_dee = nb_dee + $sign * dee.nb, \
           nb_nonce = nb_nonce + $sign * nonce.nb, \
           nb_endorsement = array_lin(nb_endorsement, endo.nba, 1, $sign), \
           nb_endorsement_op = nb_endorsement_op + $sign * endo.nb_op,
       nb_prio = array_lin(nb_prio, init_array_prio($priority, 1), 1, $sign), \
           block_rewards = block_rewards + $sign * \
           block_reward_f($block_reward, $priority, \
           $endorsements_included, $endorsers_per_block, $emmyp), \
           endorsement_rewards = endorsement_rewards + $sign * \
           end_rewards_array($endorsement_reward, endo.nba)
       FROM tr, dlg, ori, act, rvl, dbe, dee, nonce, endo \
           WHERE cycle = $cycle"];
      [%pgsql dbh (* missed endorsement *)
          "WITH endo(dn, nb) AS ( \
           SELECT UNNEST(endorsers), UNNEST(slots) FROM level_rights \
           WHERE level = $level - 1::bigint) \
           UPDATE cycle_count_baker AS ccb SET \
           nb_miss_endorsement = nb_miss_endorsement + $sign::bigint * COALESCE(endo.nb, 0) \
           FROM endo WHERE endo.dn = ccb.dn AND cycle = $cycle"];
      [%pgsql dbh (* endorsement *)
          "WITH endo(dn, nba, nb) AS ( \
           SELECT source, agg_prio(priority, array_length(slots, 1), true), \
           SUM(array_length(slots, 1)) \
           FROM endorsement_all WHERE op_block_hash = $hash GROUP BY source) \
           UPDATE cycle_count_baker AS ccb SET \
           nb_endorsement = array_lin(nb_endorsement, endo.nba, 1, $sign), \
           nb_miss_endorsement = nb_miss_endorsement - $sign * endo.nb, \
           nb_alt_endorsement = nb_alt_endorsement + $alt \
           FROM endo WHERE endo.dn = ccb.dn AND cycle = $cycle"];
      [%pgsql dbh (* baking *)
          "UPDATE cycle_count_baker SET \
           nb_baking = array_lin(nb_baking, init_array_prio($priority, 1), 1, $sign), \
           fees = fees + $sign::bigint * $fees, \
           time = time + $bktime, \
           nb_alt_baking = nb_alt_baking + $alt \
           WHERE cycle = $cycle AND dn = $baker"];
      [%pgsql dbh (* missed baking *)
          "UPDATE cycle_count_baker SET nb_miss_baking = nb_miss_baking + $sign \
           FROM (SELECT UNNEST(bakers), UNNEST(bakers_priority) \
           FROM level_rights WHERE level = $level) AS lr(baker, prio) \
           WHERE prio < $priority AND dn = baker AND cycle = $cycle"];
      [%pgsql dbh (* block rewards *)
          "UPDATE cycle_count_baker SET \
           block_rewards = block_rewards + \
           $sign * block_reward_f($block_reward, $priority, $endorsements_included,
       $endorsers_per_block, $emmyp)
       WHERE cycle = $cycle AND dn = $baker"];
      [%pgsql dbh (* endorsement rewards *)
          "UPDATE cycle_count_baker SET \
           endorsement_rewards = endorsement_rewards + $sign * array_length(slots,1)::bigint * \
           $endorsement_reward / (1 + priority)::bigint \
           FROM endorsement_all \
           WHERE op_block_hash = $hash AND cycle = $cycle AND source = dn"];
      [%pgsql dbh (* transaction as source *)
          "WITH tr_src(dn, nb) AS ( \
           SELECT source, COUNT( * ) FROM transaction_all WHERE op_block_hash = $hash \
           GROUP BY source) \
           UPDATE operation_count_user AS ocu SET \
           nb_transaction_src = nb_transaction_src + $sign * coalesce(tr_src.nb,0) \
           FROM tr_src WHERE ocu.dn = tr_src.dn"];
      [%pgsql dbh (* transaction as destination without source *)
          "WITH tr_dst(dn, nb) AS ( \
           SELECT destination, COUNT( * ) FROM transaction_all \
           WHERE op_block_hash = $hash AND source <> destination \
           GROUP BY destination) \
           UPDATE operation_count_user AS ocu SET \
           nb_transaction_dst = nb_transaction_dst + $sign * coalesce(tr_dst.nb,0) \
           FROM tr_dst WHERE ocu.dn = tr_dst.dn"];
      [%pgsql dbh (* delegation as source *)
          "WITH dlg_src(dn, nb) AS ( \
           SELECT source, COUNT( * ) FROM delegation_all WHERE op_block_hash = $hash \
           GROUP BY source) \
           UPDATE operation_count_user AS ocu SET \
           nb_delegation_src = nb_delegation_src + $sign * coalesce(dlg_src.nb,0) \
           FROM dlg_src WHERE ocu.dn = dlg_src.dn"];
      [%pgsql dbh (* delegation as delegate without source *)
          "WITH dlg_dlg(dn, nb) AS ( \
           SELECT delegate, COUNT( * ) FROM delegation_all WHERE op_block_hash = $hash \
           AND delegate <> source GROUP BY delegate) \
           UPDATE operation_count_user AS ocu SET \
           nb_delegation_dlg = nb_delegation_dlg + $sign * coalesce(dlg_dlg.nb,0) \
           FROM dlg_dlg WHERE ocu.dn = dlg_dlg.dn"];
      [%pgsql dbh (* origination as source *)
          "WITH ori_src(dn, nb) AS ( \
           SELECT source, COUNT( * ) FROM origination_all WHERE op_block_hash = $hash \
           GROUP BY source) \
           UPDATE operation_count_user AS ocu SET \
           nb_origination_src = nb_origination_src + $sign * coalesce(ori_src.nb,0) \
           FROM ori_src WHERE ocu.dn = ori_src.dn"];
      [%pgsql dbh (* origination as manager without source *)
          "WITH ori_man(dn, nb) AS ( \
           SELECT manager, COUNT( * ) FROM origination_all WHERE op_block_hash = $hash \
           AND manager <> source GROUP BY manager) \
           UPDATE operation_count_user AS ocu SET \
           nb_origination_man = nb_origination_man + $sign * coalesce(ori_man.nb,0) \
           FROM ori_man WHERE ocu.dn = ori_man.dn"];
      [%pgsql dbh (* origination as kt1 *)
          "WITH ori_kt1(dn, nb) AS ( \
           SELECT kt1, COUNT( * ) FROM origination_all WHERE op_block_hash = $hash \
           GROUP BY kt1) \
           UPDATE operation_count_user AS ocu SET \
           nb_origination_kt1 = nb_origination_kt1 + $sign * coalesce(ori_kt1.nb,0) \
           FROM ori_kt1 WHERE ocu.dn = ori_kt1.dn"];
      [%pgsql dbh (* origination as delegate *)
          "WITH ori_dlg(dn, nb) AS ( \
           SELECT delegate, COUNT( * ) FROM origination_all WHERE op_block_hash = $hash \
           GROUP BY delegate) \
           UPDATE operation_count_user AS ocu SET \
           nb_origination_dlg = nb_origination_dlg + $sign * coalesce(ori_dlg.nb,0) \
           FROM ori_dlg WHERE ocu.dn = ori_dlg.dn"];
      [%pgsql dbh (* activation *)
          "WITH act(dn, nb) AS ( \
           SELECT pkh, COUNT( * ) FROM activation_all WHERE op_block_hash = $hash \
           GROUP BY pkh) \
           UPDATE operation_count_user AS ocu SET \
           nb_activation = nb_activation + $sign * coalesce(act.nb,0) \
           FROM act WHERE ocu.dn = act.dn"];
      [%pgsql dbh (* reveal *)
          "WITH rvl(dn, nb) AS ( \
           SELECT source, COUNT( * ) FROM reveal_all WHERE op_block_hash = $hash \
           GROUP BY source) \
           UPDATE operation_count_user AS ocu SET \
           nb_reveal = nb_reveal + $sign * coalesce(rvl.nb,0) \
           FROM rvl WHERE ocu.dn = rvl.dn"];
      [%pgsql dbh (* double baking evidence as denouncer/baker*)
          "WITH dbe_bk(dn, nb) AS ( \
           SELECT denouncer, COUNT( * ) FROM double_baking_evidence_all \
           WHERE op_block_hash = $hash \
           GROUP BY denouncer) \
           UPDATE operation_count_user AS ocu SET \
           nb_dbe_bk = nb_dbe_bk + $sign * coalesce(dbe_bk.nb,0) \
           FROM dbe_bk WHERE ocu.dn = dbe_bk.dn"];
      [%pgsql dbh (* double baking evidence as accused *)
          "WITH dbe_acc(dn, nb) AS ( \
           SELECT accused, COUNT( * ) FROM double_baking_evidence_all \
           WHERE op_block_hash = $hash AND accused <> denouncer \
           GROUP BY accused) \
           UPDATE operation_count_user AS ocu SET \
           nb_dbe_acc = nb_dbe_acc + $sign * coalesce(dbe_acc.nb,0) \
           FROM dbe_acc WHERE ocu.dn = dbe_acc.dn"];
      [%pgsql dbh (* double endorsement evidence as denouncer*)
          "WITH dee_dn(dn, nb) AS ( \
           SELECT denouncer, COUNT( * ) FROM double_endorsement_evidence_all \
           WHERE op_block_hash = $hash \
           GROUP BY denouncer) \
           UPDATE operation_count_user AS ocu SET \
           nb_dee_dn = nb_dee_dn + $sign * coalesce(dee_dn.nb,0) \
           FROM dee_dn WHERE ocu.dn = dee_dn.dn"];
      [%pgsql dbh (* double endorsement evidence as accused *)
          "WITH dee_acc(dn, nb) AS ( \
           SELECT accused, COUNT( * ) FROM double_endorsement_evidence_all \
           WHERE op_block_hash = $hash AND accused <> denouncer \
           GROUP BY accused) \
           UPDATE operation_count_user AS ocu SET \
           nb_dee_acc = nb_dee_acc + $sign * coalesce(dee_acc.nb,0) \
           FROM dee_acc WHERE ocu.dn = dee_acc.dn"];
      [%pgsql dbh (* nonce revelation *)
          "WITH nonce(dn, nb) AS ( \
           SELECT baker, COUNT( * ) FROM seed_nonce_revelation_all \
           WHERE op_block_hash = $hash \
           GROUP BY baker) \
           UPDATE operation_count_user AS ocu SET \
           nb_nonce = nb_nonce + $sign * coalesce(nonce.nb,0) \
           FROM nonce WHERE ocu.dn = nonce.dn"];
      [%pgsql dbh (* endorsement *)
          "WITH endo(dn, nb) AS ( \
           SELECT source, COUNT( * ) FROM endorsement_all \
           WHERE op_block_hash = $hash GROUP BY source ) \
           UPDATE operation_count_user AS ocu SET \
           nb_endorsement = nb_endorsement + $sign * coalesce(endo.nb,0) \
           FROM endo WHERE ocu.dn = endo.dn"];
      update_count_info ~force hash level "highest";
    | _ -> debug "[Writer] [update_counts_main] block not registered\n%!"

let reset_balance_from_balance_updates account diff frz up_type level  =
  debug "[Writer] [reset_balance] %s : undoing %s balance_update : diff = %s\n%!"
    account (if frz then "frozen" else "contract") (Int64.to_string diff);
  let level = Int64.of_int32 level in
  let cycle_in_bfbu =
    match [%pgsql dbh "SELECT cycle FROM cycle_limits \
                      WHERE $level BETWEEN level_start AND level_end"] with
    | cycle :: _ -> Int32.(add (Int64.to_int32 cycle) 1l)
    | _ -> 0l in
  (* We add one because the index of balance_from_balance_updates has an offset of 1 *)
  if not frz then
    begin
      [%pgsql dbh
        "UPDATE balance_from_balance_updates \
         SET spendable_balance=spendable_balance - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu"];
      [%pgsql dbh
        "UPDATE balance_snapshot \
         SET spendable_balance=spendable_balance - $diff \
         WHERE hash=$account"]
    end
  else
    let () =
      [%pgsql dbh
        "UPDATE balance_from_balance_updates \
         SET frozen=frozen - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu"];
      [%pgsql dbh
        "UPDATE balance_snapshot \
         SET frozen=frozen - $diff \
         WHERE hash=$account"]
    in
    match up_type with
      "Reward" ->
      [%pgsql dbh
        "UPDATE balance_from_balance_updates \
         SET rewards=rewards - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu"];
      [%pgsql dbh
        "UPDATE balance_snapshot \
         SET rewards=rewards - $diff \
         WHERE hash=$account"]
    | "Fee" ->
      [%pgsql dbh
        "UPDATE balance_from_balance_updates \
         SET fees=fees - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu"];
      [%pgsql dbh
        "UPDATE balance_snapshot \
         SET fees=fees - $diff \
         WHERE hash=$account"]
    | "Deposit" ->
      [%pgsql dbh
        "UPDATE balance_from_balance_updates \
         SET deposits=deposits - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu"];
      [%pgsql dbh
        "UPDATE balance_snapshot \
         SET deposits=deposits - $diff \
         WHERE hash=$account"]
    | s -> debug "[Writer] [update_balance_updates] %s undefined\n%!" s

let update_distance_level_alt level =
  [%pgsql dbh "UPDATE block SET distance_level = -1 WHERE level > $level"] ;
  [%pgsql dbh "UPDATE endorsement_all SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE endorsement_last SET distance_level = -1 WHERE op_level > $level"];
  [%pgsql dbh "UPDATE transaction_all SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE transaction_last SET distance_level = -1 WHERE op_level > $level"];
  [%pgsql dbh "UPDATE origination_all SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE origination_last SET distance_level = -1 WHERE op_level > $level"];
  [%pgsql dbh "UPDATE delegation_all SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE delegation_last SET distance_level = -1 WHERE op_level > $level"];
  [%pgsql dbh "UPDATE activation_all SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE activation_last SET distance_level = -1 WHERE op_level > $level"];
  [%pgsql dbh "UPDATE reveal_all SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE reveal_last SET distance_level = -1 WHERE op_level > $level"];
  [%pgsql dbh "UPDATE seed_nonce_revelation_all SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE token_operations SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE activate_protocol SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE manage_accounts SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE manage_account SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE double_baking_evidence_all SET distance_level = -1 WHERE op_level > $level"] ;
  [%pgsql dbh "UPDATE double_endorsement_evidence_all SET distance_level = -1 WHERE op_level > $level"] ;
  let level32= Int64.to_int32 level in
  let bad_bal_updt =
    [%pgsql dbh "SELECT hash,diff,frozen,update_type FROM balance_updates \
                WHERE level > $level32 AND distance_level = 0"] in
  List.iter
    (fun (account,diff,frz,up_type) ->
       reset_balance_from_balance_updates account diff frz up_type level32)
    bad_bal_updt;

  [%pgsql dbh "UPDATE balance_updates SET distance_level = -1 WHERE level > $level32"]

let update_balances level block_hash =
  if included "balance_updates" then (
    debug "[Writer] [update_balance_updates]\n%!";
    let level = Int32.of_int level in
    let good_bal_updts =
      [%pgsql dbh "SELECT hash,diff,frozen,update_type FROM balance_updates \
                   WHERE block_hash=$block_hash AND level=$level"]
    in
    register_balance_from_balance_updates level good_bal_updts
  )

let update_from_delegation ?(back=false) block_hash =
  if not back then
    [%pgsql dbh
        "WITH tmp(delegator, delegate) AS (\
         SELECT DISTINCT source, delegate FROM delegation_all \
         WHERE op_block_hash = $block_hash AND NOT failed) \
         UPDATE account_info SET delegate = tmp.delegate FROM tmp \
         WHERE hash = delegator"]
  else
    [%pgsql dbh
        "nullable-results"
        "WITH tmp(delegator, level) AS (\
         SELECT DISTINCT source, op_level FROM delegation_all \
         WHERE op_block_hash = $block_hash AND NOT failed), \
         tmp2(delegator, delegate) AS (\
         (SELECT tmp.delegator, dlg.delegate FROM tmp \
         LEFT JOIN delegation_all AS dlg ON dlg.source = tmp.delegator \
         WHERE op_level < level AND distance_level = 0 AND NOT failed \
         ORDER BY op_level DESC LIMIT 1) \
         UNION \
         (SELECT tmp.delegator, ori.delegate FROM tmp \
         LEFT JOIN origination_all AS ori ON ori.kt1 = tmp.delegator \
         WHERE distance_level = 0 AND NOT failed))
       UPDATE account_info SET delegate = tmp2.delegate FROM tmp2 \
         WHERE hash = delegator"]

let update_from_manage_account ?(back=false) block_hash =
  if not back then
    [%pgsql dbh
        "WITH tmp(pkh, maxrolls, set_maxrolls, admin, set_admin, white_list, \
         delegation, recovery, set_recovery) AS (\
         SELECT DISTINCT (CASE WHEN target IS NULL THEN source ELSE target END), \
         maxrolls, set_maxrolls, admin, set_admin, white_list, delegation, \
         recovery, set_recovery \
         FROM manage_account WHERE op_block_hash = $block_hash AND NOT failed) \
         UPDATE account_info AS ai SET \
         maxrolls = CASE WHEN tmp.set_maxrolls THEN tmp.maxrolls ELSE ai.maxrolls END, \
         admin = CASE WHEN tmp.set_admin THEN tmp.admin ELSE ai.admin END, \
         white_list = COALESCE(tmp.white_list, ai.white_list), \
         delegation = COALESCE(tmp.delegation, ai.delegation), \
         recovery = CASE WHEN tmp.set_recovery THEN tmp.recovery ELSE ai.recovery END \
         FROM tmp WHERE pkh = hash"]
  else
    [%pgsql dbh
        "nullable-results"
        "WITH tmp(pkh, level) AS (\
         SELECT DISTINCT (CASE WHEN target IS NULL THEN source ELSE target END), op_level \
         FROM manage_account WHERE op_block_hash = $block_hash AND not failed), \
         tmp2(pkh, maxrolls, set_maxrolls, admin, set_admin, white_list, delegation) AS (\
         SELECT pkh, maxrolls, set_maxrolls, admin, set_admin, white_list, delegation, \
         recovery, set_recovery \
         FROM tmp \
         LEFT JOIN manage_account AS ma ON ((ma.target IS NULL AND ma.source = tmp.pkh) \
         OR ma.source = tmp.pkh) \
         WHERE (NOT ma.failed) AND ma.op_level < tmp.level AND \
         ma.distance_level = 0 \
         ORDER BY op_level DESC),
       tmp_m(pkh, maxrolls) AS (\
         SELECT pkh, maxrolls FROM tmp2 \
         WHERE set_maxrolls IS NOT NULL LIMIT 1), \
         tmp_a(pkh, admin) AS (\
         SELECT pkh, admin FROM tmp2 \
         WHERE set_admin IS NOT NULL LIMIT 1), \
         tmp_w(pkh, white_list) AS (\
         SELECT pkh, white_list FROM tmp2 \
         WHERE white_list IS NOT NULL LIMIT 1), \
         tmp_d(pkh, delegation) AS (\
         SELECT pkh, delegation FROM tmp2 \
         WHERE delegation IS NOT NULL LIMIT 1), \
         tmp_r(pkh, recovery) AS (\
         SELECT pkh, recovery FROM tmp2 \
         WHERE set_recovery IS NOT NULL LIMIT 1), \
         tmp3(pkh, maxrolls, admin, white_list, delegation, recovery) AS (\
         SELECT COALESCE(tmp_m.pkh, tmp_a.pkh, tmp_w.pkh, tmp_d.pkh, tmp_r.pkh) AS pkh0, \
         maxrolls, admin, white_list, delegation, recovery \
         FROM tmp_m, tmp_a, tmp_w, tmp_d, tmp_r \
         WHERE tmp_m.pkh = COALESCE(tmp_m.pkh, tmp_a.pkh, tmp_w.pkh, tmp_d.pkh, tmp_r.pkh) \
         AND tmp_a.pkh = COALESCE(tmp_m.pkh, tmp_a.pkh, tmp_w.pkh, tmp_d.pkh, tmp_r.pkh) \
         AND tmp_w.pkh = COALESCE(tmp_m.pkh, tmp_a.pkh, tmp_w.pkh, tmp_d.pkh, tmp_r.pkh) \
         AND tmp_d.pkh = COALESCE(tmp_m.pkh, tmp_a.pkh, tmp_w.pkh, tmp_d.pkh, tmp_r.pkh) \
         AND tmp_r.pkh = COALESCE(tmp_m.pkh, tmp_a.pkh, tmp_w.pkh, tmp_d.pkh, tmp_r.pkh)) \
         UPDATE account_info SET \
         maxrolls = tmp3.maxrolls, \
         admin = tmp3.admin, \
         white_list = tmp3.white_list, \
         delegation = tmp3.delegation, \
         recovery = tmp3.recovery \
         FROM tmp3 \
         WHERE pkh = hash"]

let update_from_reveal ?(back=false) block_hash =
  if not back then
    [%pgsql dbh
        "WITH tmp(pkh, reveal) AS (\
         SELECT DISTINCT source, hash FROM reveal_all \
         WHERE op_block_hash = $block_hash AND NOT failed) \
         UPDATE account_info SET reveal = tmp.reveal FROM tmp \
         WHERE hash = pkh"]
  else
    [%pgsql dbh
        "WITH tmp(pkh, reveal) AS (\
         SELECT DISTINCT source, hash FROM reveal_all \
         WHERE op_block_hash = $block_hash AND NOT failed) \
         UPDATE account_info SET reveal = NULL FROM tmp \
         WHERE hash = pkh"]

let update_from_activation ?(back=false) block_hash =
  if not back then
    [%pgsql dbh
        "WITH tmp(pkh, activation) AS (\
         SELECT DISTINCT pkh, hash FROM activation_all \
         WHERE op_block_hash = $block_hash) \
         UPDATE account_info SET activation = tmp.activation FROM tmp \
         WHERE hash = pkh"]
  else
    [%pgsql dbh
        "WITH tmp(pkh, activation) AS (\
         SELECT DISTINCT pkh, hash FROM activation_all \
         WHERE op_block_hash = $block_hash) \
         UPDATE account_info SET activation = NULL FROM tmp \
         WHERE hash = pkh"]

let update_from_origination ?entrypoints ?(back=false) block_hash =
  if not back then (
    [%pgsql dbh
        "WITH tmp(pkh, origination, delegate, manager, spendable, delegatable, code, \
         storage, code_hash) AS (\
         SELECT DISTINCT kt1, hash, delegate, manager, spendable, delegatable, script_code, \
         script_storage_type, script_code_hash FROM origination_all \
         WHERE op_block_hash = $block_hash AND NOT failed) \
         UPDATE account_info SET \
         origination = tmp.origination, delegate = tmp.delegate, \
         manager = tmp.manager, spendable = tmp.spendable, \
         delegatable = tmp.delegatable, code = tmp.code, storage = tmp.storage,\
         code_hash = tmp.code_hash FROM tmp \
         WHERE hash = pkh"];
    match entrypoints with
    | None -> ()
    | Some f ->
      List.iter (fun pkh ->
          let entrypoints = f pkh in
          let entrypoints = List.rev @@ List.fold_left (fun acc (name, t) ->
              Some (Dune_encoding_min.Script.encode t) :: Some name :: acc) []
              entrypoints in
          [%pgsql dbh
              "UPDATE account_info SET entrypoints = $entrypoints WHERE hash = $pkh"])
        ([%pgsql dbh
            "SELECT DISTINCT kt1 FROM origination_all WHERE op_block_hash = $block_hash AND NOT failed"]))
  else
    [%pgsql dbh
        "WITH tmp(pkh, origination) AS (\
         SELECT DISTINCT kt1, hash FROM origination_all \
         WHERE op_block_hash = $block_hash AND NOT failed) \
         UPDATE account_info SET \
         origination = NULL, delegate = NULL, manager = NULL, spendable = NULL, \
         delegatable = NULL, code = NULL, storage = NULL, code_hash = NULL, \
         entrypoints = NULL FROM tmp \
         WHERE hash = pkh"]

let update_token_balances block_hash sign =
  if included "token" then (
    if sign > 0 then
      [%pgsql dbh
          "WITH tmp(account, contract, balance, tsp) AS ( \
           SELECT unnest(balance_accounts), contract, unnest(balances), timestamp \
           FROM token_operations \
           WHERE op_block_hash = $block_hash ORDER BY counter DESC), \
           tmp2(account, contract, balance, tsp) AS ( \
           SELECT account, contract, ARRAY_AGG(balance), tsp FROM tmp \
           GROUP BY account, contract, tsp) \
           UPDATE token_balances AS tb SET \
           balance = tmp2.balance[1], last_modified = tmp2.tsp, last_block = $block_hash \
           FROM tmp2 WHERE tb.account = tmp2.account AND tb.contract = tmp2.contract"]
    else
      let l =
        [%pgsql dbh
            "SELECT unnest(balance_accounts), contract FROM token_operations \
             WHERE op_block_hash = $block_hash"] in
      List.iter (function
          | Some account, contract ->
            [%pgsql dbh
                "UPDATE token_balances SET balance = 0, last_modified = '1970-01-01 00:00:00'::timestamp, \
                 last_block = NULL \
                 WHERE contract = $contract AND account = $account"];
            [%pgsql dbh
                "WITH tmp(balance, tsp, block) AS ( \
                 SELECT unnest(top.balances), top.timestamp, top.op_block_hash \
                 FROM token_operations AS top \
                 WHERE top.contract = $contract AND $account = any(balance_accounts) \
                 AND top.distance_level = 0 \
                 ORDER BY top.counter DESC LIMIT 1) \
                 UPDATE token_balances \
                 SET balance = tmp.balance, last_modified = tmp.tsp, \
                 last_block = tmp.block \
                 FROM tmp WHERE contract = $contract AND account = $account"]
          | _ -> ()) l
  )

let update_token_info block_hash sign =
  if included "token" then
    let sign = Int64.of_int sign in
    [%pgsql dbh
        "UPDATE token_info AS ti SET \
         total_supply = (total_supply::bigint + $sign * amount::bigint)::varchar \
         FROM token_operations AS top WHERE ti.contract = top.contract AND \
         top.kind = 'mint' AND op_block_hash = $block_hash"]

let update_account_info ?entrypoints ?back block_hash =
  if included "account_info" then (
    update_from_origination ?entrypoints ?back block_hash;
    update_from_activation ?back block_hash;
    update_from_delegation ?back block_hash;
    update_from_reveal ?back block_hash;
    update_from_manage_account ?back block_hash
  )

let update_constants hash distance_level =
  match [%pgsql dbh "SELECT level_start, distance_level FROM protocol_constants \
                    WHERE block_hash = $hash"] with
  | [] -> ()
  | (level_start, _d) :: _ ->
    debug "[Writer] [update_constants]\n%!";
    let distance_level = Int32.of_int distance_level in
    if distance_level = 0l then
      [%pgsql dbh
        "WiTH last_constants(hash) AS ( \
         SELECT block_hash FROM protocol_constants WHERE distance_level = 0 \
         ORDER BY level_start DESC LIMIT 1) \
         UPDATE protocol_constants AS cst SET level_end = $level_start - 1::bigint \
         FROM last_constants AS lc \
         WHERE lc.hash = cst.block_hash"]
    else
      [%pgsql dbh
        "WiTH last_constants(hash) AS ( \
         SELECT block_hash FROM protocol_constants WHERE distance_level = 0 \
         AND block_hash <> $hash \
         ORDER BY level_start DESC LIMIT 1) \
         UPDATE protocol_constants AS cst SET level_end = NULL \
         FROM last_constants AS lc \
         WHERE lc.hash = cst.block_hash"];
    [%pgsql dbh
      "UPDATE protocol_constants SET distance_level = $distance_level \
       WHERE block_hash = $hash"]

let reset_main_chain start_level level64 =
  debug "[Writer] [reset_main_chain] %d %Ld\n%!" start_level level64 ;
  begin match [%pgsql dbh "SELECT hash, level FROM block WHERE distance_level = 0 \
                          ORDER BY level DESC LIMIT 1"] with
  | [ hash, level ] ->
    let rec aux hash level =
      if level > level64 then (
        update_counts ~force:true hash (-1L);
        update_token_balances hash (-1);
        update_token_info hash (-1);
        update_constants hash (-1);
        update_account_info ~back:true hash;
        match [%pgsql dbh "SELECT predecessor FROM block \
                          WHERE hash = $hash"] with
        | [ pred ] -> aux pred (Int64.pred level)
        | _ -> ()) in
    aux hash level
  | _ -> () end;
  update_distance_level_alt level64;
  let start_level64 = Int64.of_int start_level in
  let cycle =
    [%pgsql dbh "SELECT cycle FROM block WHERE level = $start_level64 LIMIT 1"] in
  begin match cycle with
    | [ cycle ] ->
      [%pgsql dbh "INSERT INTO switch (cycle, switch_count, longest_alt_chain) \
                  VALUES ($cycle, 0, 0) ON CONFLICT DO NOTHING"]
    | _ -> ()
  end ;
  let level = Int64.to_int level64 in
  let chain_depth = start_level - level in
  let chain_depth64 = Int64.of_int chain_depth in
  if chain_depth > 1 then
    begin match cycle with
      | [ cycle ] ->
        let row =
          [%pgsql dbh "SELECT cycle, switch_count, longest_alt_chain FROM switch \
                      WHERE cycle = $cycle"] in
        begin match row with
        | [] ->
          [%pgsql dbh "INSERT INTO switch \
                      (cycle, switch_count, longest_alt_chain) \
                      VALUES \
                      ($cycle, 1, $chain_depth64)"]
        | [ (_, switch_count, longest_alt_chain) ] ->
          let longest = max longest_alt_chain chain_depth64 in
          let switch_count = Int64.add switch_count 1L in
          [%pgsql dbh "UPDATE switch SET \
                      switch_count = $switch_count, \
                      longest_alt_chain = $longest \
                      WHERE cycle = $cycle"]
        | _ -> ()
        end
      | _ -> ()
    end

let update_distance_level_main ?entrypoints hash =
  [%pgsql dbh "UPDATE block SET distance_level = 0 WHERE hash = $hash"];
  [%pgsql dbh "UPDATE endorsement_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE endorsement_last SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE transaction_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE transaction_last SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE origination_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE origination_last SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE delegation_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE delegation_last SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE activation_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE activation_last SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE reveal_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE reveal_last SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE balance_updates SET distance_level = 0 WHERE block_hash = $hash"];
  [%pgsql dbh "UPDATE seed_nonce_revelation_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE token_operations SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE activate_protocol SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE manage_accounts SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE manage_account SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE double_baking_evidence_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  [%pgsql dbh "UPDATE double_endorsement_evidence_all SET distance_level = 0 WHERE op_block_hash = $hash"];
  update_counts ~force:true hash 1L;
  update_constants hash 0;
  update_token_balances hash 1;
  update_token_info hash 1;
  update_account_info ?entrypoints hash

let register_main_chain ?entrypoints block =
  pg_lock (fun () ->
      let hash = block.node_hash in
      let pred_hash = block.node_header.header_shell.shell_predecessor in
      let start_level = block.node_header.header_shell.shell_level in
      let rec register_aux hash pred_hash curr_level =
        debug "[Writer] [register_main_chain] %d\n%!" curr_level ;

        match  [%pgsql dbh "SELECT predecessor, level, distance_level \
                           FROM block WHERE hash = $pred_hash"] with
        | [ _, level, 0L ] ->
          reset_main_chain start_level level ;
          update_balances curr_level hash;
          update_distance_level_main ?entrypoints hash
        | [ pred, level, _ ] ->
          register_aux pred_hash pred @@ Int64.to_int level ;
          update_balances curr_level hash;
          update_distance_level_main ?entrypoints hash
        | _ ->
          debug "[Writer] [main_chain] Can't recover main chain status for %s\n%!"
            pred_hash in
      register_aux hash pred_hash start_level;
    )

let peer_to_string peer =
  match peer with
  | None -> ""
  | Some s -> s

let last_connection =
  function
  | None -> "", ""
  | Some (point, date) -> peer_to_string (Some point), date


let get_country point =
  if point = "" then "", ""
  else begin
    try
      let ip =
        try
          Scanf.sscanf point "::ffff:%[0-9.]:%_d" (fun ip -> ip)
        with _ ->
          Scanf.sscanf point "%[0-9a-fA-F:]" (fun ip -> ip)
      in
      let gi = Geoip.init_exn Geoip.GEOIP_MEMORY_CACHE in
      let country_name = Option.value ~default:"" @@ Geoip.country_name_by_name gi ip in
      let country_code = Option.value ~default:"" @@ Geoip.country_code_by_name gi ip in
      Geoip.close gi ;
      country_name, country_code
    with _ ->
      debug "[Writer] Found unparseable ip: %s\n%!" point;
      "", ""
  end

let register_network_stats stats =
  pg_lock (fun () ->
      [%pgsql dbh "UPDATE peers SET state= 'disconnected'"] ;
      List.iter
        (fun { peer_id; country=_; score ; trusted ; conn_metadata ;
               state ; id_point ; stat ;
               last_failed_connection ; last_rejected_connection ;
               last_established_connection ; last_disconnection ;
               last_seen ; last_miss } ->
          let _conn_metadata = conn_metadata in (* TODO add to db *)
          let point_id = peer_to_string id_point in
          let state =
            match state with
            | Accepted -> "accepted"
            | Running -> "running"
            | Disconnected -> "disconnected" in
          let last_failed_connection_point, last_failed_connection_date =
            last_connection last_failed_connection in
          let last_rejected_connection_point, last_rejected_connection_date =
            last_connection last_rejected_connection in
          let last_established_connection_point, last_established_connection_date =
            last_connection last_established_connection in
          let last_disconnection_point, last_disconnection_date =
            last_connection last_disconnection in
          let last_seen_point, last_seen_date = last_connection last_seen in
          let last_miss_point, last_miss_date = last_connection last_miss in
          let total_sent = stat.total_sent in
          let total_recv = stat.total_recv in
          let current_inflow = Int64.of_int stat.current_inflow in
          let current_outflow = Int64.of_int  stat.current_outflow in
          match [%pgsql dbh "SELECT peer_id, point_id, country_name, country_code \
                            FROM peers \
                            WHERE peer_id = $peer_id"] with
          | [] -> (* No entry just insert a new row *)
            let country_name, country_code = get_country point_id  in
            [%pgsql dbh
              "INSERT INTO peers \
               (peer_id, country_name, country_code, point_id, trusted, score, state, total_sent, \
               total_received, current_inflow, current_outflow, \
               last_failed_connection_point, last_failed_connection_date, \
               last_rejected_connection_point, last_rejected_connection_date, \
               last_established_connection_point,last_established_connection_date, \
               last_disconnection_point, last_disconnection_date, \
               last_seen_point, last_seen_date, \
               last_miss_point, last_miss_date) \
               VALUES \
               ($peer_id, $country_name, $country_code, $point_id, $trusted, $score, $state, \
               $total_sent, $total_recv, $current_inflow, $current_outflow, \
               $last_failed_connection_point, $last_failed_connection_date, \
               $last_rejected_connection_point, $last_rejected_connection_date, \
               $last_established_connection_point, $last_established_connection_date, \
               $last_disconnection_point, $last_disconnection_date, \
               $last_seen_point, $last_seen_date, \
               $last_miss_point, $last_miss_date)"]
          | [ _, point_id_db, country_name, country_code ] -> (* Known peer, juste update info *)
            if point_id = "" then
              [%pgsql dbh
                "UPDATE peers \
                 SET \
                 trusted = $trusted, \
                 score = $score, \
                 state = $state, \
                 total_sent = $total_sent, \
                 total_received = $total_recv, \
                 current_inflow = $current_inflow, \
                 current_outflow = $current_outflow, \
                 last_failed_connection_point = $last_failed_connection_point, \
                 last_failed_connection_date = $last_failed_connection_date, \
                 last_rejected_connection_point = $last_rejected_connection_point, \
                 last_rejected_connection_date = $last_rejected_connection_date, \
                 last_established_connection_point = $last_established_connection_point, \
                 last_established_connection_date = $last_established_connection_date, \
                 last_disconnection_point = $last_disconnection_point, \
                 last_disconnection_date = $last_disconnection_date, \
                 last_seen_point = $last_seen_point, \
                 last_seen_date = $last_seen_date, \
                 last_miss_point = $last_miss_point, \
                 last_miss_date = $last_miss_date WHERE peer_id = $peer_id"]
            else
              let country_name, country_code =
                match point_id_db with
                | None -> country_name, country_code
                | Some point_id_db ->
                  if point_id_db = point_id then country_name, country_code
                  else get_country point_id in
              [%pgsql dbh
                "UPDATE peers \
                 SET \
                 country_name = $country_name, \
                 country_code = $country_code, \
                 point_id = $point_id, \
                 trusted = $trusted, \
                 score = $score, \
                 state = $state, \
                 total_sent = $total_sent, \
                 total_received = $total_recv, \
                 current_inflow = $current_inflow, \
                 current_outflow = $current_outflow, \
                 last_failed_connection_point = $last_failed_connection_point, \
                 last_failed_connection_date = $last_failed_connection_date, \
                 last_rejected_connection_point = $last_rejected_connection_point, \
                 last_rejected_connection_date = $last_rejected_connection_date, \
                 last_established_connection_point = $last_established_connection_point, \
                 last_established_connection_date = $last_established_connection_date, \
                 last_disconnection_point = $last_disconnection_point, \
                 last_disconnection_date = $last_disconnection_date, \
                 last_seen_point = $last_seen_point, \
                 last_seen_date = $last_seen_date, \
                 last_miss_point = $last_miss_point, \
                 last_miss_date = $last_miss_date WHERE peer_id = $peer_id"]
          | _ -> () )
        stats)

let block_hash level =
  debug "[Writer] block_hash/%i\n%!" level;
  let level = Int64.of_int level in
  match [%pgsql dbh
          "SELECT hash FROM block WHERE level = $level \
           AND distance_level = 0"] with
  | [ hash ] -> hash
  | _ -> assert false


let register_crawler_activity name delay =
  let timestamp = Unix.gettimeofday () in
  let delay = Int32.of_int delay in
  [%pgsql dbh "INSERT INTO crawler_activity (name, timestamp, delay) \
              VALUES ($name, $timestamp, $delay) ON CONFLICT (name) \
              DO UPDATE SET timestamp = $timestamp, delay = $delay"]

let update_alias ?(verbose=true) hash alias =
  let user_has_alias, old_alias =
    match [%pgsql dbh "SELECT alias FROM user_alias WHERE dn = $hash"] with
    | [ old_alias ] -> true, old_alias
    | _ -> false, "" in
  let alias_in_use_by =
    match alias with
    | None -> ""
    | Some alias ->
      let query_alias =
        [%pgsql dbh "SELECT dn FROM user_alias WHERE alias = $alias"] in
      match query_alias with
      | [ user ] -> user
      | _ -> "" in
  if alias_in_use_by <> "" then
    (if verbose then debug "[Writer] alias already in use by %s\n%!" alias_in_use_by)
  else
    match user_has_alias, alias with
    | false, Some alias when String.length alias > 0 ->
      [%pgsql dbh "INSERT INTO user_alias (dn, alias) VALUES ($hash, $alias)"];
      [%pgsql dbh "UPDATE dune_user SET alias = $alias WHERE hash = $hash"]
    | true, Some alias when String.length alias > 0 && alias <> old_alias ->
      [%pgsql dbh "UPDATE user_alias SET alias = $alias WHERE dn = $hash"];
      [%pgsql dbh "UPDATE dune_user SET alias = $alias WHERE hash = $hash"]
    | true, None | true, Some "" ->
      [%pgsql dbh "DELETE FROM user_alias WHERE dn = $hash"];
      [%pgsql dbh "UPDATE dune_user SET alias = NULL WHERE hash = $hash"]
    | _ -> ()

let reset_alias {Data_types.pkh; alias} =
  match alias with
  | None -> (* remove alias (outdated) *)
    [%pgsql dbh "DELETE FROM user_alias WHERE dn = $pkh"];
    [%pgsql dbh "UPDATE dune_user SET alias = NULL WHERE hash = $pkh"]
  | Some alias ->
    match [%pgsql dbh "SELECT dn FROM user_alias WHERE alias = $alias "] with
    | [ hash ] when hash <> pkh ->
      (* remove alias from old user and set it to new *)
      [%pgsql dbh "DELETE FROM user_alias WHERE alias = $alias"];
      [%pgsql dbh "UPDATE dune_user SET alias = NULL WHERE alias = $alias"];
      begin match [%pgsql dbh "SELECT alias FROM user_alias WHERE dn = $pkh"] with
        | [] ->
          [%pgsql dbh "INSERT INTO user_alias (dn, alias) VALUES ($pkh, $alias)"];
          [%pgsql dbh "UPDATE dune_user SET alias = $alias WHERE hash = $pkh"]
        | _ ->
          [%pgsql dbh "UPDATE user_alias SET alias = $alias WHERE dn = $pkh"];
          [%pgsql dbh "UPDATE dune_user SET alias = $alias WHERE hash = $pkh"] end
    | [] ->
      (* add new alias for a user *)
      begin match [%pgsql dbh "SELECT alias FROM user_alias WHERE dn = $pkh"] with
        | [] ->
          [%pgsql dbh "INSERT INTO user_alias (dn, alias) VALUES ($pkh, $alias)"];
          [%pgsql dbh "UPDATE dune_user SET alias = $alias WHERE hash = $pkh"]
        | _ ->
          [%pgsql dbh "UPDATE user_alias SET alias = $alias WHERE dn = $pkh"];
          [%pgsql dbh "UPDATE dune_user SET alias = $alias WHERE hash = $pkh"] end
    | _ -> (* user already exist with this alias *) ()


(* update highest info and counts if in alternative branch *)
let update_count_info_highest_alt () =
  let rec aux hash =
    match [%pgsql dbh "SELECT distance_level, predecessor, level \
                      FROM block WHERE hash = $hash"] with
    | [ -1L, pred, _ ] ->
      update_counts hash (-1L);
      aux pred
    | [ _, _, level ] ->
      [%pgsql dbh "UPDATE count_info SET \
                  hash = $hash, level = $level WHERE info = 'highest'"]
    | _ -> () in
  match [%pgsql dbh "SELECT hash FROM count_info WHERE info = 'highest'"] with
  | [ hash ] -> aux hash
  | _ -> ()

(* counts from a hash (head in None) down to a level *)
let reset_counts start_level end_hash =
  let rec aux hash level acc =
    Printf.printf "%Ld\n%!" level;
    if level >= start_level then (
      begin match [%pgsql dbh "SELECT predecessor FROM block WHERE hash = $hash"] with
        | [ pred ] -> aux pred (Int64.pred level) (hash :: acc)
        | _ -> assert false end )
    else (
      List.iter (fun hash -> update_counts hash 1L) acc;
      List.nth_opt acc 0)
  in
  match end_hash with
  | None ->
    begin match [%pgsql dbh "SELECT hash, level FROM block WHERE distance_level = 0 \
                            ORDER BY level DESC LIMIT 1"] with
    | [ hash, level ] ->
      let start_hash = aux hash level [] in
      begin match start_hash with
        | None -> ()
        | Some start_hash -> update_count_info start_hash start_level "lowest" end;
      update_count_info hash level "highest";
      List.iter (fun hash -> update_counts hash 0L)
        ([%pgsql dbh "SELECT hash FROM block WHERE distance_level <> 0 \
                     AND level >= $start_level"])
    | _ -> () end
  | Some end_hash ->
     begin match [%pgsql dbh "SELECT level FROM block WHERE hash = $end_hash"] with
    | [ level ] ->
      let start_hash = aux end_hash level [] in
      begin match start_hash with
        | None -> ()
        | Some start_hash -> update_count_info start_hash start_level "lowest" end;
      update_count_info end_hash level "highest";
      List.iter (fun hash -> update_counts hash 0L)
        ([%pgsql dbh "SELECT hash FROM block WHERE distance_level <> 0 \
                     AND level >= $start_level AND level <= $level"])
    | _ -> () end

(* extend count range if needed *)
let counts_downup start_level end_level =
  let start_level = Int64.of_int start_level in
  let end_level = Int64.of_int end_level in
  update_count_info_highest_alt ();
  let lowest_level, lowest_pred =
    match [%pgsql dbh "SELECT ci.level, bl.predecessor FROM count_info AS ci \
                      INNER JOIN block AS bl ON bl.hash = ci.hash \
                      WHERE ci.info = 'lowest'"] with
    | [ level, pred ] -> Some level, Some pred
    | _ ->
      [%pgsql dbh "INSERT INTO count_info (hash, level, info) \
                  SELECT hash, $start_level, 'lowest' FROM block \
                  WHERE level = $start_level AND distance_level = 0"];
      None, None in
  let highest_level =
    match [%pgsql dbh "SELECT level FROM count_info WHERE info = 'highest'"] with
    | [ level ] -> Some level
    | _ ->
      [%pgsql dbh "INSERT INTO count_info (hash, level, info) \
                  SELECT hash, $end_level, 'highest' FROM block \
                  WHERE level = $end_level AND distance_level = 0"];
      None in
  let get_end_hash end_level =
    if end_level = -1L then None
    else
      match [%pgsql dbh "SELECT hash FROM block WHERE level = $end_level AND \
                        distance_level = 0"] with
      | [ hash ] -> Some hash
      | _ -> None in
  let pairs =
    match lowest_level, lowest_pred, highest_level with
    | Some lowest_level, Some lowest_pred, Some highest_level ->
      let low_pair =
        if start_level < lowest_level then
          [start_level, Some lowest_pred]
        else [] in
      let high_pair =
        if end_level = -1L || end_level > highest_level then
          [Int64.succ highest_level, get_end_hash end_level]
        else [] in
      low_pair @ high_pair
    | _ ->  [start_level, get_end_hash end_level] in
  List.iter (fun (start_level, end_hash) -> reset_counts start_level end_hash) pairs

let register_service
    {Data_types.srv_kind; srv_dn1; srv_name; srv_url; srv_logo; srv_logo2; srv_logo_payout;
     srv_descr; srv_sponsored; srv_page; srv_delegations_page; srv_account_page;
     srv_aliases; srv_display_delegation_page; srv_display_account_page} =
  let dn1 = Option.value ~default:"" srv_dn1 in
  let logos = [Some srv_logo; srv_logo2; srv_logo_payout] in
  let descr = Option.value ~default:"" srv_descr in
  let page = Option.value ~default:"" srv_page in
  match srv_sponsored, srv_aliases with
  | None, _ ->
    [%pgsql dbh
      "INSERT INTO services (dn1, name, url, logos, description, kind, page, \
       delegations_page, account_page, display_delegation, display_account) \
       VALUES($dn1, $srv_name, $srv_url, $logos, $descr, $srv_kind, $page, \
       $srv_delegations_page, $srv_account_page, \
       $srv_display_delegation_page, $srv_display_account_page)"]
  | Some tsp, aliases ->
    let end_tsp = Pg_helper.cal_of_string tsp in
    match aliases with
    | None ->
      [%pgsql dbh
        "INSERT INTO services (dn1, name, url, logos, description, kind, sponsored, \
         page, delegations_page, account_page, display_delegation, display_account) \
         VALUES($dn1, $srv_name, $srv_url, $logos, $descr, $srv_kind, $end_tsp, \
         $page, $srv_delegations_page, $srv_account_page, \
         $srv_display_delegation_page, $srv_display_account_page)"]
    | Some l ->
      let addresses = List.map (fun {Data_types.pkh; _} -> Some pkh) l in
      let aliases = List.map (fun {Data_types.alias; _} -> alias) l in
      [%pgsql dbh
        "INSERT INTO services (dn1, name, url, logos, description, kind, sponsored, \
         page, delegations_page, account_page, addresses, aliases, display_delegation, \
         display_account) \
         VALUES($dn1, $srv_name, $srv_url, $logos, $descr, $srv_kind, $end_tsp, \
         $page, $srv_delegations_page, $srv_account_page, $addresses, $aliases, \
         $srv_display_delegation_page, $srv_display_account_page)"]

let check_and_update_service service delegate_details =
  let open Data_types in
  let name = service.srv_name in
  match [%pgsql.object dbh "SELECT * FROM services WHERE name = $name"] with
  | [] ->
    debug "[Writer] Registering new service %s\n%!" name;
    register_service service
  | [ srv ] ->
    let srv = Pg_helper.service_from_db srv in
    let is_active = Option.fold
        ~none:false ~some:(fun d -> not d.delegate_deactivated)
        delegate_details in
    let new_kind =
      if service.srv_kind = "delegate" && not is_active then "former-delegate"
      else service.srv_kind in
    let service = {service with srv_kind = new_kind} in
    if srv <> service then (
      debug "[Writer] Updating service %s\n%!" name;
      [%pgsql dbh "DELETE FROM services WHERE name = $name"];
      register_service service)
  | _ ->
    debug "[Writer] Several services with same name: %s\n%!" name

let register_coingecko_exchange
    {Data_types.gk_last; gk_target; gk_tsp; gk_volume; gk_base; gk_market; gk_converted_last; _ } =
  let price_usd = gk_converted_last.Data_types.gk_usd in
  match [%pgsql dbh
          "SELECT timestamp FROM coingecko_exchange WHERE \
           name = $gk_market AND base = $gk_base AND target = $gk_target"] with
  | [ tsp ] -> if tsp <> gk_tsp then
      [%pgsql dbh
        "UPDATE coingecko_exchange SET \
         timestamp = $gk_tsp, volume = $gk_volume, conversion = $gk_last, \
         price_usd = $price_usd \
         WHERE name = $gk_market AND base = $gk_base AND target = $gk_target"]
  | _ ->
    [%pgsql dbh
      "INSERT INTO coingecko_exchange (name, base, target, timestamp, volume, \
       conversion, price_usd) \
       VALUES($gk_market, $gk_base, $gk_target, $gk_tsp, $gk_volume, $gk_last, \
       $price_usd)"]

let register_coingecko_prices {Data_types.gk_last_updated; gk_market_data = d; _} =
  let to_array {Data_types.gk_usd; gk_btc} = [ Some gk_usd; Some gk_btc ] in
  let last_updated = match String.index_opt gk_last_updated '.' with
    | None -> gk_last_updated
    | Some i -> (String.sub gk_last_updated 0 i) ^ "Z" in
  let last_updated = Pg_helper.cal_of_string last_updated in
  let price = to_array d.Data_types.gk_price in
  let volume = to_array d.Data_types.gk_market_volume in
  let change_1h = to_array d.Data_types.gk_1h in
  let change_24h = to_array d.Data_types.gk_24h in
  let change_7d = to_array d.Data_types.gk_7d in
  [%pgsql dbh
    "INSERT into price(last_updated, price, volume, change_1h, change_24h, change_7d, origin) \
     VALUES($last_updated, $price, $volume, $change_1h, $change_24h, $change_7d, 'coingecko') \
     ON CONFLICT DO NOTHING"]

let register_cmc_prices (btc_quote, dun_quote) =
  let to_array0 v = [ Some v; Some (v /. btc_quote.Data_types.cmc_price) ] in
  let to_array1 usd btc = [ Some usd; Some ( ((usd +. 100.) /. (btc +. 100.) -. 1.) *. 100.)  ] in
  let last_updated = match String.index_opt dun_quote.Data_types.cmc_last_updated '.' with
    | None -> dun_quote.Data_types.cmc_last_updated
    | Some i -> (String.sub dun_quote.Data_types.cmc_last_updated 0 i) ^ "Z" in
  let last_updated = Pg_helper.cal_of_string last_updated in
  let price = to_array0 dun_quote.Data_types.cmc_price in
  let volume = to_array0 dun_quote.Data_types.cmc_volume in
  let change_1h = to_array1 dun_quote.Data_types.cmc_1h btc_quote.Data_types.cmc_1h in
  let change_24h = to_array1 dun_quote.Data_types.cmc_24h btc_quote.Data_types.cmc_24h in
  let change_7d = to_array1 dun_quote.Data_types.cmc_7d btc_quote.Data_types.cmc_7d in
  [%pgsql dbh
    "INSERT into price(last_updated, price, volume, change_1h, change_24h, change_7d, origin) \
     VALUES($last_updated, $price, $volume, $change_1h, $change_24h, $change_7d, 'cmc') \
     ON CONFLICT DO NOTHING"]

let last_voting_period () =
  match [%pgsql dbh "SELECT MAX(voting_period) from quorum"] with
  | [ Some voting_period ] -> Int32.to_int voting_period
  | _ -> -1

let register_voting_rolls voting_period rolls =
  let voting_period = Int32.of_int voting_period in
  List.iter (fun {vroll_pkh; vroll_count} ->
      let vroll_count = Int32.of_int vroll_count in
      [%pgsql dbh
        "INSERT INTO snapshot_voting_rolls (voting_period, delegate, rolls) \
         VALUES($voting_period, $vroll_pkh, $vroll_count) \
         ON CONFLICT DO NOTHING"]
    ) rolls;
  [%pgsql dbh "UPDATE snapshot_voting_rolls SET ready = true \
              WHERE voting_period = $voting_period"]

let register_quorum voting_period quorum =
  let voting_period32 = Int32.of_int voting_period in
  let voting_period64 = Int64.of_int voting_period in
  [%pgsql dbh
    "INSERT INTO quorum (voting_period, value, voting_period_kind) \
     (SELECT $voting_period32, $quorum, voting_period_kind FROM block WHERE \
     voting_period = $voting_period64 LIMIT 1) \
     ON CONFLICT DO NOTHING"]

let constants ?limit () =
  let constants = match limit with
    | None ->
      [%pgsql dbh "SELECT * FROM protocol_constants WHERE distance_level = 0 \
                  ORDER BY level_start"]
    | Some limit ->
      let limit = Int64.of_int limit in
      [%pgsql dbh "SELECT * FROM protocol_constants WHERE distance_level = 0 \
                  ORDER BY level_start DESC LIMIT $limit"] in
  List.map Pg_helper.constants_from_db constants

let has_level_rights level =
  let level = Int64.of_int level in
  [%pgsql dbh "SELECT level FROM level_rights WHERE level = $level AND ready"] <> []



(* pending  operations *)

let pending_seed_nonce_revelation ?errors hash branch status tsp snr =
  let level = Int64.of_int snr.node_seed_level in
  let nonce = snr.node_seed_nonce in
  [%pgsql dbh
    "INSERT INTO seed_nonce_revelation_pending \
     (hash, branch, status, timestamp_op, errors, level, nonce) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $level, $nonce)"]

let pending_activation ?errors hash branch status tsp act =
  let pkh = act.node_act_pkh in
  let secret = act.node_act_secret in
  register_dune_user pkh;
  [%pgsql dbh
    "INSERT INTO activation_pending \
     (hash, branch, status, timestamp_op, errors, pkh, secret) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $pkh, $secret)"]

let pending_endorsement ?errors hash branch status tsp endo =
  let level = Int64.of_int endo.node_endorse_block_level in
  let priority =
    match [%pgsql dbh "SELECT priority FROM block WHERE hash = $branch"] with
    | [ p ] -> Some p
    | _ -> None in
  [%pgsql dbh
    "INSERT INTO endorsement_pending \
     (hash, branch, status, timestamp_op, errors, block_level, priority) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $level, $?priority)"]

let pending_delegation ?errors hash branch status tsp dlg =
  let source = dlg.node_del_src in
  let counter = Z.to_int64 dlg.node_del_counter in
  let fee = dlg.node_del_fee in
  let gas_limit = Z.to_int64 dlg.node_del_gas_limit in
  let storage_limit = Z.to_int64 dlg.node_del_storage_limit in
  register_dune_user source;
  let delegate = dlg.node_del_delegate in
  register_dune_user delegate;
  [%pgsql dbh
    "INSERT INTO delegation_pending \
     (hash, branch, status, timestamp_op, errors, source, fee, counter, \
     delegate, gas_limit, storage_limit) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $source, $fee, $counter, \
     $delegate, $gas_limit, $storage_limit)"]

let pending_reveal ?errors hash branch status tsp rvl =
  let source = rvl.node_rvl_src in
  let counter = Z.to_int64 rvl.node_rvl_counter in
  let fee = rvl.node_rvl_fee in
  let gas_limit = Z.to_int64 rvl.node_rvl_gas_limit in
  let storage_limit = Z.to_int64 rvl.node_rvl_storage_limit in
  register_dune_user source;
  let pubkey = rvl.node_rvl_pubkey in
  register_dune_user pubkey;
  [%pgsql dbh
    "INSERT INTO reveal_pending \
     (hash, branch, status, timestamp_op, errors, source, fee, counter, pubkey, \
     gas_limit, storage_limit) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $source, $fee, $counter, $pubkey, \
     $gas_limit, $storage_limit)"]

let pending_transaction ?errors hash branch status tsp tr =
  let source = tr.node_tr_src in
  let counter = Z.to_int64 tr.node_tr_counter in
  let fee = tr.node_tr_fee in
  let gas_limit = Z.to_int64 tr.node_tr_gas_limit in
  let storage_limit = Z.to_int64 tr.node_tr_storage_limit in
  register_dune_user source;
  let dst = tr.node_tr_dst in
  register_dune_user dst;
  let amount = tr.node_tr_amount in
  let parameters = tr.node_tr_parameters in
  let collect_fee_gas = Option.map Z.to_int64 tr.node_tr_collect_fee_gas in
  let collect_pk = tr.node_tr_collect_pk in
  [%pgsql dbh
    "INSERT INTO transaction_pending \
     (hash, branch, status, timestamp_op, errors, source, destination, fee, \
     counter, amount, parameters, gas_limit, storage_limit, \
     collect_fee_gas, collect_pk) \
     VALUES \
     ($hash, $branch, $status, $tsp, $?errors, $source, $dst, $fee, $counter, \
     $amount, $?parameters, $gas_limit, $storage_limit, \
     $?collect_fee_gas, $?collect_pk)"]

let pending_origination ?errors hash branch status tsp ori =
  let source = ori.node_or_src in
  let counter = Z.to_int64 ori.node_or_counter in
  let fee = ori.node_or_fee in
  let gas_limit = Z.to_int64 ori.node_or_gas_limit in
  let storage_limit = Z.to_int64 ori.node_or_storage_limit in
  register_dune_user source;
  let manager = ori.node_or_manager in
  register_dune_user manager;
  let delegate = match ori.node_or_delegate with
    | None -> "" | Some delegate -> delegate in
  register_dune_user delegate ;
  let balance = ori.node_or_balance in
  let spendable = match ori.node_or_spendable with
    | Some spendable -> spendable
    | None -> match ori.node_or_script with
      | Some _ -> false
      | _ -> true in
  let sc_code, sc_storage, sc_code_hash = match ori.node_or_script with
    | None -> None, None, None
    | Some sc -> sc.sc_code, Some sc.sc_storage, sc.sc_code_hash in
  let kt1 = Crypto.op_to_KT1 hash in
  let delegatable = match ori.node_or_delegatable with
    | Some delegatable -> delegatable
    | None -> true in
  register_dune_user kt1;
  [%pgsql dbh
    "INSERT INTO origination_pending \
     (hash, branch, status, timestamp_op, errors, source, kt1, fee, counter, \
     manager, delegate, script_code, script_storage_type, script_code_hash, \
     spendable, delegatable, balance, gas_limit, storage_limit) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $source, $kt1, $fee, $counter, \
     $manager, $delegate, $?sc_code, $?sc_storage, $?sc_code_hash, $spendable, \
     $delegatable, $balance, $gas_limit, $storage_limit)"]

let get_pending_timestamp hash branch tsp op =
  let aux = function [ tsp ] -> tsp | _ -> tsp in
  match op with
  | NSeed_nonce_revelation _ ->
    aux ([%pgsql dbh "SELECT timestamp_op FROM seed_nonce_revelation_pending \
                     WHERE hash = $hash AND branch = $branch"])
  | NActivation _ ->
    aux ([%pgsql dbh "SELECT timestamp_op FROM activation_pending \
                     WHERE hash = $hash AND branch = $branch"])
  | NEndorsement _ ->
    aux ([%pgsql dbh "SELECT timestamp_op FROM endorsement_pending \
                     WHERE hash = $hash AND branch = $branch"])
  | NDelegation _ ->
    aux ([%pgsql dbh "SELECT timestamp_op FROM delegation_pending \
                     WHERE hash = $hash AND branch = $branch"])
  | NReveal _ ->
    aux ([%pgsql dbh "SELECT timestamp_op FROM reveal_pending \
                     WHERE hash = $hash AND branch = $branch"])
  | NTransaction _ ->
    aux ([%pgsql dbh "SELECT timestamp_op FROM transaction_pending \
                     WHERE hash = $hash AND branch = $branch"])
  | NOrigination _ ->
    aux ([%pgsql dbh "SELECT timestamp_op FROM origination_pending \
                     WHERE hash = $hash AND branch = $branch"])
  | _ -> tsp

let clear_pending () =
  [%pgsql dbh "DELETE FROM seed_nonce_revelation_pending"];
  [%pgsql dbh "DELETE FROM activation_pending"];
  [%pgsql dbh "DELETE FROM endorsement_pending"];
  [%pgsql dbh "DELETE FROM delegation_pending"];
  [%pgsql dbh "DELETE FROM reveal_pending"];
  [%pgsql dbh "DELETE FROM transaction_pending"];
  [%pgsql dbh "DELETE FROM origination_pending"]


let register_pending tsp mempool =
  if included "pending" then (
    let get_tsp l =
      List.map (fun {pending_hash; pending_branch; pending_contents; pending_errors; _} ->
          match pending_contents with
          | [] -> pending_hash, pending_branch, [], pending_errors, tsp
          | op :: _ ->
            pending_hash, pending_branch, pending_contents, pending_errors,
            get_pending_timestamp pending_hash pending_branch tsp op) l in
    let applied = get_tsp mempool.applied in
    let refused = get_tsp mempool.refused in
    let branch_refused = get_tsp mempool.branch_refused in
    let branch_delayed = get_tsp mempool.branch_delayed in
    let unprocessed = get_tsp mempool.unprocessed in
    clear_pending ();
    let register status l = List.iter
        (fun (hash, branch, ops, errors, tsp) ->
           let errors = Option.map format_errors errors in
           List.iter (function
               | NSeed_nonce_revelation snr ->
                 pending_seed_nonce_revelation ?errors hash branch status tsp snr
               | NActivation act ->
                 pending_activation ?errors hash branch status tsp act
               | NEndorsement endo ->
                 pending_endorsement ?errors hash branch status tsp endo
               | NDelegation dlg ->
                 pending_delegation ?errors hash branch status tsp dlg
               | NReveal rvl ->
                 pending_reveal ?errors hash branch status tsp rvl
               | NTransaction tr ->
                 pending_transaction ?errors hash branch status tsp tr
               | NOrigination ori ->
                 pending_origination ?errors hash branch status tsp ori
               | _ -> ())
             ops)
        l in
    register "applied" applied;
    register "refused" refused;
    register "branch_refused" branch_refused;
    register "branch_delayed" branch_delayed;
    register "unprocessed" unprocessed
  )
