open Data_types_min

let make_transfer01 ts_src dst ts_amount =
  [ {ts_src; ts_dst = Some (Alias.to_name dst); ts_amount;
     ts_options = None; ts_flag = "ds20-1"; ts_kind = TS_Transfer} ]

let make_transfer02 ?(ts_flag="ds20-2") ts_src dst ts_amount data =
  let dst = match dst with
    | String dst -> Some dst
    | Bytes h -> Some (Crypto.hex_to_account h)
    | _ -> None in
  match dst with
  | None -> []
  | Some dst ->
    let ts_options = match data with
      | Prim ("Some", [ Bytes options ], _) -> Some options
      | _ -> None in [ {
        ts_src; ts_dst = Some (Alias.to_name dst); ts_amount; ts_options; ts_flag;
        ts_kind = TS_Transfer } ]

let make_transfer02_love ?(ts_flag="ds20-2") ts_src dst ts_amount data =
  let ts_options = match data with
    | Love_value.Value.(VConstr ("Some", [VBytes options])) ->
      Some (Love_shortcut_encoding.MBytes.to_hex options)
    | _ -> None in [ {
      ts_src; ts_dst = Some (Alias.to_name dst); ts_amount; ts_options; ts_flag;
      ts_kind = TS_Transfer } ]

let make_burn ?(ts_flag="ds20-2") ts_src ts_amount =
  [ {ts_src; ts_dst = None; ts_amount = Z.neg ts_amount;
     ts_options = None; ts_flag; ts_kind = TS_Burn} ]

let make_mint ?(ts_flag="ds20-2") ts_src l =
  List.rev @@ List.fold_left (fun acc x -> match x with
      | Prim ("Pair", [String dst; Int ts_amount], _) ->
        {ts_src; ts_dst = Some (Alias.to_name dst); ts_amount;
         ts_options = None; ts_flag; ts_kind = TS_Mint} :: acc
      | _ -> acc) [] l

let make_mint_love ?(ts_flag="ds20-2") ts_src l =
  List.rev @@ List.fold_left (fun acc x -> match x with
      | Love_value.Value.(VTuple [
          VAddress dst; VNat ts_amount ]) ->
        {ts_src; ts_dst = Some (Alias.to_name dst); ts_amount;
         ts_options = None; ts_flag; ts_kind = TS_Mint} :: acc
      | _ -> acc) [] l

let parse_athen_micheline_encoding caller e =
  let ts_src = Alias.to_name caller in
  let ts_flag = "ds20-2" in
  match e with
  | Prim ("Left", [Prim ("Pair", [String dst; Int ts_amount], _)], _) ->
    make_transfer01 ts_src dst ts_amount
  | Prim ("Left", [Prim ("Pair", [
      dst; Prim ("Pair", [Int ts_amount; data], _)], _)], _) ->
    make_transfer02 ~ts_flag ts_src dst ts_amount data
  | Prim ("Right", [Prim ("Right", [ Prim ("Right", [Int ts_amount], _) ], _)], _)
  | Prim ("Right", [Prim ("Right", [ Prim ("Right", [Prim ("Left", [Int ts_amount ], _)], _)], _)], _) ->
    make_burn ~ts_flag ts_src ts_amount
  | Prim ("Right", [Prim ("Right", [Prim ("Right", [Seq l], _)], _)], _) ->
    make_mint ~ts_flag ts_src l
  | _ -> []

let parse_babylon_micheline_encoding caller entrypoint e =
  let ts_src = Alias.to_name caller in
  let ts_flag = "ds20-2" in
  match entrypoint, e with
  | EPNamed "transfer", Prim ("Pair", [String dst; Int ts_amount], _) ->
    make_transfer01 ts_src dst ts_amount
  | EPNamed "transfer", Prim ("Pair", [
      dst; Prim ("Pair", [Int ts_amount; data], _)], _) ->
    make_transfer02 ~ts_flag ts_src dst ts_amount data
  | EPNamed "burn", Int ts_amount ->
    make_burn ~ts_flag ts_src ts_amount
  | EPNamed "mint", Seq l ->
    make_mint ~ts_flag ts_src l
  | EPDefault, e ->
    parse_athen_micheline_encoding caller e
  | _ -> []

let parse_athen_love_encoding _caller _ = []

let parse_babylon_love_encoding caller entrypoint e =
  let ts_src = Alias.to_name caller in
  let ts_flag = "ds20-2" in
  match entrypoint, e with
  | EPNamed "transfer", Love_value.Value.(VTuple [
      VAddress dst; VNat amount; data ]) ->
    make_transfer02_love ~ts_flag ts_src dst amount data
  | EPNamed "burn", Love_value.Value.VNat ts_amount ->
    make_burn ~ts_flag ts_src ts_amount
  | EPNamed "mint", Love_value.Value.VList l ->
    make_mint_love ~ts_flag ts_src l
  | _ -> []

let parse_athen_encoding caller = function
  | Micheline s -> parse_athen_micheline_encoding caller s
  | _ -> []

let parse_babylon_encoding caller entrypoint = function
  | Micheline s -> parse_babylon_micheline_encoding caller entrypoint s
  | DuneExpr (LoveExpr (LoveValue v)) ->
    parse_babylon_love_encoding caller entrypoint v
  | _ -> []

let decode caller s =
  match Api_encoding.Script.decode_parameters s with
  | Athen_scr s -> parse_athen_encoding caller s
  | Babylon_scr (e, Some s) -> parse_babylon_encoding caller e s
  | Babylon_scr (_e, None) -> []


let token_op_to_str = function
  | TS_Transfer -> "transfer"
  | TS_Burn -> "burn"
  | TS_Mint -> "mint"
  | TS_Unknown -> "unknown"

let token_info storage code =
  match Api_encoding.Script.decode storage, Option.map Api_encoding.Script.decode code with
  | Micheline (Prim ("Pair", [
      _; Prim ("Pair", [
          Int total_supply;
          Prim ("Pair", [
              Prim ("Pair", [
                  Prim ("Pair", [ Int version0; Int version1], _);
                  Prim ("Pair", [
                      Int decimals;
                      Prim ("Pair", [String name; String symbol], _) ], _) ], _);
              _ ], _) ], _) ], _)), _ ->
    let total_supply = Z.to_string total_supply in
    let decimals = Z.to_int32 decimals in
    let version = (Z.to_string version0) ^ "." ^ (Z.to_string version1) in
    Some (name, symbol, total_supply, decimals, version, "michelson")
  | DuneExpr (LoveExpr (LoveValue (Love_value.Value.(VTuple [
      VAddress _owner;
      VNat decimals;
      VString name;
      VString symbol ])))),
    Some (DuneCode (LoveCode (LoveTopContract Love_ast_types.AST.{version; _}))) ->
    let decimals = Z.to_int32 decimals in
    Some (name, symbol, "0", decimals,
          string_of_int (fst version) ^ "." ^ string_of_int (snd version), "love")
  | _ -> None
