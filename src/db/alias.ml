(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types

let alias_table : (string, string) Hashtbl.t = Hashtbl.create 1000

let add_alias {pkh; alias} =
  match alias with
  | None -> ()
  | Some alias -> Hashtbl.add alias_table pkh alias

let get_alias_tbl dn = Hashtbl.find_opt alias_table dn

let reset () = Hashtbl.reset alias_table

let to_name ?alias pkh =
  if pkh = "" then {pkh = ""; alias = None}
  else
    let alias = if alias = None then get_alias_tbl pkh else alias in
    { pkh ; alias }
