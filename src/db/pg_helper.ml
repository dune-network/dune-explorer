(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types
open Db_intf

let string_of_cal t =
  CalendarLib.Printer.Calendar.sprint Date.format t
let cal_of_string s =
  CalendarLib.Printer.Calendar.from_fstring Date.format s

let date_of_cal t =
  Date.from_string (CalendarLib.Printer.Calendar.sprint Date.format t)
let cal_of_date s =
  CalendarLib.Printer.Calendar.from_fstring Date.format (Date.to_string s)

let date_of_cal_dune t =
  Dune_types.Date.from_string
    (CalendarLib.Printer.Calendar.sprint Dune_types.Date.format t)
let cal_of_date_dune s =
  CalendarLib.Printer.Calendar.from_fstring Dune_types.Date.format
    (Dune_types.Date.to_string s)

let rows_to_option = function
  | [] -> None
  | [x] -> Some x
  | _ -> assert false

let protocol_not_found = "Can't recover this protocol"

let option_to_protocol = function
  | None -> protocol_not_found
  | Some p -> p

let hash_selector_of_hash selector =
  match String.get selector 0 with
  | 'b' | 'B' -> Block selector
  | 'd' | 'K' -> Account selector
  | 't' -> Account (Crypto.tz_to_dn selector)
  | _ -> Empty

let mem_filter filter filters = List.mem filter filters

let anon_types filters =
  List.fold_left (fun (nonce, activation, dbe, dee) s ->
      if s = "Nonce" then (true, activation, dbe, dee)
      else if s = "Activation" then (nonce, true, dbe, dee)
      else if s = "Double_baking_evidence" then (nonce, activation, true, dee)
      else if s = "Double_endorsement_evidence" then (nonce, activation, dbe, true)
      else (nonce, activation, dbe, dee)) (false, false, false, false) filters

let manager_types filters =
  List.fold_left (fun (tr, ori, del, rvl) s ->
      if s = "Transaction" then (true, ori, del, rvl)
      else if s = "Origination" then (tr, true, del, rvl)
      else if s = "Delegation" then (tr, ori, true, rvl)
      else if s = "Reveal" then (tr, ori, del, true)
      else (tr, ori, del, rvl)) (false, false, false, false) filters

let dune_manager_types filters =
  List.fold_left (fun (mac, macs, acp) s ->
      if s = "Manage_account" then (true, macs, acp)
      else if s = "Manage_accounts" then (mac, true, acp)
      else if s = "Activate_protocol" then (mac, macs, true)
      else (mac, macs, acp)) (false, false, false) filters

let noop = function None -> assert false | Some x -> x

let to_name_opt ?alias = function
  | None -> None
  | Some pkh -> Some {pkh; alias}

let test_opt f = function
  | None -> None, true
  | Some x -> Some (f x), false

let test_opti = function
  | None -> None, true
  | Some x -> Some x, false

let block_noop r =
  let block_protocol = { proto_name = r#protocol; proto_hash = r#protocol } in
  let test_protocol = { proto_name = r#test_protocol; proto_hash = r#test_protocol } in
  let timestamp = date_of_cal r#timestamp in
  { hash=r#hash; predecessor_hash=r#predecessor; fitness=r#fitness;
    baker = Alias.to_name r#baker; timestamp;
    validation_pass = Int64.to_int r#validation_pass; operations = [] ;
    protocol = block_protocol ; nb_operations = Int64.to_int r#operation_count;
    test_protocol; network=r#network; test_network=r#test_network;
    test_network_expiration=r#test_network_expiration;
    priority = Int32.to_int r#priority; level = Int64.to_int r#level;
    commited_nonce_hash=r#commited_nonce_hash; pow_nonce=r#pow_nonce;
    proto = Int64.to_int r#proto; data=r#data; signature=r#signature;
    volume=r#volume; fees=r#fees; distance_level = Int64.to_int r#distance_level;
    cycle = Int64.to_int r#cycle; endorsements_included = Int32.to_int r#endorsements_included }

let op_hash_to_bop_hash op_hash = (op_hash, "", "")

let block_op_hash r = r#op_hash

let unoption_op_hash = function
  | (None,_,_) -> None
  | (Some a,b,c) -> Some (a,b,c)

let block_hash r = r#hash

let block_with_pred_fitness r = block_noop r, r#pred_fitness

let block_ops x =
  List.map (fun x -> op_hash_to_bop_hash (noop x))
    (noop (block_op_hash x))

let block_with_ops x =
  { (block_noop x) with operations = block_ops x }

let errors_from_db = function
  | None -> None
  | Some l -> let rec aux = function
      | (err_id :: err_kind :: err_infos :: t) ->
        {err_id = Option.value ~default:"" err_id;
         err_kind = Option.value ~default:"" err_kind;
         err_info = Option.value ~default:"{}" err_infos} :: (aux t)
      | _ -> [] in
    Some (aux l)

let or_script_from_db r =
  match r#code, r#code_hash, r#storage with
  | None, None, (Some "{}" | None) -> None
  | sc_code, sc_code_hash, Some sc_storage ->
    Some {sc_code; sc_code_hash; sc_storage}
  | sc_code, sc_code_hash, None ->
    Some {sc_code; sc_code_hash; sc_storage = "{}"}

let activation_from_db r =
  let act = {
    act_pkh = Alias.to_name r#pkh; act_secret = r#secret; act_balance = r#balance;
    act_op_level = Int64.to_int r#op_level; act_timestamp = string_of_cal r#tsp } in
  Activation act

let dummy_dee endorse_op_level endorse_block_hash endorse_block_level = {
  endorse_src = Alias.to_name ""; endorse_op_level; endorse_block_level;
  endorse_block_hash; endorse_slot = []; endorse_priority = -1; endorse_timestamp = ""
}

let dee_from_db r =
  let op_level = Int64.to_int r#op_level in
  let double_endorsement_tsp = string_of_cal r#timestamp in
  let endorse1 = dummy_dee op_level r#block_hash1 (Int64.to_int r#level1) in
  let endorse2 = dummy_dee op_level r#block_hash2 (Int64.to_int r#level2) in
  let dee = {
    double_endorsement1 = endorse1 ;
    double_endorsement2 = endorse2 ;
    double_endorsement_accused = Alias.to_name r#accused;
    double_endorsement_denouncer = Alias.to_name r#denouncer;
    double_endorsement_lost_deposit = r#lost_deposit;
    double_endorsement_lost_rewards = r#lost_rewards;
    double_endorsement_lost_fees = r#lost_fees;
    double_endorsement_gain_rewards = r#gain_rewards;
    double_endorsement_op_level = op_level;
    double_endorsement_tsp
  } in
  Double_endorsement_evidence dee

let dbe_from_db r =
  let double_baking_op_level = Int64.to_int r#op_level in
  let double_baking_tsp = string_of_cal r#timestamp in
  let h1 = {
    hd_level = Int64.to_int r#h1_level ;
    hd_proto = Int64.to_int r#h1_proto ;
    hd_predecessor = r#h1_predecessor ;
    hd_timestamp = date_of_cal r#h1_tsp ;
    hd_validation_pass = Int64.to_int r#h1_validation_pass ;
    hd_operations_hash = r#h1_operations_hash ;
    hd_fitness = r#h1_fitness ;
    hd_context = r#h1_context ;
    hd_priority = Int32.to_int r#h1_priority ;
    hd_seed_nonce_hash = r#h1_commited_nonce_hash ;
    hd_proof_of_work_nonce = r#h1_pow_nonce ;
    hd_signature = r#h1_signature;
    hd_hash = None;
    hd_network = None} in
  let h2 = {
    hd_level = Int64.to_int r#h2_level ;
    hd_proto = Int64.to_int r#h2_proto ;
    hd_predecessor = r#h2_predecessor ;
    hd_timestamp = date_of_cal r#h2_tsp ;
    hd_validation_pass = Int64.to_int r#h2_validation_pass ;
    hd_operations_hash = r#h2_operations_hash ;
    hd_fitness = r#h2_fitness ;
    hd_context = r#h2_context ;
    hd_priority = Int32.to_int r#h2_priority ;
    hd_seed_nonce_hash = r#h2_commited_nonce_hash ;
    hd_proof_of_work_nonce = r#h2_pow_nonce ;
    hd_signature = r#h2_signature;
    hd_hash = None;
    hd_network = None} in
  let double_baking_main =
    if r#h1_signature = r#signature then 0
    else if r#h2_signature = r#signature then 1
    else (-1) in
  let double_baking_accused =
    match r#accused with None  -> Alias.to_name "unknown" | Some dn1 -> Alias.to_name dn1 in
  let double_baking_denouncer =
    match r#denouncer with None -> Alias.to_name "unknown" | Some dn1 -> Alias.to_name dn1 in
  let double_baking_lost_deposit = Option.value ~default:Int64.zero r#lost_deposit in
  let double_baking_lost_rewards = Option.value ~default:Int64.zero r#lost_rewards in
  let double_baking_lost_fees = Option.value ~default:Int64.zero r#lost_fees in
  let double_baking_gain_rewards = Option.value ~default:Int64.zero r#gain_rewards in
  let dbe =
    { double_baking_header1 = h1 ; double_baking_header2 = h2;
      double_baking_main ;
      double_baking_accused ;
      double_baking_denouncer ;
      double_baking_lost_deposit ;
      double_baking_lost_rewards ;
      double_baking_lost_fees ;
      double_baking_gain_rewards;
      double_baking_op_level; double_baking_tsp } in
  Double_baking_evidence dbe

let nonce_from_db r =
  let seed_level = Int64.to_int r#level in
  let seed_nonce_op_level = Int64.to_int r#op_level in
  let seed_nonce_tsp = string_of_cal r#tsp in
  Seed_nonce_revelation { seed_level ; seed_nonce = r#nonce; seed_nonce_op_level; seed_nonce_tsp }

let endorsement_from_db r =
  let endorse_block_level = Int64.to_int r#block_level in
  let endorse_op_level = Int64.to_int r#op_level in
  let endorse_priority = Int32.to_int r#priority in
  let endorse_slot = List.filter_map (Option.map Int32.to_int) r#slots in
  let endorse_timestamp = string_of_cal r#timestamp in
  Sourced (Endorsement {
      endorse_src = Alias.to_name r#source; endorse_block_hash = r#block_hash;
      endorse_block_level; endorse_slot; endorse_op_level; endorse_priority;
      endorse_timestamp })

let manager_info_from_db r =
  Alias.to_name r#source, Int64.to_int32 r#counter,
  Option.fold ~none:Z.zero ~some:Z.of_int64 r#gas_limit, Option.fold ~none:Z.zero ~some:Z.of_int64 r#storage_limit,
  Int64.to_int r#op_level, string_of_cal r#tsp, errors_from_db r#errors,
  r#fee, r#failed, r#internal

let manager_from_db op_from_db mk_op rows =
  let source, l = List.fold_left
      (fun (src, acc) r ->
         let tr = op_from_db r in
         let src = match src with
           | None when not r#internal -> Some r#source
           | _ -> src in
         src, mk_op tr :: acc) (None, []) rows in
  (match source with None -> "" | Some source -> source), List.rev l

let transaction_from_db_base r =
  let tr_src, tr_counter, tr_gas_limit, tr_storage_limit, tr_op_level, tr_timestamp,
      tr_errors, tr_fee, tr_failed, tr_internal = manager_info_from_db r in
  let tr_dst = Alias.to_name r#destination in
  let tr_parameters = match r#parameters with
    | Some "" -> None
    | _ -> r#parameters in {
    tr_src; tr_dst; tr_amount = r#amount; tr_counter; tr_fee;
    tr_gas_limit; tr_storage_limit; tr_parameters; tr_failed;
    tr_internal; tr_burn = r#burn_dun; tr_op_level; tr_timestamp;
    tr_errors; tr_collect_fee_gas = r#collect_fee_gas; tr_collect_pk = r#collect_pk }

let transaction_from_db rows =
  manager_from_db transaction_from_db_base (fun x -> Transaction x) rows

let origination_from_db_base r =
  let or_src, or_counter, or_gas_limit, or_storage_limit, or_op_level, or_timestamp,
      or_errors, or_fee, or_failed, or_internal = manager_info_from_db r in
  let or_delegate = match r#delegate with
    | None -> Alias.to_name r#source
    | Some del -> Alias.to_name del in
  let or_script = or_script_from_db r in
  let or_kt1 = Alias.to_name r#kt1 in
  let or_manager = Alias.to_name r#manager in {
    or_src; or_kt1; or_manager; or_delegate; or_script; or_spendable = r#spendable;
    or_delegatable = r#delegatable; or_balance = r#balance; or_counter;
    or_fee; or_gas_limit; or_storage_limit;
    or_failed; or_internal; or_burn = r#burn_dun;
    or_op_level; or_timestamp; or_errors }

let origination_from_db rows =
  manager_from_db origination_from_db_base (fun x -> Origination x) rows

let delegation_from_db_base r =
  let del_src, del_counter, del_gas_limit, del_storage_limit, del_op_level,
      del_timestamp, del_errors, del_fee, del_failed, del_internal =
    manager_info_from_db r in
  let del_delegate = match r#delegate with
    | None -> Alias.to_name ""
    | Some dn -> Alias.to_name dn in
  { del_src; del_delegate; del_counter; del_fee; del_gas_limit; del_storage_limit;
    del_failed; del_internal; del_op_level; del_timestamp; del_errors }

let delegation_from_db rows =
  manager_from_db delegation_from_db_base (fun x -> Delegation x) rows

let reveal_from_db_base r =
  let rvl_src, rvl_counter, rvl_gas_limit, rvl_storage_limit, rvl_op_level,
      rvl_timestamp, rvl_errors, rvl_fee, rvl_failed, rvl_internal =
    manager_info_from_db r in
  let rvl_pubkey = Option.value ~default:"" r#pubkey in {
    rvl_src; rvl_pubkey; rvl_counter; rvl_fee; rvl_gas_limit; rvl_storage_limit;
    rvl_failed; rvl_internal; rvl_op_level; rvl_timestamp; rvl_errors }

let reveal_from_db rows =
  manager_from_db reveal_from_db_base (fun x -> Reveal x) rows

let manage_account_from_db_base r =
  let mac_src, mac_counter, mac_gas_limit, mac_storage_limit, mac_op_level,
      mac_timestamp, mac_errors, mac_fee, mac_failed, mac_internal =
    manager_info_from_db r in
  let mac_target = Option.map (fun t -> Alias.to_name t, r#signature) r#target in
  let mac_maxrolls = if r#set_maxrolls then Some (Option.map Int32.to_int r#maxrolls) else None in
  let mac_admin = if r#set_admin then Some (Option.map Alias.to_name r#admin) else None in
  let mac_white_list = Option.map (List.filter_map  (Option.map Alias.to_name)) r#white_list in
  let mac_recovery = if r#set_recovery then Some (Option.map Alias.to_name r#recovery) else None in
  let mac_actions = match r#actions_name, r#actions_arg with
    | Some names, Some args ->
      Some (List.combine
              (List.filter_map (fun x -> x) names)
              (List.filter_map (fun x -> x) args))
    | _ -> None in {
    mac_src; mac_counter; mac_fee; mac_gas_limit; mac_storage_limit;
    mac_target; mac_maxrolls; mac_admin; mac_white_list; mac_delegation = r#delegation;
    mac_recovery; mac_actions; mac_failed; mac_internal; mac_op_level;
    mac_timestamp; mac_errors }

let manage_account_from_db rows =
  manager_from_db manage_account_from_db_base (fun x -> Manage_account x) rows

let manage_accounts_from_db_base r =
  let macs_src, macs_counter, macs_gas_limit, macs_storage_limit, macs_op_level,
      macs_timestamp, macs_errors, macs_fee, macs_failed, macs_internal =
    manager_info_from_db r in
  let macs_bytes = `Hex r#bytes in {
    macs_src; macs_bytes; macs_counter; macs_fee; macs_gas_limit; macs_storage_limit;
    macs_failed; macs_internal; macs_op_level; macs_timestamp; macs_errors }

let manage_accounts_from_db rows =
  manager_from_db manage_accounts_from_db_base (fun x -> Manage_accounts x) rows

let activate_protocol_from_db_base r =
  let acp_src, acp_counter, acp_gas_limit, acp_storage_limit, acp_op_level,
      acp_timestamp, acp_errors, acp_fee, acp_failed, acp_internal =
    manager_info_from_db r in {
    acp_src; acp_parameters = r#parameters; acp_protocol = r#protocol;
    acp_counter; acp_fee; acp_gas_limit; acp_storage_limit;
    acp_failed; acp_internal; acp_op_level; acp_timestamp;
    acp_errors }

let activate_protocol_from_db rows =
  manager_from_db activate_protocol_from_db_base (fun x -> Activate_protocol x) rows

let op_type_from_db f r =
  {op_hash = r#hash; op_block_hash = r#op_block_hash;
   op_network_hash = r#network; op_type = f r}

let manager_op_type_from_db f r =
  op_type_from_db (fun r -> Sourced (Manager ("manager", Alias.to_name r#source, [f r]))) r

let dune_manager_op_type_from_db f r =
  op_type_from_db (fun r -> Sourced (Dune_manager (Alias.to_name r#source, [f r]))) r

let transaction_from_db_list rows =
  List.map
    (manager_op_type_from_db (fun r -> Transaction (transaction_from_db_base r))) rows

let token_from_db_list rows =
  List.map
    (fun r ->
      let op_level = Int64.to_int r#op_level in
      let timestamp = string_of_cal r#tsp in
      let ts_src = Alias.to_name r#source in
      let ts_dst = Option.map Alias.to_name r#destination in
      let ts_kind = match r#kind with
        | "burn" -> TS_Burn
        | "transfer" -> TS_Transfer
        | "mint" -> TS_Mint
        | _ -> TS_Unknown in
      let op = {ts_src; ts_dst; ts_amount = Z.of_string r#amount;
                ts_flag = r#flag; ts_options = None; ts_kind} in
      { op_hash = r#transaction; op_block_hash = r#op_block_hash ; op_network_hash = r#network;
        op_type = Tokened (op_level, timestamp , op) }) rows

let delegation_from_db_list rows =
  List.map
    (manager_op_type_from_db (fun r -> Delegation (delegation_from_db_base r))) rows

let reveal_from_db_list rows =
  List.map
    (manager_op_type_from_db (fun r -> Reveal (reveal_from_db_base r))) rows

let origination_from_db_list rows =
  List.map
    (manager_op_type_from_db (fun r -> Origination (origination_from_db_base r))) rows

let activation_from_db_list rows =
  List.map (op_type_from_db (fun r -> Anonymous [activation_from_db r])) rows

let endorsement_from_db_list rows =
  List.map (op_type_from_db (fun r -> endorsement_from_db r)) rows

let manage_account_from_db_list rows =
  List.map
    (dune_manager_op_type_from_db (fun r -> Manage_account (manage_account_from_db_base r))) rows

let manage_accounts_from_db_list rows =
  List.map
    (dune_manager_op_type_from_db (fun r -> Manage_accounts (manage_accounts_from_db_base r))) rows

let activate_protocol_from_db_list rows =
  List.map
    (dune_manager_op_type_from_db (fun r -> Activate_protocol (activate_protocol_from_db_base r))) rows

let bakings_from_db_list rows =
  List.rev @@
  List.fold_left
    (fun acc bk -> match bk with
       | (bk_block_hash, bk_baker_hash, bk_level, bk_cycle, bk_priority,
          Some bk_missed_priority, bk_distance_level, bk_fees, Some bk_bktime,
          Some bk_baked, bk_tsp, bk_endorsements_included) ->
         let bk_level = Int64.to_int bk_level in
         let bk_cycle = Int64.to_int bk_cycle in
         let bk_priority = Int32.to_int bk_priority in
         let bk_distance_level = Int64.to_int bk_distance_level in
         let bk_baker_hash = Alias.to_name bk_baker_hash in
         let bk_tsp = string_of_cal bk_tsp in
         let bk_missed_priority =
           if bk_baked then Some (Int32.to_int bk_missed_priority)
           else None in
         let bk_endorsements_included = Int32.to_int bk_endorsements_included in
         let cst = Infos.constants ~level:bk_level in
         let bk_rewards = Infos.block_rewards cst bk_endorsements_included bk_priority in
         {bk_block_hash; bk_baker_hash; bk_level; bk_cycle ; bk_priority;
          bk_missed_priority; bk_distance_level; bk_fees;
          bk_bktime = int_of_float bk_bktime; bk_baked; bk_tsp;
          bk_rewards; bk_endorsements_included} :: acc
       | _ -> acc ) [] rows

let bakings_endorsement_from_db_list rows =
  List.rev @@ List.fold_left (fun acc row -> match row with
      | (Some ebk_level, Some ebk_lr_nslot, ebk_block, ebk_source, ebk_cycle,
         ebk_priority, ebk_dist, ebk_slots, ebk_tsp) ->
        let ebk_level = Int64.to_int ebk_level in
        let ebk_cycle = Option.map Int64.to_int ebk_cycle in
        let ebk_dist = Option.map Int32.to_int ebk_dist in
        let ebk_source = Option.map Alias.to_name ebk_source in
        let ebk_priority = Option.map Int32.to_int ebk_priority in
        let ebk_slots = Option.map (List.filter_map (Option.map Int32.to_int)) ebk_slots in
        let ebk_tsp = Option.map string_of_cal ebk_tsp in
        let ebk_lr_nslot = Int32.to_int ebk_lr_nslot in
        {ebk_block; ebk_source; ebk_level; ebk_cycle; ebk_priority;
         ebk_dist; ebk_slots; ebk_lr_nslot; ebk_tsp} :: acc
      | _ -> acc) [] rows

let cycle_bakings_from_db_list current_cycle rows =
  List.rev @@ List.fold_left
    (fun acc cbk -> match cbk with
       | (cbk_cycle, Some cnt_all, Some cnt_steal, cnt_miss, Some cbk_priority,
          dun_fee, cbk_bktime, block_rewards, block_security_deposit) ->
         let cbk_cycle = Int64.to_int cbk_cycle in
         let dun_reward = block_rewards in
         let dun_deposit = Int64.mul block_security_deposit cnt_all in
         let cbk_priority, cbk_bktime = if cnt_all = 0L then None, None else
             Some cbk_priority,
             Some Int64.(to_int @@ div (of_float cbk_bktime) cnt_all) in
         {cbk_cycle; cbk_depth = current_cycle - cbk_cycle;
          cbk_count = {cnt_all; cnt_miss; cnt_steal};
          cbk_dun = {dun_fee; dun_reward; dun_deposit};
          cbk_priority; cbk_bktime} :: acc
       | _ -> acc ) [] rows

let cycle_endorsements_from_db_list current_cycle rows =
  List.rev @@ List.fold_left
    (fun acc ced -> match ced with
       | (ced_cycle, Some cnt_all, cnt_miss, Some ced_priority, dun_reward,
          endorsement_security_deposit) ->
         let ced_cycle = Int64.to_int ced_cycle in
         let dun_deposit = Int64.mul cnt_all endorsement_security_deposit in
         {ced_cycle; ced_depth = current_cycle - ced_cycle;
          ced_slots = {cnt_all; cnt_miss; cnt_steal=0L};
          ced_dun = {dun_fee=0L; dun_reward; dun_deposit};
          ced_priority} :: acc
       | _ -> acc) [] rows


let rights_from_db_list rows =
  List.rev @@
  List.fold_left
    (fun acc row -> match row with
       | ( Some pr_level, Some pr_bakers, Some pr_endorsers,
           Some pr_bakers_priority, baked_priority, baker ) ->
         let r_level = Int64.to_int pr_level in
         let r_bakers = List.rev @@
           List.fold_left (fun acc s ->
               match s with Some s -> (Alias.to_name s) :: acc | None -> acc) [] pr_bakers in
         let r_endorsers = List.rev @@
           List.fold_left (fun acc s ->
               match s with Some s -> (Alias.to_name s) :: acc | None -> acc) [] pr_endorsers in
         let r_bakers_priority = List.rev @@
           List.fold_left (fun acc s ->
               match s with Some s -> Int32.to_int s :: acc | None -> acc)
           [] pr_bakers_priority in
       let r_baked = match baked_priority, baker with
         | Some prio, Some baker -> Some (Alias.to_name baker, Int32.to_int prio)
         | _ -> None in
       { r_level; r_bakers; r_endorsers ; r_bakers_priority; r_baked } :: acc
       | _ -> acc)
    [] rows

let baker_rights_from_db_list rows =
  List.rev @@ List.fold_left
    (fun acc x -> match x with
       | ( pr_level, pr_cycle, Some pr_prio, Some pr_depth ) ->
         let br_level = Int64.to_int pr_level in
         let br_cycle = Int64.to_int pr_cycle in
         let br_priority = Int32.to_int pr_prio in
         let br_depth = Int64.to_int pr_depth in
         {br_level; br_cycle; br_priority; br_depth} :: acc
       | _ -> acc) [] rows

let endorser_rights_from_db_list rows =
  List.rev @@ List.fold_left
    (fun acc x -> match x with
       | ( pr_level, pr_cycle, Some pr_nslot, Some pr_depth) ->
         let er_level = Int64.to_int pr_level in
         let er_cycle = Int64.to_int pr_cycle in
         let er_nslot = Int32.to_int pr_nslot in
         let er_depth = Int64.to_int pr_depth in
         {er_level; er_cycle; er_nslot; er_depth} :: acc
       | _ -> acc) [] rows

let service_from_db r =
  let srv_dn1 = if r#dn1 = "" then None else Some r#dn1 in
  let srv_logo = Option.value ~default:"" (Option.value ~default:None (List.nth_opt r#logos 0)) in
  let srv_logo2 = Option.value ~default:None (List.nth_opt r#logos 1) in
  let srv_logo_payout = Option.value ~default:None (List.nth_opt r#logos 2) in
  let srv_sponsored = Option.fold ~none:None
      ~some:(fun tsp -> Some (Date.to_string @@ date_of_cal tsp)) r#sponsored in
  let srv_aliases = match r#addresses, r#aliases with
    | Some addresses, Some aliases ->
      Some (
        List.map2 (fun pkh alias ->
            let pkh = Option.value ~default:"" pkh in {pkh; alias}) addresses aliases)
    | _ -> None in
  let srv_descr = if r#description = Some "" then None else r#description in
  let srv_page = if r#page = Some "" then None else r#page in
  {srv_kind = r#kind; srv_dn1; srv_name = r#name; srv_url = r#url;
   srv_logo; srv_logo2; srv_logo_payout;
   srv_descr; srv_sponsored; srv_page; srv_delegations_page = r#delegations_page;
   srv_account_page = r#account_page; srv_aliases;
   srv_display_delegation_page = r#display_delegation;
   srv_display_account_page = r#display_account}

let constants_from_db
    (level_start, _level_end, _protocol, _block_hash, _distance_level,
     proof_of_work_nonce_size, nonce_length, max_revelations_per_block,
     max_operation_data_length, preserved_cycles, blocks_per_cycle,
     blocks_per_commitment, blocks_per_roll_snapshot,
     blocks_per_voting_period, time_between_blocks, endorsers_per_block,
     hard_gas_limit_per_operation, hard_gas_limit_per_block,
     proof_of_work_threshold, tokens_per_roll, michelson_maximum_type_size,
     seed_nonce_revelation_tip, origination_burn, block_security_deposit,
     endorsement_security_deposit, block_reward, endorsement_reward,
     cost_per_byte, hard_storage_limit_per_operation, max_proposals_per_delegate,
     test_chain_duration, hard_gas_limit_to_pay_fees, max_operation_ttl,
     protocol_revision, frozen_account_cycles, allow_collect_call,
     quorum_min, quorum_max, min_proposal_quorum, initial_endorsers,
     delay_per_missing_endorsement) =
  let proof_of_work_nonce_size = Int32.to_int proof_of_work_nonce_size in
  let nonce_length = Int32.to_int nonce_length in
  let max_revelations_per_block = Int32.to_int max_revelations_per_block in
  let max_operation_data_length = Int32.to_int max_operation_data_length in
  let preserved_cycles = Int32.to_int preserved_cycles in
  let blocks_per_cycle = Int32.to_int blocks_per_cycle in
  let blocks_per_commitment = Int32.to_int blocks_per_commitment in
  let blocks_per_roll_snapshot = Int32.to_int blocks_per_roll_snapshot in
  let blocks_per_voting_period = Int32.to_int blocks_per_voting_period in
  let endorsers_per_block = Int32.to_int endorsers_per_block in
  let michelson_maximum_type_size = Int32.to_int michelson_maximum_type_size in
  let max_proposals_per_delegate = Option.map Int32.to_int max_proposals_per_delegate in
  let time_between_blocks = List.rev @@ List.fold_left (fun acc i -> match i with
      | None -> acc | Some i -> (Int32.to_int i) :: acc) [] time_between_blocks in
  let max_operation_ttl = Int32.to_int max_operation_ttl in
  let protocol_revision = Option.map Int32.to_int protocol_revision in
  let frozen_account_cycles = Option.map Int32.to_int frozen_account_cycles in
  let quorum_min = Option.map Int32.to_int quorum_min in
  let quorum_max = Option.map Int32.to_int quorum_max in
  let min_proposal_quorum = Option.map Int32.to_int min_proposal_quorum in
  let initial_endorsers = Option.map Int32.to_int initial_endorsers in
  let delay_per_missing_endorsement = Option.map Int32.to_int delay_per_missing_endorsement in
  (Int64.to_int level_start,
   { Dune_types.proof_of_work_nonce_size; nonce_length; max_revelations_per_block;
     max_operation_data_length; preserved_cycles; blocks_per_cycle;
     blocks_per_commitment; blocks_per_roll_snapshot;
     blocks_per_voting_period; time_between_blocks; endorsers_per_block;
     hard_gas_limit_per_operation; hard_gas_limit_per_block;
     proof_of_work_threshold; tokens_per_roll; michelson_maximum_type_size;
     seed_nonce_revelation_tip; origination_burn; block_security_deposit;
     endorsement_security_deposit; block_reward; endorsement_reward;
     cost_per_byte; hard_storage_limit_per_operation;
     max_proposals_per_delegate; test_chain_duration;
     hard_gas_limit_to_pay_fees; max_operation_ttl;
     protocol_revision; frozen_account_cycles; allow_collect_call;
     quorum_min; quorum_max; min_proposal_quorum; initial_endorsers;
     delay_per_missing_endorsement})

let merge_constants_params ?constants params level f =
  let open Dune_types in
  match constants, params, level with
  | Some constants, Some params, Some level ->
    constants level (fun cst ->
        let choose p c = match p with None -> c | Some p -> p in
        let choose_opt p c = match p with None -> c | Some p -> Some p in
        let constants = {
          proof_of_work_nonce_size =
            choose params.proof_of_work_nonce_size_opt cst.proof_of_work_nonce_size;
          nonce_length =
            choose params.nonce_length_opt cst.nonce_length;
          max_revelations_per_block =
            choose params.max_revelations_per_block_opt cst.max_revelations_per_block;
          max_operation_data_length =
            choose params.max_operation_data_length_opt cst.max_operation_data_length;
          preserved_cycles =
            choose params.preserved_cycles_opt cst.preserved_cycles;
          blocks_per_cycle =
            choose params.blocks_per_cycle_opt cst.blocks_per_cycle;
          blocks_per_commitment =
            choose params.blocks_per_commitment_opt cst.blocks_per_commitment;
          blocks_per_roll_snapshot =
            choose params.blocks_per_roll_snapshot_opt cst.blocks_per_roll_snapshot;
          blocks_per_voting_period =
            choose params.blocks_per_voting_period_opt cst.blocks_per_voting_period;
          time_between_blocks =
            choose params.time_between_blocks_opt cst.time_between_blocks;
          endorsers_per_block =
            choose params.endorsers_per_block_opt cst.endorsers_per_block;
          hard_gas_limit_per_operation =
            choose params.hard_gas_limit_per_operation_opt cst.hard_gas_limit_per_operation;
          hard_gas_limit_per_block =
            choose params.hard_gas_limit_per_block_opt cst.hard_gas_limit_per_block;
          proof_of_work_threshold =
            choose params.proof_of_work_threshold_opt cst.proof_of_work_threshold;
          tokens_per_roll =
            choose params.tokens_per_roll_opt cst.tokens_per_roll;
          michelson_maximum_type_size =
            choose params.michelson_maximum_type_size_opt cst.michelson_maximum_type_size;
          seed_nonce_revelation_tip =
            choose params.seed_nonce_revelation_tip_opt cst.seed_nonce_revelation_tip;
          origination_burn = (match params.origination_size_opt with
              | None -> cst.origination_burn
              | Some p -> Int64.(mul (of_int p) 1_000L));
          block_security_deposit =
            choose params.block_security_deposit_opt cst.block_security_deposit;
          endorsement_security_deposit =
            choose params.endorsement_security_deposit_opt cst.endorsement_security_deposit;
          block_reward =
            choose params.block_reward_opt cst.block_reward;
          endorsement_reward =
            choose params.endorsement_reward_opt cst.endorsement_reward;
          cost_per_byte =
            choose params.cost_per_byte_opt cst.cost_per_byte;
          hard_storage_limit_per_operation =
            choose params.hard_storage_limit_per_operation_opt cst.hard_storage_limit_per_operation;
          max_proposals_per_delegate =
            choose_opt params.max_proposals_per_delegate_opt cst.max_proposals_per_delegate;
          test_chain_duration = cst.test_chain_duration;
          hard_gas_limit_to_pay_fees =
            choose params.hard_gas_limit_to_pay_fees_opt cst.hard_gas_limit_to_pay_fees;
          max_operation_ttl =
            choose params.max_operation_ttl_opt cst.max_operation_ttl;
          protocol_revision =
            choose_opt params.protocol_revision_opt cst.protocol_revision;
          frozen_account_cycles =
            choose_opt params.frozen_account_cycles_opt cst.frozen_account_cycles;
          allow_collect_call =
            choose_opt params.allow_collect_call_opt cst.allow_collect_call;
          quorum_min =
            choose_opt params.quorum_min_opt cst.quorum_min;
          quorum_max =
            choose_opt params.quorum_max_opt cst.quorum_max;
          min_proposal_quorum =
            choose_opt params.min_proposal_quorum_opt cst.min_proposal_quorum;
          initial_endorsers =
            choose_opt params.initial_endorsers_opt cst.initial_endorsers;
          delay_per_missing_endorsement =
            choose_opt params.delay_per_missing_endorsement_opt cst.delay_per_missing_endorsement;
        } in
        f constants)
  | _ -> ()

let batch_operation mk_op mk_ops rows =
  let grouped =
    List.rev @@ List.fold_left
      (fun acc r ->
         let op = mk_op r in
         match List.assoc_opt r#hash acc with
         | Some (block, network, ops) ->
           (r#hash, (block, network, op :: ops)) :: (List.remove_assoc r#hash acc)
         | None -> (r#hash, (r#op_block_hash, r#network, [ op ] )) :: acc)
      [] rows in
  List.map (fun (op_hash, (op_block_hash, op_network_hash, ops)) ->
      { op_hash; op_block_hash; op_network_hash;
        op_type = mk_ops (List.rev ops) }) grouped

let manager_batch_operation mk_op rows =
  let grouped =
    List.rev @@ List.fold_left
      (fun acc r ->
         let hash, block, network, src, op =
           r#hash, r#op_block_hash, r#network, r#source, mk_op r in
         match List.assoc_opt hash acc with
         | Some (block, network, src, ops) ->
           (hash, (block, network, src, op :: ops)) :: (List.remove_assoc hash acc)
         | None -> (hash, (block, network, src, [ op ] )) :: acc)
      [] rows in
  List.map (fun (op_hash, (op_block_hash, op_network_hash, src, ops)) ->
      { op_hash; op_block_hash; op_network_hash;
        op_type = Sourced (Manager ("manager", Alias.to_name src, List.rev ops)) }) grouped

let dune_manager_batch_operation mk_op rows =
  let grouped =
    List.rev @@ List.fold_left
      (fun acc r ->
         let hash, block, network, src, op =
           r#hash, r#op_block_hash, r#network, r#source, mk_op r in
         match List.assoc_opt hash acc with
         | Some (block, network, src, ops) ->
           (hash, (block, network, src, op :: ops)) :: (List.remove_assoc hash acc)
         | None -> (hash, (block, network, src, [ op ] )) :: acc)
      [] rows in
  List.map (fun (op_hash, (op_block_hash, op_network_hash, src, ops)) ->
      { op_hash; op_block_hash; op_network_hash;
        op_type = Sourced (Dune_manager (Alias.to_name src, List.rev ops)) }) grouped


(* pending operations *)
let pending_info_from_db r =
  let pe_tsp = string_of_cal r#timestamp_op in
  let pe_errors = errors_from_db r#errors in
  {pe_hash = r#hash; pe_branch = r#branch; pe_status = r#status; pe_tsp; pe_errors}
let pending_manager_from_db r =
  let pe_src = Alias.to_name r#source in
  let pe_gas_limit = Option.map Z.of_int64 r#gas_limit in
  let pe_storage_limit = Option.map Z.of_int64 r#storage_limit in
  {pe_src; pe_counter = r#counter; pe_fee = r#fee; pe_gas_limit; pe_storage_limit}
let pending_seed_nonce_revelation_from_db r =
  let pe_seed_level = Int64.to_int r#level in
  PSeed_nonce_revelation
    {pe_seed_info = pending_info_from_db r; pe_seed_level; pe_seed_nonce = r#nonce}
let pending_activation_from_db r =
  let pe_act_pkh = Alias.to_name r#pkh in
  PActivation
    {pe_act_info = pending_info_from_db r; pe_act_pkh; pe_act_secret = r#secret}
let pending_endorsement_from_db r =
  let pe_end_level = Int64.to_int r#block_level in
  let pe_end_prio = Option.map Int32.to_int r#priority in
  PEndorsement
    {pe_end_info = pending_info_from_db r; pe_end_level; pe_end_prio}
let pending_transaction_from_db r =
  let pe_tr_dst = Alias.to_name r#destination in
  PTransaction
    {pe_tr_info = pending_info_from_db r; pe_tr_man = pending_manager_from_db r;
     pe_tr_dst; pe_tr_amount = r#amount; pe_tr_parameters = r#parameters;
     pe_tr_collect_fee_gas = r#collect_fee_gas; pe_tr_collect_pk = r#collect_pk}
let pending_origination_from_db r =
  let pe_or_script = or_script_from_db r in
  let pe_or_kt1 = Alias.to_name r#kt1 in
  let pe_or_manager = Alias.to_name r#manager in
  let pe_or_delegate = Option.map Alias.to_name r#delegate in
  POrigination
    {pe_or_info = pending_info_from_db r; pe_or_man = pending_manager_from_db r;
     pe_or_kt1; pe_or_manager; pe_or_delegate; pe_or_script;
     pe_or_spendable = r#spendable; pe_or_delegatable = r#delegatable;
     pe_or_balance = r#balance}
let pending_delegation_from_db r =
  let pe_del_delegate = Alias.to_name r#delegate in
  PDelegation
    {pe_del_info = pending_info_from_db r; pe_del_man = pending_manager_from_db r;
     pe_del_delegate}
let pending_reveal_from_db r =
  PReveal
    {pe_rvl_info = pending_info_from_db r; pe_rvl_man = pending_manager_from_db r;
     pe_rvl_pubkey = r#pubkey}

let rec entrypoints_from_db = function
  | (Some name) :: (Some sc) :: l ->
    (name, Dune_encoding.Script.decode sc) :: (entrypoints_from_db l)
  | _ -> []

let account_info_from_db r =
  let ai_name = Alias.to_name (Option.value ~default:"" r#hash) in
  let ai_balance = Option.value ~default:0L r#spendable_balance in
  let ai_delegate = Option.map Alias.to_name r#delegate in
  let ai_script = or_script_from_db r in
  let ai_maxrolls = Option.map Int32.to_int r#maxrolls in
  let ai_admin = Option.map Alias.to_name r#admin in
  let ai_white_list = Option.map (List.filter_map (Option.map Alias.to_name)) r#white_list in
  let ai_recovery = Option.map Alias.to_name r#recovery in
  let ai_entrypoints = Option.map entrypoints_from_db r#entrypoints in
  {ai_name; ai_balance; ai_delegate; ai_script; ai_origination = r#origination;
   ai_maxrolls; ai_admin; ai_white_list; ai_delegation = r#delegation; ai_recovery;
   ai_reveal = r#reveal; ai_activation = r#activation; ai_entrypoints}

let test_constraint_i {cstt_e; cstt_g; cstt_l} =
  let x_e, nox_e = test_opti cstt_e in
  let x_g, nox_g = test_opti cstt_g in
  let x_l, nox_l = test_opti cstt_l in
  x_e, x_g, x_l, nox_e, nox_g, nox_l

let test_constraint f {cstt_e; cstt_g; cstt_l} =
  let x_e, nox_e = test_opt f cstt_e in
  let x_g, nox_g = test_opt f cstt_g in
  let x_l, nox_l = test_opt f cstt_l in
  x_e, x_g, x_l, nox_e, nox_g, nox_l

let test_hash_type = function
  | None -> None, None, true, true
  | Some (Ast.Is h) -> Some h, None, false, true
  | Some (Ast.IsNot h) -> None, Some h, true, false
  | Some (Ast.StartsWith h) -> Some (h ^ "%"), None, false, true
  | Some (Ast.EndsWith h) -> Some ("%" ^ h), None, false, true
  | Some (Ast.Contains h) -> Some ("%" ^ h ^ "%"), None, false, true

let boolean_for_blocks_search
  ~hash ~pred ~baker ~network ~protocol ~signature ~fitness ~volume ~fees
   ~level ~cycle ~votingp ~priority ~nop ~tsp ~valpass ~cycle_pos ~nendo =
  let hash, hashd, nohash, nohashd = test_hash_type hash in
  let pred, predd, nopred, nopredd = test_hash_type pred in
  let baker, bakerd, nobaker, nobakerd = test_hash_type baker in
  let network, networkd, nonetwork, nonetworkd = test_hash_type network in
  let protocol, protocold, noprotocol, noprotocold = test_hash_type protocol in
  let signature, signatured, nosignature, nosignatured = test_hash_type signature in
  let fitness_e, fitness_g, fitness_l, nofitness_e, nofitness_g, nofitness_l =
    test_constraint (Printf.sprintf "00 %.16x") fitness in
  let volume_e, volume_g, volume_l, novolume_e, novolume_g, novolume_l =
    test_constraint_i volume in
  let fees_e, fees_g, fees_l, nofees_e, nofees_g, nofees_l =
    test_constraint_i fees in
  let level_e, level_g, level_l, nolevel_e, nolevel_g, nolevel_l =
    test_constraint Int64.of_int level in
  let cycle_e, cycle_g, cycle_l, nocycle_e, nocycle_g, nocycle_l =
    test_constraint Int64.of_int cycle in
  let votingp_e, votingp_g, votingp_l, novotingp_e, novotingp_g, novotingp_l =
    test_constraint Int64.of_int votingp in
  let priority_e, priority_g, priority_l, nopriority_e, nopriority_g, nopriority_l =
    test_constraint Int32.of_int priority in
  let nop_e, nop_g, nop_l, nonop_e, nonop_g, nonop_l =
    test_constraint Int64.of_int nop in
  let tsp_e, tsp_g, tsp_l, notsp_e, notsp_g, notsp_l =
    test_constraint cal_of_string tsp in
  let valpass_e, valpass_g, valpass_l, novalpass_e, novalpass_g, novalpass_l =
    test_constraint Int64.of_int valpass in
  let cycle_pos_e, cycle_pos_g, cycle_pos_l, nocycle_pos_e, nocycle_pos_g, nocycle_pos_l =
    test_constraint Int64.of_int cycle_pos in
  let nendo_e, nendo_g, nendo_l, nonendo_e, nonendo_g, nonendo_l =
    test_constraint Int32.of_int nendo in
  (hash, hashd, pred, predd, baker, bakerd, network, networkd,
   protocol, protocold, signature, signatured,
   fitness_e, fitness_g, fitness_l, volume_e, volume_g, volume_l, fees_e, fees_g, fees_l,
   level_e, level_g, level_l, cycle_e, cycle_g, cycle_l, votingp_e, votingp_g, votingp_l,
   priority_e, priority_g, priority_l, nop_e, nop_g, nop_l, tsp_e, tsp_g, tsp_l,
   valpass_e, valpass_g, valpass_l, cycle_pos_e, cycle_pos_g, cycle_pos_l,
   nendo_e, nendo_g, nendo_l),
  (nohash, nohashd, nopred, nopredd, nobaker, nobakerd, nonetwork, nonetworkd,
   noprotocol, noprotocold, nosignature, nosignatured,
   nofitness_e, nofitness_g, nofitness_l, novolume_e, novolume_g, novolume_l,
   nofees_e, nofees_g, nofees_l, nolevel_e, nolevel_g, nolevel_l,
   nocycle_e, nocycle_g, nocycle_l, novotingp_e, novotingp_g, novotingp_l,
   nopriority_e, nopriority_g, nopriority_l, nonop_e, nonop_g, nonop_l,
   notsp_e, notsp_g, notsp_l, novalpass_e, novalpass_g, novalpass_l,
   nocycle_pos_e, nocycle_pos_g, nocycle_pos_l,
   nonendo_e, nonendo_g, nonendo_l)

let boolean_for_global_operations_search ~hash ~src ~bhash ~level ?cycle ~tsp =
  let hash, hashd, nohash, nohashd = test_hash_type hash in
  let src, srcd, nosrc, nosrcd = test_hash_type src in
  let bhash, bhashd, nobhash, nobhashd = test_hash_type bhash in
  let level_e, level_g, level_l, nolevel_e, nolevel_g, nolevel_l =
    test_constraint_i level in
  let cycle_constraint = match cycle with
    | None -> None
    | Some cycle -> Some (test_constraint Int64.of_int cycle) in
  let tsp_e, tsp_g, tsp_l, notsp_e, notsp_g, notsp_l =
    test_constraint cal_of_string tsp in
  (hash, hashd, src, srcd, bhash, bhashd,
   level_e, level_g, level_l, tsp_e, tsp_g, tsp_l),
  (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
   nolevel_e, nolevel_g, nolevel_l,
   notsp_e, notsp_g, notsp_l),
  cycle_constraint

let boolean_for_manager_operations_search
    ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter =
  let
  (hash, hashd, src, srcd, bhash, bhashd, level_e, level_g, level_l, tsp_e, tsp_g, tsp_l),
  (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
   nolevel_e, nolevel_g, nolevel_l, notsp_e, notsp_g, notsp_l), _ =
    boolean_for_global_operations_search ~hash ~src ~bhash ~level ?cycle:None ~tsp  in
  let failed, nofailed = test_opti failed in
  let internal, nointernal = test_opti internal in
  let fee_e, fee_g, fee_l, nofee_e, nofee_g, nofee_l = test_constraint_i fee in
  let counter_e, counter_g, counter_l, nocounter_e, nocounter_g, nocounter_l =
    test_constraint Int64.of_int counter in
    (hash, hashd, src, srcd, bhash, bhashd,
     level_e, level_g, level_l, tsp_e, tsp_g, tsp_l,
     failed, internal, fee_e, fee_g, fee_l, counter_e, counter_g, counter_l),
    (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
     nolevel_e, nolevel_g, nolevel_l,
     notsp_e, notsp_g, notsp_l, nofailed, nointernal, nofee_e, nofee_g, nofee_l,
     nocounter_e, nocounter_g, nocounter_l)

let boolean_for_transactions_search
    ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dst ~amount
    ~burn ~hasparam ~collect_call =
  let
    (hash, hashd, src, srcd, bhash, bhashd,
     level_e, level_g, level_l, tsp_e, tsp_g, tsp_l,
     failed, internal, fee_e, fee_g, fee_l, counter_e, counter_g, counter_l),
    (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
     nolevel_e, nolevel_g, nolevel_l,
     notsp_e, notsp_g, notsp_l, nofailed, nointernal,
     nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l) =
    boolean_for_manager_operations_search
      ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter in
  let dst, dstd, nodst, nodstd = test_hash_type dst in
  let amount_e, amount_g, amount_l, noamount_e, noamount_g, noamount_l =
    test_constraint_i amount in
  let burn_e, burn_g, burn_l, noburn_e, noburn_g, noburn_l =
    test_constraint_i burn in
  let hasparam, nohasparam = test_opti hasparam in
  let collect_call, nocollect_call = test_opti collect_call in
    (hash, hashd, src, srcd, bhash, bhashd,
     level_e, level_g, level_l,
     tsp_e, tsp_g, tsp_l, internal, failed,
     fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
     dst, dstd, amount_e, amount_g, amount_l, burn_e, burn_g, burn_l, hasparam,
     collect_call),
    (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
     nolevel_e, nolevel_g, nolevel_l,
     notsp_e, notsp_g, notsp_l, nofailed, nointernal,
     nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
     nodst, nodstd, noamount_e, noamount_g, noamount_l,
     noburn_e, noburn_g, noburn_l, nohasparam, nocollect_call)

let boolean_for_delegations_search
    ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dlg =
  let
    (hash, hashd, src, srcd, bhash, bhashd,
     level_e, level_g, level_l,
     tsp_e, tsp_g, tsp_l, failed, internal,
     fee_e, fee_g, fee_l, counter_e, counter_g, counter_l),
    (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
     nolevel_e, nolevel_g, nolevel_l,
     notsp_e, notsp_g, notsp_l, nofailed, nointernal,
     nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l) =
    boolean_for_manager_operations_search
      ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter in
  let dlg, dlgd, nodlg, nodlgd = test_hash_type dlg in
    (hash, hashd, src, srcd, bhash, bhashd,
     level_e, level_g, level_l,
     tsp_e, tsp_g, tsp_l, failed, internal,
     fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
     dlg, dlgd),
    (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
     nolevel_e, nolevel_g, nolevel_l,
     notsp_e, notsp_g, notsp_l, nofailed, nointernal,
     nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
     nodlg, nodlgd)

let boolean_for_originations_search
    ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter ~dlg ~manager
    ~orikt1 ~balance ~burn =
  let
    (hash, hashd, src, srcd, bhash, bhashd,
     level_e, level_g, level_l,
     tsp_e, tsp_g, tsp_l, failed, internal,
     fee_e, fee_g, fee_l, counter_e, counter_g, counter_l),
    (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
     nolevel_e, nolevel_g, nolevel_l,
     notsp_e, notsp_g, notsp_l, nofailed, nointernal,
     nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l) =
    boolean_for_manager_operations_search
      ~hash ~src ~bhash ~level ~tsp ~failed ~internal ~fee ~counter in
  let dlg, dlgd, nodlg, nodlgd = test_hash_type dlg in
  let manager, nomanager = test_opti manager in
  let orikt1, noorikt1 = test_opti orikt1 in
  let balance_e, balance_g, balance_l, nobalance_e, nobalance_g, nobalance_l =
    test_constraint_i balance in
  let burn_e, burn_g, burn_l, noburn_e, noburn_g, noburn_l =
    test_constraint_i burn in
    (hash, hashd, src, srcd, bhash, bhashd,
     level_e, level_g, level_l,
     tsp_e, tsp_g, tsp_l, failed, internal,
     fee_e, fee_g, fee_l, counter_e, counter_g, counter_l,
     dlg, dlgd, manager, orikt1, balance_e, balance_g, balance_l,
     burn_e, burn_g, burn_l),
    (nohash, nohashd, nosrc, nosrcd, nobhash, nobhashd,
     nolevel_e, nolevel_g, nolevel_l,
     notsp_e, notsp_g, notsp_l, nofailed, nointernal,
     nofee_e, nofee_g, nofee_l, nocounter_e, nocounter_g, nocounter_l,
     nodlg, nodlgd, nomanager, noorikt1, nobalance_e, nobalance_g, nobalance_l,
     noburn_e, noburn_g, noburn_l)

let boolean_for_accounts_search
    ~hash ~dlg ~bblock ~baked ~balance ~sbalance ~fbalance ~rewards ~fees
    ~deposits ~delegated ~revealed ~originated  =
  let hash, hashd, nohash, nohashd = test_hash_type hash in
  let dlg, dlgd, nodlg, nodlgd = test_hash_type dlg in
  let bblock, bblockd, nobblock, nobblockd = test_hash_type bblock in
  let baked_e, baked_g, baked_l, nobaked_e, nobaked_g, nobaked_l =
    test_constraint Int64.of_int baked in
  let balance_e, balance_g, balance_l, nobalance_e, nobalance_g, nobalance_l =
    test_constraint_i balance in
  let sbalance_e, sbalance_g, sbalance_l, nosbalance_e, nosbalance_g, nosbalance_l =
    test_constraint_i sbalance in
  let fbalance_e, fbalance_g, fbalance_l, nofbalance_e, nofbalance_g, nofbalance_l =
    test_constraint_i fbalance in
  let rewards_e, rewards_g, rewards_l, norewards_e, norewards_g, norewards_l =
    test_constraint_i rewards in
  let fees_e, fees_g, fees_l, nofees_e, nofees_g, nofees_l =
    test_constraint_i fees in
  let deposits_e, deposits_g, deposits_l, nodeposits_e, nodeposits_g, nodeposits_l =
    test_constraint_i deposits in
  let delegated, nodelegated = test_opti delegated in
  let revealed, norevealed = test_opti revealed in
  let originated, nooriginated = test_opti originated in
  (hash, hashd, dlg, dlgd, bblock, bblockd,
   baked_e, baked_g, baked_l,
   balance_e, balance_g, balance_l, sbalance_e, sbalance_g, sbalance_l,
   fbalance_e, fbalance_g, fbalance_l, rewards_e, rewards_g, rewards_l,
   fees_e, fees_g, fees_l, deposits_e, deposits_g, deposits_l,
   delegated, revealed, originated),
  (nohash, nohashd, nodlg, nodlgd, nobblock, nobblockd,
   nobaked_e, nobaked_g, nobaked_l,
   nobalance_e, nobalance_g, nobalance_l, nosbalance_e, nosbalance_g, nosbalance_l,
   nofbalance_e, nofbalance_g, nofbalance_l, norewards_e, norewards_g, norewards_l,
   nofees_e, nofees_g, nofees_l, nodeposits_e, nodeposits_g, nodeposits_l,
   nodelegated, norevealed, nooriginated)

let account_from_db
    (pkh, alias, manager, ac_delegate, ac_spendable, ac_delegatable,
     ac_origination, b_spendable, b_frozen, b_rewards, b_fees, b_deposits) =
  let pkh = Option.value ~default:"" pkh in
  let b_spendable = Option.value ~default:0L b_spendable in
  let b_frozen = Option.value ~default:0L b_frozen in
  let b_rewards = Option.value ~default:0L b_rewards in
  let b_fees = Option.value ~default:0L b_fees in
  let b_deposits = Option.value ~default:0L b_deposits in
  let ac_delegate = match ac_delegate with
    | None -> None
    | Some pkh -> Some (Alias.to_name pkh) in
  { ac_name = {pkh; alias}; ac_manager = Option.map Alias.to_name manager;
    ac_spendable; ac_delegatable ; ac_delegate; ac_origination;
    ac_balance = {b_spendable; b_frozen; b_rewards; b_fees; b_deposits} }

let token_info_from_db (tk_hash, tk_name, tk_symbol, supply, decimals, tk_version, lang) =
  {tk_hash; tk_name; tk_symbol; tk_version;
   tk_supply = Z.of_string supply;
   tk_decimals = Int32.to_int decimals;
   tk_lang = Dune_utils.lang_of_string lang}

let per_day_rows f rows =
  List.fold_left (fun acc (date, value) -> match (date, value) with
      | Some date, Some value ->
        let date = CalendarLib.Printer.Date.sprint "%Y/%m/%d" date in
        (date, f value) :: acc
      | _ -> acc) [] rows

let per_day_rows2 f rows =
  List.fold_left (fun acc (date, v1, v2) -> match (date, v1, v2) with
      | Some date, Some v1, Some v2 ->
        let date = CalendarLib.Printer.Date.sprint "%Y/%m/%d" date in
        (date, f v1 v2) :: acc
      | _ -> acc) [] rows
