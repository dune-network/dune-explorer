(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types_min
open Language

val z_encoding : Z.t Json_encoding.encoding
val int64 : int64 Json_encoding.encoding
val int_of_string : int Json_encoding.encoding

module Script : sig
  val expr_encoding : script_expr_t Json_encoding.encoding
  val encode : script_expr_t -> string
  val decode : string -> script_expr_t
  val expr_str_encoding : string Json_encoding.encoding
  val script_encoding : (script_expr_t option * script_expr_t * string option) Json_encoding.encoding
  val script_str_encoding : script Json_encoding.encoding
  val parameters_encoding : extended_script Json_encoding.encoding
  val encode_parameters : extended_script -> string
  val decode_parameters : string -> extended_script
  val parameters_str_encoding : string Json_encoding.encoding
end
module Level : sig
  val encoding : node_level Json_encoding.encoding
end
module Time : sig
  val encoding : string Json_encoding.encoding
end
module Contracts : sig
  val encoding : string list Json_encoding.encoding
end
module Protocol_param : sig
  val encoding : activate_protocol_param Json_encoding.encoding
end
module Header : sig
  val shell_encoding : block_shell Json_encoding.encoding
  val encoding : block_header Json_encoding.encoding
end
module Block : sig
  val encoding : node_block Json_encoding.encoding
  val block_metadata_encoding : block_metadata Json_encoding.encoding
  val genesis_encoding : node_block Json_encoding.encoding
end
module Error : sig
  val encoding : node_op_error Json_encoding.encoding
end
module Operation : sig
  val encoding : node_operation list list Json_encoding.encoding
  val contents_and_result_list_encoding : node_operation_type list Json_encoding.encoding
  val operation_data_and_receipt_encoding :
    (node_operation_type list * string option) Json_encoding.encoding
  val origination_encoding : Dune_types_min.node_origination Json_encoding.encoding
end
module Balance_updates : sig
  val balance_encoding :
    balance_updates Json_encoding.encoding
  val encoding :
    balance_updates list Json_encoding.encoding
end
module Voting_period_repr : sig
  val kind_encoding : voting_period_kind Json_encoding.encoding
end
module Account : sig
  val encoding : node_account Json_encoding.encoding
end
