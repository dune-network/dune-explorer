(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types_min
open Language

let print_comment ppf text =
  Format.fprintf ppf "/* @[<h>%a@] */" Format.pp_print_text text

let print_string ppf text =
  Format.fprintf ppf "\"" ;
  String.iter (function
      | '"' -> Format.fprintf ppf "\\\""
      | '\n' -> Format.fprintf ppf "\\n"
      | '\r' -> Format.fprintf ppf "\\r"
      | '\b' -> Format.fprintf ppf "\\b"
      | '\t' -> Format.fprintf ppf "\\t"
      | '\\' -> Format.fprintf ppf "\\\\"
      | c -> Format.fprintf ppf "%c" c)
    text ;
  Format.fprintf ppf "\""

let print_annotations =
  Format.pp_print_list ~pp_sep:Format.pp_print_space Format.pp_print_string

type location = bool * int
type script_expr_t2 =
  | Int2 of location * Z.t
  | String2 of location * string
  | Bytes2 of location * Hex.t
  | Seq2 of location * script_expr_t2 list
  | Prim2 of location * string * script_expr_t2 list * string list

let rec tr loc = function
  | Int i -> Int2 (loc, i)
  | String s -> String2 (loc, s)
  | Bytes s -> Bytes2 (loc, s)
  | Seq l -> Seq2 (loc, List.map (tr loc) l)
  | Prim (s, l, la) -> Prim2 (loc, s, List.map (tr loc) l, la)

let location = function
  | Int2 (loc, _) -> loc
  | String2 (loc, _) -> loc
  | Bytes2 (loc, _) -> loc
  | Seq2 (loc, _) -> loc
  | Prim2 (loc, _, _, _) -> loc

let preformat root =
  let preformat_loc (_, _) = (false, 0) in
  let preformat_annots = function
    | [] -> 0
    | annots -> String.length (String.concat " " annots) + 2 in
  let rec preformat_expr = function
    | Int2 (((_, _) as loc), value) ->
      let cml, csz = preformat_loc loc in
      Int2 ((cml, String.length (Z.to_string value) + csz), value)
    | String2 ((_, _) as loc, value) ->
      let cml, csz = preformat_loc loc in
      String2 ((cml, String.length value + csz), value)
    | Bytes2 ((_, _) as loc, value) ->
      let cml, _csz = preformat_loc loc in
      Bytes2 ((cml, 0), value)
    | Prim2 ((_, _) as loc, name, items, annots) ->
      let cml, csz = preformat_loc loc in
      let asz = preformat_annots annots in
      let items = List.map preformat_expr items in
      let ml, sz =
        List.fold_left
          (fun (tml, tsz) e ->
             let (ml, sz) = location e in
             (tml || ml, tsz + 1 + sz))
          (cml, String.length name + csz + asz)
          items in
      Prim2 ((ml, sz), name, items, annots)
    | Seq2 ((_, _) as loc, items) ->
      let cml, csz = preformat_loc loc in
      let items = List.map preformat_expr items in
      let ml, sz =
        List.fold_left
          (fun (tml, tsz) e ->
             let (ml, sz) = location e in
             (tml || ml, tsz + 3 + sz))
          (cml, 4 + csz)
          items in
      Seq2 ((ml, sz), items) in
  preformat_expr root

let rec print_expr_unwrapped ppf = function
  | Prim2 ((ml, s ), name, args, annot) ->
    let name = match annot with
      | [] -> name
      | annots ->
        Format.asprintf "%s @[<h>%a@]" name print_annotations annots in
    if not ml && s < 80 then begin
      if args = [] then
        Format.fprintf ppf "%s" name
      else
        Format.fprintf ppf "@[<h>%s %a@]" name (Format.pp_print_list ~pp_sep:Format.pp_print_space print_expr) args ;
    end else begin
      if args = [] then
        Format.fprintf ppf "%s" name
      else if String.length name <= 4 then
        Format.fprintf ppf "%s @[<v 0>%a@]" name (Format.pp_print_list print_expr) args
      else
        Format.fprintf ppf "@[<v 2>%s@,%a@]" name (Format.pp_print_list print_expr) args ;
    end
  | Int2 ((_, _), value) ->
    Format.fprintf ppf "%s" (Z.to_string value)
  | String2 ((_, _), value) ->
    print_string ppf value
  | Bytes2 ((_, _), value) ->
    Format.fprintf ppf "0x%s" (Hex.show value)
  | Seq2 ((_, _), []) ->
    Format.fprintf ppf "{}"
  | Seq2 ((ml, s), items) ->
    if not ml && s < 80 then
      Format.fprintf ppf "{ @[<h 0>"
    else
      Format.fprintf ppf "{ @[<v 0>" ;
    Format.pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf " ;@ ")
      print_expr_unwrapped
      ppf items ;
    Format.fprintf ppf "@] }"

and print_expr ppf = function
  | Prim2 (_, _, _ :: _, _)
  | Prim2 (_, _, [], _ :: _) as expr ->
    Format.fprintf ppf "(%a)" print_expr_unwrapped expr
  | expr -> print_expr_unwrapped ppf expr

let print_expr_unwrapped ppf expr =
  print_expr_unwrapped ppf (preformat expr)

let print_expr ppf expr =
  print_expr ppf (preformat expr)

let to_string expr =
  let flush expr =
    print_expr_unwrapped Format.str_formatter expr ;
    Format.flush_str_formatter () in
  let expr = tr (false, 0) expr in
  match expr with
  | Seq2 (_, exprs) ->
    (String.concat ";\n"
       (List.map flush exprs))
  | Prim2 _ | Int2 _ | String2 _ | Bytes2 _ -> flush expr
