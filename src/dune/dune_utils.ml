(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types_min

let unopt ~default = function None -> default | Some value -> value

let string_of_ballot_vote = function
  | Yay -> "Yay"
  | Nay -> "Nay"
  | Pass -> "Pass"

let ballot_of_string = function
  | "Yay" | "yay" -> Yay
  | "Pass"| "pass" -> Pass
  | "Nay" | "nay" -> Nay
  | _ -> Nay

let voting_period_kind_of_string = function
  | "proposal" -> NProposal
  | "testing_vote" -> NTesting_vote
  | "testing" -> NTesting
  | "promotion_vote" -> NPromotion_vote
  | _ -> assert false

let string_of_voting_period_kind = function
  | NProposal -> "proposal"
  | NTesting_vote -> "testing_vote"
  | NTesting -> "testing"
  | NPromotion_vote -> "promotion_vote"

let pp_voting_period_kind = function
  | NProposal -> "Proposal"
  | NTesting_vote -> "Exploration"
  | NTesting -> "Testing"
  | NPromotion_vote -> "Promotion"

let string_of_op_contents = function
  | NSeed_nonce_revelation _ | NDouble_baking_evidence _
  | NDouble_endorsement_evidence _ | NActivation _ -> "Anonymous"
  | NEndorsement _ -> "Endorsement"
  | NProposals _ -> "Proposals"
  | NBallot _ -> "Ballot"
  | NTransaction _ | NDelegation _
  | NOrigination _ | NReveal _ -> "Manager"
  | NActivate -> "Activate"
  | NActivate_testnet -> "Activate_testnet"
  | NActivate_protocol _ | NManage_accounts _ | NManage_account _ -> "Dune_manager"

let string_of_anonymous_op_contents = List.fold_left (fun acc op -> match op with
    | NActivation _ -> "Activation" :: acc
    | NDouble_endorsement_evidence _ -> "Double_endorsement_evidence" :: acc
    | NDouble_baking_evidence _ -> "Double_baking_evidence" :: acc
    | NSeed_nonce_revelation _ -> "Nonce" :: acc
    | _ -> acc) []

let get_manager_metadata = function
  | NTransaction {node_tr_metadata = meta; _ }
  | NOrigination {node_or_metadata = meta; _ }
  | NReveal {node_rvl_metadata = meta; _ }
  | NDelegation {node_del_metadata = meta; _ }
  | NActivate_protocol {node_acp_metadata = meta; _ }
  | NManage_accounts {node_macs_metadata = meta; _ }
  | NManage_account {node_mac_metadata = meta; _ } -> meta
  | _ -> None

let rec string_of_manager_op_contents l = List.fold_left (fun acc op ->
    let internals = match get_manager_metadata op with
      | None -> []
      | Some meta ->
        string_of_manager_op_contents meta.manager_meta_internal_operation_results in
    match op with
    | NTransaction _ -> "Transaction" :: internals @ acc
    | NOrigination _ -> "Origination" :: internals @ acc
    | NReveal _ -> "Reveal" :: internals @ acc
    | NDelegation _ -> "Delegation" :: internals @ acc
    | NActivate_protocol _ -> "Activate_protocol" :: internals @ acc
    | NManage_accounts _ -> "Manage_accounts" :: internals @ acc
    | NManage_account _ -> "Manage_account" :: internals @ acc
    | _ -> acc) [] l

let string_of_balance_update = function
  | Contract (s, i) -> Printf.sprintf "Contract %s %Ld" s i
  | Rewards (s, i1, i2) -> Printf.sprintf "Rewards %s %d %Ld" s i1 i2
  | Fees (s, i1, i2) -> Printf.sprintf "Fees %s %d %Ld" s i1 i2
  | Deposits (s, i1, i2) -> Printf.sprintf "Deposits %s %d %Ld" s i1 i2

let is_contract addr =
  if String.length addr < 1 then false
  else String.get addr 0 = 'K'

let lang_of_string = function
  | "love" -> Language.Love
  | _ -> Language.Michelson

let lang_to_string = function
  | Language.Love -> "love"
  | Language.Michelson -> "michelson"
