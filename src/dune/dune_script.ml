open Dune_types_min
open Language

(* printers *)

let print_love_expr = function
  | LoveValue lv -> Format.asprintf "%a" Love_printer.Value.print lv
  | LoveType lt -> Format.asprintf "%a" Love_type.pretty lt

let print_love_code = function
  | LoveTopContract ltc ->
    Format.asprintf "%a" Love_printer.Ast.print_structure ltc.Love_ast_types.AST.code
  | LoveLiveContract llc ->
    Format.asprintf "%a"
      Love_printer.Value.print_livestructure llc.Love_value.LiveContract.root_struct
  | LoveFeeCode _lfc -> ""

let print_dune_expr = function LoveExpr le -> print_love_expr le
let print_dune_code = function LoveCode le -> print_love_code le

let print_script = function
  | Micheline sc -> Micheline_printer.to_string sc
  | DuneExpr de -> print_dune_expr de
  | DuneCode dc -> print_dune_code dc
