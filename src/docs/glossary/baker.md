A baker is an account in the blockchain that creates blocks containing operations.
It is the equivalent of a miner for Bitcoin, or a validator in some other PoS blockchains.
The particularity in the Tezos based blockchain, which rests on a LDPoS (liquid delegated proof of stake) algorithm, is that any user can delegate its coins to a baker and the baker has consequently more opportunities to bake (create) a block for the blockchain.
The baker has to put some deposit on stake to create a block which is frozen during a certain time. At the end of this time the deposit is returned along with some rewards. The baker can then decide to share its rewards among its delegators.
