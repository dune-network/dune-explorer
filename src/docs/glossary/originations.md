This operation is used to deploy a contract on the blockchain. It creates a KT1 account that can be called from other users.
