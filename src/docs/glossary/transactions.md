As in most blockchains, coins are exchanged from a source to a destination through a transaction.
These transactions are also used to call KT1 contract, using the `parameters` containing an `entrypoint` and a `value`.
