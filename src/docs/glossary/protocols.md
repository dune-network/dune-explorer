A protocol, designated by its hash, corresponds to a version of the code ruling how the blockchain operates (system of blocks and transactions, PoS algorithm, ...).
