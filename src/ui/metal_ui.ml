(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Ezjs_bs3.Button
open Ezjs_bs3.Attributes
open Ezjs_bs3.Input
open Ezjs_bs3.Icon
open Ezjs_bs3.Align
open Data_types

module Metal_api = Metal_api.Metal

let clear_input id =
  match Manip.by_id id with
  | None -> ()
  | Some elt -> Manip.set_value elt ""

let clear_result id =
  match Manip.by_id id with
  | None -> ()
  | Some elt -> Manip.removeChildren elt

let onkd ?kd ?return e =
  if e##.keyCode = 13 then match return with None -> true | Some r -> r e
  else match kd with | None -> true | Some kd -> kd e

let delegate_button delegate =
  let label, id = match delegate with
    | None -> "Undelegate", "metal-undelegate-button"
    | Some delegate -> "Delegate", "metal-delegate-button-" ^ delegate in
  button ~a:[
    a_class [btn; btn_default; "metal-button"]; a_style "width: 100px; display: none";
    a_id id; a_onclick (fun _e ->
        Metal_api.delegate delegate (fun _op_hash -> ());
        true) ] [ txt label ]

let send_button ?(width=100) ?(text="Send") id =
  let button_id = "metal-send-button-" ^ id in
  let popover_id = "metal-send-popover-" ^ id in
  let amount_id = "metal-send-amount-" ^ id in
  let dst_id = "metal-send-dst-" ^ id in
  let param_id = "metal-send-param-" ^ id in
  let entrypoint_id = "metal-send-entrypoint-" ^ id in
  let result_id = "metal-send-result-" ^ id in
  let content = "<div id='" ^ popover_id ^ "'></div>" in
  button ~a:[
    a_class [btn; btn_default; "metal-button"];
    a_data_toggle "popover"; a_data_content content;
    a_id button_id; a_data_html true;
    a_data_placement `Bottom;
    a_onclick (fun _e ->
        List.iter clear_input [amount_id; dst_id; param_id; entrypoint_id ];
        clear_result result_id; true);
    a_style ("width: " ^ string_of_int width ^ "px; display: none")] [
    txt text ]

let fill_popover button_id popover_id content =
  let j_obj = Printf.sprintf "jQuery(\'#%s\')" button_id in
  let elt = Js.Unsafe.eval_string j_obj  in
  Js.Unsafe.meth_call elt "on"
    [| Js.Unsafe.inject (Js.string "inserted.bs.popover");
       Js.Unsafe.inject (fun _ ->
           Manip.replaceChildren (find_component popover_id)
             content;
           Js._true) |] |> ignore

let make_transaction_arguments
    ?contract ?symbol ?decimals ?parameter ?entrypoint
    ?(language=Michelson) ~destination amount =
  match contract, symbol with
  | Some contract, Some symbol ->
    let amount = Dun.token_of_string ?decimals ~catches:[] symbol amount in
    let parameters = match language with
      | Michelson ->
        Micheline (Prim (
            "Pair", [String destination; Prim (
                "Pair", [Int amount; Prim ("None", [], [])], [])], []))
      | Love ->
        DuneExpr (LoveExpr (LoveValue (Love_value.Value.(VTuple [
            VAddress destination; VNat amount;
            VConstr ("None", []) ])))) in
        contract, "0", Some "transfer", Some (Api_encoding.Script.encode parameters)
  | _ -> destination, Int64.to_string (Dun.dun_of_string amount), entrypoint, parameter

let activate_send_button ?(text="Send") ?contract ?symbol ?decimals ?language id destination =
  let button_id = "metal-send-button-" ^ id in
  let popover_id = "metal-send-popover-" ^ id in
  let amount_id = "metal-send-amount-" ^ id in
  let dst_id = "metal-send-dst-" ^ id in
  let param_id = "metal-send-param-" ^ id in
  let entrypoint_id = "metal-send-entrypoint-" ^ id in
  let result_id = "metal-send-result-" ^ id in
  let onclick _e =
    let amount = Manip.value @@ find_component amount_id in
    let destination = match destination with
      | None -> Manip.value @@ find_component dst_id
      | Some dst -> dst in
    let entrypoint, parameter = match contract with
      | None ->
        if Dune_utils.is_contract id then
          Some (Manip.value @@ find_component entrypoint_id),
          Some (Manip.value @@ find_component param_id)
        else
          None, None
      | Some _ -> None, None in
    let destination, amount, entrypoint, parameter =
      make_transaction_arguments ?contract ?symbol ?decimals
        ?entrypoint ?parameter ?language ~destination amount in
    Metal_api.send ?parameter ?entrypoint ~destination ~amount
      (fun r ->
         List.iter clear_input [amount_id; dst_id; param_id; entrypoint_id ];
         let result_elt = find_component result_id in
         match r with
         | Metal_api.Ok op_hash ->
           Manip.replaceChildren result_elt [
             div ~a:[ a_class ["green"; text_center] ] [
               fas "check-circle";
               span ~a:[ a_class ["metal-result-operation"] ] [
                 txt @@ Common.crop_hash ~crop_len:10 op_hash ] ] ]
         | Metal_api.Canceled ->
           Manip.replaceChildren result_elt [
             div ~a:[ a_class ["red"; text_center] ] [
               fas "times-circle";
               span ~a:[ a_class ["metal-result-operation"] ] [
                 txt "cancelled" ] ] ]);
    true in
  let dst_input = match destination with
    | None ->
      [ input ~a:[ a_id dst_id; a_input_type `Text;
                   a_onkeydown (onkd ~kd:(fun _e -> clear_result result_id; true)
                                  ~return:onclick);
                   a_class [form_control]; a_style "width: 200px";
                   a_placeholder "Destination" ] () ]
    | Some _ -> [] in
  let param_input = match contract with
    | None ->
      if Dune_utils.is_contract id then
        [ input ~a:[ a_id entrypoint_id; a_input_type `Text; a_class [form_control];
                     a_placeholder "Entrypoint" ] ();
          textarea ~a:[ a_id param_id; a_class [form_control];
                        a_placeholder "Params"; a_rows 1 ] (txt "")  ]
        else []
    | Some _ -> [] in
  let inputs = dst_input @ param_input in
  let amount_style = if List.length inputs < 2 then [a_style "width: 100px"] else [] in
  let amount_input =
    input ~a:(a_id amount_id :: a_input_type `Text ::
              a_onkeydown (onkd ~kd:(fun _e -> clear_result result_id; true)
                             ~return:onclick) ::
              a_class [form_control] :: a_placeholder "Amount" :: amount_style) () in
  let inputs = amount_input :: inputs in
  let input_class, button_class =
    if List.length inputs < 3 then [input_group], [input_group_btn]
    else [text_center], [] in
  let content = [
    div ~a:[ a_class input_class ] @@
    inputs @ ([
        div ~a:[ a_class button_class ] [
          button ~a:[
            a_class [btn; btn_default]; a_button_type `Submit;
            a_onclick onclick ] [ txt text ] ] ]);
    div ~a:[ a_id result_id ] []
  ] in
  fill_popover button_id popover_id content;
  show @@ find_component button_id

let activate_delegate_button = function
  | None -> ()
  | Some id -> show @@ find_component ("metal-delegate-button-" ^ id)

let activate_buttons ?delegate_id ?text ?contract ?symbol ?decimals ?language send_id destination =
  Metal_api.ready (fun m ->
      let connect_button_id = "metal-connect-button-" ^ send_id in
      let invalid_id = "invalid-metal-network" ^ send_id in
      let f () =
        let connect_button = [
          button ~a:[
            a_class [btn; btn_default; "metal-button"];
            a_onclick (fun _e ->
                hide @@ find_component invalid_id;
                Metal_api.get_network ~m (fun (name, _url) ->
                    if Infos.(network_to_string net) = name then (
                      hide @@ find_component connect_button_id;
                      activate_send_button ?text ?contract ?symbol ?decimals ?language
                        send_id destination;
                      activate_delegate_button delegate_id)
                    else
                      show @@ find_component invalid_id);
                false
              ) ] [ txt "Metal Connect" ];
          div ~a:[ a_id invalid_id ; a_style "display: none"; a_class ["text-danger"]] [
            txt "Mismatch with Metal network" ]] in
        Manip.replaceChildren (find_component connect_button_id) connect_button in
      f ();
      Metal_api.on_state_changed ~m (fun _ ->
          show @@ find_component connect_button_id;
          hide @@ find_component ("metal-send-button-" ^ send_id);
          hide @@ find_component ("metal-delegate-button-" ^ send_id);
          f ()))
