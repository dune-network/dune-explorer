(* Uses JS library blockies https://github.com/ethereum/blockies *)

open Ezjs_tyxml

module Base58 = struct
  let alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

  let charcodes =
    let h = Hashtbl.create @@ String.length alphabet in
    String.iteri (fun i c -> Hashtbl.add h c i) alphabet;
    h

  let b58_code c =
    match Hashtbl.find_opt charcodes c with
    | None -> failwith "Not base58"
    | Some i -> i

  let base58_to_int s =
    let n = String.length s in
    let rec aux = function
      | 0 -> Z.one, Z.of_int (b58_code s.[n-1])
      | i ->
        let (pos, x) = aux (i-1) in
        let y = Z.of_int (b58_code s.[n-i-1]) in
        let pos = Z.mul pos (Z.of_int 58) in
        (pos, Z.add x (Z.mul y pos)) in
    snd (aux (n-1))

  let big_int_to_hex b =
    let b16 = Z.of_int 16 in
    let rec aux b =
      if b > Z.zero then
        let q, r = Z.div_rem b b16 in
        Printf.sprintf "%s%x" (aux q) (Z.to_int r)
      else "" in
    let s = aux b in
    if (String.length s) mod 2 = 0 then s else "0" ^ s

  let to_hex s =
    s |> base58_to_int |> big_int_to_hex
end

let create ?(size=8) ?(scale=8) tz1 =
  let seed = Base58.to_hex tz1 in
  let icon = Of_dom.of_canvas @@
    Ezjs_blockies.create ~seed ~size ~scale ~spotcolor:"#000" () in
  Manip.SetCss.borderRadius icon "3px";
  icon

let find_component id =
  match Manip.by_id id with
  | Some div -> div
  | None -> failwith ("Cannot find id " ^ id)

let update ?id ?className ?(left=false) tz1 =
  let elts =
    match id, className with
    | None, None -> []
    | Some id, _ -> [find_component id]
    | _, Some cl -> Manip.by_class cl
  in
  List.iter (fun sp ->
      let icon = create tz1 in
      Manip.removeChildren sp;
      Manip.appendChild sp icon;
      Manip.SetCss.height icon "100%";
      Manip.SetCss.width icon "100%";
      if left then
        Manip.SetCss.borderRadius icon "3px 0 0 3px"
    ) elts
