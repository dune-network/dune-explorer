(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml.Html
open Ezjs_bs3.Grid
open Ezjs_bs3.Icon
open Data_types
open Lang
open Text
open Common

let accounts_id = "accounts"
let accounts_node_id = "accounts-node"
let account_id hash = make_id "accounts-hash" hash

let node_state ts = Node_state_ui.node_state_panel accounts_node_id ts

let columns () = tr [
    td @@ cl_icon account_icon (t_ s_hash);
    td @@ cl_icon manager_icon (t_ s_manager);
    td @@ cl_icon astronaut_icon (t_ s_delegate);
    td @@ cl_icon wallet_icon (t_ s_spendable);
    td [ txt_t s_origination ] ;
    td @@ cl_icon Dun.icon (t_ s_balance);
    td [ txt_t s_frozen ]
  ]

module AccountsPanel =
  Panel.MakePageTable(struct
    let name = "accounts"
    let page_size = big_panel_number
    let theads = columns
    let title_span = Panel.title_nb s_accounts
        ~help:Glossary_doc.HAccount
    let table_class =  "default-table"
  end)

module ContractsPanel =
  Panel.MakePageTable(struct
    let name = "contracts"
    let page_size = big_panel_number
    let theads = columns
    let title_span = Panel.title_nb s_contracts
        ~help:Glossary_doc.HAccount
    let table_class =  "default-table"
  end)


let make_accounts ?(contract=false) () =
  let node_state_panel =
    div ~a:[  a_id accounts_node_id; a_class [ clg12 ] ] [
      div ~a:[ a_class [ "alert" ] ] [
        strong [ txt_t s_loading ]
      ]
    ] in
  let acc_panel =
    if contract then
      ContractsPanel.make_clg12 ~footer:true ()
    else
      AccountsPanel.make_clg12 ~footer:true ()
  in
  div [ node_state_panel; acc_panel ]

let to_rows accs =
  List.map (
    fun {ac_name; ac_manager; ac_spendable; ac_delegatable; ac_delegate;
         ac_origination; ac_balance} ->
      let td_delegate = match ac_delegatable, ac_delegate with
        | _, Some delegate when delegate.pkh <> "" ->
          account_w_blockies ~crop_len:20 ~crop_limit:md_size delegate
        | Some true, _ -> td [ txt "delegatable" ]
        | _ -> td [ txt_ () ] in
      tr [
        account_w_blockies ~crop_len:20 ~crop_limit:md_size ac_name;
        (match ac_manager with
         | None -> td [ txt_ () ]
         | Some ac_manager ->
           account_w_blockies ~crop_len:20 ~crop_limit:md_size ac_manager);
        td_delegate;
        td [ match ac_spendable with
            | None -> txt_ ()
            | Some true -> txt_t s_yes
            | Some false -> txt_t s_no ];
        td [ match ac_origination with
            | Some origination -> Common.make_link ~crop_len:10 origination
            | _ -> txt_ () ];
        td [ Dun.pp_amount ac_balance.b_spendable ];
        td [ Dun.pp_amount ac_balance.b_frozen ];
      ]) accs

let update ?(contract=false) nrows xhr_request =
  if contract then
    ContractsPanel.paginate_fun to_rows ~nrows xhr_request
  else
    AccountsPanel.paginate_fun to_rows ~nrows xhr_request
