open Ezjs_tyxml
open Html
open Ezjs_bs3.Grid
open Ezjs_bs3.Button
open Ezjs_bs3.Attributes

let current_filters = ref Ast.Empty

let filters_list_id = "filters-list"
let level_id = "level"
let cycle_id = "cycle"
let cycle_position_id = "cycle-position"
let date_id = "date"
let n_operations_id = "n-operations"
let fitness_id = "fitness"
let priority_id = "priority"
let n_endorsements_id = "n_endorsements_id"
let volume_id = "volume"
let fees_id = "fees"
let validation_pass_id = "validation-pass"
let endorsed_by_id = "endorsed-by"
let baker_id = "baker"
let bhash_id = "bhash"
let network_id = "network"
let pred_id = "pred"
let succ_id = "succ"
let proto_id = "proto"
let signature_id = "signature"
let op_kind_id = "op-kind"
let op_status_id = "op-status"
let fee_id = "fee"
let amount_id = "amount"
let ohash_id = "ohash"
let op_bhash_id = "op-bhash"
let op_from_id = "op-from"
let op_to_id = "op-to"
let internal_id = "internal"
let counter_id = "counter"
let burn_id = "burn"
let param_id = "param"
let ahash_id = "ahash"
let delegate_to_id = "delegate-to"
let block_baked_id = "black-baked"
let level_baked_id = "level-baked"
let spendable_bal_id = "spendable-bal"
let frozen_bal_id = "frozen-bal"
let rewards_bal_id = "rewards-bal"
let fees_bal_id = "fees-bal"
let deposits_bal_id = "deposits-bal"
let staking_bal_id = "staking-bal"
let delegated_id = "delegated"
let revealed_id = "revealed"
let originated_id = "originated"
let collect_call_id = "collect-call"

let id_str x = x

let get_value id tr =
  match Manip.by_id id with
  | None -> None
  | Some elt -> match Manip.value elt with
    | "" -> None
    | s -> Some (tr (String.trim s))

let get_constraints id tr filter_tr =
  List.fold_left (fun acc (x, s) -> match get_value (id ^ x) tr with
      | None -> acc
      | Some y -> (filter_tr (s, y)) :: acc) [] ["-e", Ast.Eq; "-l", Ast.Le; "-g", Ast.Gr]

let make_hash_type h =
  let n = String.length h in
  if n = 0 then Ast.Is ""
  else
    let s = (String.sub h 1 (n-1)) in match h.[0] with
    | '^' -> Ast.StartsWith s
    | '$' -> Ast.EndsWith s
    | '!' -> Ast.IsNot s
    | '=' -> Ast.Is s
    | _ -> Ast.Contains h

let get_global_filters () =
  List.concat [
    get_constraints level_id Int64.of_string (fun (s,a) -> Ast.Level (s, a));
    get_constraints cycle_id Int64.of_string (fun (s,a) -> Ast.Cycle (s, a));
    get_constraints date_id Ast.Date.of_string (fun (s,a) -> Ast.Date (s, a)) ]

let get_block_filters () =
  List.concat [
    get_constraints n_operations_id Int64.of_string (fun (s,a) -> Ast.NumOperations (s, a));
    get_constraints fitness_id Int64.of_string (fun (s,a) -> Ast.Fitness (s, a));
    get_constraints priority_id Int64.of_string (fun (s,a) -> Ast.Priority (s, a));
    get_constraints n_endorsements_id Int64.of_string (fun (s,a) -> Ast.Endorsements (s, a));
    get_constraints volume_id Dun.dun_of_string (fun (s,a) -> Ast.Volume (s, a));
    get_constraints fees_id Dun.dun_of_string (fun (s,a) -> Ast.Fees (s, a));
    get_constraints validation_pass_id Int64.of_string (fun (s,a) -> Ast.ValidationPass (s, a));
    get_constraints cycle_position_id Int64.of_string (fun (s,a) -> Ast.CyclePos (s, a));
  ] @
  (List.filter_map (fun x -> x) [
      (* get_value endorsed_by_id (fun h -> Ast.EndorsedBy (make_hash_type h)); *)
      get_value baker_id (fun h -> Ast.Baker (make_hash_type h));
      get_value bhash_id (fun h -> Ast.BHash (make_hash_type h));
      get_value network_id (fun h -> Ast.Network (make_hash_type h));
      get_value pred_id (fun h -> Ast.Pred (make_hash_type h));
      (* get_value succ_id (fun h -> Ast.Succ (make_hash_type h)); *)
      get_value proto_id (fun h -> Ast.Proto (make_hash_type h));
      get_value signature_id (fun h -> Ast.Signature (make_hash_type h));
    ])

let get_operation_filters () =
  List.concat [
    get_constraints fee_id Dun.dun_of_string (fun (s,a) -> Ast.Fee (s, a));
    get_constraints amount_id Dun.dun_of_string (fun (s,a) -> Ast.Amount (s, a));
    get_constraints counter_id Int64.of_string (fun (s,a) -> Ast.Counter (s, a));
    get_constraints burn_id Int64.of_string (fun (s,a) -> Ast.Burn (s, a));
  ] @
  (List.filter_map (fun x -> x) [
      get_value ohash_id (fun h -> Ast.OHash (make_hash_type h));
      get_value op_kind_id (function
          | "originations" ->  Ast.(Kind Origination_k)
          | "delegations" -> Ast.(Kind Delegation_k)
          | "endorsements" -> Ast.(Kind Endorsement_k)
          | "reveals" -> Ast.(Kind Reveal_k)
          | "activations" -> Ast.(Kind Activation_k)
          | _ -> Ast.(Kind Transaction_k));
      get_value op_bhash_id (fun h -> Ast.Block (make_hash_type h));
      get_value op_from_id (fun h -> Ast.From (make_hash_type h));
      get_value op_to_id (fun h -> Ast.To (make_hash_type h));
      get_value op_status_id (function
          | "failed" -> Ast.Failed true
          | _ -> Ast.Failed false);
      get_value internal_id (function
          | "internal" -> Ast.Internal true
          | _ -> Ast.Internal false);
      get_value param_id (function
          | "with" -> Ast.Param true
          | _ -> Ast.Param false);
      get_value collect_call_id (function
          | "true" -> Ast.CollectCall true
          | _ -> Ast.CollectCall false);
    ])

let get_account_filters () =
  List.concat [
    get_constraints level_baked_id Int64.of_string (fun (s,a) -> Ast.BakedLevel (s, a));
    get_constraints spendable_bal_id Dun.dun_of_string (fun (s,a) ->
        Ast.(Balance (Balance_k, s, a)));
    get_constraints frozen_bal_id Dun.dun_of_string (fun (s,a) ->
        Ast.(Balance (Frozen_balance_k, s, a)));
    get_constraints rewards_bal_id Dun.dun_of_string (fun (s,a) ->
        Ast.(Balance (Rewards_k, s, a)));
    get_constraints fees_bal_id Dun.dun_of_string (fun (s,a) ->
        Ast.(Balance (Fees_k, s, a)));
    get_constraints deposits_bal_id Dun.dun_of_string (fun (s,a) ->
        Ast.(Balance (Deposits_k, s, a)));
    get_constraints staking_bal_id Dun.dun_of_string (fun (s,a) ->
        Ast.(Balance (Staking_balance_k, s, a)))
  ] @
  (List.filter_map (fun x -> x) [
      get_value ahash_id (fun h -> Ast.AHash (make_hash_type h));
      get_value delegate_to_id (fun h -> Ast.DelegateTo (make_hash_type h));
      get_value block_baked_id (fun h -> Ast.BakedHash (make_hash_type h));
      get_value delegated_id (function
          | "delegated" -> Ast.Delegated true
          | _ -> Ast.Delegated false);
      get_value revealed_id (function
          | "revealed" -> Ast.Revealed true
          | _ -> Ast.Revealed false);
      get_value originated_id (function
          | "originated" -> Ast.Originated true
          | _ -> Ast.Originated false)
    ])

let get_filters () =
  let block_filters = get_block_filters () in
  if List.length block_filters > 0 then Ast.Blocks block_filters
  else (
    let operation_filters = get_operation_filters () in
    if List.length operation_filters > 0 then Ast.Operations operation_filters
    else (
      let account_filters = get_account_filters () in
      if List.length account_filters > 0 then Ast.Accounts account_filters
      else Ast.Empty))

let merge_filters global_filters manual_filters filters =
  let open Ast in
  let filters = match filters, manual_filters with
    | Blocks filters, Empty | Empty, Blocks filters -> Blocks filters
    | Blocks filters, Blocks manual_filters -> Blocks (filters @ manual_filters)
    | Operations filters, Empty | Empty, Operations filters -> Operations filters
    | Operations filters, Operations manual_filters -> Operations (filters @ manual_filters)
    | Accounts filters, Empty | Empty, Accounts filters -> Accounts filters
    | Accounts filters, Accounts manual_filters -> Accounts (filters @ manual_filters)
    | Bal filters, Empty | Empty, Bal filters -> Bal filters
    | Bal filters, Bal manual_filters -> Bal (filters @ manual_filters)
    | BalUpdate filters, Empty | Empty, BalUpdate filters -> BalUpdate filters
    | BalUpdate filters, BalUpdate manual_filters -> BalUpdate (filters @ manual_filters)
    | _ -> Empty in
  match filters with
  | Blocks filters ->
    Blocks (filters @ (List.map (fun x -> BGlobal x) global_filters))
  | Operations filters ->
    Operations (filters @ (List.map (fun x -> OGlobal x) global_filters))
  | Accounts filters ->
    Accounts (filters @ (List.map (fun x -> AGlobal x) global_filters))
  | Bal filters ->
    Bal (filters @ (List.map (fun x -> BalGlobal x) global_filters))
  | BalUpdate filters ->
    BalUpdate (filters @ (List.map (fun x -> BUGlobal x) global_filters))
  | Empty when global_filters <> [] ->
    Blocks (List.map (fun x -> BGlobal x) global_filters)
  | _ -> log "filters not compatible"; Empty

let manual_filter_search xhr _e =
  let manual_filters = get_filters () in
  let global_filters = get_global_filters () in
  let filters = merge_filters global_filters manual_filters Ast.Empty in
  xhr filters;
  true

let keydown_filter_search xhr e =
  if e##.keyCode = 13 then manual_filter_search xhr e
  else true

let simple_form ?(placeholder="") xhr id label_tx =
  li ~a:[ a_class ["list-group-item"; "filters-form"] ] [
    label [ txt label_tx ];
    input ~a:[ a_id id; a_placeholder placeholder;
               a_onkeydown (keydown_filter_search xhr) ] ()
  ]

let select_form id label_tx options_list =
  li ~a:[ a_class ["list-group-item"; "filters-form"] ] [
    label ~a:[ a_class [] ] [ txt label_tx ];
    select ~a:[ a_id id ; a_class []] (
      option ~a:[ a_selected () ] (txt "") ::
      (List.map (fun x -> option (txt x )) options_list))
  ]

let constraint_form ?(placeholder="") xhr id label_tx  =
  li ~a:[ a_class ["list-group-item"; "filters-form constraint-form"] ] [
    input ~a:[ a_id (id ^ "-g"); a_placeholder placeholder;
               a_onkeydown (keydown_filter_search xhr) ] ();
    label [ txt "<" ]; label [ txt label_tx ]; label [txt "="];
    input ~a:[ a_id (id ^ "-e"); a_placeholder placeholder;
             a_onkeydown (keydown_filter_search xhr)] ();
    label [ txt (" < ") ];
    input ~a:[ a_id (id ^ "-l"); a_placeholder placeholder;
               a_onkeydown (keydown_filter_search xhr)] ()
  ]

let filter_form xhr =
  div ~a:[ a_id "filters-container"; a_class ["collapse"] ] [
    ul ~a:[ a_class ["list-group"; "adv-search-filter-form"];
            a_id "list-filters"] [
      li ~a:[ a_class ["list-group-item"] ] [
        a ~a:[ a_data_toggle "collapse"; a_href "#global-filters"] [
          txt "Global filters" ]
      ];
      li ~a:[ a_class ["collapse"]; a_id "global-filters" ] [
        ul ~a:[ a_class ["list-group"] ] [
          constraint_form xhr level_id "level";
          constraint_form xhr cycle_id "cycle";
          constraint_form xhr date_id "date";
        ]
      ];
      li ~a:[ a_class ["list-group-item"] ] [
        a ~a:[ a_data_toggle "collapse"; a_href "#block-filters" ] [
          txt "Block filters" ]
      ];
      li ~a:[ a_class ["collapse"]; a_id "block-filters" ] [
        ul ~a:[ a_class ["list-group"] ] [
          constraint_form xhr n_operations_id "#operations";
          constraint_form xhr fitness_id "fitness";
          constraint_form xhr priority_id "priority";
          constraint_form xhr n_endorsements_id "#endorsements";
          constraint_form xhr volume_id "volume";
          constraint_form xhr fees_id "fees";
          constraint_form xhr validation_pass_id "validation pass";
          constraint_form xhr cycle_position_id "cycle position";
          simple_form xhr endorsed_by_id "endorsed by";
          simple_form xhr baker_id "baked by";
          simple_form xhr bhash_id "hash";
          simple_form xhr network_id "network";
          simple_form xhr pred_id "follow block";
          simple_form xhr succ_id "precede block";
          simple_form xhr proto_id "protocol";
          simple_form xhr signature_id "signature";
        ]
      ];
      li ~a:[ a_class ["list-group-item"] ] [
        a ~a:[ a_data_toggle "collapse"; a_href "#operation-filters" ] [
          txt "Operation filters" ]
      ];
      li ~a:[ a_class ["collapse"]; a_id "operation-filters" ] [
        ul ~a:[ a_class ["list-group"] ] [
          simple_form xhr ohash_id "hash";
          constraint_form xhr fee_id "fee";
          constraint_form xhr amount_id "amount";
          constraint_form xhr counter_id "counter";
          constraint_form xhr burn_id "burn";
          select_form op_kind_id "operation kind"
            ["transactions"; "originations"; "delegations"; "endorsements";
             "reveals"; "activations" ];
          select_form op_status_id "status" ["succeeded"; "failed"];
          simple_form xhr op_bhash_id "block";
          simple_form xhr op_from_id "from";
          simple_form xhr op_to_id "to";
          select_form internal_id "internal" ["internal"; "external"];
          select_form param_id "param" ["with"; "without"];
          select_form collect_call_id "collect call" ["true"; "false"];
        ]
      ];
      li ~a:[ a_class ["list-group-item"] ] [
        a ~a:[ a_data_toggle "collapse"; a_href "#account-filters" ] [
          txt "Account filters" ]
      ];
      li ~a:[ a_class ["collapse"]; a_id "account-filters" ] [
        ul ~a:[ a_class ["list-group"] ] [
          simple_form xhr ahash_id "hash";
          simple_form xhr delegate_to_id "delegated to";
          simple_form xhr block_baked_id "baked block";
          constraint_form xhr level_baked_id "baked levels";
          constraint_form xhr spendable_bal_id "balance";
          constraint_form xhr frozen_bal_id "frozen";
          constraint_form xhr rewards_bal_id "rewards";
          constraint_form xhr fees_bal_id "fees";
          constraint_form xhr deposits_bal_id "deposits";
          constraint_form xhr staking_bal_id "staking";
          select_form delegated_id "delegated" ["delegated"; "not delegated"];
          select_form revealed_id "revealed" ["revealed"; "not revealed"];
          select_form originated_id "originated" ["originated"; "not originated"];
        ]
      ]
    ];
    button ~a:[
      a_class [btn; btn_primary];
      a_onclick (manual_filter_search xhr)] [ txt "Go" ]
  ]

let make_adv_search xhr =
  let make_none name content =
    div ~a:[ a_id ("adv-" ^ name); a_style "display: none" ] [ content ] in
  div ~a:[ a_class [row]; a_style "margin-bottom:20px" ] [
    div ~a:[ a_class [clg3; cxs12] ] [
      div ~a:[ a_class [row];
               a_onclick (fun _e ->
                   if Dom_html.window##.screen##.width <= 1200 then (
                     let elt = find_component "filters-container" in
                     let display = Manip.Css.display elt in
                     (if display = "flex" then
                        Manip.SetCss.display elt "none"
                      else
                        Manip.SetCss.display elt "flex"));
                   true)
           ] [
        h4 ~a:[ a_class [cxs10] ] [ txt "More filters" ];
        h4 ~a:[ a_class [cxs2] ] [
          a ~a:[ a_class ["caret"; "pull-right"; "caret-filters"];
                 a_style "margin-top: 5px"] []
        ]
      ];
      filter_form xhr
    ];
    div ~a:[ a_class [clg9; cxs12] ] [
      div [
        h4 [txt "Active filters"];
        div ~a:[ a_id filters_list_id; a_style "padding:10px" ] [] ];
      make_none "blocks" (Blocks_ui.BlocksPanel.make ());
      make_none "transactions" (Operations_ui.TransactionsPanel.make ());
      make_none "originations" (Operations_ui.OriginationsPanel.make ());
      make_none "delegations" (Operations_ui.DelegationsPanel.make ());
      make_none "reveals" (Operations_ui.RevealsPanel.make ());
      make_none "endorsements" (Operations_ui.EndorsementsPanel.make ());
      make_none "activations" (Operations_ui.ActivationsPanel.make ());
      make_none "accounts" (Accounts_ui.AccountsPanel.make ()) ]
  ]

let kinds = ["blocks"; "transactions"; "originations"; "delegations"; "reveals";
             "endorsements"; "accounts"; "activations" ]

let hide_all () =
  List.iter (fun s -> hide (find_component ("adv-" ^ s))) kinds

let to_rows = function
  | Data_types.Blocks_sch l -> Blocks_ui.to_rows l
  | Data_types.Operations_sch (l,kind) ->
    begin
      match kind with
      | "delegations" -> Operations_ui.DelegationsPanel.to_rows l
      | "originations" -> Operations_ui.OriginationsPanel.to_rows l
      | "endorsements" -> Operations_ui.EndorsementsPanel.to_rows l
      | "activations" -> Operations_ui.ActivationsPanel.to_rows l
      | "reveals" -> Operations_ui.RevealsPanel.to_rows l
      | _ -> Operations_ui.TransactionsPanel.to_rows l
    end
  | Data_types.Accounts_sch l -> Accounts_ui.to_rows l

let clear_filters_list () =
  current_filters := Ast.Empty;
  log "remove filters";
  Manip.removeChildren (find_component filters_list_id)

let remove_filter pp filters s =
  List.fold_left (fun acc filter ->
      pp Format.str_formatter filter;
      let filter_str = Format.flush_str_formatter () in
      if s = filter_str then acc
      else filter :: acc) [] filters

let make_filter_badge callback s =
  let onclick _e =
    let filters = begin match !current_filters with
      | Ast.Blocks filters ->
        let l = remove_filter Printer.pp_block_filters filters s in
        if l = [] then Ast.Empty else Ast.Blocks l
      | Ast.Operations filters ->
        let l = remove_filter Printer.pp_operation_filters filters s in
        if l = [] then Ast.Empty else Ast.Operations l
      | Ast.Accounts filters ->
        let l = remove_filter Printer.pp_account_filters filters s in
        if l = [] then Ast.Empty else Ast.Accounts l
      | Ast.Bal filters ->
        let l = remove_filter Printer.pp_balance_filters filters s in
        if l = [] then Ast.Empty else Ast.Bal l
      | Ast.BalUpdate filters ->
        let l = remove_filter Printer.pp_balance_update_filters filters s in
        if l = [] then Ast.Empty else Ast.BalUpdate l
      | _ -> !current_filters end in
    callback filters; false in
  button ~a:[ a_button_type `Button; a_style "margin-left: 5px; margin-right: 5px;";
              a_class Ezjs_bs3.Button.[btn; btn_primary; "badge"];
              a_onclick onclick] [
    txt s;
    entity "times"
  ]

let set_search_value s =
  Manip.set_value (find_component "search")
    (Url.urldecode s)

let add_to_filter_list pp callback filters =
  List.iter (fun filter ->
      pp Format.str_formatter filter;
      let filter_str = Format.flush_str_formatter () in
      let filter_button = make_filter_badge callback filter_str in
      Manip.appendChild (find_component filters_list_id)
        filter_button) filters

let update_filters_list callback filters =
  current_filters := filters;
  match filters with
  | Ast.Blocks filters when List.length filters <> 0 ->
    add_to_filter_list Printer.pp_block_filters callback filters
  | Ast.Operations filters when List.length filters <> 0 ->
    add_to_filter_list Printer.pp_operation_filters callback filters
  | Ast.Accounts filters when List.length filters <> 0 ->
    add_to_filter_list Printer.pp_account_filters callback filters
  | Ast.Bal filters when List.length filters <> 0 ->
    add_to_filter_list Printer.pp_balance_filters callback filters
  | Ast.BalUpdate filters when List.length filters <> 0 ->
    add_to_filter_list Printer.pp_balance_update_filters callback filters
  | _ ->
    Manip.appendChild (find_component filters_list_id)
      (div [txt "No filters"])

let update_adv_search ~nrows kind xhr =
  hide_all ();
  match kind with
  | "search_blocks" ->
    Blocks_ui.BlocksPanel.paginate_fun to_rows ~nrows xhr;
    show (find_component "adv-blocks")
  | "search_transactions" ->
    Operations_ui.TransactionsPanel.paginate_fun to_rows ~nrows xhr;
    show (find_component "adv-transactions")
  | "search_accounts" ->
    Accounts_ui.AccountsPanel.paginate_fun to_rows ~nrows xhr;
    show (find_component "adv-accounts")
  | "search_delegations" ->
    Operations_ui.DelegationsPanel.paginate_fun to_rows ~nrows xhr;
    show (find_component "adv-delegations")
  | "search_activations" ->
    Operations_ui.ActivationsPanel.paginate_fun to_rows ~nrows xhr;
    show (find_component "adv-activations")
  | "search_endorsements" ->
    Operations_ui.EndorsementsPanel.paginate_fun to_rows ~nrows xhr;
    show (find_component "adv-endorsements")
  | "search_originations" ->
    Operations_ui.OriginationsPanel.paginate_fun to_rows ~nrows xhr;
    show (find_component "adv-originations")
  | "search_reveals" ->
    Operations_ui.RevealsPanel.paginate_fun to_rows ~nrows xhr;
    show (find_component "adv-reveals")
  | _ -> ()
