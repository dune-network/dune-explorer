(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Ezjs_bs3.Icon
open Ezjs_bs3.Grid
open Ezjs_bs3.Panel
open Ezjs_bs3.Table
open Lang (* s_ *)
open Text

let activation_alert_id = "activation_alert"

let cl_title = Common.responsive_column_title
let cl_icon = Common.responsive_title

module PanelOptions = struct
  let options = Panels.{
      page_size = 20;
      no_block = false;
      no_op = false;
      no_limit = true;
      no_internal = false;
      pan_hash = None;
      no_title = false
    }
end

module TransactionsPanel = Panels.TransactionsPanel(PanelOptions)
module DelegationsPanel = Panels.DelegationsPanel(PanelOptions)
module OriginationsPanel = Panels.OriginationsPanel(PanelOptions)
module RevealsPanel = Panels.RevealsPanel(PanelOptions)
module ManageAccountPanel = Panels.ManageAccountPanel(PanelOptions)
module EndorsementsPanel = Panels.EndorsementsPanel(PanelOptions)
module ActivationsPanel = Panels.ActivationsPanel(PanelOptions)
module DoubleBakingsPanel = Panels.DoubleBakingsPanel(PanelOptions)
module DoubleEndorsementsPanel = Panels.DoubleEndorsementsPanel(PanelOptions)

module Nonces = struct

  module NoncesPanel =
    Panel.MakePageNoTable(struct
      let name = "nonces"
      let page_size = 10
      let title_span = Panel.title_nb s_nonces
    end)

  let nb_revelations ~cst =
    cst.Dune_types.blocks_per_cycle / cst.Dune_types.blocks_per_commitment

  let generate_nonces nonces =
    let revealed (op_hash, _level, bl_hash) =
      match op_hash with
      | None ->
        td ~a:[a_class [cxs2; "bg-red"]] [
          a ~a:( Common.a_link bl_hash ) [ div [ space_icon () ] ] ]
      | Some op_hash ->
        td ~a:[a_class [cxs2; "bg-green"]] [
          a ~a:( Common.a_link op_hash ) [ div [ space_icon () ] ] ] in

    div ~a:[ a_class [ row ; "nonces" ] ] [
      tablex ~a:[ a_class [ btable; btable_bordered; clg12 ] ]
        [ tbody [ tr (List.map revealed nonces) ] ]
    ]

  let make () =
    div ~a:[ a_id "nonces-div" ] [
      NoncesPanel.make ~footer:true ()
    ]

  let to_rows nonces_list =
    List.map (fun (cycle, nonces) ->
        let level = match List.nth_opt nonces 0 with
          | Some (_, level, _) -> level
          | _ -> 0 in
        let cst = Infos.constants ~level in
        let n_revelations =
          List.fold_left (fun i (op_hash, _, _) ->
              if op_hash <> None then i+1 else i) 0 nonces in
        log "n_revelations %d %d %d %d"
          cst.Dune_types.blocks_per_cycle cst.Dune_types.blocks_per_commitment cycle (nb_revelations ~cst);
        make_panel
          ~panel_title_content:(
            div ~a:[ a_class [ panel_title ] ] [
              txt @@ Printf.sprintf "%.0f%% %s %d"
                (float_of_int n_revelations /.
                 float_of_int (nb_revelations ~cst) *. 100.)
                (t_ s_nonces_revelation_for_cycle)
                cycle ])
          ~panel_body_content:[ generate_nonces nonces ]
          ()
      ) nonces_list

  let update_nonces ?nrows xhr =
    NoncesPanel.paginate_fun to_rows ?nrows xhr

end
