(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Ezjs_bs3.Table
open Ezjs_bs3.Form
open Data_types
open Lang
open Text

module StringMap = Map.Make(String)

let theme = ref "default"

let do_and_update_every ?always i f =
  ignore @@ Misc_js.UpdateOnFocus.update_every ?always i f

let html_escaped s =
  let len = String.length s in
  let b = Buffer.create len in
  for i = 0 to len -1 do
    match s.[i] with
    | '<' -> Buffer.add_string b "&lt;"
    | '>' -> Buffer.add_string b "&gt;"
    | '&' -> Buffer.add_string b "&amp;"
    | '"' -> Buffer.add_string b "&quot;"
    | c -> Buffer.add_char b c
  done;
  Buffer.contents b

(* This function is set in Main, it updates the content of the page
   depending on the path given. It is used for internal links. *)
let link_dispatcher = ref (fun (_path : string list) -> ())

let pages = Hashtbl.create 113

let page_hooks = ref []
let add_page_hook f = page_hooks := !page_hooks @ [f]
let exec_page_hooks () =
  List.iter (fun f -> try f () with exn ->
      log "Exception in page_hook: %s" (Printexc.to_string exn))
    !page_hooks

let dispatch path =
  Misc_js.UpdateOnFocus.incr_page ();
  begin
    try
      try
        (Hashtbl.find pages path) ()
      with Not_found ->
        !link_dispatcher path
    with exn ->
      log "Exception in dispatch: %s" (Printexc.to_string exn)
  end;
  exec_page_hooks ()

let add_page path updater =
  Hashtbl.add pages path updater

let redrawers : (string * (unit -> unit) ) list ref = ref []

let initial_redraw_done = ref false
let register_redraw ?before name r =
  match before with
  | None -> redrawers := !redrawers @ [name, r]
  | Some before ->
    let found, l =
      List.fold_left (fun (found, acc) (namef, f) ->
          if namef = before then true, (namef, f) :: (name, r) :: acc
          else found, (namef, f) :: acc) (false, []) !redrawers in
    if found then redrawers := List.rev l
    else redrawers := !redrawers @ [name, r]

let redraw () =
  List.iter (fun (_name, f) -> try f () with exn ->
      log "Exception in redrawer: %s"
        (Printexc.to_string exn)) !redrawers

let initial_redraw () =
  initial_redraw_done := true;
  redraw ()

let redraw () =
  if !initial_redraw_done then begin
    redraw ()
  end


let () =
  Random.self_init ();
  Lang.init redraw



let api_host = ref None

let base_of_host host = EzAPI.TYPES.BASE (Ezjs_loc.proto () ^ host)
let set_api_host host = api_host := Some (base_of_host host)

let set_api_node () =
  match !api_host with
  | Some _ -> ()
  | None ->
    let host =
      match Ezjs_loc.find_arg "api" with
      | None ->
        let hosts = Infos.www.www_apis in
        hosts.( Random.int (Array.length hosts))

      | Some api_host ->
        let network = match DunscanConfig.database with
          | "testnet" | "dune_testnet" -> "testnet."
          | _ -> "" in
        match api_host with
        | ""
        | "1" | "2" | "3"
        | "4" | "5" | "6" -> Printf.sprintf "api%s.%sdunscan.io" api_host network
        | _ -> api_host
    in
    set_api_host host

(* If API_HOST is set, it should always be used, unless another API
   server is specified in the URL arguments. *)
let () =
  if DunscanConfig.api_host <> "" then begin
    let host =
      Printf.sprintf "%s%s" DunscanConfig.api_host
        (if DunscanConfig.api_port = "" then ""
         else ":" ^ DunscanConfig.api_port )
    in
    Infos.www.www_apis <- [| host |];
    set_api_node ()
  end


let api s = match !api_host with
  | None ->
    log "Error: calling uninitialized API from %S" s;
    assert false
  | Some api_host -> api_host

module OptionsStorage = Ezjs_storage.MakeLocal(struct
    type t = string StringMap.t
    let name = "options"
  end)

let options = match OptionsStorage.get () with
  | None -> StringMap.empty
  | Some list -> list

let options =
  let modified = ref false in
  let options = ref options in
  List.iter (fun (arg, value) ->
      if String.length arg > 0 &&
         match arg.[0] with
         'A'..'Z' -> true
         | _ -> false
      then begin
        modified := true;
        options := StringMap.add arg value !options
      end
    ) (Ezjs_loc.args ());
  let options = !options in
  if !modified then OptionsStorage.set options;
  options




let small_panel_number = 5
let big_panel_number = 20

type focus =
  | Focused
  | Blurred_since of float

let window_focused = ref Focused

let stop_update_delay = 3. *. 60. *. 1000. (* 3 minutes *)

let () =
  Dom_html.window##.onblur :=
    Dom_html.handler (fun _ ->
        window_focused := Blurred_since (new%js Js.date_now)##valueOf;
        Js._true);
  Dom_html.window##.onfocus :=
    Dom_html.handler (fun _ ->
        window_focused := Focused;
        Js._true)

(* ****************** *)
let loading () = span [ txt_t s_loading ]
let bullshit_s = "--"
let bullshit_d = ~-1
let txt_ () = txt "--"

(* The two functions below can probably be used in operation_ui to
   refactor some code *)
let mk_row_lbl clg lbl =
  div ~a:[ a_class [ clg; "lbl" ] ] [ Lang.txt_s lbl ]

let mk_row_val clg v =
  div ~a:[ a_class [ clg; "value" ] ] v

let make_id prefix hash = Printf.sprintf "%s-%s" prefix hash

let summary_id hash = make_id "summary" hash
let timestamp_id hash = make_id "ts" hash
let volume_id hash = make_id "volume" hash
let block_uncles_id hash = make_id "block_uncles" hash

let paginate_id container_id = make_id "paginate" container_id
let prev_id container_id = make_id "prev" container_id
let next_id container_id = make_id "next" container_id

let confirmation_blocks_id = "confirmation-blocks"

(* *********** *)

let safe_value s = if s = "" then bullshit_s else s

let responsive_title icon title =
  [
    span ~a:[ a_class [ "visible-xs-inline"; "visible-sm-inline" ] ] [
      icon ()
    ];
    span ~a:[ a_class [ "hidden-xs"; "hidden-sm" ] ] [
      txt title
    ]
  ]

let responsive_title_xs icon title =
  [
    span ~a:[ a_class [ "visible-xs-inline" ] ] [
      icon ()
    ];
    span ~a:[ a_class [ "hidden-xs" ] ] [
      txt title
    ]
  ]

let responsive_title_fun icon title value =
  [
    span ~a:[ a_class [ "visible-xs-inline"; "visible-sm-inline" ] ] [
      icon ()
    ];
    span ~a:[ a_class [ "hidden-xs"; "hidden-sm" ] ] [
      title value
    ]
  ]

let responsive_column_title title abbrev =
  [
    span ~a:[ a_class [ "visible-xs-inline"; "visible-sm-inline" ] ] [
      txt abbrev
    ];
    span ~a:[ a_class [ "hidden-xs"; "hidden-sm" ] ] [
      txt title
    ]
  ]

let make_home_loading_gif id classes =
  div ~a:[ a_id id; a_class classes ] [
    img
      ~alt:"loading"
      ~src:(uri_of_string "/images/white_loading.gif") ()
  ]

let make_loading_gif classes =
  div ~a:[ a_class classes ] [
    img
      ~alt:"loading"
      ~src:(uri_of_string "/images/loading.gif") ()
  ]

let span_loading_gif classes =
  span ~a:[ a_class classes ] [
    img
      ~alt:"loading"
      ~src:(uri_of_string "/images/loading.gif") ()
  ]

let link ?(args=[]) path =
  match args with
  | [] -> path
  | _ ->
    let args =
      String.concat "&"
        (List.map (fun (key, value) -> key ^ "=" ^ value) args)
    in
    if String.contains path '?' then
      Printf.sprintf "%s&%s" path args
    else
      Printf.sprintf "%s?%s" path args

let initial_args =
  List.fold_left (fun args name ->
      match Ezjs_loc.find_arg name with
      | None -> args
      | Some value -> (name, value) :: args)
    [] ["lang"; "node"; "api"; "test"]

let change_page ?(scroll=true) ?(args=[]) path =
  Dom_html.window##.history##pushState
    (Js.some (Js.string path))
    (Js.string "")
    (Js.some (Js.string path));
  Ezjs_loc.set_args args;
  let path =
    if String.length path > 0 && String.get path 0 = '/' then
      String.sub path 1 (String.length path - 1)
    else path in
  dispatch (String.split_on_char '/' path);
  if scroll then Dom_html.window##scroll 0 0;
  let nav_bar = find_component "mainNavBar" in
  Manip.removeClass nav_bar "in"

(* Used to add a lang=fr argument if needed *)
let a_link ?(args=[]) ?(aclass=[]) path =
  if String.length path > 4 && String.sub path 0 4 = "http" then
    (* external link *)
    [a_href (link ~args path)]
  else
    let args = initial_args @ args in
    if String.contains path '?' then
      (* internal link with internal args *)
      match aclass with
      | [] -> [ a_href (link ~args path) ]
      | _ -> [ a_href (link ~args path) ; a_class aclass ]
    else
      match aclass with
      | [] ->
        [a_onclick (fun _ ->
             change_page ~args path;
             false);
         a_href (link ~args path) ]
      | _ ->
        [a_onclick (fun _ ->
             change_page ~scroll:false ~args path;
             false);
         a_href (link ~args path);
         a_class aclass ]

let () =
  Js_of_ocaml_lwt.Lwt_js_events.(async (fun () ->
      onpopstates (fun _popStateEvent _something ->
          dispatch (Ezjs_loc.path ());
          Lwt.return ()
        )))

let set_child id v =
  let td = find_component id in
  Manip.replaceChildren td [ v ]

let set_children id v =
  let td = find_component id in
  Manip.replaceChildren td v

let crop_hash ?crop_len ?(crop_limit=max_int) hash =
  match crop_len with
  | Some crop_len when Dom_html.window##.screen##.width < crop_limit ->
    let len = String.length hash in
    if len < crop_len then hash
    else
    if crop_len >= 14 then
      Printf.sprintf "%s...%s"
        ( String.sub hash 0 7 )
        ( String.sub hash (len-4) 4 )
    else
      String.sub hash 0 crop_len ^ "..."
  | _ -> hash

let make_link_html ?(args=[]) ?(aclass=[]) ~path content =
  a ~a:( a_link ~args ~aclass path ) content

let make_link ?crop_len ?crop_limit ?(args=[]) ?(aclass=[]) ?path (content : string) =
  let path = match path with
    | None -> content
    | Some path -> path
  in
  make_link_html ~args ~aclass ~path
    [ txt @@ crop_hash ?crop_len ?crop_limit content ]

let ( local_aliases : ( string, bookmark ) Hashtbl.t ) = Hashtbl.create 11

(*
let get_alias_details account =
  match Hashtbl.find local_aliases account.pkh with
  | b -> Some b.bookmark_alias, b.bookmark_details
  | exception Not_found -> account.alias, None
*)

let get_alias ?details account =
  match Hashtbl.find local_aliases account.pkh with
  | b ->
    begin (* update details *)
      match details with
      | None -> ()
      | Some details -> b.bookmark_details <- Some details
    end;
    if b.bookmark_alias = "" then
      (* do not display an empty alias ! *)
      account.alias
    else
      Some b.bookmark_alias
  | exception Not_found -> account.alias

let choose_name account =
  match get_alias account with
  | Some name -> name
  | None -> account.pkh

let txt_account ?crop_len ?crop_limit account =
  match Hashtbl.find local_aliases account.pkh with
  | b ->
    span
      [
        txt @@ b.bookmark_alias ;
        span ~a:[ a_class ["glyphicon"; "glyphicon-star-empty"];
                  a_style "font-size:60%;top:-20%;margin-left:1%"] []
      ]
  | exception Not_found -> (
      match account.alias with
      | Some alias -> txt alias
      | None ->
        txt @@ crop_hash ?crop_len ?crop_limit account.pkh
    )

let make_link_account ?crop_len ?crop_limit ?args account =
  make_link_html ?args ~path:account.pkh [(txt_account ?crop_len ?crop_limit account)]

let make_link_level ?args hash level =
  let level_str = string_of_int level in
  make_link hash ?args ~path:level_str

let legend () =
  div ~a:[ a_class [ "legend" ] ] [
    div [
      div ~a:[ a_class [ "bg-green"; "slot-legend" ] ] [ ] ;
      span [ txt "Current Block" ] ;
    ] ;
    div [
      div ~a:[ a_class [ "slot-legend" ] ] [ ] ;
      span[ txt "No Endorsement" ] ;
    ];
    div [
      div ~a:[ a_class [ "bg-red"; "slot-legend" ] ] [ ] ;
      span [ txt "Another Block" ] ;
    ];
    div [
      div ~a:[ a_class [ "slot-double"; "slot-legend" ] ] [ ] ;
      span[ txt "Double Endorsement" ] ;
    ];
    div [
      div ~a:[ a_class [ "bg-grey"; "slot-legend" ] ] [ ] ;
      span[ txt "Pending Endorsement" ] ;
    ];
  ]

let is_double_endorsement i operations =
  let check =
    List.find_all (fun (_, _, e) ->
        List.exists (fun e -> e = i) e.endorse_slot) operations in
  match check with
  | _ :: [] -> false
  | _ -> true

let make_slot i = div [ txt @@ string_of_int i ]

let make_slots nb =
  Array.to_list @@
  Array.mapi (fun i _ ->
      let id = Printf.sprintf "slot-%d" i in
      td ~a:[ a_class [ "slot" ]; a_id id ] [ make_slot i ] )
    (Array.make nb (td []))

let make_8_endorsements_row = function
  | a :: b :: c :: d :: e :: f :: g :: h :: tl ->
    tr [ a ; b ; c ; d ; e ; f ; g ; h ], tl
  | _ -> assert false

let rec make_8_endorsements_rows slots =
  if List.length slots >= 8 then
    let row, tl = make_8_endorsements_row slots in
    row :: (make_8_endorsements_rows tl)
  else
    [ tr slots ]

let make_endorsements_slots slots =
  let rows = make_8_endorsements_rows slots in
  tablex ~a:[ a_class [ btable; btable_bordered ] ]
    [ tbody rows ]

let update_endorsements_slots bhash slots operations =
  List.mapi (fun i row ->
      begin try
          let (op_hash, op_block_hash, e) =
            List.find
              (fun (_, _, e) ->
                List.exists (fun e -> i = e) e.endorse_slot
              ) operations in
          let row =
            td ~a:[ a_class ["slot"] ] [
              a ~a:( a_link ~args:["block_hash", op_block_hash] op_hash ) [
                make_slot i ] ] in
          if is_double_endorsement i operations then
            Manip.addClass row "slot-double"
          else if op_block_hash = Utils.pending_block_hash
                  && e.endorse_block_hash = bhash then
            Manip.addClass row "bg-gray"
          else if e.endorse_block_hash = bhash then
            Manip.addClass row "bg-green"
          else
            Manip.addClass row "bg-red" ;
          row
        with Not_found -> row
      end) slots


let replace_div_by_id id list =
  match Manip.by_id id with
  | None ->
    log "replace_div_by_id %S: div not found" id
  | Some div ->
    Manip.replaceChildren div list

let update_main_content div =
  replace_div_by_id "content" [div]

let get_fitness raw_fitness =
  match String.split_on_char ' ' raw_fitness with
  | [ _version ; fitness ] ->
     (* TODO: do something with version ? *)
     int_of_float @@ float_of_string @@ "0x" ^ fitness
  | _ -> 0

let request_xhr s msg f = EzRequest.ANY.get1 (api "request_xhr") s msg (function
    | Error _ -> ()
    | Ok x -> f x)

let timestamp hash update =
  if hash <> Utils.pending_block_hash && hash <> Utils.orphan_block_hash then
    request_xhr Service.Current.timestamp "Common.timestamp" update hash

let volume hash update =
  if hash <> Utils.pending_block_hash && hash <> Utils.orphan_block_hash then
    request_xhr Service.Current.volume "Common.volume" update hash

let level hash update =
  if hash <> Utils.pending_block_hash && hash <> Utils.orphan_block_hash then
    request_xhr Service.Current.level "Common.level"
      (fun level -> update level.lvl_level) hash

let block_hash_level level =
  request_xhr Service.Current.block_hash_level "Common.block_hash_level"
    (fun hash -> dispatch [hash]) level

let balance hash update =
  request_xhr Service.Current.node_account "Account.details" update hash


(* Pagination utilities *)

let set_url_arg ?(default = "1") arg value =
  let args = Ezjs_loc.args () in
  let replaced = ref false in
  let args = List.fold_right (fun (k, v) newargs ->
                 if k = arg then begin
                     replaced := true;
                     if value = default then
                       newargs
                     else
                       (k, value) :: newargs
                   end
                 else (k, v) :: newargs
               ) args [] in
  let args = if !replaced || value = default then args else
      (arg, value) :: args in
  Ezjs_loc.set_args args


let page_range page nb_pages =
  let pages =
    if nb_pages <= 5
    then List.init nb_pages (fun i -> Some i, i = page)
    else if page <= 2
    then List.init 3 (fun i -> Some i, i = page)
    else if page >= nb_pages - 3
    then List.init 3 (fun i ->
        let p = i + nb_pages - 3 in
        Some p, p = page
      )
    else [ Some page, true ]
  in
  match pages, List.rev pages with
  | [], _ -> pages
  | (Some first, _) :: _, (Some last, _) :: _ ->
    let prefix =
      if first <> 0 then [ Some 0, false; None, false ]
      else [] in
    let suffix =
      if last <> nb_pages - 1 then [ None, false; Some (nb_pages - 1), false ]
      else [] in
    prefix @ pages @ suffix
  | _ -> assert false

let make_fetching () =  Lang.txt_t s_fetching_data

let cl_title = responsive_column_title
let cl_icon = responsive_title
let cl_icon_xs = responsive_title_xs

let get_ele_by_id coerce id =
  match Js.Opt.to_option
          (Dom_html.window##.document##getElementById (Js.string id))
  with
  | None -> failwith "get_ele_by_id: id not found"
  | Some ele ->
     match Js.Opt.to_option @@ coerce ele with
     | None -> failwith "get_ele_by_id: id not a div"
     | Some div -> div

let get_div_by_id = get_ele_by_id Dom_html.CoerceTo.div
let get_img_by_id = get_ele_by_id Dom_html.CoerceTo.img

let account_w_blockies ?(scale=2) ?(aclass=[]) ?(before=[]) ?(after=[])
    ?crop_len ?crop_limit ?args account =
  td ~a:[a_class ("account-w-blockies" :: aclass)]
    (before @ [ Blockies.create ~scale account.pkh;
                make_link_account ?crop_len ?crop_limit ?args account ] @ after)

let account_w_blockies_div ?(scale=2) ?(aclass=[]) ?(before=[]) ?(after=[])
    ?crop_len ?crop_limit ?args account =
  div ~a:[a_class ("account-w-blockies" :: aclass)]
    (before @ [ Blockies.create ~scale account.pkh;
                make_link_account ?crop_len ?crop_limit ?args account ] @ after)

let account_w_blockies_no_link ?(scale=2) ?(tdaclass=[]) ?(txtaclass=[])hash =
  td ~a:[a_class ("account-w-blockies" :: tdaclass)]
    [ Blockies.create ~scale hash;
      span ~a:[ a_class txtaclass] [ txt hash ] ]

let shuffle l =
  List.map (fun v -> Random.int 1_000_000, v) l
  |> List.sort (fun (i, _) (i', _) -> compare i i')
  |> List.map snd

let a_account s =
  a ~a:(a_link s.pkh) [txt_account s]

let hash_to_name ?alias pkh = {pkh; alias}

let time_diff timestamp =
  let now = new%js Js.date_now in
  let timestamp_f = Js.date##parse (Js.string timestamp) in
  timestamp_f -. Js.date##parse (now##toString)

let make_options ?(title="Cycle:") id_container arr update =
  let container = find_component id_container in
  let n = Array.length arr in
  let options =
    Array.to_list @@ Array.mapi (fun i s ->
        if i=0 then option ~a:[ a_selected () ] (txt s)
        else option (txt s)) arr in
  let select_elt =
    select ~a:[ a_class [form_control] ] options in
  Manip.Ev.onchange_select select_elt (fun _e ->
      let select_eltjs = To_dom.of_select select_elt in
      let opt = select_eltjs##.options##item (select_eltjs##.selectedIndex) in
      let selection =
        Js.Opt.case opt
          (fun () -> "")
          (fun opt ->
             try Js.to_string opt##.value
             with _ -> "") in
      update selection;
      true);
  let content = form ~a:[ a_class [ form_inline ] ] [
      div ~a:[ a_class [ form_group ] ] [
        label [ txt title; Ezjs_bs3.Icon.space_icon () ] ;
        select_elt ] ] in
  Manip.removeChildren container ;
  Manip.appendChild container content ;
  if n <> 0 then update arr.(0)

let update_from_attr ?(attr="data-value") f elt =
  let elt2 = Manip.get_elt attr elt in
  match Js.Opt.to_option (elt2##getAttribute (Js.string attr)) with
  | None -> Manip.replaceChildren elt (f "")
  | Some value -> Manip.replaceChildren elt (f (Js.to_string value))

let update_by_class ?(attr="data-value") cl f =
  let l = Manip.by_class cl in
  List.iter (update_from_attr ~attr f) l

let parse_attr_value s =
  List.fold_left (fun acc s ->
      let l = String.split_on_char '=' s in
      match l with
      | [ k; v ] -> (k, v) :: acc
      | _ -> acc) [] (String.split_on_char ',' s)

let download_button ?(btn_class=[]) ?btn_title elt src xhr =
  let btn_title = match btn_title with
    | None -> []
    | Some title -> [ a_title title ] in
  button ~a:([ a_button_type `Submit; a_class btn_class; a_onclick (fun _e ->
      xhr (fun s ->
          Dom_html.window##.location##.href := Js.string (src ^ s));
      true) ] @ btn_title) elt

let div_of_html html =
  let content = Dom_html.createDiv Dom_html.window##.document in
  content##.innerHTML := Js.string html ;
  Of_dom.of_div content

let span_of_html ?(classes=[]) html =
  let content = Dom_html.createSpan Dom_html.window##.document in
  content##.innerHTML := Js.string html ;
  let span = Of_dom.of_element content in
  List.iter (fun s -> Manip.addClass span s) classes;
  span

let img_path logo =
  if String.length logo >= 7 && String.sub logo 0 7 = "http://" then logo
  else Printf.sprintf "/images/%s" logo

let is_input_checked input_id =
  match Manip.by_id input_id with
  | None -> false
  | Some input_elt ->
    let input_obj = To_dom.of_input input_elt in
    Js.to_bool input_obj##.checked

let logo_src theme =
  let src = Filename.concat "/images" Infos.www.www_logo in
  if theme = "slate" then src ^ "-invert.png"
  else src ^ ".png"

let change_theme th =
  let l = Manip.by_tag "link" in
  List.iter (fun elt ->
      let elt = To_dom.of_link elt in
      let rel = elt##getAttribute(Js.string "rel") in
      Js.Opt.case rel
        (fun () -> ())
        (fun rel ->
           let rel = Js.to_string rel in
           if rel = "stylesheet" ||
              rel = "alternate stylesheet" then (
             let title = elt##getAttribute(Js.string "title") in
             Js.Opt.case title
               (fun () -> ())
               (fun title ->
                  let title = Js.to_string title in
                  elt##.disabled := Js._true;
                  if title = ("color-" ^ th) ||
                     title = ("bootstrap-" ^ th) then
                    elt##.disabled := Js._false;
               )))) l;
  theme := th;
  let logo_img = logo_src th in
  (get_img_by_id "logo")##.src := Js.string logo_img

let txtf fmt =
  Format.kfprintf (fun _fmt -> txt @@ Format.flush_str_formatter () )
    Format.str_formatter fmt

let fab name = span ~a:[ a_class ["fab"; "fa-" ^ name ] ] []
let fab_u name () = fab name

let tokens : string list ref = ref []

let failed_class failed =
  if failed then [ "danger" ] else []

let replace id elts = match Manip.by_id id with
  | None -> log "element %s not found" id
  | Some elt -> Manip.replaceChildren elt elts

let replace1 id elt = replace id [ elt ]

let far name = span ~a:[ a_class ["far"; "fa-" ^ name ] ] []
let far_u name () = far name

let activate_popovers () =
  ignore (Js.Unsafe.eval_string "jQuery('[data-toggle=\"popover\"]').popover();")
