(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is accountsuted under the terms of the GNU General Public *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is accountsuted in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Ezjs_bs3.Grid
open Ezjs_bs3.Form
open Data_types
open Text

let spf = Printf.sprintf

let tops_accounts_chart_id = "tops-accounts-pie-id"
let tops_cmd_id = "tops-cmd-id"
let tops_cmd_select_id = "tops-cmd-select-id"

let top_kind_to_title = function
  | Balances -> s_top_balances
  | Frozen_balances -> s_top_frozen_balances
  | Frozen_deposits -> s_top_frozen_deposits
  | Frozen_rewards -> s_top_frozen_rewards
  | Frozen_fees -> s_top_frozen_fees
  | Total_balances -> s_top_total_balances
  | Paid_bytes -> s_top_paid_bytes
  | Staking_balances -> s_top_staking_balances
  | Total_delegated -> s_top_total_delegated
  | Total_delegators -> s_top_total_delegators
  | Used_bytes -> s_top_used_bytes

let top_kind_to_thname = function
  | Balances -> s_balances
  | Frozen_balances -> s_frozen_balances
  | Frozen_deposits -> s_frozen_deposits
  | Frozen_rewards -> s_frozen_rewards
  | Frozen_fees -> s_frozen_fees
  | Total_balances -> s_total_balances
  | Paid_bytes -> s_paid_bytes
  | Staking_balances -> s_staking_balances
  | Total_delegated -> s_total_delegated
  | Total_delegators -> s_total_delegators
  | Used_bytes -> s_used_bytes

let make_table ranking_list =
  List.map
    (fun ((pos : int),owner_hash,balance) ->
      tr [td [Lang.txt_s @@ spf "%i" pos];
          Common.account_w_blockies ~crop_len:20 ~crop_limit:xs_size owner_hash;
          td [ Dun.pp_amount balance ];
    ]) ranking_list

let update_ranking_table kind nrows xhr =
  let module Balance_ranking_table =
    Panel.MakePageTable(
    struct
      let name = "balance_ranking"
      let theads () = tr [
          th [ Lang.txt_t s_ranking ];
          th [ Lang.txt_t s_account ];
          th [ Lang.txt_t @@ top_kind_to_thname kind ];
        ]
      let title_span = Panel.title_nb (top_kind_to_title kind)
      let table_class = "default-table"
      let page_size = 20
    end) in
  let container = find_component "top_accounts_table" in
  let content = Balance_ranking_table.make ~footer:true () in
  Manip.replaceChildren container [ content ];
  Balance_ranking_table.paginate_fun
    ~urlarg_page:"" ~urlarg_size:""
    make_table ~nrows
    (fun page page_size cb ->
       xhr page page_size
         (fun d -> cb d; Common.activate_popovers ()))


let top_kind_to_printer = function
  | Balances
  | Frozen_balances
  | Frozen_deposits
  | Frozen_rewards
  | Frozen_fees
  | Total_balances
  | Staking_balances
  | Total_delegated
   -> Dun.pp_amount
  | Paid_bytes
  | Total_delegators
  | Used_bytes ->
    (fun ?(precision=6) ?(width=15) ?(order=0) ?icon i ->
       ignore (precision, width, order, icon) ;
       span [ txt @@ Int64.to_string i ])

(* mika : add piechart on top of table  *)
(* let update_tops_accounts kind nrows total tops =
 *   let container = find_component "top_accounts_table" in
 *   let title_str = top_kind_to_title kind in
 *   let module Top =
 *     Panel.MakePageTable(
 *     struct
 *       let name = "top_accounts"
 *       let theads =
 *         Panel.theads_of_strings [
 *           s_account, 3;
 *           top_kind_to_thname kind, 1;
 *           s_percent, 1;
 *         ]
 *       let title_span = Panel.title_nb title_str
 *       let table_class = "default-table"
 *       let page_size = 20
 *     end) in
 *   let content = Top.make ~footer:true () in
 *   Manip.removeChildren container ;
 *   Manip.appendChild container content ;
 *
 *   let tops2, tops_total, _ =
 *     List.fold_left (fun (acc_tops, acc_total, acc_count) (dn1, value) ->
 *         if acc_count < 10 then
 *           (dn1, Int64.to_int value) :: acc_tops ,
 *           Int64.add acc_total value,
 *           acc_count + 1
 *         else acc_tops, acc_total, acc_count
 *       ) ([], 0L, 0) tops.top_list in
 *
 *   let tops2 =
 *     let remaining = Int64.sub total tops_total in
 *     if remaining > 0L then
 *       let str = Printf.sprintf "%s" (t_ s_others) in
 *       (str, Int64.to_int remaining) :: tops2
 *     else tops2 in
 *
 *   let data = Array.of_list tops2 in
 *
 *   (\* Table Panel *\)
 *   let rows = Array.map (fun r ->
 *       let owner_hash = fst r
 *       and nb_tops = snd r in
 *       tr [
 *         td [ txt owner_hash ] ;
 *         td [ txt @@ spf "%Ld" nb_tops ];
 *         td [ txt @@ spf "%.2f %%" (
 *             (Int64.to_float nb_tops) /. (Int64.to_float total) *. 100. )];
 *       ]) (Array.of_list tops.top_list) in
 *   Top.paginate_all rows;
 *
 *   (\* Display a pie *\)
 *   let balloon =
 *     "[[title]]<br><span style='font-size:14px'><b>[[value]] tops</b> ([[percents]]%)</span>" in
 *
 *   amcharts3_ready (fun () ->
 *       (\* Hack : pie##theme not working *\)
 *       ignore @@ Js.Unsafe.eval_string "AmCharts.theme = AmCharts.themes.light";
 *       let pie = Amcharts3.pie () in
 *       let export = Amcharts3.export () in
 *       let title = Amcharts3.title () in
 *       let subtitle = Amcharts3.title () in
 *       let legend = Amcharts3.legend () in
 *       subtitle##bold <- false;
 *       title##text <- Js.string @@ Lang.t_ title_str ;
 *       subtitle##text <- Js.string "" ;
 *       export##enabled <- false;
 *       pie##theme <- Js.string "light";
 *       pie##dataProvider <- Amcharts3.Pie.dataProvider data;
 *       pie##outlineColor <- Js.string "#FFFFFF";
 *       pie##balloonText <- Js.string balloon;
 *       pie##titles <- Js.array [| title; subtitle |] ;
 *       if Dom_html.window##screen##width <= 600 then
 *         begin
 *           pie##labelsEnabled <- false;
 *           legend##valueText <- Js.string "[[percents]]%";
 *           pie##legend <- legend
 *         end;
 *
 *       pie##export <- export ;
 *       pie##write (Js.string tops_accounts_chart_id);
 *       let div = Common.get_div_by_id tops_accounts_chart_id in
 *       div##style##width <- Js.string "100%";
 *       if Dom_html.window##screen##width <= 600 then
 *         div##style##height <- Js.string "800px"
 *       else
 *         div##style##height <- Js.string "600px"
 *     ) *)

let update nrows kind level total xhr_request =
  let container = find_component "top_accounts_table" in
  let title_str = top_kind_to_title kind in
  Manip.removeChildren container ;
  begin match level with
    | None -> ()
    | Some level ->
      let date =
        div ~a:[ a_class [ "alert"; "alert-info" ] ] [
          strong [ txt "Data collected at " ] ;
          Common.make_link @@ string_of_int level.lvl_level
        ] in
      Manip.appendChild container date
  end;
  let module Top =
    Panel.MakePageTable(
    struct
      let name = "top_accounts"
      let theads () = tr [
          th [ Lang.txt_t s_account ];
          th [ Lang.txt_t @@ top_kind_to_thname kind ];
          th [ Lang.txt_t s_percent ];
        ]
      let title_span = Panel.title_nb title_str
      let table_class = "default-table"
      let page_size = 20
    end) in
  let content = Top.make ~footer:true () in
  Manip.appendChild container content ;

  let printer = top_kind_to_printer kind in

  let to_rows accs =
    List.map (fun (hash, value) ->
        tr [
          Common.account_w_blockies hash;
          td [ printer value ];
          td [ txt @@ spf "%.2f %%" (
              (Int64.to_float value) /. (Int64.to_float total) *. 100. )]
      ]) accs.top_list
  in
  Top.paginate_fun to_rows ~nrows xhr_request

let update_cmd update =
  let live_options = [
    option ~a:[ a_selected () ] (txt "Balances");
    option (txt "Frozen balances" );
    option (txt "Frozen deposits" );
    option (txt "Frozen rewards" );
    option (txt "Frozen fees" );
    option (txt "Total balances" )
  ] in
  let context_options = [
    option (txt "Paid bytes" );
    option (txt "Staking balances" );
    option (txt "Total delegated" );
    option (txt "Total delegators" );
    option (txt "Used bytes" )
  ] in
  let optg_live = optgroup ~label:"Real Time" [] in
  let optg_context = optgroup ~label:"Context Data" [] in
  let id_container = tops_cmd_id in
  let container = find_component id_container in
  let n = 11 in
  let options = optg_live :: live_options in
  let options = match Infos.net with
    | Infos.Mainnet -> options @ ( optg_context :: context_options )
    | _ -> options in
  let select_elt =
    select ~a:[ a_class [form_control] ] options in
  Manip.Ev.onchange_select select_elt (fun _e ->
      let select_eltjs = To_dom.of_select select_elt in
      let opt = select_eltjs##.options##item (select_eltjs##.selectedIndex) in
      let selection =
        Js.Opt.case opt
          (fun () -> "")
          (fun opt ->
             try Js.to_string opt##.value
             with _ -> "") in
      update selection;
      true);
  let content = form ~a:[ a_class [ form_inline ] ] [
      div ~a:[ a_class [ form_group ] ] [
        label [ txt "Kind :"; Ezjs_bs3.Icon.space_icon () ] ;
        select_elt ] ] in
  Manip.removeChildren container ;
  Manip.appendChild container content ;
  if n <> 0 then update "Balances"

let make_page () =
  div [
    div
      ~a:[ a_id tops_cmd_id;
           a_class [ clgoffset10; clg2; cxsoffset8; cxs4; "tops-cmd" ] ] [] ;
    div ~a:[ a_id tops_accounts_chart_id; a_class [ cxs12 ] ] [] ;
    div ~a:[ a_id "top_accounts_table"; a_class [ cxs12 ] ] []
  ]
