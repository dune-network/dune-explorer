open Ezjs_tyxml.Html
open Ezjs_bs3.Icon
open Ezjs_bs3.Color

open Data_types
open Lang
open Text

type col = Html_types.td elt

type 'a cols_transaction =  {
  td_tx_block_hash : col ;
  td_tx_op_hash : col ;
  td_tx_src : col ;
  td_tx_dst : col ;
  td_tx_amount : col ;
  td_tx_counter : col ;
  td_tx_fee : col ;
  td_tx_gas_limit : col ;
  td_tx_storage_limit : col ;
  td_tx_parameters : col ;
  td_tx_arrow : col ;
  td_tx_internal : col ;
  td_tx_burn : col ;
  td_tx_op_level : col ;
  td_tx_timestamp : col ;
  tr_tx_class : 'a attrib list
}

type 'a cols_origination = {
  td_or_block_hash : col ;
  td_or_op_hash : col ;
  td_or_src : col ;
  td_or_manager : col ;
  td_or_delegate : col ;
  (* td_or_script : col ; *)
  td_or_spendable : col ;
  td_or_delegatable : col ;
  td_or_balance : col ;
  td_or_counter : col ;
  td_or_fee : col ;
  td_or_gas_limit : col ;
  td_or_storage_limit : col ;
  td_or_kt1 : col ;
  td_or_failed : col ;
  td_or_internal : col ;
  td_or_burn : col ;
  td_or_op_level : col ;
  td_or_timestamp : col;
  td_or_code_link : col;
  tr_or_class : 'a attrib list ;
}

type 'a cols_delegation = {
  td_del_block_hash : col ;
  td_del_op_hash : col ;
  td_del_src : col ;
  td_del_delegate : col ;
  td_del_counter : col ;
  td_del_fee : col ;
  td_del_gas_limit : col ;
  td_del_storage_limit : col ;
  td_del_failed : col ;
  td_del_internal : col ;
  td_del_op_level : col ;
  td_del_timestamp : col ;
  td_del_arrow : col ;
  tr_del_class : 'a attrib list ;
}

type 'a cols_reveal = {
  td_rvl_block_hash : col ;
  td_rvl_op_hash : col ;
  td_rvl_src : col ;
  td_rvl_pubkey : col ;
  td_rvl_counter : col ;
  td_rvl_fee : col ;
  td_rvl_gas_limit : col ;
  td_rvl_storage_limit : col ;
  td_rvl_failed : col ;
  td_rvl_internal : col ;
  td_rvl_op_level : col ;
  td_rvl_timestamp : col ;
  tr_rvl_class : 'a attrib list ;
}

type 'a cols_manage_account = {
  td_mac_block_hash : col ;
  td_mac_op_hash : col ;
  td_mac_src : col ;
  td_mac_counter : col ;
  td_mac_fee : col ;
  td_mac_gas_limit : col ;
  td_mac_storage_limit : col ;
  td_mac_failed : col ;
  td_mac_internal : col ;
  td_mac_op_level : col ;
  td_mac_timestamp : col ;
  td_mac_target : col ;
  td_mac_maxrolls : col ;
  td_mac_admin : col ;
  td_mac_white_list : col ;
  td_mac_delegation : col ;
  td_mac_recovery : col ;
  td_mac_actions : col ;
  tr_mac_class : 'a attrib list ;
}

type cols_seed_nonce_revelation = {
  td_seed_block_hash : col ;
  td_seed_op_hash : col ;
  td_seed_level : col ;
  td_seed_nonce : col ;
}

type cols_activation = {
  td_act_block_hash : col ;
  td_act_op_hash : col ;
  td_act_pkh : col ;
  td_act_secret : col ;
  td_act_op_level : col ;
  td_act_timestamp : col ;
}

type cols_endorsement = {
  td_endorse_op_hash : col ;
  td_endorse_src : col ;
  td_endorse_block_hash : col ;
  td_endorse_block_level : col;
  td_endorse_slot : col ;
  td_endorse_op_level : col ;
  td_endorse_priority : col ;
  td_endorse_timestamp : col ;
}

type cols_token_operation =  {
  td_tf_block_hash : col ;
  td_tf_op_hash : col ;
  td_tf_src : col ;
  td_tf_dst : col ;
  td_tf_amount : col ;
  td_tf_op_level : col ;
  td_tf_timestamp : col ;
  td_tf_kind : col;
}

type cols_kyc =  {
  td_kyc_block_hash : col ;
  td_kyc_op_hash : col ;
  td_kyc_caller : col ;
  td_kyc_param : col ;
  td_kyc_op_level : col ;
  td_kyc_timestamp : col ;
}


let crop_len_dn = 20
let crop_len_hash = 15

let from_string ?(link=false) ?crop_len ?crop_limit ?args ?aclass ?path s =
  if link then
    td [ Common.make_link ?crop_len ?crop_limit ?args ?aclass ?path s ]
  else
    td [ txt_s s ]

(* TODO faire un truc spécifique ? *)
let from_opt ?(def=Common.txt_) f = Option.fold ~none:(td [def ()]) ~some:f

let from_timestamp t =
  td [ Format_date.auto_updating_timespan t ]

let from_int ?(none=false) i =
  td [ if none then Common.txt_ () else txt @@ string_of_int i ]

let from_int32 ?(none=false) i32 =
  td [ if none then Common.txt_ () else txt @@ Int32.to_string i32 ]

let from_int64 ?(none=false) i64 =
  td [ if none then Common.txt_ () else txt @@ Int64.to_string i64 ]

let from_z ?(none=false) z =
  td [ if none then Common.txt_ () else txt @@ Z.to_string z ]

let from_bool cond =
  from_string @@ string_of_bool cond

let from_amount ?(none=false) ?(attr=[]) ?precision ?width ?order ?icon i64 =
  td ~a:attr [ if none then Common.txt_ () else Dun.pp_amount ?precision ?width ?order ?icon i64 ]

let from_amount_z ?(none=false) ?(attr=[]) ?decimals ?precision ?width ?order ?icon z =
  td ~a:attr [ if none then Common.txt_ () else Dun.pp_amount_z ?decimals ?precision ?width ?order ?icon z ]

(* Blockies *)
let from_dn1 ?scale ?aclass ?before ?after ?crop_len ?crop_limit ?args account =
  Common.account_w_blockies
    ?scale ?aclass ?before ?after ?crop_len ?crop_limit ?args account

let from_michelson_parameter =
  function
  | None -> from_string "No"
  | Some param ->
    let template =
      Printf.sprintf "<div class=\"%s\">%s</div>"
        (String.concat "\" \"" [ ])
        (Common.html_escaped param) in
    td [
      a ~a:[ Ezjs_bs3.Attributes.a_data_toggle "popover";
             Ezjs_bs3.Attributes.a_data_placement `Top;
             Ezjs_bs3.Attributes.a_data_html true;
             Ezjs_bs3.Attributes.a_data_trigger "focus";
             Ezjs_bs3.Attributes.a_role "button";
             a_tabindex 0;
             to_attrib @@ Xml.string_attrib "container" "body" ;
             Ezjs_bs3.Attributes.a_data_content template;
           ] [
        txt_t s_yes ]
    ]

let is_internal internal = if not internal then [] else [ "warning" ]

let td_failed cond =
  if cond then
    td ~a:[ a_title "Fail" ; a_class [ red ] ] [ cross_icon () ]
  else
    td ~a:[ a_title "Success" ; a_class [ green ] ] [ right_icon () ]

let td_fee ?attr ?icon internal fee =
  if internal then
    td [ Common.txt_ () ]
  else
    from_amount ?attr ~precision:2 ?icon fee

let td_internal internal =
  from_string @@ if internal then "Yes" else "No"

let td_block_hash bhash level =
  if bhash = Utils.pending_block_hash then td [ txt_t s_pending ]
  else
    td [ Common.make_link ~path:bhash @@ string_of_int level]

let tr_class ?danger_title ?warning_title ?(danger=false) ?(warning=false) () =
  if danger then
    a_class [ "danger" ] :: (match danger_title with None -> [] | Some t -> [ a_title t ])
  else if warning then
    a_class [ "warning" ] :: (match warning_title with None -> [] | Some t -> [ a_title t ])
  else []

(* Generates rows for all table *)
let from_transaction
    ?(crop_len_dn=crop_len_dn) ?(crop_len_hash=crop_len_hash) ?account
    block_hash op_hash transaction =
  let td_tx_block_hash =
    td_block_hash block_hash transaction.tr_op_level in
  let td_tx_op_hash =
    from_string
      ~args:["block_hash", block_hash]
      ~link:true
      ~crop_len:crop_len_hash op_hash in
  let td_tx_src = from_dn1 ~crop_len:crop_len_dn transaction.tr_src in
  let td_tx_arrow =
    if transaction.tr_failed then td_failed true
    else if transaction.tr_collect_fee_gas <> None then
      td ~a:[ a_title "Collect Call" ; a_class [ green ] ] [ fas "arrow-circle-right" ]
    else if transaction.tr_src = transaction.tr_dst then
      td ~a:[ a_class [ green ] ] [ right_left_arrow_icon () ]
    else if account = Some transaction.tr_src.pkh then
      td ~a:[ a_class [ red ] ] [ right_icon () ]
    else
      td_failed false in
  let collect_color =
    if transaction.tr_collect_fee_gas = None then []
    else [ a_class [blue] ] in
  let td_tx_dst = from_dn1 ~crop_len:crop_len_dn transaction.tr_dst in
  let td_tx_amount = from_amount transaction.tr_amount in
  let td_tx_counter = from_int32 ~none:transaction.tr_internal transaction.tr_counter in
  let td_tx_fee = td_fee ~attr:collect_color transaction.tr_internal transaction.tr_fee in
  let td_tx_gas_limit = from_z ~none:transaction.tr_internal transaction.tr_gas_limit in
  let td_tx_storage_limit = from_z ~none:transaction.tr_internal transaction.tr_storage_limit in
  let td_tx_internal = td_internal transaction.tr_internal in
  let td_tx_burn = from_amount ~none:transaction.tr_internal ~attr:collect_color transaction.tr_burn in
  let td_tx_parameters = from_michelson_parameter transaction.tr_parameters in
  let td_tx_op_level = from_int transaction.tr_op_level in
  let td_tx_timestamp = from_timestamp transaction.tr_timestamp in
  let tr_tx_class = tr_class
      ~warning_title:(t_ s_include_internal_operations)
      ~danger:transaction.tr_failed
      ~warning:transaction.tr_internal () in
  {
    td_tx_block_hash ;
    td_tx_op_hash ;
    td_tx_src ;
    td_tx_dst ;
    td_tx_amount ;
    td_tx_counter ;
    td_tx_fee ;
    td_tx_gas_limit ;
    td_tx_storage_limit ;
    td_tx_parameters ;
    td_tx_arrow ;
    td_tx_internal ;
    td_tx_burn ;
    td_tx_op_level ;
    td_tx_timestamp ;
    tr_tx_class
  }

let from_origination
    ?(crop_len_dn=crop_len_dn) ?(crop_len_hash=crop_len_hash)
    block_hash op_hash source orig =
  let td_or_block_hash = td_block_hash block_hash orig.or_op_level in
  let td_or_op_hash =
    from_string ~link:true ~crop_len:crop_len_hash op_hash in
  let td_or_kt1 = from_dn1 ~crop_len:crop_len_dn orig.or_kt1 in
  let td_or_balance = from_amount orig.or_balance in
  let td_or_src = from_dn1 ~crop_len:crop_len_dn source in
  let td_or_manager = from_dn1 ~crop_len:crop_len_dn orig.or_manager in
  let td_or_delegate =
    if orig.or_delegate.pkh <> "" then
      from_dn1 ~crop_len:crop_len_dn orig.or_delegate
    else
      from_string "No delegate" in
  let td_or_fee = td_fee orig.or_internal orig.or_fee in
  let td_or_internal = td_internal orig.or_internal in
  let td_or_burn = from_amount ~none:orig.or_internal orig.or_burn in
  let td_or_gas_limit = from_z ~none:orig.or_internal orig.or_gas_limit in
  let td_or_storage_limit = from_z ~none:orig.or_internal orig.or_storage_limit in
  let td_or_failed = td_failed orig.or_failed in
  let td_or_counter = from_int32 ~none:orig.or_internal orig.or_counter in
  let td_or_spendable = from_bool orig.or_spendable in
  let td_or_delegatable = from_bool orig.or_delegatable in
  let td_or_op_level = from_int orig.or_op_level in
  let td_or_timestamp = from_timestamp orig.or_timestamp in
  let tr_or_class = tr_class ~danger:orig.or_failed ~warning:orig.or_internal
      ~warning_title:(t_ s_include_internal_operations) () in
  let td_or_code_link = match orig.or_script with
    | Some { sc_code ; sc_storage;_ } when (sc_code <> None || sc_storage <> "{}") ->
      td [ Common.make_link (t_ s_yes) ~path:orig.or_kt1.pkh ]
    | _ -> td [ txt_t s_no ] in
  {
    td_or_block_hash ;
    td_or_op_hash ;
    td_or_src ;
    td_or_manager ;
    td_or_delegate ;
    td_or_spendable ;
    td_or_delegatable ;
    td_or_balance ;
    td_or_counter ;
    td_or_fee ;
    td_or_gas_limit ;
    td_or_storage_limit ;
    td_or_kt1 ;
    td_or_failed ;
    td_or_internal ;
    td_or_burn ;
    td_or_op_level ;
    td_or_timestamp ;
    td_or_code_link ;
    tr_or_class ;
  }

let from_delegation
    ?(crop_len_dn=crop_len_dn) ?(crop_len_hash=crop_len_hash)
    ?(crop_limit) ?account
    block_hash op_hash source del =
  let td_del_block_hash =
    td_block_hash block_hash del.del_op_level in
  let td_del_op_hash = from_string ~link:true ~crop_len:crop_len_hash op_hash in
  let td_del_internal = td_internal del.del_internal in
  let td_del_delegate =
    if del.del_delegate.pkh = "" then
      from_string "unset"
    else
      from_dn1 ~crop_len:crop_len_dn ?crop_limit del.del_delegate in
  let td_del_src = from_dn1 ~crop_len:crop_len_dn ?crop_limit source in
  let td_del_counter = from_int32 ~none:del.del_internal del.del_counter  in
  let td_del_fee = td_fee del.del_internal del.del_fee in
  let td_del_gas_limit = from_z ~none:del.del_internal del.del_gas_limit in
  let td_del_storage_limit = from_z ~none:del.del_internal del.del_storage_limit in
  let td_del_failed = td_failed del.del_failed in
  let td_del_op_level = from_int del.del_op_level in
  let td_del_timestamp = from_timestamp del.del_timestamp in
  let td_del_arrow =
    if del.del_failed then
      td ~a:[ a_class [ red ] ] [ cross_icon () ]
    else if account = Some source.pkh then
      td ~a:[ a_class [ red ] ] [ right_icon () ]
    else
      td ~a:[ a_class [ green ] ] [ right_icon () ] in
  let tr_del_class = tr_class
      ~warning_title:(t_ s_include_internal_operations)
      ~danger:del.del_failed
      ~warning:del.del_internal () in
  {
    td_del_block_hash ;
    td_del_op_hash ;
    td_del_src ;
    td_del_delegate ;
    td_del_counter ;
    td_del_fee ;
    td_del_gas_limit ;
    td_del_storage_limit ;
    td_del_failed ;
    td_del_internal ;
    td_del_op_level ;
    td_del_timestamp ;
    td_del_arrow ;
    tr_del_class ;
  }

let from_reveal
    ?(crop_len_dn=crop_len_dn) ?(crop_len_hash=crop_len_hash)
    ?(crop_limit)
    block_hash op_hash source rvl =
  let td_rvl_block_hash =
    td_block_hash block_hash rvl.rvl_op_level in
  let td_rvl_op_hash = from_string ~link:true ~crop_len:crop_len_hash op_hash in
  let td_rvl_internal = td_internal rvl.rvl_internal in
  let td_rvl_pubkey = from_dn1 ~crop_len:crop_len_dn ?crop_limit
      {pkh = rvl.rvl_pubkey; alias = None} in
  let td_rvl_src = from_dn1 ~crop_len:crop_len_dn ?crop_limit source in
  let td_rvl_counter = from_int32 ~none:rvl.rvl_internal rvl.rvl_counter  in
  let td_rvl_fee = td_fee rvl.rvl_internal rvl.rvl_fee in
  let td_rvl_gas_limit = from_z ~none:rvl.rvl_internal rvl.rvl_gas_limit in
  let td_rvl_storage_limit = from_z ~none:rvl.rvl_internal rvl.rvl_storage_limit in
  let td_rvl_failed = td_failed rvl.rvl_failed in
  let td_rvl_op_level = from_int rvl.rvl_op_level in
  let td_rvl_timestamp = from_timestamp rvl.rvl_timestamp in
  let tr_rvl_class = tr_class
      ~warning_title:(t_ s_include_internal_operations)
      ~danger:rvl.rvl_failed
      ~warning:rvl.rvl_internal () in
  {
    td_rvl_block_hash ;
    td_rvl_op_hash ;
    td_rvl_src ;
    td_rvl_pubkey ;
    td_rvl_counter ;
    td_rvl_fee ;
    td_rvl_gas_limit ;
    td_rvl_storage_limit ;
    td_rvl_failed ;
    td_rvl_internal ;
    td_rvl_op_level ;
    td_rvl_timestamp ;
    tr_rvl_class ;
  }

let from_manage_account
    ?(crop_len_dn=crop_len_dn) ?(crop_len_hash=crop_len_hash)
    ?(crop_limit)
    block_hash op_hash source mac =
  let td_mac_block_hash =
    td_block_hash block_hash mac.mac_op_level in
  let td_mac_op_hash = from_string ~link:true ~crop_len:crop_len_hash op_hash in
  let td_mac_internal = td_internal mac.mac_internal in
  let td_mac_src = from_dn1 ~crop_len:crop_len_dn ?crop_limit source in
  let td_mac_counter = from_int32 ~none:mac.mac_internal mac.mac_counter  in
  let td_mac_fee = td_fee mac.mac_internal mac.mac_fee in
  let td_mac_gas_limit = from_z ~none:mac.mac_internal mac.mac_gas_limit in
  let td_mac_storage_limit = from_z ~none:mac.mac_internal mac.mac_storage_limit in
  let td_mac_failed = td_failed mac.mac_failed in
  let td_mac_op_level = from_int mac.mac_op_level in
  let td_mac_timestamp = from_timestamp mac.mac_timestamp in
  let td_mac_target = from_opt
      (fun x -> from_dn1 ~crop_len:10 (fst x)) mac.mac_target in
  let td_mac_maxrolls = from_opt (from_opt ~def:(fun () -> txt "unset") from_int)
      mac.mac_maxrolls in
  let td_mac_admin =
    from_opt (from_opt ~def:(fun () -> txt "unset") (from_dn1 ~crop_len:8))
      mac.mac_admin in
  let td_mac_white_list = from_opt (function
      | [] -> td [ txt "unset" ]
      | l -> let s = String.concat ", " @@ List.map (fun {pkh; _} -> pkh) l in
        td ~a:[ a_title s] [ txt @@ Common.crop_hash ~crop_len:10 s ])
      mac.mac_white_list in
  let td_mac_delegation = from_opt
      (fun b -> if b then td [txt "open"] else td [txt "closed"])
      mac.mac_delegation in
  let td_mac_recovery = from_opt
      (from_opt ~def:(fun () -> txt "unset") (from_dn1 ~crop_len:crop_len_dn))
      mac.mac_recovery in
  let td_mac_actions = from_opt (fun l ->
      let names = String.concat ", " (List.map fst l) in
      let all = String.concat ", " (List.map (fun (name, arg) -> name ^ ": " ^ arg) l) in
      td ~a:[ a_title all ] [ txt names ]) mac.mac_actions in
  let tr_mac_class = tr_class
      ~warning_title:(t_ s_include_internal_operations)
      ~danger:mac.mac_failed
      ~warning:mac.mac_internal () in
  {
    td_mac_block_hash ;
    td_mac_op_hash ;
    td_mac_src ;
    td_mac_counter ;
    td_mac_fee ;
    td_mac_gas_limit ;
    td_mac_storage_limit ;
    td_mac_failed ;
    td_mac_internal ;
    td_mac_op_level ;
    td_mac_timestamp ;
    td_mac_target ;
    td_mac_maxrolls ;
    td_mac_admin ;
    td_mac_white_list ;
    td_mac_delegation ;
    td_mac_recovery ;
    td_mac_actions ;
    tr_mac_class ;
  }

let from_seed_nonce_revelation block_hash op_hash nonce =
  let td_seed_block_hash =
    td_block_hash block_hash nonce.seed_nonce_op_level in
  let td_seed_op_hash = from_string ~link:true op_hash in
  let td_seed_level = from_int nonce.seed_level in
  let td_seed_nonce = from_string nonce.seed_nonce in
  {
    td_seed_block_hash ;
    td_seed_op_hash ;
    td_seed_level ;
    td_seed_nonce ;
  }

let from_activation
    ?(crop_len_hash=crop_len_hash)
    block_hash op_hash act =
  let td_act_block_hash = td_block_hash block_hash act.act_op_level in
  let td_act_op_hash = from_string ~link:true ~crop_len:crop_len_hash op_hash in
  let td_act_pkh = from_dn1 act.act_pkh in
  let td_act_secret = from_string act.act_secret in
  let td_act_op_level = from_int act.act_op_level in
  let td_act_timestamp = from_timestamp act.act_timestamp in
  {
    td_act_block_hash ;
    td_act_op_hash ;
    td_act_pkh ;
    td_act_secret ;
    td_act_op_level ;
    td_act_timestamp ;
  }

let from_endorsement
    ?(crop_len_dn=crop_len_dn) ?(crop_len_hash=crop_len_hash)
    ?(crop_limit)
    block_hash op_hash endorse =
  let td_endorse_op_hash =
    from_string ~args:["block_hash", block_hash]
      ~link:true ~crop_len:crop_len_hash op_hash in
  let td_endorse_block_level = from_int endorse.endorse_block_level in
  let td_endorse_src =
    from_dn1 ?crop_limit ~crop_len:crop_len_dn endorse.endorse_src in
  let td_endorse_slot =
    td [ txt @@
         String.concat ", " @@
         List.map string_of_int endorse.endorse_slot ] in
  let td_endorse_block_hash =
    from_string
      ~link:true
      ~crop_len:crop_len_hash
      endorse.endorse_block_hash in
  let td_endorse_op_level = from_int endorse.endorse_op_level in
  let td_endorse_priority = from_int endorse.endorse_priority in
  let td_endorse_timestamp = from_timestamp endorse.endorse_timestamp in
  {
    td_endorse_op_hash ;
    td_endorse_src ;
    td_endorse_block_hash ;
    td_endorse_block_level ;
    td_endorse_slot ;
    td_endorse_op_level ;
    td_endorse_priority ;
    td_endorse_timestamp ;
  }

let from_token_operation ~symbol ?decimals
    ?(crop_len_dn=crop_len_dn) ?(crop_len_hash=crop_len_hash)
    block_hash op_hash op_level op_tsp tk =
  let icon () = span ~a:[ a_class ["dn"] ] [ txt symbol ] in
  let td_tf_block_hash = td_block_hash block_hash op_level in
  let td_tf_op_hash =
    from_string
      ~args:["block_hash", block_hash]
      ~link:true
      ~crop_len:crop_len_hash op_hash in
  let td_tf_src = match tk.ts_kind with
    | TS_Mint -> td [ Common.txt_ () ]
    | _ -> from_dn1 ~crop_len:crop_len_dn tk.ts_src in
  let td_tf_dst = match tk.ts_dst with
    | None -> td [ Common.txt_ () ]
    | Some dst -> from_dn1 ~crop_len:crop_len_dn dst in
  let td_tf_amount = from_amount_z ~icon ?decimals tk.ts_amount in
  let td_tf_op_level = from_int op_level in
  let td_tf_timestamp = from_timestamp op_tsp in
  let td_tf_kind = match tk.ts_kind with
    | TS_Burn -> td [ txt "burn" ]
    | TS_Transfer -> td [ txt "transfer" ]
    | TS_Mint -> td [ txt "mint" ]
    | TS_Unknown -> td [ Common.txt_ () ] in {
    td_tf_block_hash ;
    td_tf_op_hash ;
    td_tf_src ;
    td_tf_dst ;
    td_tf_amount ;
    td_tf_op_level ;
    td_tf_timestamp ;
    td_tf_kind
  }
