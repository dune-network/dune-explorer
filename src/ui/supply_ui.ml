open Data_types
open Ezjs_tyxml
open Html
open Ezjs_bs3.Grid

let supply_info_id = "supply-panel"

let make ?level () =
  let title = match level with
    | None -> "Supply Repartition"
    | Some level -> "Supply Repartition at Level " ^ string_of_int level in
  Ezjs_bs3.Panel.make_panel
    ~panel_title_content:(span [txt title])
    ~panel_class:["block-div"]
    ~panel_body_id:supply_info_id
    ()

let line label value =
  div ~a:[ a_class [row] ] [
    div ~a:[ a_class [csm6; clg4; clgoffset2; "text-left"; "lbl"] ] [ txt label ];
    div ~a:[ a_class [csm6; clg4; "text-right"; "value"] ] [ Dun.pp_amount ~order:2 value ] ]

let update supply =
  let panel = find_component supply_info_id in
  let subtitle_class = [ "constants-subtitle" ] in
  let content = [
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Origin" ];
      line "Dune Foundation" supply.foundation;
      line "Investors" supply.early_bakers;
      line "Tezos Holders" supply.contributors;
      line "Validator Program" supply.validator_program;
      line "Total" supply.total_supply_ico;
    ];
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Rewards" ];
      line "Baking/Endorsing Rewards" supply.unfrozen_rewards;
      line "Seed Nonce Revelation Rewards" supply.revelation_rewards;
    ];
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Burns" ];
      line "Lost Seed Nonce Revelation" supply.burned_dun_revelation;
      line "Transaction Burn" supply.burned_dun_transaction;
      line "Origination Burn" supply.burned_dun_origination;
      line "Double Baking Burn" supply.burned_dun_double_baking;
      line "Double Endorsement Burn" supply.burned_dun_double_endorsement;
      line "Protocol Burns" supply.protocol_burn
    ];
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Locked" ];
      line "Locked Inverstors" supply.locked_dun;
      line "Validator Program" supply.validator_program;
    ];
    div ~a:[ a_class [cxs12] ] [
      h3 ~a:[ a_class subtitle_class ] [ txt "Totals" ];
      line "Total Supply" (Int64.add supply.current_circulating_supply supply.validator_program);
      line "Circulating Supply" supply.current_circulating_supply;
    ]
  ] in
  Manip.replaceChildren panel content
