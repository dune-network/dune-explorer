(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Ezjs_bs3.Grid
open Ezjs_bs3.Panel
open Ezjs_bs3.Table
open Ezjs_bs3.Color
open Data_types
open Lang
open Text

type filter = Op_Txs | Op_Del | Op_Ori | Op_Rvl

let string_of_filter = function
    Op_Txs -> "transaction"
  | Op_Del -> "delegation"
  | Op_Ori -> "origination"
  | Op_Rvl -> "reveal"

let filter_of_string = function
    "transaction" -> Op_Txs
  | "delegation" -> Op_Del
  | "origination" -> Op_Ori
  | "reveal" -> Op_Rvl
  | _ -> Op_Txs

let default_filter filters =
  match List.assoc_opt "default" filters with
  | Some filter -> filter_of_string filter
  | _ -> Op_Txs

let is_active default exp =
  if default = exp then Tabs.Active else Tabs.Inactive


let mk_title icon title nb =
  span ( Common.responsive_title_fun icon
           (Panel.title_nb title)
           (match nb with Some nb -> nb | None -> -1)
       )

let tr_tab =
  Tabs.make
    (mk_title Ezjs_bs3.Icon.exchange_icon s_transactions) [ "account-tab" ]

let del_tab =
  Tabs.make
    (mk_title Ezjs_bs3.Icon.handshake_icon s_delegations) [ "account-tab" ]

let ori_tab =
  Tabs.make
    (mk_title Ezjs_bs3.Icon.link_icon s_originations) [ "account-tab" ]

let rvl_tab =
  Tabs.make
    (mk_title Ezjs_bs3.Icon.check_icon s_reveals) [ "account-tab" ]

let tabs = [
  tr_tab, Op_Txs;
  del_tab, Op_Del;
  ori_tab, Op_Ori;
  rvl_tab, Op_Rvl
]

let make_op_tabs default =
  List.map (fun (tr, filter) ->
      tr, is_active default filter) tabs |>
  Tabs.(make_tabs Tabs)

let on_show filter tab =
  Tabs.set_on_show tab (fun _ ->
      Common.set_url_arg "default" ~default:(string_of_filter Op_Txs)
      @@ string_of_filter filter
    )

let update_tabs () =
  List.iter (fun (t, filter) ->
      on_show filter t
    ) tabs

let operation_id hash = Common.make_id "operation-summary" hash
let timestamp_id hash = Common.make_id "operation-timestamp" hash
let slot i = Common.make_id "slot" (string_of_int i)

let make_sub_operation_div title content =
  div ~a:[ a_class [ "sub-operation-div"; row ] ] [
    div ~a:[ a_class [ cxs12 ] ] [
      div ~a:[ a_class [ panel; panel_primary ]] [
        div ~a:[ a_class [ panel_heading ] ] [
          h3 ~a:[ a_class [ panel_title ] ] [ txt title ]
        ] ;
        div ~a:[ a_class [ panel_body ] ] content
      ] ;
    ]
  ]

let label list = div ~a:[ a_class [ clg2; cxs12; "lbl" ] ] list
let label_t s = label [ txt_t s ]
let value ?(classes=[]) ?id list =
    let a = [ a_class ([ clg10; cxs12 ; "value" ] @ classes ) ] in
    let a = match id with
        None -> a
      | Some id -> a_id id :: a
    in
    div ~a list
let value_txt ?classes ?id s = value ?classes ?id [ txt s ]
let value_s ?classes ?id s = value ?classes ?id [ txt_t s ]


let make_seed_nonce_revelation_details index seed =
  let row_level_label = label_t s_level in
  let row_level_value = value [ Common.make_link (string_of_int seed.seed_level) ] in
  let row_nonce_label = label_t s_nonce in
  let row_nonce_value = value_txt seed.seed_nonce  in

  let title = Printf.sprintf "%i: %s" index (t_ s_seed_nonce_revelation) in
  let content =
    [ row_level_label ; row_level_value ;
      row_nonce_label ; row_nonce_value ] in
  make_sub_operation_div title content

let make_activation_details index act =

  let row_dn1_label = label_t s_dn1 in
  let row_dn1_value = value [ Common.make_link_account act.act_pkh ] in

  let row_secret_label = label_t s_secret in
  let row_secret_value = value_txt act.act_secret in

  let title = Printf.sprintf "%i: %s" index (t_ s_activation) in
  let content =
    [ row_dn1_label ; row_dn1_value ;
      row_secret_label ; row_secret_value ] in
  make_sub_operation_div title content

let make_endorsement_details ?endorse_info endorsement =
  let ops, lvl =
    match endorse_info with
    | None -> assert false
    | Some (ops, lvl) -> (ops,lvl)
  in
  let cst = Infos.constants ~level:lvl.lvl_level in
  let row_src_label = label_t s_endorser in
  let row_src_value = value
      [ Common.make_link_account endorsement.endorse_src ] in

  let row_hash_label = label_t s_endorsed_block in
  let row_hash_value = value
      [ Common.make_link endorsement.endorse_block_hash ] in

  let row_blevel_label = label_t s_endorsed_level in
  let row_blevel_value = value
    [ Common.make_link
        ~path:(string_of_int endorsement.endorse_block_level)
        (string_of_int endorsement.endorse_block_level) ] in

  let slots = Common.make_slots cst.Dune_types.endorsers_per_block in
  let slots = Common.update_endorsements_slots endorsement.endorse_block_hash slots ops in
  List.iter (fun s ->
      Manip.addClass (List.nth slots s) "bg-greener") endorsement.endorse_slot  ;
  let rows = Common.make_endorsements_slots slots in
  [ row_src_label ; row_src_value ;
    row_hash_label ; row_hash_value ;
    row_blevel_label ; row_blevel_value ;
    div ~a:[ a_class [ cxs6; cxsoffset2; "endorse-tbl" ] ] [ rows ] ;
    div ~a:[ a_class [ cxs8; cxsoffset2; "lbl" ] ] [ Common.legend () ] ]

let make_proposals_details src prop =
  let row_src_label = label_t s_source in
  let row_src_value = value [ Common.make_link_account src ] in

  let row_voting_label = label_t s_voting_period in
  let row_voting_value = value_txt
    @@ string_of_int @@ Int32.to_int prop.prop_voting_period  in

  [ div ~a:[ a_class [ "operation-div"; row ] ] [
        div ~a:[ a_class [ cxs12 ] ] [
          div ~a:[  a_class [ panel; panel_primary ]] [
            div ~a:[ a_class [ panel_body ] ]
              [ row_src_label ; row_src_value ;
                row_voting_label ; row_voting_value ]
          ]
        ]
      ]
  ]

let make_proposals_details_table prop =
  let trs = List.map (fun prop_hash ->
      tr [
        td [ txt prop_hash ];
      ])
      prop.prop_proposals in
  tablex ~a:[ a_class [ btable; "default-table" ] ] [ tbody (List.rev trs) ]

let make_ballot_details src ballot =
  let row_src_label = label_t s_source in
  let row_src_value = value [ Common.make_link_account src ] in
  let row_voting_label = label_t s_voting_period in
  let row_voting_value = value_txt
    @@ Int32.to_string ballot.ballot_voting_period  in
  let row_hash_label = label_t s_protocol_hash  in
  let row_hash_value = value_txt ballot.ballot_proposal in
  let row_vote_label = label_t s_vote in
  let row_vote_value = value_txt
    @@ Utils.string_of_ballot_vote ballot.ballot_vote in
  [ div ~a:[ a_class [ "operation-div"; row ] ] [
        div ~a:[ a_class [ cxs12 ] ] [
          div ~a:[  a_class [ panel; panel_primary ]] [
            div ~a:[ a_class [ panel_body ] ]
              [ row_src_label ; row_src_value ;
                row_voting_label ; row_voting_value;
                row_hash_label ; row_hash_value;
                row_vote_label ; row_vote_value ]
          ]
        ]
      ]
  ]

let triplet ?(mc=42) ?fals lbl v1 v2 =
  let fals = match fals with
    | None -> v1 <> v2
    | Some fals -> fals in
  let cls1, cls2 =
    if mc = 0 && fals then ["success"], ["danger"]
    else if mc = 1 && fals then ["danger"], ["success"]
    else if mc = -1 && fals then ["danger"], ["danger"]
    else [],[] in
  if v1 = "" && v2 = "" then [] else
    [div ~a:[ a_class [csm2; clg2; cxs12; "lbl" ] ] [ txt_t lbl ];
     div ~a:[ a_class ([clg4; cxs12; "value"] @ cls1) ] [ txt v1 ];
     div ~a:[ a_class ([clg4; cxs12; cxsoffset0; csmoffset1; "value" ] @ cls2) ] [ txt v2 ]]

let triplet_int ?(mc=42) lbl i1 i2 =
  triplet ~mc ?fals:(Some (i1<>i2)) lbl (string_of_int i1) (string_of_int i2)

let make_double_baking_evidence_details index dbe =
  let level = string_of_int dbe.double_baking_header1.hd_level in
  let link_str = "heads?level=" ^ level in
  let link = Common.make_link level ~path:link_str in
  let row_dbe_label = label_t s_double_baking_at_level in
  let row_dbe_value = value [ link ] in
  let row_dbe_accused_label = label_t s_offender in
  let row_dbe_accused_value = value [
      Common.make_link_account dbe.double_baking_accused
    ] in
  let row_dbe_denouncer_label = label_t s_baker  in
  let row_dbe_denouncer_value = value [
      Common.make_link_account dbe.double_baking_denouncer
    ] in
  let row_dbe_gain_label = label_t s_baker_rewards  in
  let row_dbe_gain_value = value [
      Dun.pp_amount ~precision:2 dbe.double_baking_gain_rewards
    ] in
  let row_dbe_lost_deposit_label = label_t s_lost_deposit in
  let row_dbe_lost_deposit_value = value [
      Dun.pp_amount ~precision:2 dbe.double_baking_lost_deposit
    ] in
  let row_dbe_lost_rewards_label = label_t s_lost_rewards in
  let row_dbe_lost_rewards_value = value [
      Dun.pp_amount ~precision:2 dbe.double_baking_lost_rewards
    ] in
  let row_dbe_lost_fees_label = label_t s_lost_fees in
  let row_dbe_lost_fees_value = value [
      Dun.pp_amount ~precision:2 dbe.double_baking_lost_fees
    ] in
  let h1 = dbe.double_baking_header1 and h2 = dbe.double_baking_header2 in
  let mc = dbe.double_baking_main in
  let content_diff = List.flatten
      [ triplet_int ~mc s_protocol h1.hd_proto h2.hd_proto;
        triplet ~mc s_predecessor h1.hd_predecessor h2.hd_predecessor;
        triplet ~mc s_timestamp
          (Date.to_string h1.hd_timestamp)
          (Date.to_string h2.hd_timestamp);
        triplet_int ~mc s_validation_pass h1.hd_validation_pass h2.hd_validation_pass;
        triplet ~mc s_hash h1.hd_operations_hash h2.hd_operations_hash;
        triplet ~mc s_fitness h1.hd_fitness h2.hd_fitness;
        triplet ~mc s_context h1.hd_context h2.hd_context;
        triplet_int ~mc s_priority h1.hd_priority h2.hd_priority;
        triplet ~mc s_nonce_hash h1.hd_seed_nonce_hash h2.hd_seed_nonce_hash;
        triplet ~mc s_pow_nonce h1.hd_proof_of_work_nonce h2.hd_proof_of_work_nonce;
        triplet ~mc s_signature h1.hd_signature h2.hd_signature] in
  let title = Printf.sprintf "%i: %s" index (t_ s_double_baking_evidence) in
  let content =
    [ row_dbe_label ; row_dbe_value ;
      row_dbe_accused_label ; row_dbe_accused_value ;
      row_dbe_denouncer_label ; row_dbe_denouncer_value ;
      row_dbe_gain_label ; row_dbe_gain_value ;
      row_dbe_lost_deposit_label; row_dbe_lost_deposit_value ;
      row_dbe_lost_rewards_label; row_dbe_lost_rewards_value ;
      row_dbe_lost_fees_label; row_dbe_lost_fees_value ;

    ] @ content_diff in
  make_sub_operation_div title content

let make_double_endorsement_evidence_details index dee =
  let endorse1 = dee.double_endorsement1 in
  let endorse2 = dee.double_endorsement2 in
  let title =
    Printf.sprintf "%i: %s" index (t_ s_double_endorsement_evidence) in
  let content = List.flatten
      [ triplet s_block_hash endorse1.endorse_block_hash endorse2.endorse_block_hash;
        triplet_int s_level endorse1.endorse_block_level endorse2.endorse_block_level;
        triplet s_slots
          (String.concat ", " @@ List.map string_of_int endorse1.endorse_slot)
          (String.concat ", " @@ List.map string_of_int endorse2.endorse_slot)] in
  make_sub_operation_div title content

let make_manage_account_details index mac =
  let title = Printf.sprintf "%i: %s" index @@ t_ s_manage_account in
  let row_target_label, row_target_value,
      row_signature_label, row_signature_value = match mac.mac_target with
    | None -> None, None, None, None
    | Some (target, Some signature) ->
      Some (label_t s_target), Some (value [ Common.make_link_account target ]),
      Some (label_t s_signature), Some (value_txt signature)
    | Some (target, _) ->
      Some (label_t s_target), Some (value [ Common.make_link_account target ]),
      None, None in
  let row_maxrolls_label, row_maxrolls_value = match mac.mac_maxrolls with
    | None -> None, None
    | Some None -> Some (label_t s_maxrolls), Some (value_txt "unset")
    | Some (Some m) -> Some (label_t s_maxrolls), Some (value_txt @@ string_of_int m) in
  let row_admin_label, row_admin_value = match mac.mac_admin with
    | None -> None, None
    | Some None -> Some (label_t s_admin), Some (value_txt "unset")
    | Some (Some a) -> Some (label_t s_admin), Some (value [ Common.make_link_account a ]) in
  let row_white_list_label, row_white_list_value = match mac.mac_white_list with
    | None -> None, None
    | Some [] -> Some (label_t s_white_list), Some (value_txt "unset")
    | Some l -> Some (label_t s_white_list), Some (value (List.map Common.make_link_account l)) in
  let row_delegation_label, row_delegation_value = match mac.mac_delegation with
    | None -> None, None
    | Some b -> Some (label_t s_delegation), Some (value_txt (if b then "open" else "closed")) in
  let content = List.filter_map (fun x -> x) [
      row_target_label; row_target_value; row_signature_label;
      row_signature_value; row_maxrolls_label; row_maxrolls_value;
      row_admin_label; row_admin_value; row_white_list_label;
      row_white_list_value; row_delegation_label; row_delegation_value
    ] in
  make_sub_operation_div title content

let make_manage_accounts_details index macs =
  let title = Printf.sprintf "%i: %s" index @@ t_ s_manage_accounts in
  let row_bytes_label = label_t s_bytes in
  let row_bytes_value = value_txt @@ Hex.show macs.macs_bytes in
  make_sub_operation_div title [ row_bytes_label; row_bytes_value ]

let make_activate_protocol_details index acp =
  let title = Printf.sprintf "%i: %s" index @@ t_ s_activate_protocol in
  let row_protocol_label, row_protocol_value = match acp.acp_protocol with
    | None -> None, None
    | Some p -> Some (label_t s_protocol), Some (value_txt p) in
  let row_parameters_label, row_parameters_value = match acp.acp_parameters with
    | None -> None, None
    | Some p -> Some (label_t s_parameters), Some (value_txt p) in
  make_sub_operation_div title @@
  List.filter_map (fun x -> x)  [
    row_protocol_label; row_protocol_value; row_parameters_label; row_parameters_value ]

module PanelOptions = struct
  let options = Panels.{
      page_size = Common.big_panel_number;
      no_block = false;
      no_op = true;
      no_limit = false;
      no_internal = false;
      pan_hash = None;
      no_title = true
    }
end

module TransactionsPanel = Panels.GenericPanelList(Panels.TransactionSpec(PanelOptions))
module DelegationsPanel = Panels.GenericPanelList(Panels.DelegationSpec(PanelOptions))
module OriginationsPanel = Panels.GenericPanelList(Panels.OriginationSpec(PanelOptions))
module RevealsPanel = Panels.GenericPanelList(Panels.RevealSpec(PanelOptions))

let make_transactions_tab ?price_usd default tab ops =
  ignore(price_usd);
  Tabs.make_content_panel tab (is_active default Op_Txs) @@
  TransactionsPanel.make ~page_sizer:false ~urlarg_page:"p_tr" ops

let make_delegations_details default tab ops =
  Tabs.make_content_panel tab (is_active default Op_Del) @@
  DelegationsPanel.make ~page_sizer:false ~urlarg_page:"p_del" ops

let make_originations_details ?price_usd default tab ops =
  ignore(price_usd);
  Tabs.make_content_panel tab (is_active default Op_Ori) @@
  OriginationsPanel.make ~page_sizer:false ~urlarg_page:"p_ori" ops

let make_reveals_details default tab ops =
  Tabs.make_content_panel tab (is_active default Op_Rvl) @@
  RevealsPanel.make ~page_sizer:false ~urlarg_page:"p_rvl" ops

let make_manager_details ?price_usd default ops =
  div ~a:[ a_class [ row ] ] [
    div ~a:[ a_class [ cxs12 ] ] [
      make_op_tabs default ;
      div ~a:[ a_class [ "tab-content" ] ] [
        make_transactions_tab ?price_usd default tr_tab ops ;
        make_delegations_details default del_tab ops ;
        make_originations_details ?price_usd default ori_tab ops ;
        make_reveals_details default rvl_tab ops
      ]
    ];
  ],
  (fun () ->
     let transactions_nb, delegations_nb, originations_nb, reveals_nb =
       Utils.count_manager_operations ops in
     Tabs.update_show_tab tr_tab transactions_nb ;
     Tabs.update_show_tab ori_tab originations_nb ;
     Tabs.update_show_tab del_tab delegations_nb ;
     Tabs.update_show_tab rvl_tab reveals_nb;
     List.fold_left2 (fun acc (t, filter) n ->
         match acc with
         | Some f, _ when f = filter -> if n <= 0 then None, Some t else None, None
         | Some f, before -> Some f, before
         | None, Some before ->
           if n > 0 then (Tabs.change_active before t; None, None)
           else None, Some before
         | None, None -> None, None) (Some Op_Txs, None) tabs
       [ transactions_nb; delegations_nb; originations_nb; reveals_nb ] |> ignore
  )

let parse_error_id id =
  match String.index_opt id '.' with
  | None -> id
  | Some i -> match String.index_from_opt id (i+1) '.' with
    | None -> id
    | Some i -> String.sub id (i+1) (String.length id - i - 1)

let make_operation_info_content ?price_usd operation =
  let row_block_label = label [ txt_t s_included_in_block ] in
  let row_block_value = value [ Common.make_link operation.op_block_hash ] in

  let row_hash_label = label [ txt_t s_operation_hash ] in
  let row_hash_value = value [ txt operation.op_hash ] in

  let row_level_label = label [ txt_t s_level ] in
  let row_level_value = value ~id:Common.confirmation_blocks_id
      [ Common.txt_ () ]
  in
  let row_timestamp_label = label [ txt_t s_timestamp ] in
  let row_timestamp_value =
    value [ txt @@ Utils.get_timestamp operation.op_type ] in

  let res = [ row_hash_label ; row_hash_value ;
              row_level_label ; row_level_value ;
              row_timestamp_label ; row_timestamp_value ] in
  let res = match operation.op_type with
    | Sourced sop -> begin match sop with
        | Endorsement _ | Dictator _ | Amendment _ -> res
        | Manager _ | Dune_manager _ ->
          let fees = Utils.compute_fees sop in
          let row_fee_label = label  [ txt_t s_fee ] in
          let row_fee_value = value ( Dun.with_usd price_usd fees ) in
          let errors = Utils.get_errors sop in
          let row_errors =
            if errors = [] then []
            else [
              label [ txt_t s_errors ];
              value [ div ~a:[ a_class ["list-group"]; a_style "margin-bottom:0" ] @@
                      List.concat @@ List.mapi (fun i {err_id; err_info; _} ->
                  let error_id = parse_error_id err_id in
                  match err_info with
                  | "{}" -> [ div ~a:[ a_class [red] ] [ txt error_id ] ]
                  | err_info ->
                    let collapse_id = "error-" ^ string_of_int i in [
                      a ~a:[ a_user_data "toggle" "collapse";
                             a_href ("#" ^ collapse_id) ] [
                        div ~a:[ a_class [red] ] [ txt error_id ] ];
                      div ~a:[ a_id collapse_id; a_class ["collapse"] ] [
                        code [ txt err_info ] ] ]) errors ] ] in
          res @ [ row_fee_label ; row_fee_value ] @ row_errors
      end
    | Anonymous _ | Tokened _ -> res in
  if operation.op_block_hash = Utils.pending_block_hash ||
     operation.op_block_hash = Utils.orphan_block_hash then res
  else row_block_label :: row_block_value :: res

let update_confirmation bhash level nb_confirm =
  let confirm = find_component @@ Common.confirmation_blocks_id in
  Manip.replaceChildren confirm
    [ a ~a:( Common.a_link bhash ) [ txt @@ string_of_int level ];
      txt @@
      Printf.sprintf " (%d %s)" nb_confirm (t_ s_blocks_confirmation)
    ]

let update_operation_summary ?price_usd ?endorse_info operation filters =
  let div_id = operation_id operation.op_hash in
  let container = find_component div_id in
  let default = default_filter filters in
  let header, op_info, update =
    match operation.op_type with
    | Tokened _ -> [], [], (fun () -> ())
    | Anonymous list ->
      [ txt_t s_anonymous_operations; Glossary_doc.(help HTransaction) ],
      List.mapi (fun i aop ->
          let i = i + 1 in
          match aop with
          | Seed_nonce_revelation seed ->
            make_seed_nonce_revelation_details i seed
          | Activation act ->
            make_activation_details i act
          | Double_baking_evidence dbe ->
            make_double_baking_evidence_details i dbe
          | Double_endorsement_evidence dee ->
            make_double_endorsement_evidence_details i dee)
        list,
      (fun () -> ())
    | Sourced sop ->
      begin match sop with
        | Endorsement e ->
          [ txt_t s_endorsement; Glossary_doc.(help HEndorsement) ],
          make_endorsement_details ?endorse_info e,
          (fun () -> ())
        | Amendment (src, aop) ->
          begin match aop with
            | Proposal prop ->
              [ txt_t s_proposals; Glossary_doc.(help HAmendment) ],
              make_proposals_details src prop,
              (fun () -> ())
            | Ballot ballot ->
              [ txt_t s_ballot; Glossary_doc.(help HAmendment) ],
              make_ballot_details src ballot,
              (fun () -> ())
          end
        | Manager _ ->
          let details, update =
            make_manager_details ?price_usd default [ operation ] in
          [ txt_t s_manager_operations; Glossary_doc.(help HTransaction) ],
          [ details ],
          update
        | Dictator dop ->
          begin match dop with
            | Activate ->
              [ txt_t s_activate ],
              [],
              (fun () -> ())
            | Activate_testnet ->
              [ txt_t s_activate_testnet ],
              [],
              (fun () -> ())
          end
        | Dune_manager (_, ops) ->
          [ txt "Dune Manager" ],
          List.mapi (fun i man ->
              let i = i + 1 in match man with
              | Manage_account mac -> make_manage_account_details i mac
              | Manage_accounts macs -> make_manage_accounts_details i macs
              | Activate_protocol acp -> make_activate_protocol_details i acp)
            ops, fun () -> ()
      end
  in
  let content = make_operation_info_content ?price_usd operation in
  let generic =
    div ~a:[ a_class [ "operation-div"; row ] ] [
      div ~a:[ a_class [ cxs12 ] ] [
        div ~a:[ a_class [ panel; panel_primary ]] [
          div ~a:[ a_class [ panel_heading ] ] [
            h3 ~a:[ a_class [ panel_title ] ] header
          ] ;
          div ~a:[ a_class [ panel_body ] ] content
        ] ;
      ]
    ]
  in
  Manip.removeChildren container ;
  Manip.appendChild container generic ;
  List.iter (Manip.appendChild container) op_info;
  update ();
  ignore (Js.Unsafe.eval_string
            "jQuery('[data-toggle=\"popover\"]').popover();")


(* Maker (empty) *)
let make_block_fetching () =  txt_t s_fetching_data

let make_operation_info_panel hash =
  div ~a:[ a_class [ "operation-div"; row ] ; a_id @@ operation_id hash ] [
    div ~a:[ a_class [ cxs12 ] ] [
      div ~a:[ a_class [ panel; panel_primary ]] [
        div ~a:[ a_class [ panel_heading ] ] [
          h3 ~a:[ a_class [ panel_title ] ] [ txt hash ]
        ] ;
        div ~a:[ a_class [ panel_body ] ] [
          make_block_fetching ();
        ]
      ] ;
    ]
  ]

let make_page hash = make_operation_info_panel hash
