open Ezjs_tyxml
open Html
open Ezjs_bs3.Button
open Ezjs_bs3.Form
open Ezjs_bs3.Icon

let make () =
  div ~a:[ a_class [form_inline] ] [
    div ~a:[ a_class [form_group] ] [
      label ~a:[ a_class [Ezjs_bs3.Misc.lead] ] [
        txt "Simple webview app for android: " ];
      space_icon ();
      button ~a:[
        a_button_type `Submit;
        a_onclick (fun _e ->
            Dom_html.window##.location##.href := Js.string ("/dunscan.apk"); true);
        a_class [btn; btn_primary] ] [
        txt "Download" ]
    ]
  ]
