open Ezjs_tyxml
open Html

open Ezjs_bs3.Icon

open Common
open Lang
open Text
open Gen

type panel_options = {
  page_size : int;
  no_block: bool;
  no_op: bool;
  no_limit: bool;
  no_internal: bool;
  mutable pan_hash: string option;
  no_title: bool;
}

let set_panel_hash options hash =
  options.pan_hash <- Some hash

module type PanelSpecifics = sig
  val name : string
  val page_size : int
  val title_span : int -> [> Html_types.span ] elt
  val theads : unit -> [> Html_types.tr ] elt
  val to_rows : Data_types.operation list -> [> Html_types.tr ] elt list
end

module GenericPanel (Arg : PanelSpecifics) = struct
  module Panel = Panel.MakePageTable(struct
      let name = Arg.name
      let title_span = Arg.title_span
      let table_class = "default-table"
      let theads = Arg.theads
      let page_size = Arg.page_size
    end)

  let make ?footer ?suf_id ?panel_class () =
    Panel.make ?footer ?suf_id ?panel_class ()
  let to_rows l = Arg.to_rows l
  let paginate_fun ?page_sizer ?suf_id ?urlarg ?title_span to_rows ~nrows xhr =
    let urlarg_page = Option.map (fun s -> "p_" ^ s) urlarg in
    let urlarg_size = Option.map (fun s -> "r_" ^ s) urlarg in
    Panel.paginate_fun ?page_sizer ?suf_id ?urlarg_page ?urlarg_size ?title_span
      to_rows ~nrows xhr
  let update ?page_sizer ?suf_id ?urlarg ?title_span ~nrows xhr =
    paginate_fun ?page_sizer ?suf_id ?urlarg ?title_span Arg.to_rows ~nrows xhr
end

module GenericPanelList (Arg : PanelSpecifics) = struct
  module Panel = Panel.MakePageTableList(struct
      type data = Data_types.operation
      let name = Arg.name
      let title_span = Arg.title_span
      let table_class = "default-table"
      let theads = Arg.theads
      let page_size = Arg.page_size
    end)

  let make ?page_sizer ?footer ?suf_id ?panel_class ?urlarg_page ?urlarg_size l =
    Panel.make ?page_sizer ?footer ?suf_id ?panel_class ?urlarg_page ?urlarg_size Arg.to_rows l
end


module TransactionSpec(M : sig val options : panel_options end) = struct
  let name = "Transactions"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon exchange_icon (t_ s_txn_hash) ] in
    let th_limit =
      if M.options.no_limit then []
      else [ th [ span [ txt_t s_counter ] ];
             th [ span [ txt_t s_gas_limit ] ];
             th [ span [ txt_t s_storage_limit ] ] ]  in
    let th_internal =
      if M.options.no_internal then []
      else [ th @@ cl_icon folder_icon (t_ s_internal) ] in
    tr ( th_op @ th_block @ [
        th @@ cl_icon account_icon (t_ s_from);
        th ~a:[ a_class [ "arrow" ] ] [ txt "" ] ;
        th @@ cl_icon account_icon (t_ s_to);
        th @@ cl_icon Dun.icon (t_ s_amount);
        th @@ cl_icon bill_icon (t_ s_fee);
        th @@ cl_icon (fas_u "fire") (t_ s_burn);
        th @@ cl_icon params_icon (t_ s_param) ] @
         th_limit @ th_internal)

  let nb_columns =
    let i = 14 in
    let i = if M.options.no_block then i-2 else i in
    let i = if M.options.no_op then i-1 else i in
    let i = if M.options.no_limit then i-3 else i in
    if M.options.no_internal then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_transaction ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, _src, transaction) ->
          let cols = from_transaction ?account:M.options.pan_hash
              ~crop_len_dn:15 ~crop_len_hash:8
              op_block_hash op_hash transaction in
          let td_block =
            if M.options.no_block then []
            else [ cols.td_tx_block_hash; cols.td_tx_timestamp ] in
          let td_op =
            if M.options.no_op then [] else [ cols.td_tx_op_hash ; ] in
          let td_limit =
            if M.options.no_limit then []
            else [ cols.td_tx_counter; cols.td_tx_gas_limit; cols.td_tx_storage_limit ] in
          let td_internal =
            if M.options.no_internal then [] else [ cols.td_tx_internal ] in
          tr ~a:cols.tr_tx_class (
            td_op @ td_block @ [
              cols.td_tx_src ;
              cols.td_tx_arrow ;
              cols.td_tx_dst ;
              cols.td_tx_amount ;
              cols.td_tx_fee ;
              cols.td_tx_burn ;
              cols.td_tx_parameters ] @
            td_limit @ td_internal)) @@
      Utils.get_transactions ops
end

module TransactionsPanel(M : sig val options : panel_options end) =
  GenericPanel(TransactionSpec(M))

module OriginationSpec(M : sig val options : panel_options end) = struct
  let name = "Origination"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon link_icon (t_ s_ori_hash) ] in
    let th_limit =
      if M.options.no_limit then []
      else [ th [ span [ txt_t s_counter ] ];
             th [ span [ txt_t s_gas_limit ] ];
             th [ span [ txt_t s_storage_limit ] ] ]  in
    let th_internal =
      if M.options.no_internal then []
      else [ th @@ cl_icon folder_icon (t_ s_internal) ] in
    tr ( th_op @ th_block @ [
        th @@ cl_icon account_icon @@ t_ s_new_account ;
        th @@ cl_icon Dun.icon @@ t_ s_new_balance ;
        th @@ cl_icon originator_icon @@ t_ s_originator ;
        th @@ cl_icon manager_icon @@ t_ s_manager ;
        th @@ cl_icon astronaut_icon @@ t_ s_delegate ;
        th @@ cl_icon bill_icon @@ t_ s_fee ;
        th @@ cl_icon (fas_u "fire") (t_ s_burn);
        th [ span [ txt_t s_code ] ] ] @
        th_limit @ th_internal)

  let nb_columns =
    let i = 15 in
    let i = if M.options.no_block then i-2 else i in
    let i = if M.options.no_op then i-1 else i in
    let i = if M.options.no_limit then i-3 else i in
    if M.options.no_internal then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_origination ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, src, ori) ->
          let cols = from_origination ~crop_len_dn:12 ~crop_len_hash:8
              op_block_hash op_hash src ori in
          let td_block =
            if M.options.no_block then []
            else [ cols.td_or_block_hash; cols.td_or_timestamp ] in
          let td_op =
            if M.options.no_op then [] else [ cols.td_or_op_hash ; ] in
          let td_limit =
            if M.options.no_limit then []
            else [ cols.td_or_counter; cols.td_or_gas_limit; cols.td_or_storage_limit ] in
          let td_internal =
            if M.options.no_internal then [] else [ cols.td_or_internal ] in
          tr ~a:cols.tr_or_class (
            td_op @ td_block @ [
              cols.td_or_kt1 ;
              cols.td_or_balance ;
              cols.td_or_src ;
              cols.td_or_manager ;
              cols.td_or_delegate ;
              cols.td_or_fee ;
              cols.td_or_burn ;
              cols.td_or_code_link ] @
            td_limit @ td_internal)) @@
      Utils.get_originations ops
end

module OriginationsPanel(M : sig val options : panel_options end) =
  GenericPanel(OriginationSpec(M))

module DelegationSpec(M : sig val options : panel_options end) = struct
  let name = "Delegations"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon handshake_icon (t_ s_del_hash) ] in
    let th_limit =
      if M.options.no_limit then []
      else [ th [ span [ txt_t s_counter ] ];
             th [ span [ txt_t s_gas_limit ] ];
             th [ span [ txt_t s_storage_limit ] ] ]  in
    let th_internal =
      if M.options.no_internal then []
      else [ th @@ cl_icon folder_icon (t_ s_internal) ] in
    tr ( th_op @ th_block @ [
        th @@ cl_icon account_icon @@ t_ s_account ;
        th ~a:[ a_class [ "arrow" ] ] @@ [ txt "" ] ;
        th @@ cl_icon astronaut_icon @@ t_ s_delegate ;
        th @@ cl_icon bill_icon @@ t_ s_fee ] @
         th_limit @ th_internal)

  let nb_columns =
    let i = 12 in
    let i = if M.options.no_block then i-2 else i in
    let i = if M.options.no_op then i-1 else i in
    let i = if M.options.no_limit then i-3 else i in
    if M.options.no_internal then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_delegation ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, src, del) ->
          let cols = from_delegation ~crop_len_dn:12 ~crop_len_hash:8
              op_block_hash op_hash src del in
          let td_block =
            if M.options.no_block then []
            else [ cols.td_del_block_hash; cols.td_del_timestamp ] in
          let td_op =
            if M.options.no_op then [] else [ cols.td_del_op_hash ] in
          let td_limit =
            if M.options.no_limit then []
            else [ cols.td_del_counter; cols.td_del_gas_limit; cols.td_del_storage_limit ] in
          let td_internal =
            if M.options.no_internal then [] else [ cols.td_del_internal ] in
          tr ~a:cols.tr_del_class (
            td_op @ td_block @ [
              cols.td_del_src ;
              cols.td_del_arrow ;
              cols.td_del_delegate ;
              cols.td_del_fee ] @
            td_limit @ td_internal)) @@
      Utils.get_delegations ops
end

module DelegationsPanel(M : sig val options : panel_options end) =
  GenericPanel(DelegationSpec(M))

module RevealSpec(M : sig val options : panel_options end) = struct
  let name = "Reveals"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon (far_u "lightbulb") (t_ s_rvl_hash) ] in
    let th_limit =
      if M.options.no_limit then []
      else [ th [ span [ txt_t s_counter ] ];
             th [ span [ txt_t s_gas_limit ] ];
             th [ span [ txt_t s_storage_limit ] ] ]  in
    let th_internal =
      if M.options.no_internal then []
      else [ th @@ cl_icon folder_icon (t_ s_internal) ] in
    tr ( th_op @ th_block @ [
        th @@ cl_icon account_icon @@ t_ s_account ;
        th @@ cl_icon (fas_u "key") (t_ s_public_key) ;
        th @@ cl_icon bill_icon @@ t_ s_fee ] @
         th_limit @ th_internal)

  let nb_columns =
    let i = 10 in
    let i = if M.options.no_block then i-2 else i in
    let i = if M.options.no_op then i-1 else i in
    let i = if M.options.no_limit then i-3 else i in
    if M.options.no_internal then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_reveal ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, src, rvl) ->
          let cols = from_reveal ~crop_len_dn:12 ~crop_len_hash:8
              op_block_hash op_hash src rvl in
          let td_block =
            if M.options.no_block then []
            else [ cols.td_rvl_block_hash; cols.td_rvl_timestamp ] in
          let td_op =
            if M.options.no_op then [] else [ cols.td_rvl_op_hash ] in
          let td_limit =
            if M.options.no_limit then []
            else [ cols.td_rvl_counter; cols.td_rvl_gas_limit; cols.td_rvl_storage_limit ] in
          let td_internal =
            if M.options.no_internal then [] else [ cols.td_rvl_internal ] in
          tr ~a:cols.tr_rvl_class (
            td_op @ td_block @ [
              cols.td_rvl_src ;
              cols.td_rvl_pubkey ;
              cols.td_rvl_fee ] @
            td_limit @ td_internal)) @@
      Utils.get_reveals ops
end

module RevealsPanel(M : sig val options : panel_options end) =
  GenericPanel(RevealSpec(M))

module ManageAccountSpec(M : sig val options : panel_options end) = struct
  let name = "Manage Account"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon (fas_u "cog") (s_ "Op. Hash") ] in
    let th_limit =
      if M.options.no_limit then []
      else [ th [ span [ txt_t s_counter ] ];
             th [ span [ txt_t s_gas_limit ] ];
             th [ span [ txt_t s_storage_limit ] ] ]  in
    tr ( th_op @ th_block @ [
        th @@ cl_icon account_icon @@ t_ s_account ;
        th @@ cl_icon astronaut_icon @@ t_ s_target ;
        th [ txt_t s_maxrolls ];
        th [ txt_t s_admin ];
        th [ txt_t s_white_list ];
        th [ txt_t s_delegation ];
        th [ txt_t s_recovery ];
        th [ txt_t s_actions ];
        th @@ cl_icon bill_icon @@ t_ s_fee ] @
         th_limit)

  let nb_columns =
    let i = 15 in
    let i = if M.options.no_block then i-2 else i in
    let i = if M.options.no_op then i-1 else i in
    let i = if M.options.no_limit then i-3 else i in
    if M.options.no_internal then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_manage_account ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, src, mac) ->
          let cols = from_manage_account ~crop_len_dn:10 ~crop_len_hash:8
              op_block_hash op_hash src mac in
          let td_block =
            if M.options.no_block then []
            else [ cols.td_mac_block_hash; cols.td_mac_timestamp ] in
          let td_op =
            if M.options.no_op then [] else [ cols.td_mac_op_hash ] in
          let td_limit =
            if M.options.no_limit then []
            else [ cols.td_mac_counter; cols.td_mac_gas_limit; cols.td_mac_storage_limit ] in
          tr ~a:cols.tr_mac_class (
            td_op @ td_block @ [
              cols.td_mac_src;
              cols.td_mac_target;
              cols.td_mac_maxrolls;
              cols.td_mac_admin;
              cols.td_mac_white_list;
              cols.td_mac_delegation;
              cols.td_mac_recovery;
              cols.td_mac_actions;
              cols.td_mac_fee ] @
            td_limit)) @@
      Utils.get_manage_account ops
end

module ManageAccountPanel(M : sig val options : panel_options end) =
  GenericPanel(ManageAccountSpec(M))

module EndorsementSpec(M : sig val options : panel_options end) = struct
  let name = "Endorsements"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon stamp_icon (t_ s_end_hash) ] in
    tr ( th_op @ th_block @ [
        th @@ cl_icon account_icon (t_ s_endorser) ;
        th @@ cl_icon slots_icon (t_ s_slots) ;
        th @@ cl_icon check_icon (t_ s_endorsed_block) ;
        th @@ cl_icon cube_icon (t_ s_endorsed_level) ;
        th @@ cl_icon priority_icon (t_ s_priority) ] )

  let nb_columns =
    let i = 8 in
    let i = if M.options.no_block then i-2 else i in
    if M.options.no_op then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_endorsement ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, endt) ->
          let cols = from_endorsement ~crop_len_dn:20 ~crop_len_hash:10
              op_block_hash op_hash endt in
          let td_block =
            if M.options.no_block then []
            else [ cols.td_endorse_op_level; cols.td_endorse_timestamp ] in
          let td_op =
            if M.options.no_op then [] else [ cols.td_endorse_op_hash ] in
          tr (
            td_op @ td_block @ [
              cols.td_endorse_src ;
              cols.td_endorse_slot ;
              cols.td_endorse_block_hash ;
              cols.td_endorse_block_level ;
              cols.td_endorse_priority ])) @@
          Utils.get_endorsements ops
end

module EndorsementsPanel(M : sig val options : panel_options end) =
  GenericPanel(EndorsementSpec(M))

module ActivationSpec(M : sig val options : panel_options end) = struct
  let name = "Activations"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon arrow_up_icon (t_ s_act_hash) ] in
    tr ( th_op @ th_block @ [
        th @@ cl_icon account_icon (t_ s_dn1);
        th @@ cl_icon secret_icon (t_ s_secret) ])

  let nb_columns =
    let i = 5 in
    let i = if M.options.no_block then i-2 else i in
    if M.options.no_op then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_activation ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, act) ->
          let cols = from_activation ~crop_len_hash:15
              op_block_hash op_hash act in
          let td_block =
            if M.options.no_block then []
            else [ cols.td_act_block_hash; cols.td_act_timestamp ] in
          let td_op =
            if M.options.no_op then [] else [ cols.td_act_op_hash ] in
          tr (
            td_op @ td_block @ [
              cols.td_act_pkh ;
              cols.td_act_secret ])) @@
          Utils.get_activations ops
end

module ActivationsPanel(M : sig val options : panel_options end) =
  GenericPanel(ActivationSpec(M))

module DoubleBakingSpec(M : sig val options : panel_options end) = struct
  let name = "Double Bakings Evidence"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon cookie_icon (t_ s_op_hash) ] in
    tr ( th_op @ th_block @ [
        th ~a:[ a_class [ ] ] @@ cl_icon cookie_icon (s_ "Baker");
        th ~a:[ a_class [ ] ] @@ cl_icon bill_icon (s_ "Baker Rewards");
        th ~a:[ a_class [ ] ] @@ cl_icon cookie_icon (s_ "Offender");
        th ~a:[ a_class [ ] ] @@ cl_icon cube_icon (s_ "Denounced Level");
        th ~a:[ a_class [ ] ] @@ cl_icon deposit_icon (s_ "Lost Deposits");
        th ~a:[ a_class [ ] ] @@ cl_icon cube_icon (s_ "Lost Rewards");
        th ~a:[ a_class [ ] ] @@ cl_icon bill_icon (s_ "Lost Fees") ])

  let nb_columns =
    let i = 10 in
    let i = if M.options.no_block then i-2 else i in
    if M.options.no_op then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_double_baking_evidence ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, dbe) ->
          let open Data_types in
          let data =
            let level = string_of_int dbe.double_baking_header1.hd_level in
            let path = "heads" in
            let args = [ "level", level ] in
            Common.make_link level ~path ~args in
          tr [
            td [ Common.make_link ~crop_len:15 op_hash ] ;
            td_block_hash op_block_hash dbe.double_baking_op_level;
            from_timestamp dbe.double_baking_tsp;
            from_dn1 ~crop_len:15 dbe.double_baking_denouncer ;
            from_amount ~precision:2 dbe.double_baking_gain_rewards ;
            from_dn1 ~crop_len:15 dbe.double_baking_accused ;
            td [ data ] ;
            from_amount ~precision:2 dbe.double_baking_lost_deposit ;
            from_amount ~precision:2 dbe.double_baking_lost_rewards ;
            from_amount ~precision:2 dbe.double_baking_lost_fees ]) @@
      Utils.get_double_baking_evidence ops
end

module DoubleBakingsPanel(M : sig val options : panel_options end) =
  GenericPanel(DoubleBakingSpec(M))

module DoubleEndorsementSpec(M : sig val options : panel_options end) = struct
  let name = "Double Endorsements Evidence"
  let page_size = M.options.page_size
  let title_span =
    if M.options.no_title then (fun _ -> span [])
    else Panel.title_nb (ss_ name)

  let theads () =
    let th_block =
      if M.options.no_block then []
      else [ th @@ cl_icon cube_icon (t_ s_block);
             th @@ cl_icon clock_icon (t_ s_date) ] in
    let th_op =
      if M.options.no_op then []
      else [ th @@ cl_icon cookie_icon (t_ s_op_hash) ] in
    tr ( th_op @ th_block @ [
        th ~a:[ a_class [ ] ] @@ cl_icon cookie_icon (s_ "Baker");
        th ~a:[ a_class [ ] ] @@ cl_icon bill_icon (s_ "Baker Rewards");
        th ~a:[ a_class [ ] ] @@ cl_icon cookie_icon (s_ "Offender");
        th ~a:[ a_class [ ] ] @@ cl_icon cube_icon (s_ "Denounced Level");
        th ~a:[ a_class [ ] ] @@ cl_icon deposit_icon (s_ "Lost Deposits");
        th ~a:[ a_class [ ] ] @@ cl_icon cube_icon (s_ "Lost Rewards");
        th ~a:[ a_class [ ] ] @@ cl_icon bill_icon (s_ "Lost Fees") ])

  let nb_columns =
    let i = 10 in
    let i = if M.options.no_block then i-2 else i in
    if M.options.no_op then i-1 else i

  let to_rows = function
    | [] -> [ tr [ td ~a: [ a_colspan nb_columns ] [ txt_t s_no_double_endorsement_evidence ]]]
    | ops ->
      List.map (fun (op_hash, op_block_hash, dee) ->
          let open Data_types in
          let data =
            let level = string_of_int dee.double_endorsement1.endorse_block_level in
            let path = "heads" in
            let args = [ "level", level ] in
            Common.make_link level ~path ~args in
          tr [
            td [ Common.make_link ~crop_len:15 op_hash ] ;
            td_block_hash op_block_hash dee.double_endorsement_op_level;
            from_timestamp dee.double_endorsement_tsp;
            from_dn1 ~crop_len:15 dee.double_endorsement_denouncer ;
            from_amount ~precision:2 dee.double_endorsement_gain_rewards ;
            from_dn1 ~crop_len:15 dee.double_endorsement_accused ;
            td [ data ] ;
            from_amount ~precision:2 dee.double_endorsement_lost_deposit ;
            from_amount ~precision:2 dee.double_endorsement_lost_rewards ;
            from_amount ~precision:2 dee.double_endorsement_lost_fees ]) @@
      Utils.get_double_endorsements_evidence ops
end

module DoubleEndorsementsPanel(M : sig val options : panel_options end) =
  GenericPanel(DoubleEndorsementSpec(M))
