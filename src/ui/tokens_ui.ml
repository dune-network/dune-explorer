open Ezjs_tyxml
open Html
open Data_types
open Common
open Ezjs_bs3.Icon
open Ezjs_bs3.Grid
open Ezjs_bs3.Align
open Lang
open Text

(* Transfers Panel for the token contract *)

module TransferPanel = Panel.MakePageTable(
  struct
    let title_span _ = span []
    let table_class = "default-table"
    let page_size = big_panel_number
    let name = "transfer"
    let theads () =
      tr [
        th @@ cl_icon exchange_icon (t_ s_txn_hash);
        th @@ cl_icon cube_icon (t_ s_block);
        th @@ cl_icon clock_icon (t_ s_date);
        th [ txt @@ t_ s_kind ];
        th @@ cl_icon account_icon (t_ s_from);
        th @@ cl_icon account_icon (t_ s_to);
        th @@ cl_icon Dun.icon (t_ s_amount);
      ]
  end)

let make_account_transfers info = function
  | [] -> [ tr [ td ~a: [ a_colspan 9 ] [ txt_t s_no_transfer ]]]
  | ops ->
    List.map (fun (op_hash, op_block_hash, level, tsp, tf) ->
        let open Gen in
        let symbol = Option.fold ~none:"" ~some:(fun info -> info.tk_symbol) info in
        let decimals = Option.map (fun info -> info.tk_decimals) info in
        let cols =
          from_token_operation
            ?decimals
            ~symbol
            ~crop_len_dn:15
            ~crop_len_hash:8
            op_block_hash op_hash level tsp tf in
        tr [
          cols.td_tf_op_hash ;
          cols.td_tf_block_hash ;
          cols.td_tf_timestamp ;
          cols.td_tf_kind ;
          cols.td_tf_src ;
          cols.td_tf_dst ;
          cols.td_tf_amount ;
        ]) @@ Utils.get_ts_operation ops

let make_panel ?(is_token=false) hash =
  if not is_token then div []
  else
    div [
      div ~a:[ a_id ("token-info-" ^ hash)] [];
      TransferPanel.make () ]

let update_token_info tk =
  let send_id = "token-contract-" ^ tk.tk_hash in
  Manip.replaceChildren (find_component ("token-info-" ^ tk.tk_hash)) [
      div ~a:[ a_class [row] ] [
        h3 ~a:[ a_class [cxs12; text_center] ] [ txt tk.tk_name ];
        div ~a:[ a_class [cxs8; cxsoffset2] ] [
          div ~a:[ a_class [row]; a_style "margin-bottom: 10px;" ] [
            div ~a:[ a_class [cxs6; text_center] ] [ txt "Total supply" ];
            div ~a:[ a_class [cxs6; text_center] ] [
              Dun.pp_amount_z ~decimals:tk.tk_decimals
                ~icon:(fun () -> span [ txt tk.tk_symbol ]) tk.tk_supply ] ] ];
        div ~a:[ a_class [cxs12; text_center] ] [
          div ~a:[ a_id ("metal-connect-button-" ^ send_id) ] [];
          Metal_ui.send_button ~text:"Send Token" send_id  ]
      ] ];
  Metal_ui.activate_buttons ~contract:tk.tk_hash ~symbol:tk.tk_symbol
    ~decimals:tk.tk_decimals send_id None

(* Tokens Panel for normal user *)

let token_tabs tokens =
  List.mapi (fun i tk_info ->
      Tabs.make (fun _ -> span [ txt tk_info.tk_symbol ]) [ "token-tab" ],
      if i = 0 then Tabs.Active else Tabs.Inactive
    ) tokens

let make_token_tabs token_tabs =
  Tabs.(make_tabs Tabs token_tabs)

let make_token_content (tab, tab_state) tk_info =
  let send_id = "token-" ^ tk_info.tk_hash in
  Tabs.make_content_panel tab tab_state @@
  div ~a:[ a_class [row] ] [
    div ~a:[ a_class [ "token-title" ] ] [
      Common.make_link ~aclass:[cxs12] tk_info.tk_hash;
      img ~src:("images/" ^ tk_info.tk_hash ^ ".png") ~alt:tk_info.tk_name
        ~a:[ a_width 100 ] ();
      div ~a:[ a_id ("token-balance-" ^ tk_info.tk_hash); a_class ["token-balance"]] [ Common.txt_ () ];
      div ~a:[ a_id ("metal-connect-button-" ^ send_id) ] [];
      Metal_ui.send_button ~text:"Send Token" send_id ];
    TransferPanel.make ~suf_id:tk_info.tk_hash ()
  ]

let make_tokens_panel tokens =
  let tabs = token_tabs tokens in
  div [
    make_token_tabs tabs;
    div ~a:[ a_class ["tab-content" ] ] @@
    List.map2 make_token_content tabs tokens
  ], List.map fst tabs

let update_token_balance info balance_result =
  let balance = match balance_result with
    | [ contract, balance ] when contract = info.tk_hash ->
      Dun.pp_amount_z ~icon:(fun () -> span [ txt info.tk_symbol ])
        ~decimals:info.tk_decimals (Z.of_string balance)
    | _ -> Common.txt_ () in
  Manip.replaceChildren (find_component ("token-balance-" ^ info.tk_hash)) [
    balance ]

let update_transfers info ~nrows xhr =
  TransferPanel.paginate_fun ~suf_id:info.tk_hash
    (make_account_transfers (Some info)) ~nrows xhr

let update_send_token_button account info =
  let send_id = "token-" ^ info.tk_hash in
  Metal_ui.activate_buttons ~contract:info.tk_hash ~symbol:info.tk_symbol
    ~decimals:info.tk_decimals ~language:info.tk_lang send_id (Some account)

let update_tokens account tokens tabs xhr_balance xhr_transfers =
  List.iter2 (fun info tab ->
      let update () =
        xhr_balance account info update_token_balance;
        xhr_transfers account info update_transfers;
        update_send_token_button account info
      in
      update ();
      Tabs.set_on_show tab update
  ) tokens tabs
