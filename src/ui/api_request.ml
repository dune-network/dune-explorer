(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Dune_types
open Data_types
open EzAPI.TYPES
open Service
open Common

(* Use our own version of Ezjsonm.from_string to avoid stack overflows *)
let () = EzEncodingJS.init ()

let find_url_arg arg =
  let args =
    match Url.url_of_string (Js.to_string Dom_html.window##.location##.href) with
    | None -> []
    | Some (
        Url.Http { Url.hu_arguments = args; _ }
      | Url.Https { Url.hu_arguments = args; _ }
      | Url.File { Url.fu_arguments = args; _ }
      ) -> args
  in
  List.find_opt (fun (k, _v) -> k = arg) args

let find_page () =
  match Ezjs_loc.find_arg "p" with
  | None -> 0
  | Some p_str -> try int_of_string p_str with _ -> 0

let find_level () =
  match Ezjs_loc.find_arg "level" with
  | None -> None
  | Some p_str -> try Some (int_of_string p_str) with _ -> None

module Api : sig
  val request0 :
    ?error:EzRequest.error_handler ->
    ?post:bool ->
    ?headers:(string * string) list ->
    ?params:(EzAPI.param * EzAPI.arg_value) list ->
    ('a, exn, EzAPI.no_security) EzAPI.service0 -> string ->
    ('a -> unit) -> unit

  val request1 :
    ?error:EzRequest.error_handler ->
    ?post:bool ->
    ?headers:(string * string) list ->
    ?params:(EzAPI.param * EzAPI.arg_value) list ->
    ('a, 'b, exn, EzAPI.no_security) EzAPI.service1 -> string ->
    ('b -> unit) -> 'a -> unit
end = struct
  let request0 ?error ?post ?headers ?params s name cont =
    EzXhr.get0 (api name) s name ?post ?headers ?error ?params (function
        | Error e -> begin match error with
            | None -> ()
            | Some error -> error 42 (Some (Printexc.to_string e)) end
        | Ok x -> cont x) ()
  let request1 ?error ?post ?headers ?params s name cont =
    EzXhr.get1 (api name) s name ?post ?error ?headers ?params (function
        | Error e -> begin match error with
            | None -> ()
            | Some error -> error 42 (Some (Printexc.to_string e)) end
        | Ok x -> cont x)
end
open Api

let page_params page page_size =
  [Service.param_page, I page;
   Service.param_number, I page_size]

let kind_param kind = Service.param_kind, S kind

let page_request0 ?error service name ?(params=[]) =
  fun page page_size cont ->
    request0 ?error service name cont ~params:(params @ (page_params page page_size))

let page_request1 ?error service name arg ?(params=[]) =
  fun page page_size cont ->
    request1 ?error service name cont arg ~params:(params @ (page_params page page_size))

module V = Current

module Node_state = struct

  let request_timestamps update =
    request0 V.node_timestamps "Node.timestamps"
      ~error:(fun _status _content -> update [])
      update

end

(** Toplevel requests with handler *)
let blocks ?(params=[]) ?(log_msg="Toplevel.blocks") handler =
    request0 ~params V.blocks log_msg (fun blocks -> handler blocks)

let head ?(params=[]) ?(log_msg="") handler =
  request0 ~params V.head log_msg (fun h -> handler h)

let get_services ?(params=[]) ?(log_msg="Toplevel.get_services") handler =
  request0 ~params V.get_services log_msg
    (fun services -> handler services)

(* ******* *)

module Home = struct

  let level () = request1 V.level "Home.level" Home_ui.update_progress

  let last_volume block =
    request0 V.get_services "Home.get_services"
      (fun services ->
         Home_ui.update_baker services block.baker ;
         Home_ui.update_ads services)

  let heads () =
    blocks
      ~log_msg:"Home.heads"
      ~params:[ Service.param_number, I 6 ]
      (fun blocks -> Home_ui.update_blocks blocks;
        match blocks with
        | hd :: _tl ->
          last_volume hd ;
          (level()) hd.hash
        | [] -> assert false)

  let marketcap ()  =
    request0 V.market_info "Home.market" ~params:[Service.param_origin, S "cmc"]
      (function None -> () | Some m ->
          request0 V.supply "Home.supply" (fun supply_info ->
              Home_ui.update_supply_market m.gk_market_data supply_info))

  let request () =
    heads () ;
    if Infos.api.api_config.conf_has_marketcap && Infos.net = Infos.Mainnet then
      marketcap ()

  (* let mini_stats () =
   *   request0 V.mini_stats "Charts.mini_stats" Home_ui.update_stats *)

  let h24_stats () =
    request0 V.h24_stats "Home.h24_stats" Home_ui.update_h24

  (* let current_period_sum () =
   *   request0
   *     V.current_period_sum
   *     "Home.current_period"
   *     Home_ui.update_current_period *)

end

let confirmation ?head_level update bhash =
  level bhash (fun blevel ->
      match head_level with
      | None ->
        head
          ~log_msg:"Operation+Block.confirmation(head)"
          (fun head -> update bhash blevel (head.level - blevel))
      | Some head_level ->
        update bhash blevel (head_level - blevel)
    )

module Block = struct

  let block_uncles block =
    request1 V.nb_uncles "Block.nb_uncles"
      (fun nb -> Block_ui.update_block_uncles block nb)
      block.level

  let block_succ block =
    request1 V.block_succ "Block.succ" (Block_ui.update_succ block) block.hash

  let endorsements hash level =
    request1 V.endorsements_level (Printf.sprintf "Block.endorsements [%s]" hash)
      (Block_ui.update_block_endorsements level hash) level

  let get_operations kind update hash =
    request1 V.nb_operations_hash (Printf.sprintf "Block.number_%s" kind)
      ~params:[ Service.param_type, S kind ]
      (fun nrows -> update ~nrows
          (page_request1 V.operations_bh_nomic (Printf.sprintf "Block.%s" kind) hash
             ~params:[ Service.param_type, S kind ]))
      hash

  let originations =
    get_operations "Origination" Block_ui.update_block_originations

  let endorsements_in_this_block =
    get_operations "Endorsement" Block_ui.update_block_included_endorsements

  let delegations =
    get_operations "Delegation" Block_ui.update_block_delegations

  let activations =
    get_operations "Activation" Block_ui.update_block_activations

  let transactions =
    get_operations "Transaction" Block_ui.update_block_transactions

  let manage_account =
    get_operations "Manage_account" Block_ui.update_block_manage_account

  let nb_operations hash =
    request1 V.nb_operations_block "Block.nb_operations"
      Block_ui.update_tabs_number hash

  let chain_graph ?(length=8) level =
    let params = [ Service.param_length, I length ] in
    request1 V.chain "Block.chain" ~params
      (Block_ui.update_chain_graph level length) level

  let block hash =
    request1 V.block "Block.block"
      (fun block ->
         chain_graph block.level;
         request1 V.level "Level.level"
           (fun level_details ->
              request0 V.snapshot_levels "Block.snapshot_level"
                (fun snapshots ->
                   head ~log_msg:"Block.head"
                     (fun head ->
                        get_services (fun services ->
                            Block_ui.update_block_summary level_details block
                              (List.mem block.level snapshots);
                            !Block_ui.update_block_baker services block ;
                            endorsements hash block.level ;
                            if head.level <> block.level then block_succ block ;
                            block_uncles block;
                            if block.distance_level = 0 then
                              confirmation ~head_level:head.level
                                Block_ui.update_confirmation hash)
                     )
                )
           ) block.hash)
      ~error:(fun _status _content ->
          let content = Search.not_found hash in
          Common.update_main_content content)
      hash

  let request hash =
    Block_ui.update_tabs ();
    nb_operations hash;
    block hash ;
    transactions hash ;
    originations hash ;
    endorsements_in_this_block hash ;
    delegations hash ;
    activations hash ;
    manage_account hash;

end

let with_price_usd f =
  if Infos.api.api_config.conf_has_marketcap then
    request0 V.market_info "Home.market_info" ~params:[Service.param_origin, S "coingecko"]
      (function
        | None -> f None
        | Some m -> f (Some m.gk_market_data.gk_price.gk_usd))
  else f None

module Operation = struct

  let endorsements op args block_hash =
    request1 V.level "Operation.endorsements.level"
      (fun lvl ->
         request1 V.endorsements_level "Operation.endorsements"
           (fun ops ->
              let ops = Utils.get_endorsements ops in
              let endorse_info = (ops, lvl) in
              Operation_ui.update_operation_summary ~endorse_info op args ;
              confirmation Operation_ui.update_confirmation op.op_block_hash)
           lvl.lvl_level) block_hash

  let operation hash args =
    let block_hash = List.find_opt (fun (k, _v) -> k = "block_hash") args in
    let params = match block_hash with
      | None -> []
      | Some (_k, v) -> [Service.param_block_hash, S v] in
    with_price_usd (fun price_usd ->
        request1 V.operation_nomic "Operation.operation"
          ~params
          (fun op ->
             match op.op_type with
             | Sourced Endorsement endorse ->
               endorsements op args endorse.endorse_block_hash
             | _ ->
               Operation_ui.update_operation_summary
                 ?price_usd op args ;
               confirmation Operation_ui.update_confirmation op.op_block_hash ;
               Operation_ui.update_tabs ()
          )
          ~error:(fun _status _content ->
              let content = Search.not_found hash in
              Common.update_main_content content
            )
          hash)

  let request hash args =
    operation hash args
end

module Token = struct

  let balance account info update =
    request1 V.token_balance "Token.token_balance"
      ~params:[Service.param_contract, S info.tk_hash]
      (update info) account

  let transfers account info update =
    let params = [Service.param_contract, S info.tk_hash] in
    request1 V.nb_token_operations "Token.nb_token_operations" ~params
      (fun nrows ->
         update info ~nrows
           (page_request1 V.token_operations "Token.nb_token_operations" account ~params))
      account

end

module Account = struct

  let baking_required_balance hash blce delegate =
    request1 V.required_balance "Account.required_balance"
      (fun required ->
         match delegate with
         | Some delegate when hash = delegate.pkh ->
           request1 V.node_delegate_details "Account.deactivated"
             (function
               | None -> ()
               | Some d ->
                 Account_ui.update_baking_info hash
                   ~grace_period:d.delegate_grace_period blce required )
             hash
         | _ ->
           Account_ui.update_baking_info hash blce required)
      hash

  let bonds_rewards ?price_usd hash blce =
    request1 V.account_bonds_rewards "Account.bonds_rewards"
      (fun br ->
         request1 V.extra_bonds_rewards "Account.extra_bonds_rewards"
           (fun extra ->
              Account_ui.update_bonds_rewards ?price_usd hash br extra blce)
           hash)
      hash

  let account_details hash on_finish =
    let details f =
      request1 V.node_account "Account.details"
        (function
          | None ->
            Account_ui.update_details_404 @@ Common.hash_to_name hash;
            on_finish ?price_usd:None hash 0L None
          | Some details ->
            get_services ~log_msg:"Account_details.get_services"
              (fun services ->
                 with_price_usd
                   (fun price_usd ->
                      request1 V.account_status "Account.extended_details"
                        (fun ext ->
                           request1 V.operations_bh_nomic "Account.revelation"
                             ~params:[ Service.param_type, S "Reveal" ]
                             (fun revelations ->
                                request1 V.operations_bh_nomic "Account.activation"
                                  ~params:[ Service.param_type, S "Activation" ]
                                  (fun activates ->
                                     f details services price_usd ext
                                       revelations activates)
                                  hash)
                             hash)
                        hash)))
        hash in
    details (fun details services price_usd ext revelations activates ->
        let delegate = details.acc_delegate in
        Account_ui.update_account_code
          details.acc_name details.acc_script;
        match delegate with
        | Some dg when dg.pkh = details.acc_name.pkh ->
          request1
            V.node_delegate_details
            "Account.delegate_details"
            (function
              | None ->
                Account_ui.update_details ?price_usd
                  services details ext
                  details.acc_node_timestamp
                  false 0
                  revelations activates None ;
                !Account_ui.update_logo_payout hash;
                on_finish ?price_usd hash details.acc_balance delegate
              | Some d ->
                Account_ui.update_details
                  ?price_usd
                  services
                  details ext
                  details.acc_node_timestamp
                  d.delegate_deactivated d.delegate_grace_period
                  revelations activates
                  (Some d.delegate_staking_balance);
                !Account_ui.update_logo_payout hash;
                on_finish
                  ?price_usd
                  hash details.acc_balance delegate
            ) hash
        | _ ->
          Account_ui.update_details services
            details ext details.acc_node_timestamp
            false 0
            revelations activates None ;
          !Account_ui.update_logo_payout hash;
          on_finish
            ?price_usd
            hash details.acc_balance delegate)

  let transactions_number hash =
    request1 V.nb_operations_hash "Account.number_transactions"
      ~params:[ Service.param_type, S "Transaction" ]
      Account_ui.update_account_transactions_number
      hash

  let transactions hash =
    request1 V.nb_operations_hash "Account.number_transactions"
      ~params:[ Service.param_type, S "Transaction" ]
      (fun nrows ->
         Account_ui.update_account_transactions ~nrows
           (page_request1 V.operations_bh_nomic "Account.transactions" hash
              ~params:[ Service.param_type, S "Transaction" ]))
      hash

  let delegations_number hash =
    request1 V.nb_operations_hash "Account.number_delegations"
      ~params:[ Service.param_type, S "Delegation" ]
      (fun nrows_delegation ->
         request1 V.nb_operations_hash "Account.number_originations_delegate"
           ~params:[ Service.param_type, S "Origination";
                     Service.param_delegate, S "true"]
           (fun nrows_or ->
              request1 V.node_delegated_contracts "Account.delegated_contracts"
                (function
                  | None ->
                    let nrows = max nrows_delegation nrows_or in
                    Account_ui.update_account_delegations_number nrows
                  | Some deleguees ->
                    let nrows =  max nrows_delegation @@
                      max (List.length deleguees) nrows_or in
                    Account_ui.update_account_delegations_number nrows)
                hash)
           hash)
      hash

  let sublist ?(start=0) length l =
    let rec iter start length l rev =
      if length = 0 then List.rev rev
      else match start, l with
        | _, [] -> List.rev rev
        | 0, hd :: tl -> iter 0 (length-1) tl (hd :: rev)
        | i, _hd :: tl -> iter (i-1) length tl rev
    in
    iter start length l []

  let delegations hash =
    request1 V.nb_operations_hash "Account.number_delegations"
      ~params:[ Service.param_type, S "Delegation" ]
      (fun nrows_delegation ->
         Account_ui.update_account_delegations ~nrows:nrows_delegation
           (page_request1 V.operations_bh_nomic "Account.delegations" hash
              ~params:[ Service.param_type, S "Delegation" ]) ;
         request1 V.nb_operations_hash "Account.number_originations_delegate"
           ~params:[ Service.param_type, S "Origination";
                     Service.param_delegate, S "true"]
           (fun nrows_or ->
              Account_ui.update_account_originations_delegate ~nrows:nrows_or
                (page_request1 V.operations_bh_nomic "Account.originations_delegate" hash
                   ~params:[ Service.param_type, S "Origination";
                             Service.param_delegate, S "true" ]) ;
              request1 V.node_delegated_contracts "Account.delegated_contracts"
                (function
                  | None -> Account_ui.update_account_no_deleguees ()
                  | Some contracts ->
                    let nrows = List.length contracts in
                    Account_ui.update_account_deleguees hash ~nrows
                      (fun page page_size f ->
                         let start = page * page_size in
                         let contracts = sublist ~start page_size contracts in
                         f contracts))
                hash)
           hash)
      hash

  let originations_number hash =
    request1 V.nb_operations_hash "Account.number_originations"
      ~params:[ Service.param_type, S "Origination" ]
      Account_ui.update_account_originations_number hash

  let originations hash =
    request1 V.nb_operations_hash "Account.number_originations"
      ~params:[ Service.param_type, S "Origination" ]
      (fun nrows ->
         Account_ui.update_account_originations ~nrows
           (page_request1 V.operations_bh_nomic "Account.originations" hash
              ~params:[ Service.param_type, S "Origination" ]))
      hash

  let endorsements_number hash =
    request1 V.nb_operations_hash "Account.number_endorsements"
      ~params:[ Service.param_type, S "Endorsement" ]
      Account_ui.update_account_endorsements_number hash

  let endorsements hash =
    request1 V.nb_operations_hash "Account.number_endorsements"
      ~params:[ Service.param_type, S "Endorsement" ]
      (fun nrows ->
         Account_ui.update_account_endorsements ~nrows
           (page_request1 V.operations_bh_nomic "Account.endorsements" hash
              ~params:[ Service.param_type, S "Endorsement" ]))
      hash

  let manage_account_number hash =
    let params = [ Service.param_type, S "Manage_account" ] in
    request1 V.nb_operations_hash "Account.number_manage_account" ~params
      Account_ui.update_account_manage_account_number hash

  let manage_account ?info hash =
    Account_ui.update_manage_account_info info;
    let params = [ Service.param_type, S "Manage_account" ] in
    request1 V.nb_operations_hash "Account.number_manage_account" ~params
      (fun nrows ->
         Account_ui.update_account_manage_account ~nrows
           (page_request1 V.operations_bh_nomic "Account.manage_account" hash ~params))
      hash

  let account_info hash f =
    request1 V.account_info "Account.manage_account_info" f hash

  let bakings_status hash delegate =
    match delegate with
    | Some dlg when hash = dlg.pkh ->
      request1 V.nb_bakings "Account.number_bakings"
        (fun nb_bak ->
           request1 V.nb_baker_rights "Account.number_baking_rights"
             (fun nb_bak_rights ->
                request1 V.nb_bakings_endorsement "Account.number_baking_endorsements"
                  (fun nb_end ->
                     request1 V.nb_endorser_rights "Account.number_endorsement_rights"
                       (fun nb_end_rights ->
                          let active =
                            (nb_bak + nb_bak_rights + nb_end + nb_end_rights) <> 0 in
                          Account_ui.update_account_bakings_status active
                       ) hash
                  ) hash
             ) hash
        ) hash;
      request1 V.nb_cycle_rewards "Account.number_cycle_rewards"
        (fun nrows -> Account_ui.update_account_rewards_status (nrows <> 0)) hash
    | _ ->
      Account_ui.update_account_bakings_status false;
      request1 V.nb_cycle_delegator_rewards "Account.number_cycle_delegator_rewards"
        (fun nrows ->
           Account_ui.update_account_rewards_status (nrows <> 0)) hash

  let bakings ?cycle hash =
    let params = match cycle with
      | None -> []
      | Some cycle -> [Service.param_cycle, I cycle] in
    request1 V.nb_bakings "Account.number_bakings" ~params
      (fun nrows ->
         Account_ui.update_account_bakings ?cycle ~nrows
           (page_request1 V.bakings "Account.bakings" hash ~params))
      hash

  let bakings_endorsement ?cycle hash =
    let params = match cycle with
      | None -> []
      | Some cycle -> [Service.param_cycle, I cycle] in
    request1
      V.nb_bakings_endorsement "Account.number_bakings_endorsement" ~params
      (fun nrows ->
         Account_ui.update_account_bakings_endorsement ?cycle ~nrows
           (page_request1 V.bakings_endorsement "Account.bakings_endorsement" hash ~params))
      hash

  let baking_rights ?cycle hash =
    let params = match cycle with
      | None -> []
      | Some cycle -> [Service.param_cycle, I cycle] in
    request1
      V.nb_baker_rights "Account.number_cycle_baker_rights" ~params
      (fun nrows -> Account_ui.update_account_baking_rights ?cycle ~nrows
          (page_request1
             V.baker_rights "Account.cycle_baker_rights" hash ~params))
      hash

  let endorsement_rights ?cycle hash =
    let params = match cycle with
      | None -> []
      | Some cycle -> [Service.param_cycle, I cycle] in
    request1
      V.nb_endorser_rights "Account.number_cycle_endorser_rights" ~params
      (fun nrows -> Account_ui.update_account_endorsement_rights ?cycle ~nrows
          (page_request1
             V.endorser_rights "Account.cycle_endorser_rights" ~params
             hash))
      hash

  let cycle_rewards ?cycle hash sbalance rewards =
    let params = match cycle with
      | None -> []
      | Some cycle -> [Service.param_cycle, I cycle] in
    request1
      V.nb_delegators "Account.number_delegators" ~params
      (fun nrows -> Account_ui.update_account_rewards ?cycle sbalance rewards ~nrows
          (page_request1
             V.rewards_split_fast "Account.rewards_split" hash ~params))
      hash

  let rolls_history hash =
    request1 V.rolls_history "Account.rolls_history"
      ~params:(page_params 0 50)
      (Baking_ui.update_chart_div hash)
      hash

  let bakings_tables delegate hash =
    match delegate with
    | Some dlg when dlg.pkh = hash ->
      let rec close_bakings () =
        request1 V.last_baking_and_endorsement "Account.last_baking_and_endorsement"
          (fun last_infos ->
             request1 V.next_baking_and_endorsement "Account.next_baking_and_endorsement"
               (fun next_infos ->
                  Account_ui.update_close_baking_and_endorsement
                    last_infos
                    next_infos
                    close_bakings
               ) hash
          ) hash
      in
      close_bakings ();

      request1 V.nb_bakings_history "Account.number_bakings_history"
        (fun nrows -> let nrows = nrows in
          Account_ui.update_baking_history ~nrows
            (page_request1 V.bakings_history "Account.bakings_history" hash)
            baking_rights bakings hash)
        hash;

      request1 V.nb_endorsements_history "Account.number_endorsements_history"
        (fun nrows -> let nrows = nrows in
          Account_ui.update_endorsement_history ~nrows
            (page_request1 V.endorsements_history "Account.endorsement_history" hash)
            endorsement_rights bakings_endorsement hash)
        hash;

      rolls_history hash;

      request1 V.baker_version "Account.baker_version" Account_ui.update_baker_version hash
    | _ -> ()

  let rewards_tables delegate hash =
    match delegate with
    | Some dlg when dlg.pkh = hash ->
      request1 V.nb_cycle_rewards "Account.number_cycle_rewards"
        (fun nrows ->
           Account_ui.update_rewards_history ~nrows
             (page_request1 V.rewards_split_cycles "Account.rewards_split_cycles" hash)
             cycle_rewards hash)
        hash
    | Some _dlg ->
      request1 V.nb_cycle_delegator_rewards "Account.number_cycle_delegator_rewards"
        (fun nrows ->
           Account_ui.update_delegator_rewards_history ~nrows
             (page_request1 V.delegator_rewards_with_details
                "Account.delegator_rewards" hash))
        hash
    | _ -> ()

  let balance_history ?price_usd hash =
    request0
      V.head
      "Balance_updates.level"
      (fun head ->
         let level = head.level in
         let cst = Infos.constants ~level in
         let current_cycle = head.cycle in
         request1
           V.balance_history
           "Balance_update.balance_history"
           ~params:[Service.param_cycle, I current_cycle]
           (fun hist ->
              request1
                V.cycle_all_rights
                "Balance_update.cycle_all_rights"
                ~params:[Service.param_cycle, I current_cycle]
                (fun (baks,ends) ->
                   request1
                     V.cycle_frozen
                     "Balance_update.cycle_frozen"
                     ~params:[Service.param_cycle, I (current_cycle - cst.preserved_cycles)]
                     (fun upcoming_unfrozen ->
                        request1
                          V.node_delegate_details
                          "Account.staking_balance"
                          (fun details ->
                             let staking_balance = Option.map (fun x -> x.delegate_staking_balance) details in
                             Balance_ui.update_balance_ui
                               ?price_usd
                               ?staking_balance
                               hash
                               hist
                               level
                               baks
                               ends
                               upcoming_unfrozen
                          )
                          hash
                     )
                     hash
                ) hash
           ) hash
      )

  let balance_update_number hash =
    request1
      V.balance_updates_number
      "Balance_update.balance_update_number"
      ~params:[]
      (Account_ui.update_nb_balance_updates hash)
      hash

  let balance_updates hash =
    request1
      V.balance_updates_number
      "Balance_update.balance_update_number"
      ~params:[]
      ~error:(fun _ _ -> balance_history hash)
      (fun nrows ->
         with_price_usd (fun price_usd ->
             Account_ui.update_account_balance_updates ?price_usd
               hash
               ~nrows
               (page_request1
                  V.balance_updates
                  "Balance_update.balance_updates"
                  hash
                  ~params:[];
               );
             balance_history ?price_usd hash
           ))
      hash

  let transfers_number ?is_token hash =
    let params = [ Service.param_type, S "Token" ] in
    request1 V.nb_operations_hash "Account.number_tokens" ~params
      (Account_ui.update_transfers_number ?is_token)
      hash

  let transfers ?is_token hash =
    let params = [ Service.param_type, S "Token" ] in
    request1 V.token_info "Account.token_info"
      (fun info ->
         request1 V.nb_operations_hash "Account.number_transfers" ~params
           (fun nrows ->
              Account_ui.update_account_transfers ?is_token ~nrows info
                (page_request1 V.operations_bh_nomic "Account.transfers" hash ~params))
           hash;
         match info with None -> () | Some info -> Tokens_ui.update_token_info info)
      hash

  let tokens_number hash =
    request1 V.account_tokens "Account.account_tokens"
      (fun tokens -> Account_ui.update_tokens_number (List.length tokens))
      hash

  let tokens hash =
    request1 V.account_tokens "Account.account_tokens"
      (fun tokens ->
         Account_ui.update_account_tokens hash tokens
           Token.balance Token.transfers)
      hash

  let request ?is_token hash filters =
    let default = Account_ui.default_filter ?is_token filters in
    transactions_number hash;
    delegations_number hash;
    originations_number hash;
    endorsements_number hash;
    manage_account_number hash;
    balance_update_number hash;
    transfers_number ?is_token hash;
    tokens_number hash;
    account_details hash (fun ?price_usd hash balance delegate ->
        bakings_status hash delegate;
        Account_ui.update_bakings default hash (bakings_tables delegate);
        Account_ui.update_rewards default hash (rewards_tables delegate);
        bonds_rewards ?price_usd hash balance;
        baking_required_balance hash balance delegate;
        Account_ui.update_metal_buttons hash delegate;
      );
    account_info hash (fun info ->
        Account_ui.update_manage_account default hash (manage_account ?info) ;
        match info with
        | None -> ()
        | Some info ->
          Account_ui.update_account_entrypoints info.ai_entrypoints
      );
    Account_ui.update_transactions default hash transactions ;
    Account_ui.update_delegations default hash delegations ;
    Account_ui.update_originations default hash originations ;
    Account_ui.update_endorsements default hash endorsements ;
    Account_ui.update_balance_updates default hash balance_updates ;
    Account_ui.update_transfers ?is_token default hash (transfers ?is_token);
    Account_ui.update_tokens default hash tokens;
end

module Blocks = struct

  let snapshot_blocks () =
    request0 V.nb_snapshot_blocks "Blocks.nb_snapshot_blocks"
      (fun nrows -> Blocks_ui.update_snapshot_blocks ~nrows
          (page_request0 V.snapshot_blocks "Blocks.snapshot_blocks"))

  let request_baking_rights ?filter () =
    let params = match filter with
      | None -> []
      | Some filter -> [Service.param_search_filter, S filter] in
    request0 V.nb_cycle_rights "Blocks.nb_pending_priorities" ~params
      (fun nrows -> Blocks_ui.update_baking_rights ~nrows
          (page_request0 V.cycle_rights "Blocks.baking_rights" ~params));
    let params = (Service.param_future, S "false") :: params in
    request0 V.nb_cycle_rights "Blocks.nb_pending_priorities" ~params
      (fun nrows -> Blocks_ui.update_baking_rights ~future:false ~nrows
          (page_request0 V.cycle_rights "Blocks.baking_rights" ~params))
  let request () =
    head
      ~log_msg:"Blocks.head"
      (fun head -> let nrows = head.level+1 in
        request0 V.snapshot_levels "Blocks.snapshot_levels"
          (fun snapshots ->
             Blocks_ui.update_blocks ~snapshots ~nrows
               (fun page page_size cont ->
                  request0 V.blocks_with_pred_fitness "Blocks.blocks"
                    ~params:(page_params page page_size)
                    (fun l ->
                       let blocks = List.map fst l in
                       cont l; List.iter Block.block_uncles blocks))))
end

module Accounts = struct

  let accounts ?contract () =
    let params = match contract with
      | None -> []
      | Some contract ->
        [ Service.param_contract, S (string_of_bool contract) ] in
    request0 V.nb_accounts "Accounts.number_accounts" ~params
      (fun nrows ->
         Accounts_ui.update
           ~contract:(match contract with None -> false | Some c -> c)
           nrows
           (page_request0 V.accounts "Accounts.account" ~params))

  let request ?contract () =
    accounts ?contract ()

end

module Top_accounts = struct

  let request () =
    Top_accounts_ui.update_cmd (fun selection ->
        let kind = Utils.selection_to_kind selection in
        match kind with
        | Balances | Frozen_balances
        | Frozen_deposits | Frozen_rewards
        | Frozen_fees | Total_balances ->
          let params = [ kind_param (Utils.top_kind_to_str kind) ] in
          request0 V.balance_number "Balance_update.balance_number" ~params
            (fun nrows ->
               Top_accounts_ui.update_ranking_table kind nrows
                 (page_request0
                    V.balance_ranking ~params
                    "Balance_update.balance_ranking"))

        | _ ->
          let param = kind_param (Utils.top_kind_to_str kind) in
          request0 V.nb_tops "Top_accounts.nb_tops"
            (fun nrows ->
               request0 V.context_days "Context.days"
                 (fun days -> match days with
                    | hd :: _ ->
                      request1 V.context_stats "Context.stats"
                        (fun context ->
                           let total = Utils.top_kind_to_total context kind in
                           let level = context.context_level in
                           Top_accounts_ui.update
                             nrows
                             kind
                             level
                             total
                             (page_request0 V.tops "Top_accounts.tops" ~params:[ param ]))
                        hd
                    | _ -> ()))
            ~params:[ param ])

end

module Heads = struct

  let request () =
    match find_level () with
    | None ->
      request0
        V.nb_heads "Heads.nb_heads"
        (fun nrows ->
           Blocks_ui.Heads.update ~nrows
             (fun page page_size cont ->
                request0
                  V.heads_with_pred_fitness "Heads.heads"
                  ~params:(page_params page page_size)
                  (fun l -> let blocks = List.map fst l in
                    cont l; List.iter Block.block_uncles blocks)
                  ))
    | Some level ->
      (* we do this to avoid to paginate when showing uncles of a
         given level *)
      request1 V.nb_uncles "Heads.nb_uncles"
        (fun nrows ->
           Blocks_ui.update_blocks ~alt:true ~level ~nrows
             (page_request0 V.heads_with_pred_fitness "Heads.uncles"
                ~params:[Service.param_level, I level]))
        level

end

module Operations = struct

  (* generic updater of operations. Parameterized by the kind of the
  operation and an update function. *)
  module Updater(Arg : sig
      val op_kind : string
      val update :
        ?page_sizer:bool -> ?suf_id:string -> ?urlarg:string ->
        ?title_span:(int -> Html_types.span elt) ->
        nrows:int ->
        (int -> int -> (Data_types.operation list -> unit) -> unit) ->
        unit
    end) : sig
    val request : pending:bool -> unit
  end = struct
    let request ~pending =
    let params = [ Service.param_type, S Arg.op_kind ] in
    let params, kind =
      if pending then
        (Service.param_status, S "Pending") :: params,
        Printf.sprintf "Pending-%s" Arg.op_kind
      else params, Arg.op_kind
    in
    request0 V.nb_operations (Printf.sprintf "Operations.number_%s" kind)
      ~params
      (fun nrows ->
         Arg.update ~nrows
           (page_request0 V.operations_nomic (Printf.sprintf  "Operations.%s" kind)
              ~params))
  end

  module Activations =
    Updater(struct
      let op_kind = "Activation"
      let update  = Operations_ui.ActivationsPanel.update
    end)

  module Transactions =
    Updater(struct
      let op_kind = "Transaction"
      let update  = Operations_ui.TransactionsPanel.update
    end)

  module Endorsements =
    Updater(struct
      let op_kind = "Endorsement"
      let update  = Operations_ui.EndorsementsPanel.update
    end)

  module Delegations =
    Updater(struct
      let op_kind = "Delegation"
      let update  = Operations_ui.DelegationsPanel.update
    end)

  module Originations =
    Updater(struct
      let op_kind = "Origination"
      let update  = Operations_ui.OriginationsPanel.update
    end)

  module Manage_account =
    Updater(struct
      let op_kind = "Manage_account"
      let update  = Operations_ui.ManageAccountPanel.update
    end)

  module Double_baking =
    Updater(struct
      let op_kind = "Double_baking_evidence"
      let update  = Operations_ui.DoubleBakingsPanel.update
    end)

  module Double_endorsement =
    Updater(struct
      let op_kind = "Double_endorsement_evidence"
      let update  = Operations_ui.DoubleEndorsementsPanel.update
    end)

  module Nonces = struct

    let request () =
      request0 V.nb_cycle "Nonces.nb_cycle"
        (fun nrows ->
           Operations_ui.Nonces.update_nonces ~nrows
             (page_request0 V.nonces "Nonces.nonces"))
  end

end

module Network = struct

  let request () =
    let params = match Ezjs_loc.find_arg "state" with
      | Some "running"
      | None -> [ Service.param_peers, S "running" ]
      | _ -> []
    in
    request0 V.country_stats "Network_stats" ~params Network_stats_ui.update_map;
    request0 V.nb_network_peers "Network.nb" ~params
      (fun nrows -> Network_stats_ui.update_peers ~nrows
          (page_request0 V.network_stats "Network.stats.network" ~params))
end

module Charts = struct

  let request_bakers () =
    request0 V.nb_cycle "Charts.nb_cycle"
      (fun last_cycle ->
         Charts_ui.update_bakers_cmd last_cycle
           (fun cycle ->
              let params =
                if cycle = "All" then []
                else [ Service.param_cycle, I (int_of_string cycle) ] in
              request0 V.bakers "Charts.bakers"
                (fun bakers ->
                   Charts_ui.update_bakers_chart (Array.of_list bakers))
                ~params
              ))

  let request_blocks_per_day () =
    request0 V.blocks_per_day "Charts.blocks_per_day"
      Charts_ui.update_blocks_per_day

  let request_delay_per_day () =
    request0 V.blocks_per_day "Charts.blocks_per_day"
      Charts_ui.update_delay_per_day

  let request_bakers_per_day () =
    request0 V.bakers_per_day "Charts.bakers_per_day"
      Charts_ui.update_bakers_per_day

  let request_priorities_per_day () =
    request0 V.priorities_per_day "Charts.priorities_per_day"
      Charts_ui.update_priorities_per_day

  let request_operations_per_day () =
    request0 V.operations_per_day "Charts.operations_per_day"
      Charts_ui.update_operations_per_day

  let request_operations_per_block_per_day () =
    request0 V.operations_per_block_per_day "Charts.operations_per_block_per_day"
      Charts_ui.update_operations_per_block_per_day

  let request_fees_per_day () =
    request0 V.fees_per_day "Charts.fees_per_day"
      Charts_ui.update_fees_per_day

  let request_volume_per_day () =
    request0 V.volume_per_day "Charts.volume_per_day"
      Charts_ui.update_volume_per_day

  let request_market_prices ?origin () =
    let params = match origin with None -> [] | Some ori -> [Service.param_origin, S ori] in
    request0 V.market_prices "Charts.market_prices" ~params
      Charts_ui.update_market_prices
end

module Server = struct

  let info redraw =
    request0 V.api_server_info "Server.info"
      (fun info ->
         Infos.api.api_config <- info.api_config ;
         Infos.init_constants (List.rev info.api_config.conf_constants);
         Infos.api.api_versions <- info.api_versions ;
         Infos.api.api_date <- info.api_date ;
         Format_date.set_server_date info.api_date ;
         redraw ()
      )

end

module Rolls_distribution = struct

  let request () =
    request0 V.max_roll_cycle "Rolls_distribution.max_cycle"
      (fun max_cycle ->
         Rolls_distribution_ui.update_cmd max_cycle
           (fun cycle ->
              let cycle = int_of_string cycle in
              request1 V.rolls_distribution "Rolls_distribution"
                (Rolls_distribution_ui.update_rolls_distrib cycle)
                cycle))
end

module Health = struct

  let stats () =
    head
      ~log_msg:"Health.head"
      (fun head ->
         request1 V.level "Health.level"
           (fun level -> Health_stats_ui.update_cmd level.lvl_cycle
               (fun cycle ->
                  let cycle = int_of_string cycle in
                  request1 V.health_stats "Health.stats"
                    (Health_stats_ui.update_health_page cycle)
                    cycle))
           head.hash)

  let request = stats
end

module Context = struct

  let stats () =
    request0 V.context_days "Context.days"
      (fun days -> match days with
         | _hd :: _ ->
           Context_stats_ui.update_cmd days (fun day ->
               request1 V.context_stats "Context.stats"
                 Context_stats_ui.update_context_page
                 day)
         | _ -> Context_stats_ui.update_context_empty ())

  let request = stats
end

module Search = struct

  let search_account update hash =
    request1 V.nb_search_account "Search.nb.account"
      (fun nb ->
         if nb = 0 then
           update nb hash []
         else
           request1 V.search_account "Search.account"
             (update nb hash)
             hash)
      hash

  let search hash =
    match hash.[0], hash.[1] with
    | 'o', ( 'n' | 'p' | 'q' ) ->
      request1 V.nb_search_operation "Search.nb.operation"
        (fun nb ->
           if nb = 0 then Search.update_search 0 hash []
           else
             request1 V.search_operation "Search.operation"
               (fun res ->
                  Search.update_search nb hash
                    (List.map (fun s ->
                         { pkh = s; alias = None }, ""
                       ) res)
               )
               hash)
        hash
    | 'B', ( 'K' | 'L' | 'M' ) ->
      request1 V.nb_search_block "Search.nb.block"
        (fun nb ->
           if nb = 0 then Search.update_search 0 hash []
           else
             request1 V.search_block "Search.block"
               (fun res ->
                  Search.update_search nb hash
                    (List.map (fun s ->
                         { pkh = s; alias = None }, ""
                       ) res)
               )
               hash)
        hash
    | _ ->
      search_account Search.update_search hash;
      Search.update_search 0 hash []

  let parse s =
    let s = (Url.urldecode s) ^ "\n" in
    log "parsing %S" s;

    try
      Parse_utils.parse s
      (* let lexbuf = Lexing.from_string s in
       * Parser.main Lexer.token lexbuf *)
    with Lexer.Eof | Parsing.Parse_error | _ as exn ->
      log "parse error : %s" (Printexc.to_string exn);
      Ast.Blocks []

  let params_of_filters filters =
    let open Ast in
    let constraints acc param v = function
      | Gr -> (param "g", v) :: acc
      | Le -> (param "l", v) :: acc
      | Eq -> (param "e", v) :: acc in
    let hash_type acc param = function
      | Is v -> (param "is", S v) :: acc
      | IsNot v -> (param "isnot", S v) :: acc
      | StartsWith v -> (param "start", S v) :: acc
      | EndsWith v -> (param "end", S v) :: acc
      | Contains v -> (param "inc", S v) :: acc in
    let global_filtering acc = function
      | Level (s, a) -> constraints acc param_level_sch (I (Int64.to_int a)) s
      | Cycle (s, a) -> constraints acc param_cycle_sch (I (Int64.to_int a)) s
      | Date (s, d) -> constraints acc param_date (S (Date.to_string d)) s
      | Quantity _ | SortBy _ | SortOrder _ | ConsumedGas _ | NonceHash _ -> acc in
    let operation_filtering (acc, kind) = function
      | Kind k -> begin match k with
          | Transaction_k -> acc, "search_transactions"
          | Origination_k -> acc, "search_originations"
          | Delegation_k -> acc, "search_delegations"
          | Endorsement_k -> acc, "search_endorsements"
          | Reveal_k -> acc, "search_reveals"
          | Activation_k -> acc, "search_activations"
        end
      | Failed failed -> (param_failed, S (string_of_bool failed)) :: acc, kind
      | Fee (s, a) -> constraints acc param_fee (S (Int64.to_string a)) s, kind
      | Amount (s, a) -> constraints acc param_amount (S (Int64.to_string a)) s, kind
      | OHash h -> hash_type acc param_ohash h, kind
      | Block h -> hash_type acc param_block_sch h, kind
      | From h -> hash_type acc param_from h, kind
      | To h -> hash_type acc param_to h, kind
      | Internal b -> (param_internal, S (string_of_bool b)) :: acc, kind
      | Counter (s, a) -> constraints acc param_counter (I (Int64.to_int a)) s, kind
      | Burn (s, a) -> constraints acc param_burn (S (Int64.to_string a)) s, kind
      | Param b -> (param_hasparam, S (string_of_bool b)) :: acc, kind
      | CollectCall b -> (param_collect_call, S (string_of_bool b)) :: acc, kind
      | OGlobal g -> global_filtering acc g, kind in
    let block_filtering acc = function
      | Reward _ -> acc
      | NumOperations (s, a) -> constraints acc param_nop (I (Int64.to_int a)) s
      | Fitness (s, a) -> constraints acc param_fitness (I (Int64.to_int a)) s
      | Priority (s, a) -> constraints acc param_priority (I (Int64.to_int a)) s
      | Endorsements (s, a) -> constraints acc param_endorsements (I (Int64.to_int a)) s
      | EndorsedBy h -> hash_type acc param_endorsedby h
      | Volume (s, a) -> constraints acc param_volume (S (Int64.to_string a)) s
      | Ast.Fees (s, a) -> constraints acc param_fees (S (Int64.to_string a)) s
      | Baker b -> hash_type acc param_baker b
      | BHash h -> hash_type acc param_bhash h
      | Network h -> hash_type acc param_network h
      | Pred h -> hash_type acc param_predecessor h
      | Succ h -> hash_type acc param_successor h
      | Proto h -> hash_type acc param_proto h
      | Signature h -> hash_type acc param_signature h
      | ValidationPass (s, a) -> constraints acc param_valpass (I (Int64.to_int a)) s
      | CyclePos (s, a) -> constraints acc param_cycle_pos (I (Int64.to_int a)) s
      | Uncles _ | ActiveProposal _ -> acc
      | BGlobal g -> global_filtering acc g in
    let balance_kind_to_str = function
      | Balance_k -> "sp"
      | Staking_balance_k -> "stak"
      | Frozen_balance_k -> "fro"
      | Rewards_k -> "rew"
      | Fees_k -> "fee"
      | Deposits_k -> "dep" in
    let account_filtering acc = function
      | AHash h -> hash_type acc param_ahash h
      | Balance (k, s, a) -> let prefix = balance_kind_to_str k in
          constraints acc (param_balance prefix) (S (Int64.to_string a)) s
      | DelegateTo h -> hash_type acc param_dlg h
      | BakedHash h -> hash_type acc param_bakedblock h
      | BakedLevel (s, a) -> constraints acc param_bakedlevel (I (Int64.to_int a)) s
      | Delegated b -> (param_delegated, S (string_of_bool b)) :: acc
      | Revealed b -> (param_revealed, S (string_of_bool b)) :: acc
      | Originated b -> (param_originated, S (string_of_bool b)) :: acc
      | AGlobal g -> global_filtering acc g in
    let balance_filtering acc = function
      | Kinds k -> List.fold_left (fun acc2 f -> match f with
          | Balance_k -> (param_bal_kind, S "balance") :: acc2
          | Staking_balance_k -> (param_bal_kind, S "staking_balance") :: acc2
          | Frozen_balance_k -> (param_bal_kind, S "frozen_balance") :: acc2
          | Rewards_k -> (param_bal_kind, S "rewards") :: acc2
          | Fees_k -> (param_bal_kind, S "fees") :: acc2
          | Deposits_k -> (param_bal_kind, S "deposits") :: acc2) acc k
      | BalBlock h -> hash_type acc param_balblock h
      | BalAccount h -> hash_type acc param_balaccount h
      | BalGlobal g -> global_filtering acc g in
    match filters with
    | Blocks filters -> List.fold_left block_filtering [] filters, "search_blocks"
    | Accounts filters -> List.fold_left account_filtering [] filters, "search_accounts"
    | Operations filters ->
      List.fold_left operation_filtering ([], "search_transactions") filters
    | Bal filters -> List.fold_left balance_filtering [] filters, "search_balances"
    | BalUpdate _filters -> [], "search_balance_updates"
    | Empty -> [], "search_empty"

  let adv_search s =
    let n = String.length s in
    if n = 51 && (s.[0] = 'o' && (s.[1] = 'n' || s.[1] = 'o' || s.[1] = 'p') ||
                  s.[0] = 'B' && (s.[1] = 'K' || s.[1] = 'L' || s.[1] = 'M')) ||
       n = 36 && (s.[0] = 't' && s.[1] = 'z' || s.[0] = 'K' && s.[1] = 'T' ||
                  s.[0] = 'd' && s.[1] = 'n') then
      Common.change_page s
    else (match int_of_string_opt s with
        | Some _level -> Common.change_page s
        | None ->
          let args = (("q", s) :: Common.initial_args) in
          Common.change_page ~args "advanced_search")

  let rec advanced_request_from_filters filters =
    Adv_search_ui.clear_filters_list ();
    Adv_search_ui.update_filters_list advanced_request_from_filters filters;
    let params, search_kind = params_of_filters filters in
    request1 V.nb_adv_search "Search.nb_adv_search" ~params
      (fun nrows ->
         Adv_search_ui.update_adv_search ~nrows search_kind
           (page_request1 V.adv_search "Search.adv_search" ~params search_kind))
      search_kind

  let advanced_request () =
    match Ezjs_loc.find_arg "q" with
    | None -> log "search argument not found"
    | Some s ->
      Adv_search_ui.set_search_value s;
      advanced_request_from_filters (parse s)

  let request = search
end

module Protocols = struct
  let protocols_request () =
    request0 V.nb_protocol "Protocol.nb_protocol"
      (fun nrows ->
         Protocols_ui.update ~nrows
           (page_request0 V.protocols "Protocol.protocols"))

  let constants_request ?level ?protocol () =
    let params = match level, protocol with
      | Some level, _ -> [param_level, I level]
      | _, Some protocol -> [param_protocol, S protocol]
      | _ -> [] in
    request0 V.constants "Protocol.constants" ~params
      (function
        | None -> ()
        | Some constants -> Protocols_ui.update_constants constants)

end

module Supply = struct
  let request ?level () =
    let params = Option.map (fun lv -> [Service.param_level, I lv]) level in
    request0 ?params V.supply "Supply.supply" Supply_ui.update
end

module CSV = struct
  let transactions hash update =
    match Infos.www.www_recaptcha_key, Infos.www.www_csv_server with
    | Some site_key, Some (csv_api, _) ->
      Ezjs_recaptcha.V3.check site_key (fun token ->
          EzXhr.get1 (base_of_host csv_api)
            V.transaction_account_csv "CSV.transaction_account"
            ~params:[ Service.param_token, S token ]
            (function
              | Error _ -> ()
              | Ok s -> update s ) hash )
    | _ -> log "No site key or no csv api"
end
