(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Data_types

let dun_units = 1_000_000L
let ten = Z.of_int 10
let units = [|"y"; "z"; "a"; "f"; "p"; "n"; "μ"; "m"; ""; "K"; "M"; "B"; "T"; "P"; "E"; "Z"; "Y"|]
let unit_offset = 8

let icon () =
  span ~a:[a_class ["dn"]]
    (List.map (fun sym ->
        if sym.[0] = '#' then
          entity sym
        else
          txt sym)
        Infos.www.www_currency_symbol)

let dollar () =
  span ~a:[a_class ["dn"]] [
      entity "#x24"
  ]

let mu_icon () = entity "#956"

let sep =
  try
    match Js.to_string (Js.number_of_float 0.1)##toLocaleString with
    | "0.1" -> '.'
    | "0,1" -> ','
    | _ -> '.'
  with _ -> '.'

let non_zeroes s =
  let rec iter s n = match s.[n-1] with
    | '0' -> iter s (n-1)
    | '.' | ',' -> n-1
    | _ -> n in
  iter s (String.length s)

let amount ?(icon=icon) volumeL = (* in mudun *)
  if volumeL > 0L && volumeL < 1000L then
    let number = Js.number_of_float @@ Int64.to_float volumeL in
    span [ txt @@ (Js.to_string number##toLocaleString) ^ " "; mu_icon (); icon ()]
  else
    let dun = Int64.div volumeL dun_units in
    let mudun = Int64.sub volumeL (Int64.mul dun_units dun) in
    let dun = Js.to_string ((Js.number_of_float (Int64.to_float dun))##toLocaleString ) in
    let s = Printf.sprintf "%s%c%06Ld" dun sep mudun in
    let n = non_zeroes s in
    let s = String.sub s 0 n in
    span [ txt (s ^ " "); icon () ]

let pp_amount_z ?(decimals=6) ?(precision=6) ?(width=15) ?order ?(icon=icon) volume =
  if volume = Z.zero then span [ txt "0 "; icon () ]
  else
    let sign = if volume < Z.zero then "-" else "" in
    let volume = Z.abs volume in
    let volume, decimals =
      match decimals mod 3 with
      | 0 -> volume, decimals
      | 1 -> Z.(mul volume ~$100), decimals + 2
      | _ -> Z.mul volume ten, decimals + 1 in
    let ndecimal = String.length (Z.to_string volume) in
    let diff_length = ndecimal - width in
    let order = match order with
      | None -> max ((ndecimal - 1) / 3) ((ndecimal - width + 2) / 3)
      | Some order -> order in
    let unit_float = if diff_length < 0 then Z.one else Z.(ten ** diff_length) in
    let unit_int = if order < 0 then Z.one else Z.pow ten (order * 3) in
    let volume_float = Z.div volume unit_float in
    let volume_int = Z.div volume unit_int in
    let decimal = Z.sub (Z.mul volume_float unit_float)
        (Z.mul volume_int unit_int) in
    let volume_int, decimal, precision =
      if order * 3 < precision then volume_int, decimal, order * 3 else
        let unit_precision = Z.pow ten (order * 3 - precision) in
        let tmp = Z.div decimal unit_precision in
        let unit_precision2 = Z.div unit_precision (Z.of_int 10) in
        if unit_precision2 = Z.zero || tmp = Z.zero ||
           Z.div (Z.sub decimal (Z.mul tmp unit_precision))
             (Z.div unit_precision ten) < (Z.of_int 5) then
          volume_int, tmp, precision
        else if Z.succ tmp = Z.(ten ** precision) then
          Z.succ volume_int, Z.zero, precision
        else
          volume_int, Z.succ tmp, precision
    in
    let num = Js.(to_string @@ (number_of_float (Z.to_float volume_int))##toLocaleString) in
    let decimal_str = Printf.sprintf "%c%0*Ld" sep precision (Z.to_int64 decimal) in
    let n = non_zeroes decimal_str in
    let decimal_str = String.sub decimal_str 0 n in
    span ~a:[ a_class ["pp-dun"] ] [
      span ~a:[ a_class ["pp-dun-unit"] ] [ txt (sign ^ num) ] ;
      if decimal <> Z.zero then
        span ~a:[ a_class ["pp-dun-decimal"] ] [ txt decimal_str ]
      else
        span [ ] ;
      span [ txt " "; txt units.(max (order + unit_offset - (decimals / 3)) 0); icon() ] ;
    ]

let pp_amount ?(precision=6) ?(width=15) ?order ?(icon=icon) volumeL =
  if volumeL = 0L then span [ txt "0 "; icon ()]
  else
    let sign =
      if volumeL < 0L then "-"
      else "" in
    let volumeL = Int64.abs volumeL in
    let units = [|mu_icon (); txt " m"; txt ""; txt "K";
                  txt "M"; txt "B"; txt "T"|] in
    let ndecimal = String.length (Int64.to_string volumeL) in
    let diff_length = ndecimal - width in
    let order = match order with
      | None -> if ndecimal > 6 then
          max 2 ((ndecimal - width + 2) / 3)
        else if ndecimal > 3 then
          max 1 ((ndecimal - width + 2) / 3)
        else
          max 0 ((ndecimal - width + 2) / 3)
      | Some order -> order in
    let unit_float = if diff_length < 0 then 1L else
        Int64.(of_float (10. ** (float diff_length))) in
    let unit_int = if order < 0 then 1L else
        Int64.(of_float (10. ** (float (order * 3 )))) in
    let volume_float = Int64.div volumeL unit_float in
    let volume_int = Int64.div volumeL unit_int in
    let decimal = Int64.sub (Int64.mul volume_float unit_float)
        (Int64.mul volume_int unit_int) in
    let volume_int, decimal, precision =
      if order * 3 < precision then volume_int, decimal, order * 3 else
        let unit_precision = Int64.of_float (10. ** float (order * 3 - precision)) in
        let tmp = Int64.div decimal unit_precision in
        let unit_precision2 = Int64.div unit_precision 10L in
        if unit_precision2 = 0L || tmp = 0L ||
           Int64.div (Int64.sub decimal (Int64.mul tmp unit_precision))
             (Int64.div unit_precision 10L) < 5L then
          volume_int, tmp, precision
        else if Int64.succ tmp = Int64.of_float (10. ** float precision) then
          Int64.succ volume_int, Int64.zero, precision
        else
          volume_int, Int64.succ tmp, precision
    in
    let num = Js.to_string
        ((Js.number_of_float
            (Int64.to_float volume_int))##toLocaleString ) in
    let decimal_str = Printf.sprintf "%c%0*Ld" sep precision decimal in
    let n = non_zeroes decimal_str in
    let decimal_str = String.sub decimal_str 0 n in
    span ~a:[ a_class ["pp-dun"] ] [
      span ~a:[ a_class ["pp-dun-unit"] ] [ txt (sign ^ num) ] ;
      if decimal <> 0L then
        span ~a:[ a_class ["pp-dun-decimal"] ] [ txt decimal_str ]
      else
        span [ ] ;
      span [ txt " "; units.(max order 0); icon() ] ;
    ]

let pp_amount_float ?precision ?width ?order volumef =
  pp_amount ?precision ?width ?order (Int64.of_float volumef)

let approx_amount volumeL = (* in mudun *)
  let number =
    Js.number_of_float @@
    (Int64.to_float volumeL /. Int64.to_float dun_units) in
  span
    [ txt @@ Js.to_string number##toLocaleString ^ " "; icon ()]

let amount_float volume = amount (Int64.of_float volume)

let approx_amount_float volume = approx_amount (Int64.of_float volume)

let amount_float_dun volume =
  let volumeL = Int64.of_float volume in
  let number =
    Js.number_of_float @@
    Int64.to_float volumeL in
  span
    [ txt @@ Js.to_string number##toLocaleString ^ " "; icon ()]

(* To print statistics *)

(* amount expressed in 100_000 sub-unit of dn
    (alphanet = 1 kdun, zeronet=0.1 dun) *)
let amount_100000u f =
  let f = f /. 10_000. in
  if f < 0.01 then
    span [txt "0 "]
  else
    let m =
      if f < 10. then
        Printf.sprintf "%.2f " f
      else
      if f < 1_000. then
        Printf.sprintf "%.0f K" f
      else
        if f < 10_000. then
          Printf.sprintf "%.2f M" (f /. 1_000.)
        else
          if f < 100_000. then
            Printf.sprintf "%.1f M" (f /. 1_000.)
          else
            if f < 1_000_000. then
              Printf.sprintf "%.0f M" (f /. 1_000.)
            else
              if f < 10_000_000. then
                Printf.sprintf "%.2f B" (f /. 1_000_000.)
              else
                if f < 100_000_000. then
                  Printf.sprintf "%.1f B" (f /. 1_000_000.)
                else
                  Printf.sprintf "%.0f B" (f /. 1_000_000.)
    in
    span [txt m; icon()]

let with_usd ?precision ?width ?order price_usd dun =
  match price_usd with
  | None -> [ pp_amount ?precision ?width ?order dun ]
  | Some price_usd ->
    let number =
      Js.number_of_float @@
      (Int64.to_float dun *. price_usd /.
       Int64.to_float dun_units) in
    (* Display USD value only on mainnet *)
    pp_amount ?precision ?width ?order dun ::
    match Infos.net with
    | Infos.Mainnet ->
      [
        span ~a:[ a_class [ "usd-price" ] ] [
          txt " ($";
          txt @@ Js.to_string number##toLocaleString ;
          txt ")" ]
      ]
    | _ -> []

let catch_unit_z ?n ?nsymbol ?(decimals=6) symbol s =
  let n = Option.value ~default:(String.length s) n in
  let nsymbol = Option.value ~default:(String.length symbol) nsymbol in
  if n > nsymbol && String.sub s (n-nsymbol) nsymbol = symbol then
    Some (nsymbol, decimals)
  else None

(* let z_of_string decimals s =
 *   Z.of_float (float_of_string s *. (Float.pow 10. decimals)) *)

let z_of_string decimals s =
  let s = String.trim s in
  match String.split_on_char sep s with
  | [ s ] -> Some (Z.mul (Z.pow ten decimals) @@ Z.of_string s)
  | [ u; d] ->
    let u = Z.mul (Z.pow ten decimals) @@ Z.of_string u in
    let n = String.length d in
    let decimal_unit, d, round =
      let du = decimals - n in
      if du >= 0 then du, d, Z.zero
      else
        let round = if int_of_char (String.get d decimals) - 48 > 4 then Z.one else Z.zero in
        let d = String.sub d 0 decimals in
        0, d, round in
    let d = Z.add round (Z.mul (Z.pow ten decimal_unit) @@ Z.of_string d) in
    Some (Z.add u d)
  | _ -> None

let token_of_string ?(decimals=6) ?(catches=["u", 0; "m", 3]) symbol s =
  let s = String.lowercase_ascii @@ String.trim s in
  let symbol = String.lowercase_ascii symbol in
  let n = String.length s in
  let nsymbol = String.length symbol in
  let tr decimals s = Z.mul (Z.pow ten decimals) @@ Z.of_string (String.trim s) in
  let tr_sub decimals s n i = tr decimals (String.sub s 0 (n-i)) in
  let l = List.map (fun (c, decimals) ->
      catch_unit_z ~n ~nsymbol:(nsymbol + String.length c) ~decimals (c ^ symbol))
      catches @ [ catch_unit_z ~n ~nsymbol ~decimals symbol ] in
  let rec iter = function
    | [] -> tr 0 s
    | h :: t -> match h s with
      | Some (i, d) -> tr_sub d s n i
      | None -> iter t in
  iter l

let catch_unit_int64 ?n ?nsymbol ?(factor=1000000L) symbol s =
  let n = Option.value ~default:(String.length s) n in
  let nsymbol = Option.value ~default:(String.length symbol) nsymbol in
  if n > nsymbol && String.sub s (n-nsymbol) nsymbol = symbol then
    Some (nsymbol, factor)
  else None

let int64_of_string factor s =
  Int64.of_float (float_of_string s *. (Int64.to_float factor))

let dun_of_string ?(catches=["u", 1L; "m", 1000L]) s =
  let s = String.lowercase_ascii @@ String.trim s in
  let n = String.length s in
  let tr factor s = Int64.(mul factor @@ of_string (String.trim s)) in
  let tr_sub factor s n i = tr factor (String.sub s 0 (n-i)) in
  let l =
    List.map (fun (c, factor) ->
        catch_unit_int64 ~n ~nsymbol:(3 + String.length c) ~factor (c ^ "dun")) catches @
    List.map (fun (c, factor) ->
        catch_unit_int64 ~n ~nsymbol:(2 + String.length c) ~factor (c ^ "dn")) catches @
    [ catch_unit_int64 ~n ~nsymbol:3  "dun"; catch_unit_int64 ~n ~nsymbol:2  "dn" ] in
  let rec iter = function
    | [] -> tr_sub 1L s n 0
    | h :: t -> match h s with
      | Some (i, f) -> tr_sub f s n i
      | None -> iter t in
  iter l
