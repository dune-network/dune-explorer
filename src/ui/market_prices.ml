open Ezjs_tyxml
open Html

let select_id = "market-select"

let make () =
  div [
    div ~a:[ a_id select_id ] [];
    Charts_ui.make_chart_panel ()
  ]

let update ?origin xhr =
  Common.make_options ~title:"Origin" select_id
    [| "all"; "Coinmarketcap"; "Coingecko" |]
    (fun s ->
       let origin = match s with
         | "Coinmarketcap" -> Some "cmc"
         | "Coingecko" -> Some "coingecko"
         | _ -> origin in
       xhr ?origin ())
