(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Data_types

module Services = struct

  let info : (www_server_info, exn, EzAPI.no_security) EzAPI.service0 =
    EzAPI.service
      ~params:[]
      ~name:"info"
      ~output:Api_encoding.WWW.www_server_info
      EzAPI.Path.(root // "info.json?version=%{commit}" )

  let tokens : (string list, exn, EzAPI.no_security) EzAPI.service0 =
    EzAPI.service
      ~name:"tokens_json"
      ~output:Json_encoding.(list string)
      EzAPI.Path.(root // "tokens.json?version=%{commit}" )
end

let host =
  match Ezjs_loc.url () with
  | Url.Http hu ->
    Printf.sprintf "http://%s:%d" hu.Url.hu_host hu.Url.hu_port
  | Url.Https hu ->
    Printf.sprintf "https://%s:%d" hu.Url.hu_host hu.Url.hu_port
  | _ -> "https://dunscan.io"

let error f json_name status content =
  let content = match content with
    | None -> "network error"
    | Some content -> content in
  log "/%s.json: error %d: %s\n%!" json_name status content;
  f None


let info f =
  EzXhr.get0 (EzAPI.TYPES.BASE host) Services.info "www.info"
    ~error:(error f "info")
    (function
      | Error e -> error f "info" 42 (Some (Printexc.to_string e))
      | Ok info -> f (Some info)) ()

let tokens f =
  EzXhr.get0 (EzAPI.TYPES.BASE host) Services.tokens "www.tokens"
    ~error:(error f "tokens")
    (function
      | Error e -> error f "info" 42 (Some (Printexc.to_string e))
      | Ok tokens -> f (Some tokens)) ()
