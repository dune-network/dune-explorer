(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Ezjs_bs3.Icon
open Ezjs_bs3.Grid
open Ezjs_bs3.Panel
open Dune_types
open Data_types
open Text

let spf = Printf.sprintf

let cl_title = Common.responsive_column_title
let cl_icon = Common.responsive_title

type bl_filter = Bl_Txs | Bl_Del | Bl_Ori | Bl_Endt | Bl_Act | Bl_Man

let graph_id = "chain-graph"

let string_of_filter = function
    Bl_Txs -> "transaction"
  | Bl_Del -> "delegation"
  | Bl_Ori -> "origination"
  | Bl_Endt -> "endorsement"
  | Bl_Act -> "activation"
  | Bl_Man -> "manage-account"

let filter_of_string = function
    "transaction" -> Bl_Txs
  | "delegation" -> Bl_Del
  | "origination" -> Bl_Ori
  | "endorsement" -> Bl_Endt
  | "activation" -> Bl_Act
  | "manage-account" -> Bl_Man
  | _ -> Bl_Txs

let default_filter filters =
  match List.assoc_opt "default" filters with
  | Some filter -> filter_of_string filter
  | _ -> Bl_Txs

let is_active default exp =
  if default = exp then Tabs.Active else Tabs.Inactive

let mk_title icon title nb =
  span @@ Common.responsive_title_fun icon
    (Panel.title_nb title)
    (match nb with Some nb -> nb | None -> -1)

let tr_tab = Tabs.make
    (mk_title exchange_icon s_transactions) [
    "block-tab" ]

let del_tab = Tabs.make
    (mk_title handshake_icon s_delegations) [
    "block-tab" ]

let ori_tab = Tabs.make
    (mk_title link_icon s_originations) [
    "block-tab" ]

let endt_tab = Tabs.make
    (mk_title stamp_icon s_endorsements) [
    "block-tab" ]


let act_tab = Tabs.make
    (mk_title arrow_up_icon s_activations) [
    "block-tab" ]

let man_tab = Tabs.make
    (mk_title (fas_u "cog") s_manage_account) [
    "block-tab" ]

module PanelOptions = struct
  let options = Panels.{
      page_size = Common.big_panel_number;
      no_block = true;
      no_op = false;
      no_limit = true;
      no_internal = false;
      pan_hash = None;
      no_title = true
    }
end

module TransactionsPanel = Panels.TransactionsPanel(PanelOptions)
module DelegationsPanel = Panels.DelegationsPanel(PanelOptions)
module OriginationsPanel = Panels.OriginationsPanel(PanelOptions)
module ManageAccountPanel = Panels.ManageAccountPanel(PanelOptions)
module EndorsementsPanel = Panels.EndorsementsPanel(PanelOptions)
module ActivationsPanel = Panels.ActivationsPanel(PanelOptions)

let baker_id hash = Common.make_id "block-baker" hash
let endorsements_id hash = Common.make_id "block-endorsements" hash
let succ_id hash = Common.make_id "block-succ" hash
let internal_succ_id hash = Common.make_id "internal-block-succ" hash

(* Maker (empty) *)
let make_block_fetching () =  Lang.txt_t s_fetching_data

let make_transaction_view default =
  Tabs.make_content_panel tr_tab (is_active default Bl_Txs) @@
  TransactionsPanel.make ()

let make_delegations_view default =
  Tabs.make_content_panel del_tab (is_active default Bl_Del) @@
  DelegationsPanel.make ()

let make_originations_view default =
  Tabs.make_content_panel ori_tab (is_active default Bl_Ori) @@
  OriginationsPanel.make ()

let make_endorsements_view default =
  Tabs.make_content_panel endt_tab (is_active default Bl_Endt) @@
  EndorsementsPanel.make ()

let make_activations_view default =
  Tabs.make_content_panel act_tab (is_active default Bl_Act) @@
  ActivationsPanel.make ()

let make_manage_account_view default =
  Tabs.make_content_panel man_tab (is_active default Bl_Man) @@
  ManageAccountPanel.make ()

let make_summary_template =
  let mk_row_lbl4 lbl = Common.mk_row_lbl clg4 lbl in
  let mk_row_val8_pcd v = Common.mk_row_val clg8 [ txt v ] in
  let mk_row_val8_txtarea v =
    Common.mk_row_val clg8 [ textarea ~a:[ a_disabled () ] ( txt v ) ]
  in
  fun ?block ?level_details hash  ->
    let level, fitness, timestamp,
        _pred, link_pred, nonce, protocol, network, _baker, pow, validation_pass,
        priority, proto, data, signature, volume, fees, dist_lvl,
        rewards, included_endorsements =
      match block with
      | None ->
        Common.bullshit_s, Common.bullshit_s,
        Common.bullshit_s, Common.bullshit_s,
        Common.txt_ (), Common.bullshit_s,
        Common.bullshit_s, Common.bullshit_s,
        Common.bullshit_s, Common.bullshit_s,
        Common.bullshit_s, Common.bullshit_s,
        Common.bullshit_s, Common.bullshit_s,
        Common.bullshit_s, Common.txt_ (),
        Common.txt_ (), None, Common.txt_ (), Common.bullshit_s
      | Some block ->
        let link_pred =
          Common.make_link block.predecessor_hash
            ~path:(
               if block.distance_level = 0 && block.level > 0 then
                 string_of_int (block.level - 1)
               else
                 block.predecessor_hash
             ) in
        let cst = Infos.constants_opt ~level:block.level in
        let rewards = match cst with
          | None -> 0L
          | Some cst -> Infos.block_rewards cst block.endorsements_included block.priority in
        Common.safe_value @@ string_of_int block.level,
        Common.safe_value @@ string_of_int @@ Common.get_fitness block.fitness,
        Common.safe_value @@ Date.to_string block.timestamp,
        Common.safe_value @@ block.predecessor_hash,
        link_pred,
        Common.safe_value @@ block.commited_nonce_hash,
        Common.safe_value @@ block.protocol.proto_name,
        Common.safe_value @@ block.network,
        Common.safe_value @@ block.baker.pkh,
        Common.safe_value @@ block.pow_nonce,
        Common.safe_value @@ string_of_int block.validation_pass,
        Common.safe_value @@ string_of_int block.priority,
        Common.safe_value @@ string_of_int block.proto,
        Common.safe_value @@ block.data,
        Common.safe_value @@ block.signature,
        Dun.pp_amount block.volume,
        Dun.pp_amount block.fees,
        Some block.distance_level,
        Dun.pp_amount rewards,
        Common.safe_value @@ string_of_int block.endorsements_included
    in
    let row_level_label = mk_row_lbl4 "Level" in
    let row_level_value =
      div ~a:[ a_id Common.confirmation_blocks_id; a_class [ clg8; "value" ] ]
        [ txt level ] in
    let row_fitness_label = mk_row_lbl4 "Fitness" in
    let row_fitness_value = mk_row_val8_pcd fitness in
    let row_timestamp_label = mk_row_lbl4 "Timestamp" in
    let row_timestamp_value = mk_row_val8_pcd timestamp in
    let row_hash_label = Common.mk_row_lbl clg2 "Hash" in
    let cls = [ clg10; "value" ] in
    let cls =
      match dist_lvl with
        None -> cls
      | Some 0 -> "bg-block-main-chain" :: cls
      | Some _ -> "bg-block-alt-chain" :: cls
    in
    let row_hash_value = div ~a:[ a_class cls ] [ txt hash ] in
    let row_pred_label = Common.mk_row_lbl clg2 "Predecessor" in
    let row_pred_value = Common.mk_row_val clg10 [ link_pred  ] in
    let row_succ_label = Common.mk_row_lbl clg2 "Successor" in
    let row_succ_value =
      div ~a:[ a_class [ clg10; "value" ] ; a_id @@ internal_succ_id hash ]
        [ Common.txt_ () ] in
    let row_volume_label = mk_row_lbl4 "Volume" in
    let row_volume_value = Common.mk_row_val clg8 [ volume ] in
    let row_fees_label = mk_row_lbl4 "Fees"  in
    let row_fees_value = Common.mk_row_val clg8 [ fees ] in
    let row_uncles_label = mk_row_lbl4 "Uncles" in
    let row_uncles_value =
      div ~a:[ a_class [ clg8; "value" ] ] [
        span ~a:[ a_id @@ Common.block_uncles_id hash ]
          [ Common.txt_ () ] ] in
    let row_nonce_hash_label = mk_row_lbl4 "Nonce Hash" in
    let row_nonce_hash_value = mk_row_val8_txtarea nonce in
    let row_validation_pass_label = mk_row_lbl4 "Validation Pass" in
    let row_validation_pass_value = mk_row_val8_pcd validation_pass in
    let row_protocol_label = mk_row_lbl4 "Protocol" in
    let row_protocol_value = mk_row_val8_txtarea protocol in
    let row_network_label = mk_row_lbl4 "Network" in
    let row_network_value = mk_row_val8_pcd network in
    let row_pow_label = mk_row_lbl4 "POW Nonce" in
    let row_pow_value = mk_row_val8_pcd pow in
    let row_proto_label = mk_row_lbl4 "Proto Changes" in
    let row_proto_value = mk_row_val8_pcd proto in
    let row_priority_label = mk_row_lbl4 "Priority" in
    let row_priority_value = mk_row_val8_pcd priority in
    let row_data_label = mk_row_lbl4 "Data" in
    let row_data_value = mk_row_val8_txtarea data in
    let row_signature_label = mk_row_lbl4 "Signature" in
    let row_signature_value = mk_row_val8_txtarea signature in
    let row_rewards_label = mk_row_lbl4 "Rewards" in
    let row_rewards_value = Common.mk_row_val clg8 [ rewards ] in
    let row_included_endorsements_label = mk_row_lbl4 "Included Endorsements" in
    let row_included_endorsements_value = mk_row_val8_pcd included_endorsements in
    let row_cycle_label = mk_row_lbl4 "Cycle" in
    let row_cycle_position_label = mk_row_lbl4 "Cycle Position" in
    let row_cycle_value, row_cycle_position_value =
      match level_details with
      | None -> mk_row_val8_pcd Common.bullshit_s,
                mk_row_val8_pcd Common.bullshit_s
      | Some level_details ->
        let cst = Infos.constants_opt ~level:level_details.lvl_level in
        mk_row_val8_pcd @@ string_of_int level_details.lvl_cycle,
        mk_row_val8_pcd @@ Printf.sprintf
          "%d / %d" level_details.lvl_cycle_position
          (match cst with Some cst -> (cst.blocks_per_cycle - 1) | None -> 0) in
    let full =
      div ~a:[ a_class [ clg12 ] ] [
        div ~a:[ a_class [ row ] ]  [
          row_hash_label ; row_hash_value ;
          row_pred_label ; row_pred_value ;
          row_succ_label ; row_succ_value ] ] in
    let right =
      div ~a:[ a_class [ clg6 ] ] [
        div ~a:[ a_class [ row ] ]  [
          row_nonce_hash_label ; row_nonce_hash_value ;
          row_protocol_label ; row_protocol_value ;
          row_proto_label ; row_proto_value ;
          row_network_label ; row_network_value ;
          row_pow_label ; row_pow_value ;
          row_data_label ; row_data_value ;
          row_signature_label ; row_signature_value ;
          row_rewards_label; row_rewards_value ] ] in
    let left =
      div ~a:[ a_class [ clg6 ] ] [
        div ~a:[ a_class [ row ] ]  [
          row_timestamp_label ; row_timestamp_value ;
          row_volume_label ; row_volume_value ;
          row_fees_label ; row_fees_value ;
          row_uncles_label ; row_uncles_value ;
          row_level_label ; row_level_value ;
          row_cycle_label ; row_cycle_value ;
          row_cycle_position_label ; row_cycle_position_value ;
          row_fitness_label ; row_fitness_value ;
          row_validation_pass_label ; row_validation_pass_value ;
          row_priority_label ; row_priority_value ;
          row_included_endorsements_label; row_included_endorsements_value ] ] in
    [ full; left; right]

let make_block_summary ?block ?level_details hash =
  make_summary_template ?block ?level_details hash

let make_block_endorsements () =
  let slots = Common.make_slots 8 in
  let rows = Common.make_endorsements_slots slots in
  div [
    div ~a:[ a_class [ row; "endorsements-table" ] ]
      [ div ~a:[ a_class [ clg12 ] ] [ rows ]  ] ;
    Common.legend ()
  ]

(* Updater *)
let update_block_uncles block nb_uncles =
  let uncles = string_of_int nb_uncles in
  let to_update = find_component @@ Common.block_uncles_id block.hash in
  let content =
    if nb_uncles = 0 then txt uncles
    else
      let path = "heads" in
      let args = [ "level", string_of_int block.level ] in
      Common.make_link uncles ~args ~path
  in
  Manip.replaceChildren to_update [ content ]

let update_succ block succ_hash_opt =
  match succ_hash_opt with
    Some succ_hash -> (
      let href_succ, link_succ =
        if block.distance_level = 0 then
          string_of_int @@ block.level + 1,
          Common.make_link succ_hash
            ~path:(string_of_int ( block.level + 1 ) )
        else
          succ_hash,
          Common.make_link ~path:succ_hash succ_hash
      in

      (* update hyperlink for succ_id *)
      let button = find_component @@ succ_id block.hash in
      (To_dom.of_a button)##.href := Js.string href_succ ;
      Manip.removeClass button "disabled" ;

      (* update hyperlink and txt for internal_succ_id *)
      let container = find_component @@ internal_succ_id block.hash in
      Manip.removeChildren container ;
      Manip.appendChild container link_succ
    )
  | None -> ()

let update_block_baker =
  ref (fun (_services : Data_types.service list) (_block : block) -> ())

let update_block_transactions ~nrows xhr =
  TransactionsPanel.update ~urlarg:"btr" ~nrows xhr

let update_block_originations ~nrows xhr =
  OriginationsPanel.update ~urlarg:"bor" ~nrows xhr

let update_block_included_endorsements ~nrows xhr =
  EndorsementsPanel.update ~urlarg:"ben" ~nrows xhr

let update_block_delegations ~nrows xhr =
  DelegationsPanel.update ~urlarg:"bdl" ~nrows xhr

let update_block_activations ~nrows xhr =
  ActivationsPanel.update ~urlarg:"bac" ~nrows xhr

let update_block_manage_account ~nrows xhr =
  ManageAccountPanel.update ~urlarg:"bman" ~nrows xhr

let aria_hidden () = to_attrib @@ Xml.string_attrib "aria-hidden" "true"

let update_block_summary level_details block is_snapshot =
  let container = find_component @@ Common.summary_id block.hash in
  let link_pred =
    if block.level = 0 then
      a ~a:[ a_class [ "disabled" ] ] [
        span
          ~a:[
            a_class [ "glyphicon"; "glyphicon-triangle-left" ] ;
            aria_hidden () ] []
      ]
    else
      let href_pred =
        if block.distance_level = 0 && block.level <> 0 then
          Common.a_link (string_of_int ( block.level - 1 ))
        else
          Common.a_link block.predecessor_hash
      in
      a ~a:href_pred  [
        span
          ~a:[
            a_class [ "glyphicon"; "glyphicon-triangle-left" ] ;
            aria_hidden () ] []
      ] in
  let body_attr = if is_snapshot then
      [ a_class [ panel_body; "snapshot-block"];
        a_title "this block is a snapshot" ]
  else [ a_class [ panel_body ] ] in
  let to_update =
    div ~a:[ a_id @@ Common.summary_id block.hash;
             a_class [ panel; panel_primary ]] [
      div ~a:[ a_class [ panel_heading ] ] [
        div ~a:[ a_class [ row ] ] [
          h3 ~a:[ a_class [ clg12; panel_title; "no-overflow" ] ] ([
            span ~a:[ a_class [ "nav-level" ] ] [
              link_pred ;
              txt @@ string_of_int block.level ;
              a ~a:[ a_id @@ succ_id block.hash; a_class [ "disabled" ] ] [
                span
                  ~a:[
                    a_class [ "glyphicon"; "glyphicon-triangle-right" ] ;
                    aria_hidden () ] []
              ]
            ] ;
            Lang.txt_t s_block_information ;
            Glossary_doc.(help HBlock) ;
          ] @ (if is_snapshot then [space_icon (); camera_icon ()] else []))
        ]
      ] ;
      div ~a:body_attr
        (make_summary_template ~block ~level_details block.hash ) ] in
  Manip.removeChildren container ;
  Manip.appendChild container to_update

let update_block_endorsements level hash operations =
  let cst = Infos.constants_opt ~level in
  let container = find_component @@ endorsements_id hash in
  let ends = Utils.get_endorsements operations in
  let slots = Common.make_slots (match cst with Some cst -> cst.endorsers_per_block | None -> 0) in
  let slots = Common.update_endorsements_slots hash slots ends in
  let rows = Common.make_endorsements_slots slots in
  let endorsements_numbers =
    List.fold_left (fun acc (_op_hash, _op_block_hash, endorse) ->
        let nslot = List.length endorse.endorse_slot in
        if endorse.endorse_block_hash = hash then
          (nslot + fst acc, nslot + snd acc)
        else (fst acc, nslot + snd acc)) (0,0) ends in
  let to_update_heading =
    div ~a:[ a_class [ panel_heading ] ] [
      h3 ~a:[ a_class [ panel_title ] ] [
        txt @@ spf "%s (%d/%d)"
          (Lang.s_ "Endorsements of this block")
          (fst endorsements_numbers) (snd endorsements_numbers);
        Glossary_doc.(help HEndorsement)
      ] ] in
  let to_update_body =
    div [
      div ~a:[ a_id @@ endorsements_id hash;
               a_class [ row; "endorsements-table" ] ] [
        div ~a:[ a_class [ clg12 ] ] [ rows ]
      ] ;
      Common.legend () ] in
  Manip.removeChildren container ;
  Manip.appendChild container to_update_heading ;
  Manip.appendChild container to_update_body

let tabs = [
  tr_tab, Bl_Txs ;
  del_tab, Bl_Del ;
  ori_tab, Bl_Ori ;
  endt_tab, Bl_Endt ;
  act_tab, Bl_Act ;
  man_tab, Bl_Man
]

let make_tabs default =
  List.map (fun (tr, filter) -> tr, is_active default filter) tabs
  |> Tabs.(make_tabs Tabs)

let on_show ?(default=Bl_Txs) filter tab =
  Tabs.set_on_show tab (fun _ ->
      Common.set_url_arg "default" ~default:(string_of_filter default)
      @@ string_of_filter filter
    )

let update_tabs () =
    List.iter (fun (t, filter) ->
      on_show filter t
    ) tabs

let update_tabs_number ?(default=Bl_Txs)
    (_n_tot, n_tr, n_ori, n_dlg, _n_rvl, n_end, n_mac, _n_macs, _n_acp, n_act, _n_seed,
     _n_dbe, _n_dee) =
  Tabs.update_show_tab tr_tab n_tr ;
  Tabs.update_show_tab ori_tab n_ori ;
  Tabs.update_show_tab del_tab n_dlg ;
  Tabs.update_show_tab endt_tab n_end ;
  Tabs.update_show_tab act_tab n_act ;
  Tabs.update_show_tab man_tab n_mac ;
  List.fold_left2 (fun acc (t, filter) n ->
      match acc with
      | Some f, _ when f = filter -> if n <= 0 then None, Some t else None, None
      | Some f, before -> Some f, before
      | None, Some before ->
        if n > 0 then (Tabs.change_active before t; None, None)
        else None, Some before
      | None, None -> None, None) (Some default, None) tabs
    [ n_tr; n_dlg; n_ori; n_end; n_act; n_mac ] |> ignore

let list_partition f l =
  List.fold_left (fun acc x ->
      let r = f x in
      match List.assoc_opt r acc with
      | None -> (r, [ x ]) :: acc
      | Some l -> (r, x :: l) :: List.remove_assoc r acc) [] l

let list_index f l =
  let rec aux i = function
    | [] -> -1
    | h :: _t when f h -> i
    | _ :: t -> aux (i+1) t in
  aux 0 l

let make_json l =
  let s =
    Printf.sprintf "{%s}" @@
    String.concat ", " @@ List.map (fun (x, y) -> Printf.sprintf "\"%s\": \"%s\"" x y) l in
  Js._JSON##parse (Js.string s)


let update_chain_graph main_level length blocks =
  let diff = 50 in
  let size = 150 in
  let bls = list_partition (fun b -> b.level) blocks in
  let bls = List.sort (fun (level1, _) (level2, _) ->
      compare level2 level1) bls in
  let bls, _, _max_height = List.fold_left (fun (acc, preds, mh) (level, l) ->
      let lmain, lalt = List.partition (fun b ->
          not @@ List.exists (fun (hash, _) -> b.predecessor_hash = hash) preds) l in
      let lmain, preds_main, start = List.fold_left (fun (acc2, preds_main, i) b ->
          if b.distance_level = 0 then (0, b) :: acc2, preds_main, i
          else (i, b) :: acc2, (b.hash, i) :: preds_main, i + 1)
          ([], [], 1) lmain in
      let lalt = List.sort (fun b1 b2 ->
          compare
            (list_index (fun (hash, _) -> hash = b1.predecessor_hash) preds)
            (list_index (fun (hash, _) -> hash = b2.predecessor_hash) preds)) lalt in
      let lalt, preds_alt, max_height = List.fold_left (fun (acc2, preds_alt, i) b ->
          let i = match List.assoc_opt b.predecessor_hash preds with
            | None -> i
            | Some j -> max i j in
          (i, b) :: acc2, (b.hash, i) :: preds_alt, i + 1)
          ([], [], start) lalt in
      (level, List.rev lmain @ List.rev lalt) :: acc,
      List.rev preds_main @ List.rev preds_alt,
      max mh max_height
    ) ([], [], 0) bls in
  let props = List.fold_left (fun acc (level, l) ->
      let pos = (diff + size) * (length / 2 - main_level + level), - size in
      let node = Ezjs_cytoscape.node ~pos (string_of_int level) in
      (Js.Unsafe.coerce node##.data)##.color := Js.string "#add8e6";
      (Js.Unsafe.coerce node##.data)##.height := Js.string (string_of_int diff);
      (Js.Unsafe.coerce node##.data)##.label := Js.string @@ string_of_int level;
      let acc = node :: acc in
      List.fold_left (fun acc2 (height, b) ->
          let pos = (diff + size) * (length / 2 - main_level + level), (size + diff / 2) * height in
          let node = Ezjs_cytoscape.node ~pos b.hash in
          (Js.Unsafe.coerce node##.data)##.color :=
            if b.distance_level = 0 then Js.string "#0f0"
            else Js.string "#f00";
          (Js.Unsafe.coerce node##.data)##.height :=
            Js.string (string_of_int (int_of_float (0.9 *. float_of_int size)) ^ "px");
          (Js.Unsafe.coerce node##.data)##.label :=
            Js.string @@ Printf.sprintf "%s\n\npriority\n%d\nendorsements\n%d"
              (Common.crop_hash ~crop_len:8 b.hash) b.priority b.endorsements_included;
          if level = main_level - length / 2 then node :: acc2
          else node :: Ezjs_cytoscape.edge b.predecessor_hash b.hash :: acc2) acc l) [] bls in
  let style = Js.array [|
      object%js
        val selector = Js.string "node"
        val style = Js.Unsafe.inject @@ make_json [
            "width", string_of_int size ^ "px";
            "height", "data(height)";
            "label", "data(label)";
            "background-color", "data(color)";
            "font-size", string_of_int (size / 8) ^ "px";
            "shape", "rectangle";
            "text-valign", "center";
            "text-wrap", "wrap";
            "text-max-width", string_of_int (int_of_float (0.9 *. float_of_int size)) ^ "px"
          ]
      end |] in
  let g = Ezjs_cytoscape.mk_graph ~style ~props graph_id in
  g##.userZoomingEnabled := Js._false;
  g##.autoungrabify := Js._true;
  let cy = Ezjs_cytoscape.display g in
  Ezjs_cytoscape.on cy "click" "node"
    (fun e -> match Js.Opt.to_option e##.target with
       | None -> ()
       | Some node -> Common.change_page (Js.to_string (Js.Unsafe.coerce node)##id));
  let elt = Dom_html.getElementById (graph_id ^ "-container") in
  (Js.Unsafe.coerce elt)##scroll 500 0

let make_page hash filters =
  let default = default_filter filters in

  let summary =
    div ~a:[ a_id @@ Common.summary_id hash ; a_class [ clg12 ] ] [
      div ~a:[ a_class [ panel; panel_primary ]] [
        div ~a:[ a_class [ panel_heading ] ] [
          div ~a:[ a_class [ row ] ] [
            h3 ~a:[ a_class [ clg9; csm9; cxs9; panel_title ] ] [
              span ~a:[ a_class [ "nav-level" ] ] [
                a ~a:[ a_id @@ succ_id hash; a_class [ "disabled" ] ] [
                  span
                    ~a:[
                      a_class [ "glyphicon"; "glyphicon-triangle-left" ] ;
                      aria_hidden () ] []
                ] ;
                a ~a:[ a_class [ "disabled" ] ] [
                  span
                    ~a:[
                      a_class [ "glyphicon"; "glyphicon-triangle-right" ] ;
                      aria_hidden () ] []
                ]
              ] ;
            ] ;
            Common.make_loading_gif [ "loading-heading"; clg3; csm3; cxs3 ]
          ]
        ] ;
        div ~a:[ a_class [ panel_body ] ] (make_block_summary hash) ]
    ] in

  let transactions = make_transaction_view default in
  let delegations = make_delegations_view default in
  let originations = make_originations_view default in
  let endorsements = make_endorsements_view default in
  let activations = make_activations_view default in
  let manage_account = make_manage_account_view default in

  let baker =
    div ~a:[ a_class [ panel; panel_primary; "block-operations-panel" ]] [
      div ~a:[ a_class [ panel_heading ] ] [
        h3 ~a:[ a_class [ panel_title ] ] [
          Lang.txt_t s_baker
        ]
      ] ;
      p ~a:[ a_id @@ baker_id hash ; a_class [ "baker" ; "no-overflow" ]]
        [ Common.txt_ () ]
    ] in

  let bl_endorsements =
    div ~a:[ a_id @@ endorsements_id hash ;
             a_class [ panel; panel_primary; "block-operations-panel" ]] [
      div ~a:[ a_class [ panel_heading ] ] [
        div ~a:[ a_class [ row ] ] [
          h3 ~a:[ a_class [ clg9; csm9; cxs9; panel_title ] ] [
            txt @@ spf "%s (%s)"
              (Lang.s_ "Endorsements") Common.bullshit_s
          ] ;
          Common.make_loading_gif [ "loading-heading"; clg3; csm3; cxs3 ]
        ]
      ] ;
      make_block_endorsements ()
    ] in

  div ~a:[ a_class [ "block-div"; row ] ] [
    summary ;

    div ~a:[ a_class [ "block-operations" ] ] [
      div ~a:[ a_class [ clg9; cxs12 ] ] [
        make_tabs default;
        div ~a:[ a_class [ "tab-content" ] ] [
          transactions;
          delegations;
          originations;
          endorsements;
          activations;
          manage_account
        ]
      ];
      div ~a:[ a_class [ clg3; csm12; cxs12 ] ] [
        baker ;
        bl_endorsements ;

      ]
    ];
    div ~a:[ a_class [ cxs12 ] ] [
      div ~a:[ a_id (graph_id ^ "-container"); a_style "overflow: auto; margin-bottom: 20px; max-height: 2000px" ] [
        div ~a:[ a_id graph_id; a_style "min-height:500px; min-width:1600px; display:block"] []
      ]
    ]
  ]

let update_confirmation _bhash level nb_confirm =
  let confirm = find_component @@ Common.confirmation_blocks_id in
  Manip.replaceChildren confirm
    [ txt @@ string_of_int level ;
      txt @@ Printf.sprintf " (%d %s)" nb_confirm
        (Lang.s_ "block confirmations")]
