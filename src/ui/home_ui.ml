(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Ezjs_bs3.Icon
open Ezjs_bs3.Grid
open Ezjs_bs3.Color
open Ezjs_bs3.Panel
open Dune_types
open Data_types
open Lang
open Text
open Common

let home_blocks_id = "home-heads"
let home_last_baker_id = "home-last-baker"
let home_last_baker_logo_id = "home-last-baker-logo"
let home_last_baker_website_id = "home-last-baker-website"
let home_circulating_supply_id = "home-circulating-supply"
let home_marketcap_id = "home-marketcap"
let home_volume_id = "home-volume"
let home_usd_price_id = "home-dune-price-usd"
let home_btc_price_id = "home-dune-price-btc"
let home_last_block_id = "home-last-block-container"
let home_sponsored_id = "home-sponsored-case"
let home_change_24h_id = "home-change-24h"

let period_container_id = "period-container"
let period_title_id = "period-title"
let period_info_id = "period-info"
let cycle_container_id = "cycle-container"
let blocks_loading_id = "home-blocks-loading"

let start_block_id = "start-block"
let end_block_id = "end-block"
let progress_title_id = "progress-title"
let bar_id = "bar"
let bar_span_id = "bar-span"

let odometer_active_baker_rate_id = "odometer-baker-rate"

let odometer_manager_id = "odometer-man"
let odometer_endorsement_id = "odometer-end"
let odometer_rewards_id = "odometer-rew"
(* let odometer_transac_id = "odometer-tr"
 * let odometer_orig_id = "odometer-or"
 * let odometer_del_id = "odometer-del"
 * let odometer_act_id = "odometer-act" *)

let fan_end_id = "fan-endors"
let fan_block_id = "fan-block"
let fan_baking_rate_id = "fan-baking-rate"
let fan_activation_rate_id = "fan-activation-rate"

let color ?(limit=0.) f =
    if f > limit then
      [ a_class [green]; a_title (Printf.sprintf "value above %.2g%%" limit) ]
    else if f < -1. *. limit then
      [ a_class [red]; a_title (Printf.sprintf "value below -%.2g%%" limit) ]
    else
      [ a_class [blue]; a_title (Printf.sprintf "value between -%.2g%% and %.2g%%" limit limit) ]

let additional_divs = ref
    (div [] : (Html_types.div_content_fun elt))

(* Last blocks *)
let make_home_blocks blocks =
  let theads = tr [
      th @@ cl_icon_xs clock_icon (t_ s_age);
      th @@ cl_icon_xs cube_icon (t_ s_level);
      th ~a:[ a_class [ "hidden-xs" ] ] @@
      cl_icon_xs cookie_icon (t_ s_baker);
      th ~a:[ a_class [ "hidden-xs" ] ] @@
      cl_icon_xs (number_icon cube_icon) (t_ s_nbops);
      th @@ cl_icon_xs Dun.icon (t_ s_volume);
      th ~a:[ a_class [ "hidden-xl" ] ] @@ cl_icon Dun.icon (t_ s_fees);
    ] in
  tablex ~a:[ a_class [ "table" ] ] [
    tbody @@
    theads ::
    List.map (fun block ->
        let timestamp_str = Date.to_string block.timestamp in
        let td_timestamp = td ~a:[ a_class [ "bk-home-ts" ] ] [ ] in
        Manip.appendChild td_timestamp
          (Format_date.auto_updating_timespan timestamp_str);
        tr [
          td_timestamp ;
          td [ make_link @@ string_of_int block.level ] ;
          account_w_blockies
            ~aclass:["hidden-xs"; "no-overflow"]
            ~crop_len:15
            block.baker ;
          td ~a:[ a_class [ "hidden-xs" ] ]
            [ txt @@ string_of_int block.nb_operations ] ;
          td [ Dun.pp_amount ~width:3 block.volume ] ;
          td ~a:[ a_class [ "hidden-xl" ] ] [
            Dun.pp_amount ~width:3 block.fees ] ;
        ]
      ) blocks
  ]

let update_progress level =
  let bar = find_component bar_id in
  let cycle_position = level.lvl_cycle_position in
  let cst = Infos.constants ~level:level.lvl_level in
  let blocks_per_cycle = cst.blocks_per_cycle in
  let percent = (cycle_position + 1 )* 100 / blocks_per_cycle in
  Manip.SetCss.width bar (Printf.sprintf "%d%%" percent) ;
  let bar_span = find_component bar_span_id in
  let levels_left = cst.blocks_per_cycle - level.lvl_cycle_position in
  let time_between_blocks = List.hd cst.time_between_blocks in
  Manip.setInnerHtml bar_span
    (Printf.sprintf "%d%% Est. %s" percent
       (Format_date.time_before_level ~time_between_blocks levels_left));
  let component = find_component progress_title_id in
  let to_update =
    p [
      cube_icon () ; space_icon () ;
      txt @@
      t_subst s_subst_block_cycle
        (function
            "level" -> string_of_int level.lvl_level
          | "cycle" -> string_of_int level.lvl_cycle
          | _ -> "????")
    ]  in
  Manip.replaceChildren component [ to_update ];
  let start_block = find_component start_block_id in
  let end_block = find_component end_block_id in
  Manip.setInnerHtml start_block @@
  string_of_int (level.lvl_level - level.lvl_cycle_position);
  Manip.setInnerHtml end_block @@
  string_of_int (level.lvl_level - level.lvl_cycle_position + blocks_per_cycle - 1);
  ()

let update_blocks blocks =
  let block_table = make_home_blocks blocks in
  let content_div = find_component home_blocks_id in
  begin try
      let loading_div = find_component blocks_loading_id in
      Manip.removeSelf loading_div;
    with _ -> ()
  end ;
  Manip.removeChildren content_div;
  Manip.appendChild content_div block_table

let get_sponsored : (Data_types.service list -> Data_types.service list) ref =
  ref (fun _ -> [])

let get_ads : (Data_types.service list -> Data_types.service list) ref =
  ref (fun _ -> [])

let update_baker services baker =
  let container = find_component home_last_baker_id in
  let sponsors = !get_sponsored services in
  let website_container = find_component home_last_baker_website_id in
  let logo_container = find_component home_last_baker_logo_id in
  let sponsor =
    List.find_opt (fun b ->
        match b.srv_dn1 with
        | None -> false
        | Some dn1 -> dn1 = baker.pkh)
      sponsors in
  begin
    match sponsor with
    | None ->
      let new_logo =
        Blockies.create ~scale:16 baker.pkh in
      hide website_container ;
      (* Manip.replaceChildren
       *   website_container [ a [ fas "cookie-bite" ]]; *)
      Manip.replaceChildren logo_container [ new_logo ]
    | Some sponsor ->
      show website_container ;
      Manip.replaceChildren
        website_container [ a ~a:[ a_href sponsor.srv_url] [ fas "link" ]];
      let new_logo =
        a ~a:[ a_href sponsor.srv_url ]
          [ img ~src:(img_path sponsor.srv_logo) ~alt:sponsor.srv_name () ] in
      Manip.replaceChildren logo_container [ new_logo ]
  end ;
  Manip.replaceChildren container [ make_link_account ~crop_len:30 baker ]

let update_ads services =
  let container = find_component home_sponsored_id in
  let ads =
    List.filter (fun s -> match s.srv_sponsored with
        | Some tsp when time_diff tsp > 0. -> true
        | _ -> false ) (!get_ads services) in
  let minutes = (new%js Js.date_now)##getMinutes in
  let hours = (new%js Js.date_now)##getHours in
  let winner = (minutes + ( 60 * hours )) mod (List.length ads) in
  let ad =
    try
      List.nth (List.rev ads) winner
    with _ -> assert false
  in
  let new_content =
    match ad.srv_descr with
    | None ->
      div [
        a ~a:[ a_href ad.srv_url ]
          [ img ~src:(img_path ad.srv_logo) ~alt:ad.srv_name () ]
      ]
    | Some descr ->
      div [
        span ~a:[ a_class [ clg4 ; csm3; cxs2 ;
                            "hidden-xxs" ] ] [
          a ~a:[ a_href ad.srv_url ]
            [ img ~src:(img_path ad.srv_logo) ~alt:ad.srv_name () ]
        ] ;
        span_of_html ~classes:[ clg6 ; csm9; cxs10; "home-sponsored-descr" ]
          descr
      ] in
  Manip.replaceChildren container [ new_content ] ;
  Manip.replaceChildren (find_component "home-sponsored-title")
    [ a ~a:[ a_href ad.srv_url ] [ txt "Sponsored" ] ]

let update_supply circulating_supply =
  set_child home_circulating_supply_id @@
  h3 [ Dun.pp_amount ~width:5 ~precision:2 circulating_supply ]

let update_marketcap circulating_supply price_usd =
  set_child home_marketcap_id @@
  h3 [ Dun.pp_amount ~width:3 ~precision:2 ~icon:Dun.dollar @@
       Int64.(of_float @@ (to_float circulating_supply) *. price_usd)  ]

let update_price_usd price_usd change_1h =
  let up_icon () = fas "arrow-up" in
  set_child home_usd_price_id @@
  h3 [ span [
      txtf "%.3f" price_usd ;
      space () ;
      Dun.dollar () ;
      space () ;
      span ~a:(a_style "font-size:13px" :: color ~limit:0.25 change_1h ) [
        (if change_1h < 0. then down_icon () else up_icon ());
        space ();
        txt @@ Printf.sprintf "%.2f%%" change_1h ]]]

let update_price_btc price_btc change_1h =
  set_child home_btc_price_id @@
  h3 [ span ~a:(color ~limit:0.25 change_1h) [
      Dun.pp_amount_z ~decimals:10 ~icon:(fun () -> span [ txt "BTC" ]) @@
      Z.of_int64 @@ Int64.of_float (price_btc *. 10_000_000_000.) ] ]

let update_volume volume_24h =
  set_child home_volume_id @@
  h3 [ Dun.pp_amount ~width:4 ~precision:2 ~icon:Dun.dollar
         (Int64.of_float @@ volume_24h *. 1_000_000.) ]

let update_supply_market market supply =
  update_supply supply.current_circulating_supply;
  update_marketcap supply.current_circulating_supply market.gk_price.gk_usd;
  update_price_usd market.gk_price.gk_usd market.gk_1h.gk_usd;
  update_price_btc market.gk_price.gk_btc market.gk_1h.gk_btc;
  update_volume market.gk_market_volume.gk_usd

let update_fan h24 =
  let end_rate = int_of_float h24.h24_end_rate in
  let blocks_rate = int_of_float h24.h24_block_0_rate in
  let baking_rate = int_of_float h24.h24_baking_rate in
  let exec_end =
    Printf.sprintf "var bar = document.getElementById('%s').ldBar; bar.set(%d)"
      fan_end_id
      end_rate in
  let exec_blocks =
    Printf.sprintf "var bar = document.getElementById('%s').ldBar; bar.set(%d)"
      fan_block_id
      blocks_rate in
  let exec_baking_rate =
    Printf.sprintf "var bar = document.getElementById('%s').ldBar; bar.set(%d)"
      fan_baking_rate_id
      baking_rate in
  ignore (Js.Unsafe.eval_string exec_end) ;
  ignore (Js.Unsafe.eval_string exec_blocks) ;
  ignore (Js.Unsafe.eval_string exec_baking_rate)

let init_fan () =
  let fan_end = Printf.sprintf "var bar=new ldBar(\".%s\",{});" fan_end_id in
  let fan_bl = Printf.sprintf "var bar=new ldBar(\".%s\",{});" fan_block_id in
  let fan_br = Printf.sprintf "var bar=new ldBar(\".%s\",{});" fan_baking_rate_id in
  ignore (Js.Unsafe.eval_string fan_end) ;
  ignore (Js.Unsafe.eval_string fan_bl) ;
  ignore (Js.Unsafe.eval_string fan_br)

let make_fan percent fan_init_class =
  let set_fan bar =
    let barjs = To_dom.of_element bar in
    barjs##setAttribute (Js.string "data-value")
      (Js.string @@ string_of_int percent) ;
    barjs##setAttribute (Js.string "data-preset")
      (Js.string "circle") in
  let pb =
    div ~a:[ a_id fan_init_class ;
             a_class [ fan_init_class; "ldBar"; "label-center"; "auto" ] ] [] in
  ignore (set_fan pb);
  pb

let make_sm_fan ?color percent fan_init_class =
  let set_fan bar =
    let barjs = To_dom.of_element bar in
    barjs##setAttribute (Js.string "data-value")
      (Js.string @@ string_of_int percent) ;
    barjs##setAttribute (Js.string "style")
      (Js.string "width:50%; height:50%");
    begin match color with
      | Some color ->
        barjs##setAttribute (Js.string "data-stroke")
          (Js.string color);
      | None -> () end ;
    barjs##setAttribute (Js.string "data-preset")
      (Js.string "circle") in
  let pb =
    div ~a:[ a_id fan_init_class ;
             a_class [ fan_init_class; "ldBar"; "label-center"; "auto" ] ] [] in
  ignore (set_fan pb);
  pb

let update_odometer h24 =
  let od_manager = find_component odometer_manager_id in
  let od_endorsement = find_component odometer_endorsement_id in
  let od_rewards = find_component odometer_rewards_id in
  (* let od1 = find_component odometer_transac_id in
   * let od2 = find_component odometer_orig_id in
   * let od3 = find_component odometer_del_id in
   * let od4 = find_component odometer_act_id in *)
  let od_staking_ratio = find_component odometer_active_baker_rate_id in
  Manip.setInnerHtml od_manager @@ string_of_int @@
  h24.h24_transactions + h24.h24_originations + h24.h24_delegations;
  Manip.setInnerHtml od_endorsement @@ string_of_int h24.h24_endorsements;
  Manip.setInnerHtml od_rewards @@ Int64.(to_string @@
  div h24.h24_rewards Dun.dun_units);
  (* Manip.setInnerHtml od2 @@ string_of_int h24.h24_originations ;
   * Manip.setInnerHtml od3 @@ string_of_int h24.h24_delegations ;
   * Manip.setInnerHtml od4 @@ string_of_int h24.h24_activations ; *)
  Manip.setInnerHtml od_staking_ratio @@ string_of_int h24.h24_active_baker

let init_odometer () =
  let od_manager = To_dom.of_element @@ find_component odometer_manager_id in
  let od_endorsement = To_dom.of_element @@ find_component odometer_endorsement_id in
  let od_rewards = To_dom.of_element @@ find_component odometer_rewards_id in
  (* let od1 = To_dom.of_element @@ find_component odometer_transac_id in
   * let od2 = To_dom.of_element @@ find_component odometer_orig_id in
   * let od3 = To_dom.of_element @@ find_component odometer_del_id in
   * let od4 = To_dom.of_element @@ find_component odometer_act_id in *)
  let od5 = To_dom.of_element @@ find_component odometer_active_baker_rate_id in
  ignore @@ Ezjs_odometer.Odometer.odometer od_manager;
  ignore @@ Ezjs_odometer.Odometer.odometer od_endorsement;
  ignore @@ Ezjs_odometer.Odometer.odometer od_rewards;
  (* ignore @@ Odometer.odometer od1 ;
   * ignore @@ Odometer.odometer od2 ;
   * ignore @@ Odometer.odometer od3 ;
   * ignore @@ Odometer.odometer od4 ; *)
  ignore @@ Ezjs_odometer.Odometer.odometer od5

let update_h24 h24 =
  update_odometer h24 ;
  update_fan h24

let color_pc value threshold =
  if value >= threshold then green
  else red

(* let make_quorum_panel turn_out expected_quorum smajority =
 *   let period_quorum_fan_id = "period-quorum-fan-id" in
 *   let period_majo_fan_id = "period-majo-fan-id" in
 *   let to_color = color_pc turn_out expected_quorum in
 *   let turn_out = int_of_float turn_out in
 *   let sm_color = color_pc smajority 80. in
 *   let majo = int_of_float smajority in
 *   let fan_quorum =
 *     div ~a:[ a_class [ "period-fan"; to_color ] ] [
 *       make_sm_fan turn_out ~color:to_color period_quorum_fan_id ] in
 *   let fan_majo =
 *     div ~a:[ a_class [ "period-fan"; sm_color ] ] [
 *       make_sm_fan majo ~color:sm_color period_majo_fan_id ] in
 *   let quorum =
 *     div [
 *       fan_quorum ;
 *       div ~a:[ a_class [ "sub-sm-fan" ] ] [
 *         span [ txt @@ Printf.sprintf "Threshold %.1f%%" expected_quorum ] ] ;
 *     ] in
 *   let majo =
 *     div [
 *       fan_majo ;
 *       div ~a:[ a_class [ "sub-sm-fan" ] ] [
 *         span [ txt @@ Printf.sprintf "Threshold %.0f%%" 80. ] ] ;
 *     ] in
 *   [ period_quorum_fan_id ; period_majo_fan_id ],
 *   div ~a:[ a_class [ "hidden-xs" ; cxs4 ; csm4 ] ] [ quorum ],
 *   div ~a:[ a_class [ "hidden-xs" ; cxs4 ; csm4 ] ] [ majo ] *)

(* let make_period_sum_div sum =
 *   let period_progress_fan_id = "period-p-fan-id" in
 *   let cst = Infos.constants ~level:sum.sum_level in
 *   let start_level = sum.sum_period * cst.blocks_per_voting_period + 1 in
 *   let end_level = (sum.sum_period + 1) * cst.blocks_per_voting_period in
 *   let period_position = sum.sum_level - start_level + 1 in
 *   let time_between_blocks = List.hd cst.time_between_blocks in
 *   let est_time =
 *     if end_level - sum.sum_level = 0 then ""
 *     else
 *       "~ " ^
 *       (Format_date.time_before_level ~time_between_blocks (end_level - sum.sum_level)) ^
 *       " left" in
 *   let percent = period_position * 100 / cst.blocks_per_voting_period in
 *   let fan_progress =
 *     div ~a:[ a_class [ "period-fan" ] ] [
 *       make_sm_fan percent period_progress_fan_id ] in
 *   let progress =
 *     div ~a:[ a_class [ cxs12; csm4 ] ] [
 *       fan_progress ;
 *       div ~a:[ a_class [ "sub-sm-fan" ] ] [
 *         span [ txt @@ Printf.sprintf "%s" est_time ] ] ;
 *     ] in
 *   let fans, div_info1, div_info2 =
 *     match sum.sum_period_info with
 *     | Sum_proposal_empty ->
 *       [],
 *       div ~a:[ a_class [ "hidden-xs" ; cxs8 ; csm8 ] ] [ span [ txt "No proposal yet" ] ],
 *       div []
 *     | Sum_proposal (hash, count, pc) ->
 *       let number_div =
 *         div [
 *           div [
 *             span ~a:[ a_class [ "period-sum-prop-number" ] ] [
 *               txt @@ string_of_int count ] ] ;
 *           div [
 *             span ~a:[ a_class [ "sub-sm-fan" ] ] [ txt "proposals" ] ]
 *         ]
 *       in
 *       let link = match List.assoc_opt hash !Proposals_ui.archive_links with
 *         | Some (name, path) -> Common.make_link ~crop_len:20 ~path name
 *         | None -> Common.txt_ () in
 *       let pc_div =
 *         div [
 *           div [
 *             span ~a:[ a_class [ "period-sum-prop-number"; green ] ] [
 *               txt @@ Printf.sprintf "%.0f%%" pc ]
 *           ] ;
 *           div [
 *             span ~a:[ a_class [ "sub-sm-fan" ] ] [ link ] ]
 *         ]
 *       in
 *       [],
 *       div ~a:[ a_class [ "hidden-xs" ; cxs4 ; csm4 ] ] [ number_div],
 *       div ~a:[ a_class [ "hidden-xs" ; cxs4 ; csm4 ] ] [ pc_div ]
 *     | Sum_testing_vote (turn_out, expected_quorum, smajority) ->
 *       make_quorum_panel turn_out expected_quorum smajority
 *     | Sum_testing ->
 *       [],
 *       div ~a:[ a_class [ "hidden-xs" ; cxs8 ; csm8 ] ] [ span [ txt "Currently in testing" ] ],
 *       div []
 *     | Sum_promo (turn_out, expected_quorum, smajority) ->
 *       make_quorum_panel turn_out expected_quorum smajority
 *   in
 *   period_progress_fan_id :: fans, div [ progress ; div_info1 ; div_info2 ] *)

(* let update_current_period period_sum =
 *   let container = find_component period_container_id in
 *   let title_div = find_component period_title_id in
 *   let str = Utils.voting_period_of_summary period_sum.sum_period_info in
 *   let p_title_str = Printf.sprintf "Period %d : %s" period_sum.sum_period str in
 *   let xs_p_title_str = Printf.sprintf "%d : %s" period_sum.sum_period str in
 *   let new_title = [
 *     span ~a:[ a_class [ "hidden-xs" ] ] [
 *       fas "vote-yea" ; space_icon () ;
 *       a ~a:(a_link "/proposals") [ txt p_title_str ] ];
 *     span ~a:[ a_class [ "visible-xs" ] ] [
 *       fas "vote-yea" ; space_icon () ;
 *       a ~a:(a_link "/proposals") [ txt xs_p_title_str ] ];
 *   ]
 *   in
 *   let fans, to_update = make_period_sum_div period_sum in
 *   Manip.removeChildren container ;
 *   Manip.removeChildren title_div ;
 *   List.iter (Manip.appendChild title_div) new_title ;
 *   Manip.appendChild container to_update ;
 *   List.iter (fun fid ->
 *       let fan = Printf.sprintf "var bar=new ldBar(\".%s\",{});" fid in
 *       ignore (Js.Unsafe.eval_string fan)
 *     ) fans *)

let make_stats () =
  let marketcap_title =
    h2 [
      chart_line_icon () ;
      space_icon () ;
      a ~a:(a_link "https://coinmarketcap.com/currencies/dune-network") [txt_t s_market_cap] ;
    ] in
  let total_circulating_title =
    h2 ~a:[ a_title "Circulating Supply" ] [
      Dun.icon ();
      space_icon ();
      a ~a:(a_link "/supply") [ txt_t s_supply ] ] in
  let volume_title =
    h2 [
      fas "dollar-sign";
      space_icon () ;
      a ~a:(a_link "https://coinmarketcap.com/currencies/dune-network") [txt "Volume 24h"]
    ] in
  let usd_price_title =
    h2 ~a:[ a_title "Dune USD Price (last hour)" ] [
      fas "dollar-sign";
      space_icon () ;
      a ~a:(a_link "/market_prices") [txt "Dune USD Price"]
    ] in
  let btc_price_title =
    h2 [
      fab "btc" ;
      space_icon () ;
      a ~a:(a_link "/market_prices") [txt "Dune BTC Price"]
    ] in
  let progress_title =
    div ~a:[ a_id progress_title_id ] [
      span [
        cube_icon () ;
        space_icon () ;
        txt_t s_block
      ]
    ] in
  (* let period_title =
   *   h2 ~a:[ a_id period_title_id ] [
   *     span ~a:[ a_class [ "hidden-xs" ] ] [
   *       fas "vote-yea" ; space_icon () ;
   *       txt_t s_dunscan_period ];
   *     span ~a:[ a_class [ "visible-xs" ] ] [
   *       fas "vote-yea" ; space_icon () ;
   *       txt_t s_dunscan_period_xs ];
   *   ] in *)
  let last_baker_title =
    h2 [
      cookie_icon () ;
      space_icon () ;
      txt "Latest Baker"
    ] in

  let sponsored_title =
    h2 [
      fas "star" ;
      space_icon () ;
      span ~a:[ a_id "home-sponsored-title" ] [ txt "Sponsored" ]
    ] in
  let _period_div =
    div ~a:[ a_id period_container_id ] [] in
  let start_block = div ~a:[a_class [clg3 ; cxs3]; a_id start_block_id] [] in
  let cycle_container =
    div ~a:[ a_class ([clg6; cxs6] @ [ "no-overflow" ]); a_id cycle_container_id ] [
      div ~a:[ a_class [ "progress" ] ] [
        div ~a:[ a_id bar_id; a_class ["progress-bar"; "progress-bar-striped"];
                 a_role [ "progressbar" ] ] [
          span ~a:[ a_id bar_span_id ] []]]] in
  let end_block = div ~a:[a_class [clg2 ; cxs3]; a_id end_block_id ] [] in

  let panel_class = [ "home-panel" ] in
  (* Panels *)
  let market_supply_panels = [
    div ~a:[ a_class [ csm1; "hidden-xs"] ] [ ] ;
    div ~a:[ a_class [ clg2 ; csm5; cxs6] ] [
      make_panel
        ~panel_class
        ~panel_title_content:(total_circulating_title)
        ~panel_body_content:[
          div ~a:[ a_id home_circulating_supply_id ] [ h3 [txt "0"] ] ] ()
    ] ;
    div ~a:[ a_class [ clg2 ; csm5 ; cxs6 ] ] [
      make_panel
        ~panel_class
        ~panel_title_content:(marketcap_title)
        ~panel_body_content:[ div ~a:[ a_id home_marketcap_id] [ h3 [txt "0"] ] ] ()
    ] ;
    div ~a:[ a_class [ clg2 ; csm5; cxs6 ; clgoffset0 ; csmoffset1 ; cxsoffset0 ] ] [
      make_panel
        ~panel_class
        ~panel_title_content:(usd_price_title)
        ~panel_body_content:[ div ~a:[ a_id home_usd_price_id ] [ h3 [txt "0"] ] ] ()
    ] ;
    div ~a:[ a_class [ clg2 ; csm5 ; cxs6 ] ] [
      make_panel
        ~panel_class
        ~panel_title_content:(btc_price_title)
        ~panel_body_content:[ div ~a:[ a_id home_btc_price_id ] [ h3 [txt "0"] ] ] ()
    ] ;
    div ~a:[ a_class [ clg2 ; "hidden-md"; "hidden-sm" ; "hidden-xs"] ] [
      make_panel
        ~panel_class
        ~panel_title_content:(volume_title)
        ~panel_body_content:[ div ~a:[ a_id home_volume_id ] [ h3 [txt "0"] ] ] ()
    ] ;
    div ~a:[ a_class [ csm1; "hidden-xs"] ] [ ]
  ] in

  div ~a:[ a_class [ row ] ] (
    (match Infos.net with Infos.Mainnet -> market_supply_panels | _ -> []) @ [
      (* new row *)
      div ~a:[ a_class [ clg4 ; csm6; cxs12] ] [
        make_panel
          ~panel_class
          ~panel_title_content:(
            div [ progress_title ])
          ~panel_body_content:[
            div ~a:[ a_class ["no-overflow" ]; a_id home_last_block_id ]
              [ start_block ; cycle_container ; end_block ] ] ()
      ] ;
      div ~a:[ a_class [ clg4 ; csm6; cxs12] ] [
        make_panel
          ~panel_class
          ~panel_title_content:(last_baker_title)
          ~panel_body_content:[
            div [
              div ~a:[ a_class [ csm2; "hidden-xs" ; cxsoffset1 ] ;
                       a_id home_last_baker_logo_id ] [ ] ;
              div ~a:[ a_class [ csm7 ; cxs9 ; "no-overflow"; csmoffset0 ; cxsoffset1 ] ;
                       a_id home_last_baker_id ]  [ txt "--" ] ;
              div ~a:[ a_class [ cxs1; "hidden-sm"; "hidden-xs" ] ;
                       a_id home_last_baker_website_id ]  [ space () ] ;
            ]
          ] ()
      ] ;
      div ~a:[ a_class [ clg4; clgoffset0; csm6; csmoffset3; cxs12] ] [
        make_panel
          ~panel_class:("home-sponsored" :: panel_class)
          ~panel_title_content:(sponsored_title)
          ~panel_body_content:[ div ~a:[ a_id home_sponsored_id ] [ txt "--" ] ] ()
      ]
    ])

let make_page () =
  (* Stats *)
  let stats_div =
    div [ make_stats () ] in

  (* Blocks  *)
  let block_div =
    div ~a:[ a_id home_blocks_id ;
             a_class [ "blocks-div"; csm12 ] ] [] in

  (* Latests Blocks/Transactions *)
  div ~a:[ a_class [ "summary" ] ] [
    stats_div ;
    div ~a:[ a_class [row] ] [
      div ~a:[ a_class [ clg6; cxs12 ] ] [
        div ~a:[ a_class [ row ] ] [
          div ~a:[ a_class [ clg11; "section-title" ] ]
            [ cubes_icon () ; space_icon () ;
              txt_t s_blocks ; Glossary_doc.(help HBlock);
              span [a ~a:(a_link ~aclass:[ "paginate"; "ntm" ] "/blocks")
                      [ txt_t s_view_all ]
                   ]
            ] ;
          make_home_loading_gif blocks_loading_id [ cxs12 ];
          block_div;
        ]
      ] ;
      div ~a:[ a_class [ clg6; cxs12 ] ] [
        div ~a:[ a_class [ row ] ] [

          div ~a:[ a_class [ cxs12; "section-title" ] ]
            [ chart_line_icon () ; space_icon () ;
              txt_t s_last_24h ;
            ] ;

          div ~a:[ a_class [ cxs6; "stat-item" ] ] [
            div [ h4 [ txt_t s_endorsement_rate ] ] ;
            make_fan 0 fan_end_id ] ;
          div ~a:[ a_class [ cxs6; "stat-item" ] ] [
            div [ h4 [ txt_t s_block_prio_0_baked ] ] ;
            make_fan 0 fan_block_id ] ;

          div ~a:[ a_class [ cxs6; cmd4; "stat-item"; "odometer-item" ] ] [
            a ~a:(a_link "transactions") [
              div [ h4 [ txt_t s_operations] ] ;
              div ~a:[ a_id odometer_manager_id ;
                       a_class [ "odometer"; "odometer-theme-train-station" ] ] [
                txt "0"] ;
            ]
          ] ;
          div ~a:[ a_class [ cxs6; cmd4; "stat-item"; "odometer-item" ] ] [
            a ~a:(a_link "endorsements") [
              div [ h4 [ txt_t s_endorsements] ] ;
              div ~a:[ a_id odometer_endorsement_id ;
                       a_class [ "odometer"; "odometer-theme-train-station" ] ] [
                txt "0"] ;
            ]
          ] ;
          div ~a:[ a_class [ cxs12; cmd4; "stat-item"; "odometer-item" ] ] [
            a ~a:(a_link "bakers") [
              div [ h4 [ txt_t s_rewards] ] ;
              div ~a:[ a_id odometer_rewards_id ;
                       a_class [ "odometer"; "odometer-theme-train-station" ] ] [
                txt "0"] ;
              Dun.icon ();
            ]
          ] ;

          (* div ~a:[ a_class [ cmd3 ; cxs6; "stat-item"; "odometer-item" ] ] [
           *   a ~a:(a_link "originations") [
           *     div [ h4 [ txt_t s_originations] ] ;
           *     div ~a:[ a_id odometer_orig_id ;
           *              a_class [ "odometer"; "odometer-theme-train-station" ] ] [
           *       txt "0"] ;
           *   ]
           * ] ;
           * div ~a:[ a_class [ cmd3; cxs6; "stat-item"; "odometer-item" ] ] [
           *   a ~a:(a_link "delegations") [
           *     div [ h4 [ txt_t s_delegations ] ] ;
           *     div ~a:[ a_id odometer_del_id ;
           *              a_class [ "odometer"; "odometer-theme-train-station" ] ] [
           *       txt "0"] ;
           *   ]
           * ] ;
           * div ~a:[ a_class [ cmd3; cxs6 ; "stat-item"; "odometer-item" ] ] [
           *   a ~a:(a_link "activations") [
           *     div [ h4 [ txt_t s_activations ] ] ;
           *     div ~a:[ a_id odometer_act_id ;
           *              a_class [ "odometer"; "odometer-theme-train-station" ] ] [
           *       txt "0"] ;
           *   ]
           * ] ; *)
          div ~a:[ a_class [ cxs12; "section-title" ] ]
            [ chart_line_icon () ; space_icon () ;
              txt_t s_last_snapshot ] ;

          div ~a:[ a_class [ cxs6; "stat-item" ] ] [
            div [ h4 [ txt_t s_staking_ratio ] ] ;
            make_fan 0 fan_baking_rate_id
          ] ;

          div ~a:[ a_class [ cxs6; "stat-item" ] ] [
            a ~a:(a_link "rolls-distribution") [
              div [ h4 [ txt_t s_roll_owners ] ] ;
              div ~a:[ a_id odometer_active_baker_rate_id ;
                       a_class [ "odometer"; "odometer-theme-train-station" ] ] [
                txt "0"] ;
            ]
          ] ;

        ]
      ]
    ]
  ]
