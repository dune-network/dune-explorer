(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ezjs_tyxml
open Html
open Data_types
open Lang
open Text

(* Home page *)
let home () =
  (* Home page with 10 last block *)
  (* Proposals_ui.load_archive_links (); *)
  let home_div = Home_ui.make_page () in
  Common.update_main_content home_div;
  Home_ui.init_fan () ;
  Home_ui.init_odometer () ;
  Common.do_and_update_every 30 Api_request.Home.request;
  Common.do_and_update_every 30 Api_request.Home.h24_stats
  (* Common.do_and_update_every 60 Api_request.Home.current_period_sum *)
  (* Api_request.Home.mini_stats () *)

let block_hash hash args =
  let content = Block_ui.make_page hash args in
  Common.update_main_content content;
  Api_request.Block.request hash

let operations hash =
  let content = Operation_ui.make_page hash in
  Common.update_main_content content;
  Api_request.Operation.request hash (Ezjs_loc.args ())

let account hash args =
  let is_token = List.mem hash !Common.tokens in
  let content = Account_ui.make_page ~is_token hash args
      ~csv_xhr:(Api_request.CSV.transactions hash) in
  Common.update_main_content content;
  Api_request.Account.request ~is_token hash (Ezjs_loc.args ())

let glossary () = Glossary_doc.make_doc ()

let blocks () =
  let content = Blocks_ui.make_blocks () in
  Common.update_main_content content;
  Common.do_and_update_every 60 Api_request.Blocks.request

let snapshot_blocks () =
  let content = Blocks_ui.make_snapshot_blocks () in
  Common.update_main_content content;
  Api_request.Blocks.snapshot_blocks ()

let baking_rights () =
  let content = Blocks_ui.make_baking_rights () in
  Common.update_main_content content;
  Blocks_ui.filter_handler (Api_request.Search.search_account Blocks_ui.update_filter)
    Api_request.Blocks.request_baking_rights;
  Api_request.Blocks.request_baking_rights ()

let heads () =
  let content = Blocks_ui.Heads.make () in
  Common.update_main_content content;
  Common.do_and_update_every 60 Api_request.Heads.request

let accounts () =
  let content = Accounts_ui.make_accounts () in
  Common.update_main_content content;
  Api_request.Accounts.request ~contract:false ()

let top_accounts () =
  let content = Top_accounts_ui.make_page () in
  Common.update_main_content content;
  Api_request.Top_accounts.request ()

let contracts () =
  let content = Accounts_ui.make_accounts ~contract:true () in
  Common.update_main_content content;
  Api_request.Accounts.request ~contract:true ()

let transactions ~refresh ~pending =
  let content = Operations_ui.TransactionsPanel.make () in
  Common.update_main_content content;
  Common.do_and_update_every refresh
    (fun () -> Api_request.Operations.Transactions.request ~pending)

let activations ~refresh ~pending =
  let content = Operations_ui.ActivationsPanel.make () in
  Common.update_main_content content;
  Common.do_and_update_every refresh
    (fun () -> Api_request.Operations.Activations.request ~pending)

let endorsements ~refresh ~pending =
  let content = Operations_ui.EndorsementsPanel.make () in
  Common.update_main_content content;
  Common.do_and_update_every
    refresh (fun () -> Api_request.Operations.Endorsements.request ~pending)

let delegations ~refresh ~pending =
  let content = Operations_ui.DelegationsPanel.make () in
  Common.update_main_content content;
  Common.do_and_update_every
    refresh (fun () -> Api_request.Operations.Delegations.request ~pending)

let originations ~refresh ~pending =
  let content = Operations_ui.OriginationsPanel.make () in
  Common.update_main_content content;
  Common.do_and_update_every
    refresh (fun () -> Api_request.Operations.Originations.request ~pending)

let manage_account ~refresh ~pending =
  let content = Operations_ui.ManageAccountPanel.make () in
  Common.update_main_content content;
  Common.do_and_update_every
    refresh (fun () -> Api_request.Operations.Manage_account.request ~pending)

let double_baking ~refresh ~pending =
  let content = Operations_ui.DoubleBakingsPanel.make () in
  Common.update_main_content content;
  Common.do_and_update_every
    refresh (fun () -> Api_request.Operations.Double_baking.request ~pending)

let double_endorsement ~refresh ~pending =
  let content = Operations_ui.DoubleEndorsementsPanel.make () in
  Common.update_main_content content;
  Common.do_and_update_every
  refresh (fun () ->
	Api_request.Operations.Double_endorsement.request ~pending)

let nonces () =
  let content = Operations_ui.Nonces.make () in
  Common.update_main_content content;
  Common.do_and_update_every 300 Api_request.Operations.Nonces.request

let network_stats () =
  let content = Network_stats_ui.make_panel () in
  Common.update_main_content content;
  Api_request.Network.request ()

let charts_bakers () =
  let content = Charts_ui.make_bakers_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_bakers ()

let charts_blocks_per_day () =
  let content = Charts_ui.make_blocks_per_day_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_blocks_per_day ()

let charts_delay_per_day () =
  let content = Charts_ui.make_delay_per_day_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_delay_per_day ()

let charts_priorities_per_day () =
  let content = Charts_ui.make_priorities_per_day_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_priorities_per_day ()

let charts_bakers_per_day () =
  let content = Charts_ui.make_bakers_per_day_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_bakers_per_day ()

let charts_operations_per_day () =
  let content = Charts_ui.make_operations_per_day_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_operations_per_day ()

let charts_operations_per_block_per_day () =
  let content = Charts_ui.make_operations_per_block_per_day_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_operations_per_block_per_day ()

let charts_fees_per_day () =
  let content = Charts_ui.make_chart_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_fees_per_day ()

let charts_volume_per_day () =
  let content = Charts_ui.make_chart_panel () in
  Common.update_main_content content;
  Api_request.Charts.request_volume_per_day ()

let health_stats () =
  let content = Health_stats_ui.make_page () in
  Common.update_main_content content;
  Api_request.Health.request ()

let context_stats () =
  let content = Context_stats_ui.make_page () in
  Common.update_main_content content;
  Api_request.Context.request ()

let rolls_distrib () =
  let content = Rolls_distribution_ui.make_page () in
  Common.update_main_content content;
  Api_request.Rolls_distribution.request ()

let not_found hash =
  Ezjs_xhr.get "Content"
    ("/pages/" ^ hash ^ ".html")
    ~error:(fun _code ->
        let content = Search.not_found hash in
        Common.update_main_content content)
    (fun res ->
       let content_div = Manip.by_id "content" in
       match content_div with
       | None -> log "no content div found"
       | Some content_div -> Manip.setInnerHtml content_div res
    )

let protocols () =
  let content = Protocols_ui.make () in
  Common.update_main_content content;
  Api_request.Protocols.protocols_request ()

let market_prices () =
  let content = Market_prices.make () in
  Common.update_main_content content;
  let origin = Ezjs_loc.find_arg "origin" in
  Market_prices.update ?origin Api_request.Charts.request_market_prices

let redoc () =
  let doc = Dom_html.document in
  let s = doc##createElement (Js.string "script") in
  s##setAttribute (Js.string "src") (Js.string "https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js");
  Dom.addEventListener s
    (Dom.Event.make "load")
    (Dom.handler (fun _e ->
         let redoc_div = div ~a:[ a_id "redoc-div" ] [] in
         Common.update_main_content redoc_div;
         Redoc.init "dunscan_openapi.json" redoc_div |> ignore;
         Js._true))
    Js._true |> ignore;
  Manip.appendChild
    (Of_dom.of_body Dom_html.document##.body)
    (Of_dom.of_element s)


let apps () =
  let content = Apps_ui.make () in
  Common.update_main_content content

let constants args =
  let level, protocol = List.fold_left (fun (level, protocol) (k, v) ->
      match k with
      | "level" -> int_of_string_opt v, protocol
      | "protocol" -> level, Some v
      | _ -> level, protocol) (None, None) args in
  Common.update_main_content @@
  Protocols_ui.make_constants ?level ?protocol Api_request.Protocols.constants_request;
  Api_request.Protocols.constants_request ?level ?protocol ()

let advanced_search () =
  let content = Adv_search_ui.make_adv_search
      Api_request.Search.advanced_request_from_filters in
  Common.update_main_content content;
  Api_request.Search.advanced_request ()

let supply args =
  let level = Option.map int_of_string @@ List.assoc_opt "level" args in
  let content = Supply_ui.make ?level () in
  Common.update_main_content content;
  Api_request.Supply.request ?level ()

(* try path functions *)
let try_level path =
  try
    let level = int_of_string path in
    Common.block_hash_level level
  with Failure _f -> not_found path

let dispatch path =
  log "Serving %S.." (String.concat "/" path);
  Search.clear_input ();
  match path with
  (* PATHS *)
  | [] -> home ()
  | [path] ->
    begin
      match path with
      | "" -> home ()
      | "workers-status"
      | "api" -> redoc ()
      | "glossary" -> glossary ()
      | "blocks" -> blocks ()
      | "baking-rights" -> baking_rights ()
      | "heads" -> heads ()
      | "contracts" -> contracts ()
      | "context" -> context_stats ()
      | "accounts" -> accounts ()
      | "top" -> top_accounts ()
      | "activations" -> activations ~refresh:30 ~pending:false
      | "transactions" -> transactions ~refresh:30 ~pending:false
      | "endorsements" -> endorsements ~refresh:60 ~pending:false
      | "delegations"  -> delegations  ~refresh:60 ~pending:false
      | "originations" -> originations ~refresh:60 ~pending:false
      | "manage_account" -> manage_account ~refresh:60 ~pending:false
      | "double-baking" -> double_baking ~refresh:60 ~pending:false
      | "double-endorsement" -> double_endorsement ~refresh:60 ~pending:false
      | "pending-activations" -> activations ~refresh:10 ~pending:true
      | "pending-transactions" -> transactions ~refresh:10 ~pending:true
      | "pending-endorsements" -> endorsements ~refresh:10 ~pending:true
      | "pending-delegations"  -> delegations  ~refresh:10 ~pending:true
      | "pending-originations" -> originations ~refresh:10 ~pending:true
      | "pending-double-baking" -> double_baking ~refresh:10 ~pending:true
      | "pending-double-endorsement" -> double_endorsement ~refresh:10 ~pending:true
      | "nonces" -> nonces ()
      | "network" -> network_stats ()
      | "charts_bakers" | "bakers" -> charts_bakers ()
      | "blocksperday" -> charts_blocks_per_day ()
      | "delayperday" -> charts_delay_per_day ()
      | "prioperday" -> charts_priorities_per_day ()
      | "bakersperday" -> charts_bakers_per_day ()
      | "opsperday" -> charts_operations_per_day ()
      | "opsperblock" -> charts_operations_per_block_per_day ()
      | "feesperday" -> charts_fees_per_day ()
      | "volumeperday" -> charts_volume_per_day ()
      | "health" -> health_stats ()
      | "rolls-distribution" -> rolls_distrib ()
      | "snapshot-blocks" -> snapshot_blocks ()
      | "protocols" -> protocols ()
      | "market_prices" -> market_prices ()
      | "apps" -> apps ()
      | "constants" -> constants (Ezjs_loc.args ())
      | "advanced_search" -> advanced_search ()
      | "supply" -> supply (Ezjs_loc.args ())
      | _ ->
        (* Specific hashes *)
        match String.get path 0 with
        | 'o' | 'O' -> operations path
        | 'b' | 'B' -> block_hash path (Ezjs_loc.args ())
        | _ ->
          if String.length path = 36 then
            match String.sub path 0 2 with
            | "dn" | "KT" -> account path (Ezjs_loc.args ())
            | "tz" ->
              log "test %s %s" path (Crypto.tz_to_dn path);
              account (Crypto.tz_to_dn path) (Ezjs_loc.args ())
            | _ -> try_level path
          else
            try_level path
    end
  | _ -> not_found (Ezjs_loc.path_string ())

let () = Common.link_dispatcher := dispatch


let switch_button _services =
  let network = Infos.api.api_config.conf_network in
  let button = find_component (network ^ "-button") in
  Manip.addClass button "btn-primary"

let setup_page _services =
  Dom_html.window##.document##.title := Js.string (t_ s_dunscan_title);
  let cookies = Ezjs_cookie.all () in
  begin
    let theme = Option.value ~default:!Common.theme (List.assoc_opt "theme" cookies) in
    let logo_img = Common.logo_src theme in
    (Common.get_img_by_id "logo")##.src := Js.string logo_img;
  end;
  (* begin translations *)
  begin
    let span = find_component "search-go" in
    Manip.replaceChildren span [ txt_t s_go ]
  end;
  begin
    let input = find_component "search" in
    Js.Opt.case
      (Dom_html.CoerceTo.input
         (Manip.get_elt "input" input))
      (fun () -> assert false)
      (fun input ->
         input##.placeholder := Js.string (t_ s_search_by_address))
  end;
  (* end translations *)
  begin
    let span = find_component "network-buttons" in
    Manip.removeChildren span ;
    List.iter (fun (network, link) ->
        let network_id = network ^ "-button" in
        let ele = a ~a:[ a_class ["btn"; "btn-default"; "btn-network"];
                         a_id network_id ;
                         a_href link] [txt network]
        in
        Manip.appendChild span ele ;
      ) Infos.www.www_networks;
  end;
  Ezjs_xhr.get "footer" Infos.www.www_footer (fun res ->
      (Common.get_div_by_id "footer")##.innerHTML := Js.string res)

let run () =
  Common.dispatch ( Ezjs_loc.path () )

let runner = ref run

let () =
  Common.register_redraw "setup" setup_page;
  Common.register_redraw "switch-button" switch_button;
  Common.register_redraw "search" (
    fun () -> Search.search_handler Api_request.Search.request Api_request.Search.adv_search);
  Ezjs_xhr.get "Getting Menu" "menu.json"
    (fun menu ->
       let menu = EzEncoding.destruct Menu.menu_encoding menu in
       Common.register_redraw ~before:"login-button" "menu" (Menu.create menu))

(*
   If `!Common.api_host` is set, it means that the API_HOST was hardcoded
  in the sources, and should be used, unless another API server is specified
  in the URL argument. In this case, we only download `info.json` for its
  language part.

   If !Common.api_host is not set, we need to download `info.json` before
  any prior call to the API node.
*)

let register_service_worker () =
  Ezjs_push.registration "./sw.js?version=%{commit}" Ezjs_push.update_worker

let init () =
  EzXhr.init ();
  register_service_worker ();
  Common.register_redraw "runner" !runner;
  Ezjs_onload.add (fun () ->
      Www_request.info (function
          | None ->
            log "Fatal error: could not download 'info.json'";
          | Some info ->
            log "'info.json' loaded";
            Infos.www.www_currency_name <- info.www_currency_name;
            Infos.www.www_currency_short <- info.www_currency_short;
            Infos.www.www_currency_symbol <- info.www_currency_symbol;
            Infos.www.www_languages <- info.www_languages ;
            Infos.www.www_footer <- info.www_footer ;
            Infos.www.www_logo <- info.www_logo ;
            Infos.www.www_networks <- info.www_networks ;
            Infos.www.www_themes <- info.www_themes ;
            Infos.www.www_recaptcha_key <- info.www_recaptcha_key ;
            Infos.www.www_csv_server <- info.www_csv_server ;
            Infos.www.www_charts_server <- info.www_charts_server ;
            begin
              match Infos.www.www_auth with
              | Some _ -> ()
              | None ->
                Infos.www.www_auth <- info.www_auth ;
            end;
            begin
              match !Common.api_host with
              | Some _ -> ()
              | None ->
                Infos.www.www_apis <- info.www_apis ;
                Common.set_api_node () ;
            end;
            Www_request.tokens (function tokens ->
                (match tokens with None -> () | Some tokens -> Common.tokens := tokens);
                Api_request.Server.info Common.initial_redraw
              )
        )
    )
