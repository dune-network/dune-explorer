(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Json_encoding
type address = {url: string; port: int}

let debug fmt = Utils.debug !Debug_constants.debug_config fmt

let address_encoding =
  conv
    (fun {url; port} -> (url, port, None))
    (fun (url, port, _pref) -> {url; port})
    (obj3
       (req "url" string)
       (req "port" int)
       (opt "pref" string))


let all_addresses = ref []
let api_addresses = ref [||]
let services_filename = ref None
let secret_key = ref None
let csv_dir = ref None
let crawler_flags = Hashtbl.create 20

type config = {
  name : string ;
  crawler : address list ;
  api : address list ;
  services_filename : string option ;
  data_path : string option ;
  recaptcha_secret_key : string option;
  csv_dir : string option;
  include_flags : string list option; (* included are loaded before excluded *)
  exclude_flags : string list option;
}

let config_encoding =
  conv
    (fun { name; crawler; api; services_filename; data_path;
           recaptcha_secret_key; csv_dir; include_flags; exclude_flags }
      -> ( name, crawler, api, services_filename, data_path, None, None,
           recaptcha_secret_key, csv_dir, include_flags, exclude_flags ))
    (fun ( name, crawler, api, services_filename ,data_path,
           _max_default_blocks, _max_default_operations, recaptcha_secret_key ,
           csv_dir, include_flags, exclude_flags )
      -> { name; crawler; api; services_filename; data_path;
           recaptcha_secret_key; csv_dir; include_flags; exclude_flags }) @@
  EzEncoding.obj11
    (req "name" string)
    (req "crawler" (list address_encoding))
    (req "api" (list address_encoding))
    (opt "services" string)
    (opt "data" string)
    (opt "max_default_blocks" int)
    (opt "max_default_operations" int)
    (opt "recaptcha_secret_key" string)
    (opt "csv_dir" string)
    (opt "includes" (list string))
    (opt "excludes" (list string))

let configs_encoding = list config_encoding

let configs_example = [
  { name = "betanet-main";
    crawler = [ { url = "http://localhost"; port = 18732 } ];
    api = [
      { url = "http://another-host"; port = 18732 };
    ];
    services_filename = None ;
    data_path = None;
    recaptcha_secret_key = None;
    csv_dir = None;
    include_flags = None;
    exclude_flags = None
  }
]

let () =
  Printexc.register_printer (fun exn ->
      match exn with
      | Json_encoding.Cannot_destruct (path, exn) ->
        let s = Printf.sprintf "Cannot destruct JSON (%s, %s)"
            (Json_query.json_pointer_of_path path)
            (Printexc.to_string exn)
        in
        Some s
      | _ -> None)

let error msg =
  Printf.eprintf "Fatal error: %s\n%!" msg;
  Printf.eprintf "Configuration file should look like:\n%s\n%!"
    (EzEncoding.construct ~compact:false configs_encoding configs_example) ;
  exit 1

let load_json_config encoding filename =
  try
    let ic = open_in filename in
    let json = Ezjsonm.from_channel ic in
    close_in ic ;
    destruct encoding json
  with
  | exn ->
    Printf.eprintf "Fatal error while reading %S:\n  %s\n%!"
      filename (Printexc.to_string exn);
    exit 2

let parse_config file =
  let file = match file with
    | None -> error "No config file specified"
    | Some f when not (Sys.file_exists f) ->
      error "Config file does not exist"
    | Some f -> f in
  load_json_config configs_encoding file

let load_config_api ?(crawler="main") file =
  let wanted_name = Printf.sprintf "%s-%s" DunscanConfig.database crawler in
  Printf.eprintf "Looking for config %S\n%!" wanted_name;
  let config =
    List.find_opt (fun c -> c.name = wanted_name)
      (parse_config file) in
  match config with
  | None -> error "No config file specified"
  | Some c ->
    debug "[Config] Running network : %S\n%!" c.name ;
    let crawler_addresses =
      List.map (fun {url;port} ->
          ("crawler", Printf.sprintf "%s:%d" url port)) c.crawler
    in
    let addresses =
      let addresses =
        List.map (fun {url;port} ->
            ("balance", Printf.sprintf "%s:%d" url port)) c.api
      in
      all_addresses := addresses @ crawler_addresses;
      match addresses with
      | [] -> crawler_addresses
      | _ -> addresses
    in
    services_filename := c.services_filename ;
    api_addresses := Array.of_list addresses;
    secret_key := c.recaptcha_secret_key;
    csv_dir := c.csv_dir;
    begin
      match !api_addresses with
      | [||] -> error "No node addresses for balance queries"
      | _ -> ()
    end

let flag_operation_type = [
  "seed_nonce_revelation"; "double_baking_evidence"; "double_endorsement_evidence";
  "activation"; "endorsement"; "proposal"; "ballot"; "transaction"; "origination";
  "delegation"; "reveal"; "activate_protocol"; "manage_accounts"; "manage_account" ]

let flag_other = ["operation" ; "block_operation"; "pending"; "counts";
	          "balance_updates"; "account_info"; "token" ]

let flag_names = flag_other @ flag_operation_type

let include_crawler_flag s =
  Hashtbl.add crawler_flags s true;
  if List.mem s flag_operation_type then (
    Hashtbl.add crawler_flags "block_operation" true;
    Hashtbl.add crawler_flags "operation" true);
  if s = "operation" || s = "pending" then
    Hashtbl.add crawler_flags "block_operation" true;
  if s = "token" then (
    Hashtbl.add crawler_flags "block_operation" true;
    Hashtbl.add crawler_flags "operation" true;
    Hashtbl.add crawler_flags "transaction" true;
    Hashtbl.add crawler_flags "origination" true);
  if s = "account_info" then (
    Hashtbl.add crawler_flags "block_operation" true;
    Hashtbl.add crawler_flags "operation" true;
    Hashtbl.add crawler_flags "origination" true;
    Hashtbl.add crawler_flags "activation" true;
    Hashtbl.add crawler_flags "delegation" true;
    Hashtbl.add crawler_flags "reveal" true;
    Hashtbl.add crawler_flags "manage_account" true)


let exclude_crawler_flag s =
  Hashtbl.add crawler_flags s false;
  if s = "operation" || s = "block_operation" then (
    List.iter (fun s2 -> Hashtbl.add crawler_flags s2 false) flag_operation_type;
    Hashtbl.add crawler_flags "token" false;
    Hashtbl.add crawler_flags "account_info" false);
  if s = "transaction" || s = "origination" then (
    Hashtbl.add crawler_flags "token" false;
    Hashtbl.add crawler_flags "account_info" false);
  if s = "transaction" || s = "origination" || s = "activation" || s = "delegation"
     || s = "reveal" || s = "manage_account" then
    Hashtbl.add crawler_flags "account_info" false

let remove_crawler_flag s =
  Hashtbl.remove crawler_flags s

let include_all_crawler_flags () =
  List.iter (fun s -> Hashtbl.add crawler_flags s true) flag_names

let get_config_crawler ~crawler file =
  let wanted_name = Printf.sprintf "%s-%s" DunscanConfig.database crawler in
  Printf.eprintf "Looking for config %S\n%!" wanted_name;
  let config =
    List.find_opt (fun c -> c.name = wanted_name)
      (parse_config (Some file)) in
  match config with
  | None -> error "No config file specified"
  | Some c ->
    debug "[Config] Running network : %S\n url %s\n%!"
      c.name
      (String.concat " "
         (List.map (fun { url ; port; _} ->
              Printf.sprintf "%s:%d" url  port) c.crawler)) ;
    (* load crawler_flags *)
    begin match c.include_flags with
      | None -> include_all_crawler_flags ()
      | Some l -> List.iter include_crawler_flag l end;
    begin match c.exclude_flags with
      | None -> ()
      | Some l -> List.iter exclude_crawler_flag l end;
    { Data_types.nodes =
        List.map (fun {url; port; _} ->
            { Data_types.address = url; port }) c.crawler;
      data_path = c.data_path ;
    }

let get_addresses () = !all_addresses
let get_services_filename () = !services_filename
let get_api_address () =
  let api_addresses = !api_addresses in
  snd api_addresses.(Random.int (Array.length api_addresses))

let get_secret_key () = !secret_key
let get_csv_dir () = !csv_dir
