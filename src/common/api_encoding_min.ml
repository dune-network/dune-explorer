(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Json_encoding
open Data_types_min

module type Base_sig = sig
  val int64 : int64 encoding
  val int : int encoding
  val z_encoding : Z.t encoding
  val hex_encoding : Hex.t encoding
  val top_option : 'a encoding -> 'a option encoding
  val ign : 'a encoding -> 'a encoding
  module Date : sig
    val encoding : Date.t encoding
  end
  module Script :sig
    val expr_encoding : script_expr_t encoding
    val encode : script_expr_t -> string
    val decode : string -> script_expr_t
    val expr_str_encoding : string encoding
    val script_encoding : (script_expr_t option * script_expr_t * string option) encoding
    val script_str_encoding : script encoding
    val encoding_from_str : script encoding
    val parameters_encoding : extended_script encoding
    val encode_parameters : extended_script -> string
    val decode_parameters : string -> extended_script
    val parameters_str_encoding : string encoding
  end
end

module Base : Base_sig = struct
  let int64 = EzEncoding.int64
  let int = EzEncoding.int
  let z_encoding =
    def "zarith"
      ~title: "Big number"
      ~description: "Decimal representation of a big number" @@
    conv Z.to_string Z.of_string string
  let hex_encoding =
    conv (fun h -> Hex.show h) (fun s -> `Hex s) string

  let top_option encoding =
    union [
      case encoding
        (fun x -> x)
        (fun x -> Some x);
      case empty
        (function None -> Some () | Some _ -> None)
        (fun () -> None) ]

  let ign encoding = conv
    (fun x -> x, ()) (fun (x, _) -> x) @@ merge_objs encoding unit

  module Date = struct
    let encoding = conv Date.to_string Date.from_string string
  end

  module Script = struct
    include Dune_encoding_min.Script
    let encoding_from_str =
      conv
        (fun {sc_code; sc_storage; sc_code_hash} -> (sc_code, sc_storage, sc_code_hash))
        (fun (sc_code, sc_storage, sc_code_hash) -> {sc_code; sc_storage; sc_code_hash})
        (obj3
           (opt "code" string)
           (req "storage" string)
           (opt "code_hash" string))
  end

end

module type MinV_sig = sig
  module Error : sig val encoding : op_error encoding end
  val account_name_encoding : account_name encoding
  val origination_encoding : bool -> origination encoding
end

open Base

module Error = struct
  let optional =
    merge_objs
      (EzEncoding.obj12
         (opt "contract_handle" string)
         (opt "contract" string)
         (opt "balance" int64)
         (opt "amount" int64)
         (opt "incompatible_protocol_version" string)
         (opt "missing_key" (list string))
         (opt "function" string)
         (opt "existing_key" (list string))
         (opt "corrupted_data" (list string))
         (opt "hash" string)
         (opt "expectedKinds" (list string))
         (opt "wrongKind" string))
      unit

  let encode_error e = EzEncoding.construct optional e
  let decode_error s = EzEncoding.destruct optional s

  let encoding =
    conv
      (fun {err_kind; err_id; err_info} -> (err_kind, err_id), err_info)
      (fun ((err_kind, err_id), err_info) -> {err_kind; err_id; err_info}) @@
    merge_objs
      (obj2
         (req "kind" string)
         (req "id" string))
      (conv decode_error encode_error optional)
end

module MinV1 : MinV_sig = struct
  module Error = Error
  let account_name_encoding =
    def "account_name"
      ~title:"Account Name"
      ~description:"Address and alias of an account" @@
    conv
      (fun {pkh; alias} -> (pkh, alias))
      (fun (pkh, alias) -> {pkh; alias})
      (obj2
         (req "tz" string)
         (opt "alias" string))

  let origination_encoding micheline =
    let script_encoding =
      if micheline then Script.script_str_encoding
      else Script.encoding_from_str in
    def "origination"
      ~title:"Origination"
      ~description:"Dune origination" @@
    conv
      (fun { or_src ; or_manager ; or_delegate ; or_script ; or_spendable ;
             or_delegatable ; or_balance ; or_counter ; or_fee ;
             or_gas_limit ; or_storage_limit ; or_kt1 ;
             or_failed ; or_internal ; or_burn; or_op_level;
             or_timestamp; or_errors } ->
        ("origination", or_src, or_manager, or_balance, or_spendable,
         or_delegatable, or_delegate, or_script, or_kt1,
         or_failed, or_internal, or_burn,
         or_counter, or_fee, or_gas_limit, or_storage_limit,
         or_op_level, or_timestamp, or_errors))
      (fun (_k, or_src, or_manager, or_balance, or_spendable,
            or_delegatable, or_delegate, or_script, or_kt1,
            or_failed, or_internal, or_burn,
            or_counter, or_fee, or_gas_limit, or_storage_limit,
            or_op_level, or_timestamp, or_errors) ->
        { or_src; or_manager; or_delegate; or_script; or_spendable;
          or_delegatable; or_balance; or_counter; or_fee; or_gas_limit;
          or_storage_limit; or_kt1; or_failed; or_internal; or_burn;
          or_op_level; or_timestamp; or_errors})
      (EzEncoding.obj19
         (req "kind" string)
         (req "src" account_name_encoding)
         (req "managerPubkey" account_name_encoding)
         (req "balance" int64)
         (dft "spendable" bool false)
         (dft "delegatable" bool false)
         (dft "delegate" account_name_encoding {pkh=""; alias=None})
         (opt "script" script_encoding)
         (dft "tz1" account_name_encoding {pkh=""; alias=None})
         (req "failed" bool)
         (req "internal" bool)
         (req "burn_dun" int64)
         (req "counter" int32)
         (req "fee" int64)
         (req "gas_limit" z_encoding)
         (req "storage_limit" z_encoding)
         (req "op_level" int)
         (req "timestamp" string)
         (opt "errors" (list Error.encoding))
      )
end

module MinV4 : MinV_sig = struct
  module Error = Error
  let account_name_encoding =
    def "account_name"
      ~title:"Account Name"
      ~description:"Address and alias of an account" @@
    conv
      (fun {pkh; alias} -> (pkh, alias))
      (fun (pkh, alias) -> {pkh; alias})
      (obj2
         (req "pkh" string)
         (opt "alias" string))

  let origination_encoding micheline =
    let script_encoding =
      if micheline then Script.script_str_encoding
      else Script.encoding_from_str in
    def "origination"
      ~title:"Origination"
      ~description:"Dune origination" @@
    conv
      (fun { or_src ; or_manager ; or_delegate ; or_script ; or_spendable ;
             or_delegatable ; or_balance ; or_counter ; or_fee ;
             or_gas_limit ; or_storage_limit ; or_kt1 ;
             or_failed ; or_internal ; or_burn; or_op_level;
             or_timestamp; or_errors } ->
        ("origination", or_src, or_manager, or_balance, or_spendable,
         or_delegatable, or_delegate, or_script, or_kt1,
         or_failed, or_internal, or_burn,
         or_counter, or_fee, or_gas_limit, or_storage_limit,
         or_op_level, or_timestamp, or_errors))
      (fun (_k, or_src, or_manager, or_balance, or_spendable,
            or_delegatable, or_delegate, or_script, or_kt1,
            or_failed, or_internal, or_burn,
            or_counter, or_fee, or_gas_limit, or_storage_limit,
            or_op_level, or_timestamp, or_errors) ->
        { or_src; or_manager; or_delegate; or_script; or_spendable;
          or_delegatable; or_balance; or_counter; or_fee; or_gas_limit;
          or_storage_limit; or_kt1; or_failed; or_internal; or_burn;
          or_op_level; or_timestamp; or_errors})
      (EzEncoding.obj19
         (req "kind" string)
         (req "src" account_name_encoding)
         (req "managerPubkey" account_name_encoding)
         (req "balance" int64)
         (dft "spendable" bool false)
         (dft "delegatable" bool false)
         (dft "delegate" account_name_encoding {pkh=""; alias=None})
         (opt "script" script_encoding)
         (dft "kt1" account_name_encoding {pkh=""; alias=None})
         (req "failed" bool)
         (req "internal" bool)
         (req "burn_dun" int64)
         (req "counter" int32)
         (req "fee" int64)
         (req "gas_limit" z_encoding)
         (req "storage_limit" z_encoding)
         (req "op_level" int)
         (req "timestamp" string)
         (opt "errors" (list Error.encoding)))
end

module type Api_encoding_min_sig = sig

  val account_name_encoding : account_name encoding
  module Protocol : sig val encoding : protocol encoding end
  module Block : sig
    val operation_encoding : (string * string * string) encoding
    val header_encoding : header encoding
    val encoding : block encoding
    val blocks : block list encoding
  end

  module Error : sig val encoding : op_error encoding end

  module Op : sig
    val dictator_encoding : dictator_operation encoding
    val choice_encoding : ballot_type encoding
    val amendment_encoding : (account_name * amendment_operation) encoding
    val endorsement_encoding : endorsement encoding
    val transaction_encoding : bool -> transaction encoding
    val origination_encoding : bool -> origination encoding
    val delegation_encoding : delegation encoding
    val reveal_encoding : reveal encoding
    val manager_encoding : bool -> (string * account_name * manager_operation list) encoding
    val sourced_operation_encoding : bool -> sourced_operations encoding
    val seed_nonce_revelation_encoding : seed_nonce_revelation encoding
    val activation_encoding : activation encoding
    val double_baking_evidence_encoding : double_baking_evidence encoding
    val double_endorsement_evidence_encoding : double_endorsement_evidence encoding
    val anonymous_operation_encoding : anonymous_operation list encoding
    val ts_operation_encoding : ts_operation encoding
    val token_operation_encoding : (int * string * ts_operation) encoding
    val proto_operation_encoding : bool -> operation_type encoding
    val operation : bool -> operation encoding
    val operations : bool -> operation list encoding
  end

  module Account_details : sig
    val encoding : account_details encoding
    val info_encoding : account_info encoding
  end
end

module type V = functor (MinV : MinV_sig) -> Api_encoding_min_sig

module V(MinV : MinV_sig) = struct

  let account_name_encoding = MinV.account_name_encoding
  module Error = MinV.Error

  module Protocol = struct

    let encoding =
      def "protocol_hash"
        ~title:"Protocol"
        ~description:"Dune protocol" @@
      (conv
         (fun { proto_name; proto_hash } -> ( proto_name, proto_hash ))
         (fun ( proto_name, proto_hash ) -> { proto_name; proto_hash } )
         (obj2
            (req "name" string)
            (req "hash" string)))
  end

  module Block = struct

    let operation_encoding =
      def "block_operation"
        ~title:"Block operation"
        ~description:"Operation in a block" @@
      obj3
        (req "hash" string)
        (req "branch" string)
        (req "data" string)

    let header_encoding =
      conv
        (fun {hd_level; hd_proto; hd_predecessor; hd_timestamp; hd_validation_pass;
              hd_operations_hash; hd_fitness; hd_context; hd_priority;
              hd_seed_nonce_hash; hd_proof_of_work_nonce; hd_signature; hd_hash;
              hd_network}
          -> (hd_level, hd_proto, hd_predecessor, hd_timestamp, hd_validation_pass,
              hd_operations_hash, hd_fitness, hd_context, hd_priority,
              hd_seed_nonce_hash, hd_proof_of_work_nonce, hd_signature, hd_hash,
              hd_network))
        (fun (hd_level, hd_proto, hd_predecessor, hd_timestamp, hd_validation_pass,
              hd_operations_hash, hd_fitness, hd_context, hd_priority,
              hd_seed_nonce_hash, hd_proof_of_work_nonce, hd_signature, hd_hash,
              hd_network)
          -> {hd_level; hd_proto; hd_predecessor; hd_timestamp; hd_validation_pass;
              hd_operations_hash; hd_fitness; hd_context; hd_priority;
              hd_seed_nonce_hash; hd_proof_of_work_nonce; hd_signature; hd_hash;
              hd_network})
        (EzEncoding.obj14
           (req "level" int)
           (req "proto" int)
           (req "predecessor" string)
           (req "timestamp" Date.encoding)
           (req "validation_pass" int)
           (req "operations_hash" string)
           (req "fitness" string)
           (req "context" string)
           (req "priority" int)
           (req "seed_nonce_hash" string)
           (req "proof_of_work_nonce_hash" string)
           (req "signature" string)
           (opt "hash" string)
           (opt "network" string))

    let encoding =
      def "block"
        ~title:"Block"
        ~description:"Dune block" @@
      conv
        (fun { hash; predecessor_hash; fitness; baker;
               timestamp; validation_pass; operations; protocol; nb_operations ;
               test_protocol; network; test_network;
               test_network_expiration; priority; level;
               commited_nonce_hash; pow_nonce; proto; data; signature;
               volume; fees; distance_level; cycle; endorsements_included }
          ->
            ((hash, predecessor_hash, fitness,
              timestamp, validation_pass, [ operations ], protocol,
              test_protocol, network, test_network,
              test_network_expiration,
              baker, nb_operations, priority, level, commited_nonce_hash,
              pow_nonce, proto, data, signature, volume, fees, distance_level,
              cycle), endorsements_included))
        (fun ((hash, predecessor_hash, fitness,
               timestamp, validation_pass, operations, protocol,
               test_protocol, network, test_network,
               test_network_expiration,
               baker, nb_operations, priority, level, commited_nonce_hash,
               pow_nonce, proto, data, signature,
               volume, fees, distance_level, cycle), endorsements_included)
          ->
            { hash; predecessor_hash; fitness; baker ;
              nb_operations ;
              timestamp; validation_pass ;
              operations = List.flatten operations ; protocol;
              test_protocol; network; test_network;
              test_network_expiration; priority; level;
              commited_nonce_hash; pow_nonce; proto; data; signature;
              volume; fees; distance_level; cycle; endorsements_included } )
        (merge_objs
           (EzEncoding.obj24
              (req "hash" string)
              (req "predecessor_hash" string)
              (req "fitness" string)
              (req "timestamp" Date.encoding)
              (req "validation_pass" int)
              (req "operations" (list (list operation_encoding)))
              (req "protocol" Protocol.encoding)
              (req "test_protocol" Protocol.encoding)
              (req "network" string)
              (req "test_network" string)
              (req "test_network_expiration" string)
              (req "baker" account_name_encoding)
              (dft "nb_operations" int 0)
              (req "priority" int)
              (req "level" int)
              (req "commited_nonce_hash" string)
              (req "pow_nonce" string)
              (req "proto" int)
              (req "data" string)
              (req "signature" string)
              (req "volume" int64)
              (req "fees" int64)
              (req "distance_level" int)
              (req "cycle" int))
           (obj1 (req "endorsements_included" int)))

    let blocks = list encoding

  end

  module Op = struct

    let dictator_encoding =
      let activate_encoding =
        def "activate" ~title:"Activate" @@
        obj2
          (req "chain" string) (* needs to be "activate" *)
          (req "hash" string) in
      let activate_testnet_encoding =
        def "activate_testnet" ~title:"Activate Testnet" @@
        obj2
          (req "chain" string) (* needs to be "activate_testchain" *)
          (req "hash" string) in
      def "dictator_operation"
        ~title:"Dictator operation"
        ~description:"Dune dictator operation" @@
      union [
        case activate_encoding
          (function _ -> None)
          (fun (_chain, _hash) -> Activate) ;
        case activate_testnet_encoding
          (function _ -> None)
          (fun (_chain, _hash) -> Activate)
      ]

    let choice_encoding =
      conv
        (function Yay -> "Yay" | Nay -> "Nay" | Pass -> "Pass")
        (function  "Yay" | "yay" -> Yay | "Pass"| "pass" -> Pass | "Nay" | "nay" -> Nay
                 | _ -> Nay)
        string

    let ballot_encoding =
      def "ballot"
        ~title:"Ballot"
        ~description:"Dune ballot" @@
      (obj4
         (req "kind" string)
         (req "period" int32)
         (req "proposal" string)
         (req "ballot" choice_encoding))

    let proposal_encoding =
      def "proposal"
        ~title:"Proposal"
        ~description:"Dune proposal" @@
      (obj3
         (req "kind" string)
         (req "period" int32)
         (req "proposals" (list string)))

    let amendment_encoding =
      def "amendment"
        ~title:"Amendment"
        ~description:"Dune amendment" @@
      merge_objs
        (obj1 (req "source" account_name_encoding))
        (union [
            case proposal_encoding
              (function
                | Proposal {prop_voting_period ; prop_proposals } ->
                  Some ("Proposal", prop_voting_period, prop_proposals)
                | _ -> None)
              (fun (_k, prop_voting_period, prop_proposals) ->
                 Proposal { prop_voting_period ; prop_proposals }) ;
            case ballot_encoding
              (function
                | Ballot { ballot_voting_period ; ballot_proposal ; ballot_vote } ->
                  Some ("Ballot", ballot_voting_period, ballot_proposal, ballot_vote)
                | _ -> None)
              (fun (_k, ballot_voting_period, ballot_proposal, ballot_vote) ->
                 Ballot {  ballot_voting_period ; ballot_proposal ; ballot_vote })
          ])

    let endorsement_encoding =
      def "endorsement"
        ~title:"Endorsement"
        ~description:"Dune endorsement" @@
      conv
        (fun { endorse_src ; endorse_block_hash; endorse_block_level;
               endorse_slot; endorse_op_level; endorse_priority; endorse_timestamp }
          -> ("endorsement", endorse_block_hash, endorse_block_level,
              endorse_src, endorse_slot, endorse_op_level, endorse_priority,
              endorse_timestamp))
        (fun (_k,  endorse_block_hash, endorse_block_level, endorse_src,
              endorse_slot, endorse_op_level, endorse_priority, endorse_timestamp)
          -> { endorse_src; endorse_block_hash; endorse_block_level;
               endorse_slot; endorse_op_level; endorse_priority; endorse_timestamp })
        (obj8
           (req "kind" string)
           (req "block" string)
           (req "level" int)
           (req "endorser" account_name_encoding)
           (req "slots" (list int))
           (req "op_level" int)
           (req "priority" int)
           (req "timestamp" string))

    let transaction_encoding script =
       let param_encoding =
        if script then Script.parameters_str_encoding
        else string in
      def "transaction"
        ~title:"Transaction"
        ~description:"Dune transaction" @@
      conv
        (fun { tr_src; tr_amount; tr_counter ; tr_fee ; tr_gas_limit ; tr_storage_limit ;
               tr_dst; tr_parameters ; tr_failed ;
               tr_internal ; tr_burn; tr_op_level; tr_timestamp; tr_errors;
               tr_collect_fee_gas; tr_collect_pk } ->
          ("transaction", tr_src, tr_amount, tr_dst, tr_parameters,
           tr_failed, tr_internal, tr_burn, tr_counter, tr_fee, tr_gas_limit,
           tr_storage_limit, tr_op_level, tr_timestamp, tr_errors, tr_collect_fee_gas,
           tr_collect_pk))
        (fun (_k, tr_src, tr_amount, tr_dst, tr_parameters, tr_failed, tr_internal,
              tr_burn, tr_counter, tr_fee, tr_gas_limit, tr_storage_limit,
              tr_op_level, tr_timestamp, tr_errors, tr_collect_fee_gas,
              tr_collect_pk) ->
          { tr_src; tr_amount; tr_counter ; tr_fee ; tr_gas_limit ; tr_storage_limit ;
            tr_dst; tr_parameters; tr_failed ; tr_internal ; tr_burn; tr_op_level;
            tr_timestamp; tr_errors; tr_collect_fee_gas; tr_collect_pk })
        (EzEncoding.obj17
           (req "kind" string)
           (req "src" account_name_encoding)
           (req "amount" int64)
           (req "destination" account_name_encoding)
           (opt "parameters" param_encoding)
           (req "failed" bool)
           (req "internal" bool)
           (req "burn" int64)
           (req "counter" int32)
           (req "fee" int64)
           (req "gas_limit" z_encoding)
           (req "storage_limit" z_encoding)
           (req "op_level" int)
           (req "timestamp" string)
           (opt "errors" (list Error.encoding))
           (opt "collect_fee_gas" int64)
           (opt "collect_pk" string))

    let origination_encoding = MinV.origination_encoding

    let delegation_encoding =
      def "delegation"
        ~title:"Delegation"
        ~description:"Dune delegation" @@
      conv
        (fun { del_src ; del_delegate ; del_counter ; del_fee ;
               del_gas_limit ; del_storage_limit ;
               del_failed ; del_internal; del_op_level; del_timestamp; del_errors } ->
          ("delegation", del_src, del_delegate, del_counter, del_fee,
           del_gas_limit, del_storage_limit, del_failed, del_internal,
           del_op_level, del_timestamp, del_errors))
        (fun (_k, del_src, del_delegate, del_counter, del_fee, del_gas_limit,
              del_storage_limit, del_failed, del_internal, del_op_level,
              del_timestamp, del_errors) ->
          { del_src ; del_delegate ; del_counter ; del_fee ; del_gas_limit ;
            del_storage_limit ; del_failed ; del_internal; del_op_level;
            del_timestamp; del_errors })
        (EzEncoding.obj12
           (req "kind" string)
           (req "src" account_name_encoding)
           (dft "delegate" account_name_encoding {pkh=""; alias = None})
           (req "counter" int32)
           (req "fee" int64)
           (req "gas_limit" z_encoding)
           (req "storage_limit" z_encoding)
           (req "failed" bool)
           (req "internal" bool)
           (req "op_level" int)
           (req "timestamp" string)
           (opt "errors" (list Error.encoding)))

    let reveal_encoding =
      def "reveal"
        ~title:"Reveal"
        ~description:"Dune reveal" @@
      conv
        (fun { rvl_src; rvl_pubkey; rvl_counter; rvl_fee; rvl_gas_limit;
               rvl_storage_limit; rvl_failed; rvl_internal; rvl_op_level;
               rvl_timestamp; rvl_errors }
          -> ("reveal", rvl_src, rvl_pubkey, rvl_counter, rvl_fee, rvl_gas_limit,
              rvl_storage_limit, rvl_failed, rvl_internal, rvl_op_level,
              rvl_timestamp, rvl_errors))
        (fun (_k, rvl_src, rvl_pubkey, rvl_counter, rvl_fee, rvl_gas_limit,
              rvl_storage_limit, rvl_failed, rvl_internal, rvl_op_level,
              rvl_timestamp, rvl_errors)
          -> { rvl_src; rvl_pubkey; rvl_counter; rvl_fee; rvl_gas_limit;
               rvl_storage_limit; rvl_failed; rvl_internal; rvl_op_level;
               rvl_timestamp; rvl_errors })
        (EzEncoding.obj12
           (req "kind" string)
           (req "src" account_name_encoding)
           (req "public_key" string)
           (req "counter" int32)
           (req "fee" int64)
           (req "gas_limit" z_encoding)
           (req "storage_limit" z_encoding)
           (req "failed" bool)
           (req "internal" bool)
           (req "op_level" int)
           (req "timestamp" string)
           (opt "errors" (list Error.encoding)))

    let manager_encoding script =
      def "manager_operation"
        ~title:"Manager operation"
        ~description:"Dune manager operation" @@
      (obj3
         (req "kind" string)
         (req "source" account_name_encoding)
         (req "operations"
            (list (union [
                 case reveal_encoding
                   (function
                     | Reveal rvl -> Some rvl
                     | _ -> None)
                   (fun rvl -> Reveal rvl) ;
                 case (transaction_encoding script)
                   (function
                     | Transaction tr -> Some tr
                     | _ -> None)
                   (fun tr -> Transaction tr) ;
                 case (origination_encoding script)
                   (function
                     | Origination ori -> Some ori
                     | _ -> None)
                   (fun ori -> Origination ori) ;
                 case delegation_encoding
                   (function
                     | Delegation del -> Some del
                     | _ -> None)
                   (fun del -> Delegation del) ]))))

    let manage_account_encoding =
      def "manage_account"
        ~title:"Manage Account"
        ~description:"Dune manage_account" @@
      conv
        (fun { mac_src; mac_maxrolls; mac_admin; mac_white_list; mac_delegation;
               mac_recovery; mac_actions;
               mac_target; mac_counter; mac_fee; mac_gas_limit;
               mac_storage_limit; mac_failed; mac_internal; mac_op_level;
               mac_timestamp; mac_errors }
          -> ("manage_account", mac_src, mac_maxrolls, mac_admin, mac_white_list,
              mac_delegation, mac_recovery, mac_actions, mac_target, mac_counter,
              mac_fee, mac_gas_limit, mac_storage_limit, mac_failed, mac_internal,
              mac_op_level, mac_timestamp, mac_errors))
        (fun (_k, mac_src, mac_maxrolls, mac_admin, mac_white_list, mac_delegation,
              mac_recovery, mac_actions, mac_target, mac_counter, mac_fee, mac_gas_limit,
              mac_storage_limit, mac_failed, mac_internal, mac_op_level,
              mac_timestamp, mac_errors)
          -> { mac_src; mac_maxrolls; mac_admin; mac_white_list; mac_delegation;
               mac_recovery; mac_actions; mac_target; mac_counter; mac_fee;
               mac_gas_limit; mac_storage_limit; mac_failed; mac_internal;
               mac_op_level; mac_timestamp; mac_errors })
        (EzEncoding.obj18
           (req "kind" string)
           (req "src" account_name_encoding)
           (opt "maxrolls" (option int))
           (opt "admin" (option account_name_encoding))
           (opt "white_list" (list account_name_encoding))
           (opt "delegation" bool)
           (opt "recovery" (option account_name_encoding))
           (opt "actions" (list (obj2 (req "name" string) (req "arg" string))))
           (opt "target"
              (obj2 (req "target" account_name_encoding) (opt "signature" string)))
           (req "counter" int32)
           (req "fee" int64)
           (req "gas_limit" z_encoding)
           (req "storage_limit" z_encoding)
           (req "failed" bool)
           (req "internal" bool)
           (req "op_level" int)
           (req "timestamp" string)
           (opt "errors" (list Error.encoding)))

    let manage_accounts_encoding =
      def "manage_accounts"
        ~title:"Manage Accounts"
        ~description:"Dune manage_accounts" @@
      conv
        (fun { macs_src; macs_bytes; macs_counter; macs_fee; macs_gas_limit;
               macs_storage_limit; macs_failed; macs_internal; macs_op_level;
               macs_timestamp; macs_errors }
          -> ("manage_account", macs_src, macs_bytes, macs_counter, macs_fee,
              macs_gas_limit, macs_storage_limit, macs_failed, macs_internal, macs_op_level,
              macs_timestamp, macs_errors))
        (fun (_k, macs_src, macs_bytes, macs_counter, macs_fee, macs_gas_limit,
              macs_storage_limit, macs_failed, macs_internal, macs_op_level,
              macs_timestamp, macs_errors)
          -> { macs_src; macs_bytes; macs_counter; macs_fee; macs_gas_limit;
               macs_storage_limit; macs_failed; macs_internal; macs_op_level;
               macs_timestamp; macs_errors })
        (EzEncoding.obj12
           (req "kind" string)
           (req "src" account_name_encoding)
           (req "bytes" hex_encoding)
           (req "counter" int32)
           (req "fee" int64)
           (req "gas_limit" z_encoding)
           (req "storage_limit" z_encoding)
           (req "failed" bool)
           (req "internal" bool)
           (req "op_level" int)
           (req "timestamp" string)
           (opt "errors" (list Error.encoding)))

    let activate_protocol_encoding =
      def "activate_protocol"
        ~title:"Activate Protocol"
        ~description:"Dune activate_protocol" @@
      conv
        (fun { acp_src; acp_protocol; acp_parameters; acp_counter; acp_fee;
               acp_gas_limit; acp_storage_limit; acp_failed; acp_internal;
               acp_op_level; acp_timestamp; acp_errors }
          -> ("manage_account", acp_src, acp_protocol, acp_parameters, acp_counter,
              acp_fee, acp_gas_limit, acp_storage_limit, acp_failed, acp_internal,
              acp_op_level, acp_timestamp, acp_errors))
        (fun (_k, acp_src, acp_protocol, acp_parameters, acp_counter, acp_fee,
              acp_gas_limit, acp_storage_limit, acp_failed, acp_internal,
              acp_op_level, acp_timestamp, acp_errors)
          -> { acp_src; acp_protocol; acp_parameters; acp_counter; acp_fee;
               acp_gas_limit; acp_storage_limit; acp_failed; acp_internal;
               acp_op_level; acp_timestamp; acp_errors })
        (EzEncoding.obj13
           (req "kind" string)
           (req "src" account_name_encoding)
           (opt "protocol" string)
           (opt "parameters" string)
           (req "counter" int32)
           (req "fee" int64)
           (req "gas_limit" z_encoding)
           (req "storage_limit" z_encoding)
           (req "failed" bool)
           (req "internal" bool)
           (req "op_level" int)
           (req "timestamp" string)
           (opt "errors" (list Error.encoding)))

    let dune_manager_encoding =
      def "dune_manager_operation"
        ~title:"Dune Manager operation"
        ~description:"Dune manager operation" @@
      (obj2
         (req "source" account_name_encoding)
         (req "operations"
            (list (union [
                 case manage_account_encoding
                   (function Manage_account man -> Some man | _ -> None)
                   (fun man -> Manage_account man) ;
                 case manage_accounts_encoding
                   (function Manage_accounts macs -> Some macs | _ -> None)
                   (fun macs -> Manage_accounts macs) ;
                 case activate_protocol_encoding
                   (function Activate_protocol acp -> Some acp | _ -> None)
                   (fun acp -> Activate_protocol acp) ;
               ]))))

    let sourced_operation_encoding micheline =
      def "signed_operation"
        ~title:"Signed operation"
        ~description:"Dune signed operation" @@
      union [
        case endorsement_encoding
          (function Endorsement e -> Some e | _ -> None)
          (fun c -> Endorsement c) ;
        case (manager_encoding micheline)
          (function Manager m -> Some m | _ -> None)
          (fun m -> Manager m) ;
        case amendment_encoding
          (function Amendment (source, am) -> Some (source, am) | _ -> None)
          (fun (source, am) -> Amendment (source, am)) ;
        case dictator_encoding
          (fun _ -> None)
          (fun d -> Dictator d);
        case dune_manager_encoding
          (function Dune_manager dm -> Some dm | _ -> None)
          (fun dm -> Dune_manager dm)
      ]

    let seed_nonce_revelation_encoding =
      def "nonce_revelation"
        ~title:"Nonce Revelation"
        ~description:"Dune seed nonce revelation" @@
      conv
        (fun { seed_level; seed_nonce; seed_nonce_op_level; seed_nonce_tsp }
          -> ("nonce", seed_level, seed_nonce, seed_nonce_op_level, seed_nonce_tsp))
        (fun (_k, seed_level, seed_nonce, seed_nonce_op_level, seed_nonce_tsp)
          -> { seed_level; seed_nonce; seed_nonce_op_level; seed_nonce_tsp } )
        (obj5
           (req "kind" string)
           (req "level" int)
           (req "nonce" string)
           (req "op_level" int)
           (req "timestamp" string))

    let activation_encoding =
      def "activation"
        ~title:"Activation"
        ~description:"Dune activation" @@
      conv
        (fun { act_pkh; act_secret; act_balance; act_op_level; act_timestamp }
          -> ("activation", act_pkh, act_secret, act_balance, act_op_level,
              act_timestamp))
        (fun (_, act_pkh, act_secret, act_balance, act_op_level, act_timestamp)
          -> { act_pkh ; act_secret; act_balance; act_op_level; act_timestamp })
        (obj6
           (req "kind" string)
           (req "pkh" account_name_encoding)
           (req "secret" string)
           (opt "balance" int64)
           (req "op_level" int)
           (req "timestamp" string))

    let double_baking_evidence_encoding =
      def "double_baking_evidence"
        ~title:"Double baking evidence"
        ~description:"Dune double baking evidence" @@
      conv
        (fun {double_baking_header1; double_baking_header2;
              double_baking_main ; double_baking_accused ;
              double_baking_denouncer ; double_baking_lost_deposit ;
              double_baking_lost_rewards ; double_baking_lost_fees;
              double_baking_gain_rewards; double_baking_op_level;
              double_baking_tsp } ->
          ("double_baking_evidence", double_baking_header1, double_baking_header2,
           double_baking_main, double_baking_accused,
           double_baking_denouncer, double_baking_lost_deposit,
           double_baking_lost_rewards, double_baking_lost_fees,
           double_baking_gain_rewards, double_baking_op_level, double_baking_tsp))
        (fun (_k, double_baking_header1, double_baking_header2,
              double_baking_main, double_baking_accused,
              double_baking_denouncer, double_baking_lost_deposit,
              double_baking_lost_rewards, double_baking_lost_fees,
              double_baking_gain_rewards, double_baking_op_level,
              double_baking_tsp) ->
          {double_baking_header1; double_baking_header2; double_baking_main ;
           double_baking_accused ; double_baking_denouncer ;
           double_baking_lost_deposit ; double_baking_lost_rewards ;
           double_baking_lost_fees; double_baking_gain_rewards;
           double_baking_op_level; double_baking_tsp})
        (EzEncoding.obj12
           (req "kind" string)
           (req "op1" Block.header_encoding)
           (req "op2" Block.header_encoding)
           (req "main" int)
           (req "offender" account_name_encoding)
           (req "denouncer" account_name_encoding)
           (req "lost_deposit" int64)
           (req "lost_rewards" int64)
           (req "lost_fees" int64)
           (req "gain_rewards" int64)
           (req "op_level" int)
           (req "timestamp" string))

    let double_endorsement_evidence_encoding =
      def "double_endorsement_evidence"
        ~title:"Double endorsement evidence"
        ~description:"Dune double endorsement evidence" @@
      conv
        (fun {double_endorsement1; double_endorsement2; double_endorsement_accused;
              double_endorsement_denouncer; double_endorsement_lost_deposit;
              double_endorsement_lost_rewards; double_endorsement_lost_fees;
              double_endorsement_gain_rewards; double_endorsement_op_level;
              double_endorsement_tsp} ->
          ("double_endorsement_evidence", double_endorsement1, double_endorsement2,
           double_endorsement_accused, double_endorsement_denouncer,
           double_endorsement_lost_deposit, double_endorsement_lost_rewards,
           double_endorsement_lost_fees, double_endorsement_gain_rewards,
           double_endorsement_op_level, double_endorsement_tsp))
        (fun (_k, double_endorsement1, double_endorsement2,
              double_endorsement_accused, double_endorsement_denouncer,
              double_endorsement_lost_deposit, double_endorsement_lost_rewards,
              double_endorsement_lost_fees, double_endorsement_gain_rewards,
              double_endorsement_op_level, double_endorsement_tsp) ->
          {double_endorsement1; double_endorsement2; double_endorsement_accused;
           double_endorsement_denouncer; double_endorsement_lost_deposit;
           double_endorsement_lost_rewards; double_endorsement_lost_fees;
           double_endorsement_gain_rewards; double_endorsement_op_level;
           double_endorsement_tsp})
        (EzEncoding.obj11
           (req "kind" string)
           (req "op1" endorsement_encoding)
           (req "op2" endorsement_encoding)
           (req "offender" account_name_encoding)
           (req "denouncer" account_name_encoding)
           (req "lost_deposit" int64)
           (req "lost_rewards" int64)
           (req "lost_fees" int64)
           (req "gain_rewards" int64)
           (req "op_level" int)
           (req "timestamp" string))

    let anonymous_operation_encoding =
      def "unsigned_operation"
        ~title:"Unsigned operation"
        ~description:"Dune unsigned operation" @@
      obj1
        (req "operations"
           (list
              (union [
                  case seed_nonce_revelation_encoding
                    (function
                      | Seed_nonce_revelation s -> Some s
                      | _ -> None)
                    (fun s -> Seed_nonce_revelation s) ;
                  case (activation_encoding)
                    (function
                      | Activation a -> Some a
                      | _ -> None)
                    (fun act -> Activation act) ;
                  case (double_baking_evidence_encoding)
                    (function
                      | Double_baking_evidence dbe -> Some dbe
                      | _ -> None)
                    (fun evidence -> Double_baking_evidence evidence) ;
                  case (double_endorsement_evidence_encoding)
                    (function
                      | Double_endorsement_evidence dee -> Some dee
                      | _ -> None)
                    (fun evidence -> Double_endorsement_evidence evidence)])))

    let ts_operation_kind_encoding = conv
        (function TS_Burn -> "burn" | TS_Transfer -> "transfer" | TS_Mint -> "mint"
                | TS_Unknown -> "unknown")
        (function "burn" -> TS_Burn | "transfer" -> TS_Transfer | "mint" -> TS_Mint
                | _ -> TS_Unknown)
        string

    let ts_operation_encoding =
      conv
        (fun {ts_src; ts_dst; ts_amount; ts_options; ts_flag; ts_kind}
          -> (ts_src, ts_dst, ts_amount, ts_options, ts_flag, ts_kind))
        (fun (ts_src, ts_dst, ts_amount, ts_options, ts_flag, ts_kind)
          -> {ts_src; ts_dst; ts_amount; ts_options; ts_flag; ts_kind})
        (obj6
           (req "source" account_name_encoding)
           (opt "destination" account_name_encoding)
           (req "amount" z_encoding)
           (opt "options" hex_encoding)
           (req "standard" string)
           (req "kind" ts_operation_kind_encoding))

    let token_operation_encoding =
      obj3
        (req "level" int)
        (req "timestamp" string)
        (req "operation" ts_operation_encoding)

    let proto_operation_encoding micheline =
      def "proto_operation"
        ~title:"Proto operation"
        ~description:"Dune proto operation" @@
      union [
        case (anonymous_operation_encoding )
          (function
            | Anonymous a -> Some a
            | _ -> None)
          (fun a -> Anonymous a) ;
        case (sourced_operation_encoding micheline)
          (function
            | Sourced s -> Some s
            | _ -> None)
          (fun s -> Sourced s);
        case token_operation_encoding
          (function
            | Tokened t -> Some t
            | _ -> None)
          (fun t -> Tokened t)
      ]

    let operation micheline =
      def "operation"
        ~title:"Operation"
        ~description:"Operation information" @@
      conv
        (fun { op_hash; op_block_hash; op_network_hash; op_type }
          -> ( op_hash, op_block_hash, op_network_hash, op_type ))
        (fun ( op_hash, op_block_hash, op_network_hash, op_type ) ->
           { op_hash; op_block_hash; op_network_hash; op_type } )
        (obj4
           (req "hash" string)
           (req "block_hash" string)
           (req "network_hash" string)
           (req "type" (proto_operation_encoding micheline)))

    let operations micheline = list (operation micheline)

  end

  module Account_details = struct
    let athen_encoding =
      conv
        (fun {acc_name; acc_manager; acc_balance; acc_spendable; acc_delegatable;
              acc_script; acc_storage; acc_counter; acc_node_timestamp; acc_delegate }
          -> let acc_dlgt = (acc_delegatable, acc_delegate) in
            let acc_manager = match acc_manager with
              | None -> acc_name
              | Some acc_manager -> acc_manager in
            (acc_name, acc_manager, acc_balance, acc_spendable, acc_dlgt,
             acc_script, acc_storage, acc_counter, acc_node_timestamp))
        (fun (acc_name, acc_manager, acc_balance, acc_spendable,
              (acc_delegatable, acc_delegate),
              acc_script, acc_storage, acc_counter, acc_node_timestamp)
          -> let acc_manager = Some acc_manager in
            {acc_name; acc_manager; acc_balance; acc_spendable; acc_delegatable;
              acc_script; acc_storage; acc_counter; acc_node_timestamp; acc_delegate})
        (obj9
           (req "name" account_name_encoding)
           (req "manager" account_name_encoding)
           (req "balance" int64)
           (opt "spendable" bool)
           (req "delegate" (
               obj2
                 (opt "setable" bool)
                 (opt "value" account_name_encoding)))
           (opt "script" Script.script_encoding)
           (opt "storage" Script.expr_encoding)
           (opt "counter" z_encoding)
           (opt "node_timestamp" string))

    let babylon_encoding =
      def "account_details"
        ~title:"Account Details"
        ~description:"Detailled account information" @@
      conv
        (fun {acc_name; acc_balance; acc_delegate; acc_script; acc_storage;
              acc_counter; acc_node_timestamp; _}
          -> (acc_name, acc_balance, acc_delegate, acc_script, acc_storage,
              acc_counter, acc_node_timestamp))
        (fun (acc_name, acc_balance, acc_delegate,
              acc_script, acc_storage, acc_counter, acc_node_timestamp)
          -> {acc_name; acc_balance; acc_delegate; acc_script; acc_storage;
              acc_counter; acc_node_timestamp; acc_manager = None; acc_spendable = None;
              acc_delegatable = None})
        (obj7
           (req "name" account_name_encoding)
           (req "balance" int64)
           (opt "delegate" account_name_encoding)
           (opt "script" Script.script_encoding)
           (opt "storage" Script.expr_encoding)
           (opt "counter" z_encoding)
           (opt "node_timestamp" string))

    let encoding =
      def "account_details"
        ~title:"Account Details"
        ~description:"Detailled account information" @@
      union [
        case athen_encoding
          (fun a -> if a.acc_manager = None then None else Some a)
          (fun a -> a);
        case babylon_encoding
          (fun a -> if a.acc_manager = None then Some a else None)
          (fun a -> a)
      ]

    let info_encoding =
      conv
        (fun {ai_name; ai_balance; ai_delegate; ai_script; ai_origination;
              ai_maxrolls; ai_admin; ai_white_list; ai_delegation; ai_recovery;
              ai_reveal; ai_activation; ai_entrypoints}
          -> (ai_name, ai_balance, ai_delegate, ai_script, ai_origination,
              ai_maxrolls, ai_admin, ai_white_list, ai_delegation, ai_recovery,
              ai_reveal, ai_activation, ai_entrypoints))
        ( fun (ai_name, ai_balance, ai_delegate, ai_script, ai_origination,
               ai_maxrolls, ai_admin, ai_white_list, ai_delegation, ai_recovery,
               ai_reveal, ai_activation, ai_entrypoints)
          -> {ai_name; ai_balance; ai_delegate; ai_script; ai_origination;
              ai_maxrolls; ai_admin; ai_white_list; ai_delegation; ai_recovery;
              ai_reveal; ai_activation; ai_entrypoints})
        (EzEncoding.obj13
           (req "name" account_name_encoding)
           (req "balance" int64)
           (opt "delegate" account_name_encoding)
           (opt "script" Script.script_str_encoding)
           (opt "origination" string)
           (opt "maxrolls" int)
           (opt "admin" account_name_encoding)
           (opt "white_list" (list account_name_encoding))
           (opt "delegation" bool)
           (opt "recovery" account_name_encoding)
           (opt "reveal" string)
           (opt "activation" string)
           (opt "entrypoints" (list (tup2 string Script.expr_encoding))))
  end
end

module V1 = V(MinV1)
module V4 = V(MinV4)
