(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types

let pending_block_hash = "prevalidation"

let orphan_block_hash = "Orphan"

let debug flag fmt =
  if flag then Printf.eprintf fmt
  else Printf.ifprintf stderr fmt

let unopt_i64_slot =
  List.map (function
      | None -> assert false
      | Some s -> Int64.to_int s)


let opt_list list = List.map (fun elt -> Some elt) list

let get_op_from_double_endorsement_evidence = function
  | Anonymous _ | Tokened _ -> assert false
  | Sourced op ->
    begin match op with
      | Endorsement op -> op
      | _ -> assert false
    end

let string_of_op_type = function
  | Anonymous _ -> "Anonymous"
  | Tokened _ -> "Tokened"
  | Sourced op ->
    begin match op with
      | Endorsement _ -> "Endorsement"
      | Amendment (_ , op) ->
        begin match op with
          | Proposal _ -> "Proposals"
          | Ballot _ -> "Ballot"
        end
      | Manager (_, _, _) -> "Manager"
      | Dictator op ->
        begin match op with
          | Activate -> "Activate"
          | Activate_testnet -> "Activate_testnet"
        end
      | Dune_manager _ -> "Dune_manager"
    end

let string_of_manager_op_type = function
  | Sourced Manager (_, _, list) ->
    (if List.exists (function Transaction _ -> true | _ -> false) list then
       [ "Transaction" ]
     else []) @
    (if List.exists (function Origination _ -> true | _ -> false) list then
       [ "Origination" ]
     else []) @
    (if List.exists (function Reveal _ -> true | _ -> false) list then
       [ "Reveal" ]
     else []) @
    (if List.exists (function Delegation _ -> true | _ -> false) list then
       [ "Delegation" ]
     else [])
  | _ -> []

let string_of_anonymous_op_type = function
  | Anonymous list ->
    (if List.exists (function
         | Seed_nonce_revelation _ -> true
         | _ -> false) list then
       [ "Nonce" ]
     else []) @
    (if List.exists (function Activation _ -> true | _ -> false) list then
       [ "Activation" ]
     else []) @
    (if List.exists (function
         | Double_endorsement_evidence _ -> true
         | _ -> false) list then
       [ "Double_endorsement_evidence" ]
     else []) @
    (if List.exists (function
         | Double_baking_evidence _ -> true
         | _ -> false) list then
       [ "Double_baking_evidence" ]
     else [])
  | _ -> []

let json_root = function
  | `O ctns -> `O ctns
  | `A ctns -> `A ctns
  | `Null -> `O []
  | oth -> `A [ oth ]

let split_ymd_timestamp date_str =
  let l = String.split_on_char '-' date_str in
  match l with
  | [ year; month ; day ] ->
    int_of_string year,
    int_of_string month,
    int_of_string day
  | _ -> assert false

let voting_period_of_summary = function
  | Sum_proposal _ -> "Proposal"
  | Sum_proposal_empty -> "Proposal"
  | Sum_testing_vote _ -> "Exploration"
  | Sum_testing -> "Testing"
  | Sum_promo _ -> "Promotion"

let pp_voting_period_status = function
  | VPS_passed -> "VP passed"
  | VPS_wait -> "VP wait (future)"
  | VPS_current -> "VP current"
  | VPS_ignored -> "VP never happened"

let string_of_ballot_vote = function
  | Yay -> "Yay"
  | Nay -> "Nay"
  | Pass -> "Pass"

let ballot_of_string = function
  | "Yay" | "yay" -> Yay
  | "Pass"| "pass" -> Pass
  | "Nay" | "nay" -> Nay
  | _ -> Nay

let int_of_ballot_vote rolls = function
  | Yay -> rolls
  | Nay -> -rolls
  | Pass -> 0

let ballot_of_int = function
  | i when i > 0 -> Yay
  | i when i < 0 -> Nay
  | _ -> Pass

let compute_fees = function
  | Endorsement _ | Dictator _ | Amendment _ -> 0L
  | Manager (_, _ , ops) ->
    List.fold_left (fun acc op -> match op with
        | Transaction {tr_fee = fee; _} | Origination {or_fee = fee; _}
        | Delegation {del_fee = fee; _} | Reveal {rvl_fee = fee; _}
          -> Int64.add fee acc) 0L ops
  | Dune_manager (_, ops) ->
    List.fold_left (fun acc op -> match op with
        | Manage_account {mac_fee = fee; _} | Manage_accounts {macs_fee = fee; _}
        | Activate_protocol {acp_fee = fee; _} -> Int64.add fee acc) 0L ops

let get_errors = function
  | Endorsement _ | Dictator _ | Amendment _ -> []
  | Manager (_, _ , ops) ->
    List.fold_left (fun acc op -> match op with
        | Transaction {tr_errors = err; _} | Origination {or_errors = err; _}
        | Delegation {del_errors = err; _} | Reveal {rvl_errors = err; _}
          -> acc @ Option.value ~default:[] err) [] ops
  | Dune_manager (_, ops) ->
    List.fold_left (fun acc op -> match op with
        | Manage_account {mac_errors = err; _} | Manage_accounts {macs_errors = err; _}
        | Activate_protocol {acp_errors = err; _} -> acc @ Option.value ~default:[] err) [] ops

let get_level = function
  | Anonymous []
  | Sourced (Dictator _) | Sourced (Amendment _)
  | Sourced (Manager (_, _ , [])) | Sourced (Dune_manager (_, [])) -> 0
  | Sourced (Manager (_, _, h :: _)) -> (match h with
      | Transaction {tr_op_level = level; _} | Origination {or_op_level = level; _}
      | Delegation {del_op_level = level; _} | Reveal {rvl_op_level = level; _}
        -> level)
  | Sourced (Dune_manager (_, h :: _)) -> (match h with
      | Manage_account {mac_op_level = level; _} | Manage_accounts {macs_op_level = level; _}
      | Activate_protocol {acp_op_level = level; _} -> level)
  | Sourced (Endorsement {endorse_op_level; _}) -> endorse_op_level
  | Anonymous (h ::_) -> (match h with
      | Seed_nonce_revelation {seed_nonce_op_level = level; _}
      | Activation {act_op_level = level; _}
      | Double_endorsement_evidence {double_endorsement_op_level = level; _}
      | Double_baking_evidence {double_baking_op_level = level; _} -> level)
  | Tokened (level, _, _) -> level

let get_source = function
  | Dictator _ | Amendment _ -> {pkh = ""; alias = None}
  | Manager (_, src , _) | Dune_manager (src, _)
  | Endorsement {endorse_src = src; _} -> src

let get_timestamp = function
  | Anonymous []
  | Sourced (Dictator _) | Sourced (Amendment _)
  | Sourced (Manager (_, _ , [])) | Sourced (Dune_manager (_, [])) -> ""
  | Sourced (Manager (_, _, h :: _)) -> (match h with
      | Transaction {tr_timestamp = tsp; _} | Origination {or_timestamp = tsp; _}
      | Delegation {del_timestamp = tsp; _} | Reveal {rvl_timestamp = tsp; _}
        -> tsp)
  | Sourced (Dune_manager (_, h :: _)) -> (match h with
      | Manage_account {mac_timestamp = tsp; _} | Manage_accounts {macs_timestamp = tsp; _}
      | Activate_protocol {acp_timestamp = tsp; _} -> tsp)
  | Sourced (Endorsement {endorse_timestamp; _}) -> endorse_timestamp
  | Anonymous (h ::_) -> (match h with
      | Seed_nonce_revelation {seed_nonce_tsp = tsp; _}
      | Activation {act_timestamp = tsp; _}
      | Double_endorsement_evidence {double_endorsement_tsp = tsp; _}
      | Double_baking_evidence {double_baking_tsp = tsp; _} -> tsp)
  | Tokened (_, tsp, _) -> tsp

let get_transactions ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous _ | Tokened _ -> acc
      | Sourced sop ->
         begin match sop with
         | Endorsement _ | Amendment _ | Dictator _ | Dune_manager _ -> acc
         | Manager (_, src, list) ->
            List.fold_left (fun acc mop ->
                match mop with
                | Transaction tr ->
                   (op.op_hash, op.op_block_hash, src, tr) :: acc
                | Origination _ | Reveal _ | Delegation _ -> acc
              ) acc list
         end
    ) [] ops

let get_delegations ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous _ | Tokened _ -> acc
      | Sourced sop ->
         begin match sop with
         | Endorsement _ | Amendment _ | Dictator _ | Dune_manager _ -> acc
         | Manager (_, src, list) ->
            List.fold_left (fun acc mop ->
                match mop with
                | Delegation del ->
                   (op.op_hash, op.op_block_hash, src, del) :: acc
                | Origination _ | Reveal _ | Transaction _ -> acc
              ) acc list
         end
    ) [] ops

let get_originations ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous _ | Tokened _ -> acc
      | Sourced sop ->
         begin match sop with
         | Endorsement _ | Amendment _ | Dictator _ | Dune_manager _ -> acc
         | Manager (_, src, list) ->
            List.fold_left (fun acc mop ->
                match mop with
                | Origination ori ->
                   (op.op_hash, op.op_block_hash, src, ori) :: acc
                | Reveal _ | Transaction _ | Delegation _ -> acc
              ) acc list
         end
    ) [] ops

let get_endorsement_op_type op = match op with
  | Anonymous _ | Tokened _ -> None
  | Sourced sop ->
     begin match sop with
     | Endorsement endorse -> Some endorse
     | _ -> None
     end

let get_endorsements ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous _ | Tokened _ -> acc
      | Sourced sop ->
         begin match sop with
         | Endorsement endorse ->
            (op.op_hash, op.op_block_hash, endorse) :: acc
         | _ -> acc
         end
    ) [] ops

let get_activations ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous list ->
         List.fold_left (fun acc aop ->
             match aop with
             | Activation act ->
                (op.op_hash, op.op_block_hash, act) :: acc
             | _ -> acc)
           acc list
      | Sourced _ | Tokened _ -> acc
    ) [] ops

let get_double_baking_evidence ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous list ->
         List.fold_left (fun acc aop ->
             match aop with
             | Double_baking_evidence dbe ->
                (op.op_hash, op.op_block_hash, dbe) :: acc
             | _ -> acc)
           acc list
      | Sourced _ | Tokened _ -> acc
    ) [] ops

let get_double_endorsements_evidence ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous list ->
         List.fold_left (fun acc aop ->
             match aop with
             | Double_endorsement_evidence dee ->
                (op.op_hash, op.op_block_hash, dee) :: acc
             | _ -> acc)
           acc list
      | Sourced _ | Tokened _ -> acc
    ) [] ops

let get_seed_nonce_revelations ops =
  List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous list ->
         List.fold_left (fun acc aop ->
             match aop with
             | Seed_nonce_revelation nonce ->
                (op.op_hash, op.op_block_hash, nonce) :: acc
             | _ -> acc)
           acc list
      | Sourced _ | Tokened _ -> acc
    ) [] ops

let get_reveals ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous _ | Tokened _ -> acc
      | Sourced sop ->
         begin match sop with
         | Endorsement _ | Amendment _ | Dictator _ | Dune_manager _ -> acc
         | Manager (_, src, list) ->
            List.fold_left (fun acc mop ->
                match mop with
                | Reveal rvl ->
                   (op.op_hash, op.op_block_hash, src, rvl) :: acc
                | Origination _ | Transaction _ | Delegation _ -> acc
              ) acc list
         end
    ) [] ops

let get_ts_operation ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Tokened (level, tsp, aop) ->
        (op.op_hash, op.op_block_hash, level, tsp, aop) :: acc
      | Sourced _ | Anonymous _ -> acc
    ) [] ops

let get_manage_account ops =
  List.rev @@ List.fold_left (fun acc op ->
      match op.op_type with
      | Anonymous _ | Tokened _ -> acc
      | Sourced sop ->
        match sop with
        | Endorsement _ | Amendment _ | Dictator _ | Manager _ -> acc
        | Dune_manager (src, list) ->
          List.fold_left (fun acc mop ->
              match mop with
              | Manage_account mac ->
                (op.op_hash, op.op_block_hash, src, mac) :: acc
              | _ -> acc) acc list
    ) [] ops

let count_manager_operations ops =
  List.fold_left (fun acc op -> match op.op_type with
      | Anonymous _ | Tokened _ -> acc
      | Sourced sop ->
         begin match sop with
         | Endorsement _ | Amendment _ | Dictator _ | Dune_manager _ -> acc
         | Manager (_, _, list) ->
            List.fold_left (fun  (n_tr, n_del, n_ori, n_rvl) mop ->
               match mop with
               | Transaction _ -> (n_tr + 1, n_del, n_ori, n_rvl)
               | Delegation _ -> (n_tr, n_del + 1, n_ori, n_rvl)
               | Origination _ -> (n_tr, n_del, n_ori + 1, n_rvl)
               | Reveal _ -> (n_tr, n_del, n_ori, n_rvl + 1)
             ) acc list
         end
    ) (0, 0, 0, 0) ops

let selection_to_kind = function
  | "Balances" -> Balances
  | "Frozen balances" -> Frozen_balances
  | "Frozen deposits" -> Frozen_deposits
  | "Frozen rewards" -> Frozen_rewards
  | "Frozen fees" -> Frozen_fees
  | "Total balances" -> Total_balances
  | "Paid bytes" -> Paid_bytes
  | "Staking balances" -> Staking_balances
  | "Total delegated" -> Total_delegated
  | "Total delegators" -> Total_delegators
  | "Used bytes" -> Used_bytes
  | _ -> Balances

let top_kind_to_str = function
  | Balances -> "Balances"
  | Frozen_balances -> "Frozen_balances"
  | Frozen_deposits -> "Frozen_deposits"
  | Frozen_rewards -> "Frozen_rewards"
  | Frozen_fees -> "Frozen_fees"
  | Total_balances -> "Total_balances"
  | Paid_bytes -> "Paid_bytes"
  | Staking_balances -> "Staking_balances"
  | Total_delegated -> "Total_delegated"
  | Total_delegators -> "Total_delegators"
  | Used_bytes -> "Used_bytes"

let top_kind_of_str = function
  | "Balances" -> Balances
  | "Frozen_balances" -> Frozen_balances
  | "Frozen_deposits" -> Frozen_deposits
  | "Frozen_rewards" -> Frozen_rewards
  | "Frozen_fees" -> Frozen_fees
  | "Total_balances" -> Total_balances
  | "Paid_bytes" -> Paid_bytes
  | "Staking_balances" -> Staking_balances
  | "Total_delegated" -> Total_delegated
  | "Total_delegators" -> Total_delegators
  | "Used_bytes" -> Used_bytes
  | _ -> Balances

let top_kind_to_total context = function
  | Balances -> context.context_current_balances
  | Frozen_balances -> context.context_frozen_balances
  | Frozen_deposits -> context.context_frozen_deposits
  | Frozen_rewards -> context.context_frozen_rewards
  | Frozen_fees -> context.context_frozen_fees
  | Total_balances -> context.context_full_balances
  | Paid_bytes -> context.context_paid_bytes
  | Staking_balances -> context.context_staking_balances
  | Total_delegated -> context.context_delegated
  | Total_delegators -> Int64.of_int context.context_delegators
  | Used_bytes -> context.context_used_bytes
