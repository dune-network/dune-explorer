(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

module StringMap = Map.Make(String)
open Lwt.Infix
open Dune_types

type node_state = {
  mutable current : string option ;
  mutable next_req : float ;
  mutable requested : bool ;
}

let node_states = ref StringMap.empty

(* Query a node for the timestamp of the last block. Do it at most
   once per minute. We don't need to be more accurate, as the interface
   displays a green light if we are less that 9 minutes late.
*)
let get_timestamp address =
  let base = EzAPI.TYPES.BASE address in
  let time = EzAPIServerUtils.req_time () in
  let state = match StringMap.find_opt address !node_states with
    | Some state -> state
    | None ->
      let state = {
        current = None ;
        next_req = 0. ;
        requested = false ;
      } in
      node_states := StringMap.add address state !node_states;
      state in
  if state.next_req < time then begin
    if not state.requested then
      let received current =
        state.requested <- false;
        state.current <- current;
        current in
      state.requested <- true ;
      state.next_req <- time +. 60.;
      EzCohttp_lwt.get0 ~msg:"Node.timestamp" base Service.Node.head_shell >|= function
      | Error _ -> received None
      | Ok shell ->
        let timestamp = Date.to_string shell.shell_timestamp in
        received (Some timestamp)
    else
      Lwt.return None
  end else
    Lwt.return state.current

let timestamps () =
  Lwt_list.map_s (fun (kind, address) ->
      get_timestamp address >|= fun current -> kind, current)
    (Config.get_addresses ())

let with_timestamped_address f =
  let address = Config.get_api_address () in
  get_timestamp address >>= (f address)

let wrap_res f = function
  | Error (EzRequest_lwt.KnownError {code; error}) ->
    Error (code, Some (Printf.sprintf "kind: %s, id: %s" error.node_err_kind error.node_err_id))
  | Error (EzRequest_lwt.UnknownError {code; msg}) -> Error (code, msg)
  | Ok x ->
    Ok (f x)

let account_dec hash =
  with_timestamped_address @@ fun address timestamp ->
  let base = EzAPI.TYPES.BASE address in
  EzCohttp_lwt.get1 ~msg:"Node.account" base Service.Node.account hash >|=
  (wrap_res (fun account -> timestamp, account))

let delegate_details hash =
  let base = EzAPI.TYPES.BASE (Config.get_api_address ()) in
  EzCohttp_lwt.get1 ~msg:"Node.delegate_details" base Service.Node.delegate hash >|=
  (wrap_res (fun x -> x))
