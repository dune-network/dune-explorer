(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types
open Data_types
open Db_intf

let (>>=) = Lwt.(>>=)
let to_api x = EzAPIServerUtils.return (Ok x)
let (>|=) v f = v >>= (fun v -> to_api (f v))
let max_number_of_replies = 50

(*********************************************************************)
(* Handlers                                                          *)
(*********************************************************************)
module V1 = struct

  let operations_param params =
    match EzAPI.find_param Service.param_operations params with
    | None -> None
    | Some b -> Some (bool_of_string b)

  let number_params params =
    match EzAPI.find_param Service.param_number params with
    | None -> None
    | Some i_str -> Some (min (int_of_string i_str) max_number_of_replies)

  let pagination_params params =
    let page_size = number_params params in
    let page =
      match EzAPI.find_param Service.param_page params with
      | None -> None
      | Some i_str -> Some (int_of_string i_str) in
    (page, page_size)

  let contract_params params =
    match EzAPI.find_param Service.param_contract params with
    | None -> None
    | Some b_str -> Some (bool_of_string b_str)

  let filters_params params =
    EzAPI.find_params Service.param_type params

  let peers_params params = EzAPI.find_param Service.param_peers params

  let level_param params =
    match EzAPI.find_param Service.param_level params with
    | None -> None
    | Some i_str -> Some (int_of_string i_str)

  let cycle_param params =
    match EzAPI.find_param Service.param_cycle params with
    | None -> None
    | Some b -> Some (int_of_string b)

  let delegate_params params =
    match EzAPI.find_param Service.param_delegate params with
    | None -> None
    | Some b_str -> Some (bool_of_string b_str)

  let search_filter_params params =
    EzAPI.find_param Service.param_search_filter params

  let future_params params =
    match EzAPI.find_param Service.param_future params with
    | None -> None
    | Some b_str -> Some (bool_of_string b_str)

  let tops_kind_params params =
    Option.map Utils.top_kind_of_str @@ EzAPI.find_param Service.param_kind params

  let block_hash_params params =
    EzAPI.find_param Service.param_block_hash params

  let period_params params =
    match EzAPI.find_param Service.param_period params with
    | None -> None
    | Some p_str -> Some (int_of_string p_str)

  let period_kind_params params =
    match EzAPI.find_param Service.param_period_kind params with
    | None -> None
    | Some p_str -> Some (Dune_utils.voting_period_kind_of_string p_str)

  let ballot_params params = EzAPI.find_param Service.param_ballot params
  let protocol_params params = EzAPI.find_param Service.param_protocol params

  let token_contract_params params = EzAPI.find_param Service.param_token_contract params

  let account_params params = EzAPI.find_param Service.param_account params
  let marketcap_params params =
    Option.map bool_of_string @@ EzAPI.find_param Service.param_marketcap params


  (* Block *)

  (* /block/BHASH *)
  let block (params, hash) () =
    let operations = operations_param params in
    Dbr.block ?operations @@ Hash hash >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block

  (* /head *)
  let head _params () =
    Dbr.head () >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block

  let head_cycle _params () =
    Dbr.head_cycle () >>= to_api

  (* /genesis *)
  let genesis _params () =
    Dbr.block (Level 0) >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block

  (* /blocks *)
  let blocks params () =
    let operations = operations_param params in
    let page, page_size = pagination_params params in
    Dbr.blocks ?page ?page_size ?operations () >>= to_api

  let blocks_with_pred_fitness params () =
    let page, page_size = pagination_params params in
    Dbr.blocks_with_pred_fitness ?page ?page_size () >>= to_api

  let nb_snapshot_blocks _params () =
    Dbr.nb_snapshot_blocks () >>= to_api

  let snapshot_blocks params () =
    let page, page_size = pagination_params params in
    Dbr.snapshot_blocks ?page ?page_size () >>= to_api

  let snapshot_levels _params () =
    Dbr.snapshot_levels () >>= to_api

  (* /heads *)
  let heads params () =
    let page, page_size = pagination_params params in
    let level = level_param params in
    Dbr.heads ?page ?page_size ?level () >>= to_api

  let nb_heads _params () =
    Dbr.nb_heads () >>= to_api

  let heads_with_pred_fitness params () =
    let page, page_size = pagination_params params in
    let level = level_param params in
    Dbr.heads_with_pred_fitness ?page ?page_size ?level () >>= to_api

  (* /nb_uncles/LEVEL *)
  let nb_uncles (_params, level) () =
    Dbr.nb_uncles ~level () >>= to_api

  let nb_cycle _params () =
    Dbr.nb_cycle () >>= to_api

  (* Account / Contract  *)

  (* /accounts *)
  let accounts params () =
    let page, page_size = pagination_params params in
    let contract = contract_params params in
    Dbr.accounts ?page ?page_size ?contract () >>= to_api

  (* /number_accounts/ *)
  let nb_accounts params () =
    let contract = contract_params params in
    Dbr.nb_accounts ?contract () >>= to_api

  let bonds_rewards (_params, hash) () =
    Dbr.account_bonds_rewards hash >>= to_api

  let extra_bonds_rewards (_params, hash) () =
    Dbr.extra_bonds_rewards hash >>= to_api

  let max_roll_cycle (_params) () =
    Dbr.max_roll_cycle () >>= to_api

  let rolls_distribution (_params, cycle) () =
    Dbr.rolls_distribution cycle >>= to_api

  let roll_number (_params, hash) () =
    Dbr.roll_number hash >>= to_api

  let rolls_history (params, hash) () =
    let page, page_size = pagination_params params in
    Dbr.rolls_history ?page ?page_size hash >>= to_api

  let account_status (_params, hash) () =
    Dbr.account_status hash >>= to_api

  let account_from_alias (_params, alias) () =
    Dbr.account_from_alias alias >>= to_api

  let account_info (_params, hash) () =
    Dbr.account_info hash >>= to_api

  (* Rewards *)

  let nb_cycle_rewards (_params, hash) () =
    Dbr.nb_cycle_rewards hash >>= to_api

  let nb_delegators (params, hash) () =
    let cycle = cycle_param params in
    Dbr.nb_delegators ?cycle hash >>= to_api

  let rewards_split_cycles (params, hash) () =
    let page, page_size = pagination_params params in
    Dbr.delegate_rewards_split_cycles ?page ?page_size hash
    >>= to_api

  let rewards_split (params, hash) () =
    let page, page_size = pagination_params params in
    let cycle = cycle_param params in
    Dbr.delegate_rewards_split ?page ?page_size ?cycle hash >>=
    to_api

  let rewards_split_fast (params, hash) () =
    let page, page_size = pagination_params params in
    let cycle = cycle_param params in
    Dbr.delegate_rewards_split_fast ?page ?page_size ?cycle hash >>=
    to_api

  let nb_cycle_delegator_rewards (_params, hash) () =
    Dbr.nb_cycle_delegator_rewards hash >>= to_api

  let delegator_rewards (params, hash) () =
    let page, page_size = pagination_params params in
    Dbr.delegator_rewards ?page ?page_size hash >>= to_api

  let delegator_rewards_with_details (params, hash) () =
    let page, page_size = pagination_params params in
    Dbr.delegator_rewards_with_details ?page ?page_size hash >>= to_api

  (* Operations *)

  (* /operation/OHASH *)
  let operation (params, ohash) () =
    let block_hash = block_hash_params params in
    Dbr.operation ?block_hash ohash >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some op -> to_api op

  let make_operations ?hash params =
    let filters = filters_params params in
    let page, page_size = pagination_params params in
    let delegate = delegate_params params in
    let selector = match hash with
      | None -> Empty
      | Some hash -> Pg_helper.hash_selector_of_hash hash in
    Dbr.operations
      ?delegate
      ?filters
      ?page
      ?page_size
      selector

  (* /operations *)
  let operations params () =
    make_operations params >>= to_api

  (* /operations/BHASH *)
  let operations_bh (params, block_hash) () =
    let hash = if block_hash = "" then None else Some block_hash in
    make_operations ?hash params >>= to_api

  let pending_operations params () =
    let selector = match account_params params with
      | None -> Empty
      | Some hash -> Pg_helper.hash_selector_of_hash hash in
    let filters = filters_params params in
    let page, page_size = pagination_params params in
    Dbr.pending_operations ?filters ?page ?page_size selector >>=
    to_api

  (* /number_operations/HASH *)
  let nb_operations_hash (params, hash) () =
    let filters = filters_params params in
    let delegate = delegate_params params in
    let hash_selector = Pg_helper.hash_selector_of_hash hash in
    Dbr.nb_operations ?delegate ?filters hash_selector >>= to_api

  (* /number_operations/ *)
  let nb_operations params () =
    let filters = filters_params params in
    Dbr.nb_operations ?filters Empty >>= to_api

  let nb_pending_operations params () =
    let selector = match account_params params with
      | None -> Empty
      | Some hash -> Pg_helper.hash_selector_of_hash hash in
    let filters = filters_params params in
    Dbr.nb_pending_operations ?filters selector >>= to_api

  let nb_operations_block (_, block_hash) () =
    Dbr.nb_operations_block_all block_hash >>= to_api

  let nb_operations_account (_, account_hash) () =
    Dbr.nb_operations_account_all account_hash >>= to_api

  (* /endorsements/level *)
  let endorsements_level (_params, level) () =
    Dbr.endorsements @@ Level level >>= to_api

  let nonces params () =
    let page, page_size = pagination_params params in
    Dbr.nonces ?page ?page_size () >>= to_api

  let transaction_account_csv (params, hash) () =
    let token = EzAPI.find_param Service.param_token params in
    match token, Config.get_secret_key (), Config.get_csv_dir () with
    | Some token, Some secret_key, Some csv_dir ->
      EzRecaptcha.verify secret_key token >>= (function
          | Ok captcha ->
            if captcha.EzRecaptcha.cap_success then (
              match captcha.EzRecaptcha.cap_score with
              | Some score when score > 0.5 ->
                let date = CalendarLib.Printer.Date.to_string (CalendarLib.Date.today ()) in
                let filename = Printf.sprintf "transactions_%s_%s.csv" hash date in
                let csv_files = Sys.readdir csv_dir in
                if Array.exists (fun s -> s = filename) csv_files then
                  to_api filename
                else
                  Dbr.operations ~page_size:max_int ~filters:["Transaction"] (Account hash)
                  >>= fun ltr ->
                  let header = [ "transaction"; "block"; "source"; "destination";
                                 "amount"; "fee"; "date"; "failed"; "burned dun" ] in
                  let l =
                    List.fold_left (fun acc o -> acc @ (Data_string.transaction header o)) [] ltr in
                  Csv_lwt.save ~quote_all:true (csv_dir ^ filename) (header :: l)
                  >>= fun () -> to_api filename
              | _ ->
                Printf.eprintf "CSV transaction for %s failed captcha verification with token %S\n%!" hash token;
                Lwt.fail EzAPI.ResultNotfound)
            else (
              Printf.eprintf "CSV transaction for %s failed captcha verification with token %S\n%!" hash token;
              Lwt.fail EzAPI.ResultNotfound)
          | Error (code, content) ->
            Printf.eprintf "Error %d: %s" code (Option.value ~default:"" content);
            Lwt.fail EzAPI.ResultNotfound)
    | _ ->
      Printf.eprintf "No captcha or no secret key\n%!";
      Lwt.fail EzAPI.ResultNotfound


  (* Bakings *)

  let bakings (params, hash) () =
    let page, page_size = pagination_params params in
    let cycle = cycle_param params in
    Dbr.bakings ?page ?page_size ?cycle hash >>= to_api

  (* /number_bakings/HASH *)
  let nb_bakings (params, hash) () =
    let cycle = cycle_param params in
    Dbr.nb_bakings ?cycle hash >>= to_api

  let nb_bakings_endorsement (params, hash) () =
    let cycle = cycle_param params in
    Dbr.nb_bakings_endorsement ?cycle hash >>= to_api

  let bakings_endorsement (params, hash) () =
    let page, page_size = pagination_params params in
    let cycle = cycle_param params in
    Dbr.bakings_endorsement ?page ?page_size ?cycle hash >>= to_api

  let cycle_bakings (params, hash) () =
    let page, page_size = pagination_params params in
    Dbr.cycle_bakings_sv ?page ?page_size hash >>= to_api

  let nb_cycle_bakings (_params, hash) () =
    Dbr.nb_cycle_bakings hash >>= to_api

  let cycle_endorsements (params, hash) () =
    let page, page_size = pagination_params params in
    Dbr.cycle_endorsements_sv ?page ?page_size hash >>= to_api

  let nb_cycle_endorsements (_params, hash) () =
    Dbr.nb_cycle_endorsements hash >>= to_api

  let nb_bakings_history (_params, hash) () =
    Dbr.nb_bakings_history hash >>= to_api

  let bakings_history (params, hash) () =
    let page, page_size = pagination_params params in
    Dbr.bakings_history ?page ?page_size hash >>= to_api

  let total_bakings (_params, hash) () =
    Dbr.total_bakings hash >>= to_api

  let nb_endorsements_history (_params, hash) () =
    Dbr.nb_endorsements_history hash >>= to_api

  let endorsements_history (params, hash) () =
    let page, page_size = pagination_params params in
    Dbr.endorsements_history ?page ?page_size hash >>= to_api

  let total_endorsements (_params, hash) () =
    Dbr.total_endorsements hash >>= to_api

  let last_baking_and_endorsement (_params, hash) () =
    Dbr.last_baking_and_endorsement hash >>= to_api

  let next_baking_and_endorsement (_params, hash) () =
    Dbr.next_baking_and_endorsement hash >>= to_api


  (* Rights *)

  (* /number_bakings/HASH *)
  let nb_cycle_rights params () =
    let filter = search_filter_params params in
    let future = future_params params in
    Dbr.nb_cycle_rights ?future ?filter () >>= to_api

  let cycle_rights params () =
    let page, page_size = pagination_params params in
    let filter = search_filter_params params in
    let future = future_params params in
    Dbr.cycle_rights ?future ?filter ?page ?page_size () >>= to_api

  (* /number_bakings/HASH *)
  let nb_baker_rights (params, hash) () =
    let cycle = cycle_param params in
    Dbr.nb_baker_rights ?cycle hash >>= to_api

  let baker_rights (params, hash) () =
    let page, page_size = pagination_params params in
    let cycle = cycle_param params in
    Dbr.baker_rights ?cycle ?page ?page_size hash >>= to_api

  let cycle_baker_rights (_params, hash) () =
    Dbr.cycle_baker_rights hash >>= to_api

  let cycle_all_rights (params,hash) () =
    let cycle = cycle_param params in
    let prio = number_params params in
    Dbr.cycle_all_rights ?cycle ?prio hash >>= to_api

  (* /number_bakings/HASH *)
  let nb_endorser_rights (params, hash) () =
    let cycle = cycle_param params in
    Dbr.nb_endorser_rights ?cycle hash >>= to_api

  let endorser_rights (params, hash) () =
    let page, page_size = pagination_params params in
    let cycle = cycle_param params in
    Dbr.endorser_rights ?cycle ?page ?page_size hash >>= to_api

  let cycle_endorser_rights (_params, hash) () =
    Dbr.cycle_endorser_rights hash >>= to_api

  let required_balance (_params, hash) () =
    Dbr.required_balance hash >>= to_api


  (* Block *)

  let block_succ (_params, hash) () =
    Dbr.block_successor hash >>= function
    | None      -> to_api None
    | Some hash -> to_api (Some hash)

  let block_pred (_params, hash) () =
    Dbr.block (Hash hash) >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block.predecessor_hash

  let timestamp (_params, hash) () =
    Dbr.block (Hash hash) >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block ->
      to_api @@
      Date.to_string block.timestamp

  let level (_params, hash) () =
    Dbr.level ~hash () >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some level -> to_api level

  let fitness (_params, hash) () =
    Dbr.block (Hash hash) >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block.fitness

  let network (_params, hash) () =
    Dbr.block (Hash hash) >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block.network

  let priority (_params, hash) () =
    Dbr.block (Hash hash) >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block.priority

  module VolumeCache = struct

    module StringMap = Map.Make(String)

    let cache = ref StringMap.empty

    let get hash f =
      Lwt.catch
        (fun () -> Lwt.return @@ StringMap.find hash !cache)
        (function
          | Not_found ->
            f () >>= fun x ->
            cache := StringMap.add hash x !cache;
            Lwt.return x
          | e -> Lwt.fail e
        )

  end

  let volume hash =
    make_operations ?hash (EzAPI.request [ "type", [ "Transaction" ]])
    >>= fun operations ->
    Lwt.return @@
    List.fold_left (fun sum op ->
        match op.op_type with
        | Sourced Manager (_, _, list) ->
          List.fold_left (fun sum op -> match op with
              | Transaction tr -> Int64.(add tr.tr_amount sum)
              | _ -> sum) sum list
        | _ -> sum  (* should never happen *))
      Int64.zero operations

  let volume (_params, block_hash) () =
    (if block_hash = "" then volume None
     else
       VolumeCache.get block_hash (fun () -> volume (Some block_hash)) )
    >>= to_api


  (* Level *)

  let block_level (params, level) () =
    let operations = operations_param params in
    Dbr.block ?operations @@ Level level >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block

  let block_hash_level (_params, level) () =
    Dbr.block @@ Level level >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some block -> to_api block.hash

  let chain (params, level) () =
    let length = Option.map int_of_string @@ EzAPI.find_param Service.param_length params in
    Dbr.chain ?length level >>= to_api

  (* Stats *)

  let market_info params () =
    let marketcap = marketcap_params params in
    let origin = EzAPI.find_param Service.param_origin params in
    Dbr.market_info ?marketcap ?origin () >>= to_api

  (* /number_network_peers/ *)
  let nb_network_peers params () =
    let state = peers_params params in
    Dbr.nb_network_peers ?state () >>= to_api

  let network_stats params () =
    let page, page_size = pagination_params params in
    let state = peers_params params in
    Dbr.network_stats ?state ?page ?page_size () >>= to_api

  let country_stats params () =
    let state = peers_params params in
    Dbr.country_stats ?state () >>= to_api

  let baker_stats (_params, hash) () =
    Dbr.baker_stats hash >>= to_api

  let bakers_stats params () =
    let cycle = cycle_param params in
    Dbr.bakers_stats ?cycle () >>= to_api

  let make_per_day l =
    let dates, values = List.split l in
    { pd_days = Array.of_list dates; pd_value = Array.of_list values }

  let blocks_per_day _params () =
    Dbr.blocks_per_day () >|= make_per_day

  let bakers_per_day _params () =
    Dbr.bakers_per_day () >|= make_per_day

  let priorities_per_day _params () =
    Dbr.priorities_per_day () >|= make_per_day

  let operations_per_day _params () =
    Dbr.operations_per_day () >|= make_per_day

  let operations_per_block_per_day _params () =
    Dbr.operations_per_block_per_day () >|= make_per_day

  let fees_per_day _params () =
    Dbr.fees_per_day () >|= make_per_day

  let volume_per_day _params () =
    Dbr.volume_per_day () >|= make_per_day

  let mini_stats  _params () =
    Dbr.stats_time () >>= to_api

  let health_stats (_params, cycle) () =
    Dbr.health_stats cycle >>= to_api

  let context_days _params () =
    Dbr.context_days () >>= to_api

  let context_stats (_params, day) () =
    let y, m, d = Utils.split_ymd_timestamp day in
    let day = CalendarLib.Calendar.make y m d 0 0 0 in
    Dbr.context_stats day >>= function
    | None -> Lwt.fail EzAPI.ResultNotfound
    | Some st -> to_api st

  let nb_tops params () =
    let kind = tops_kind_params params in
    Dbr.nb_tops ?kind () >>= to_api

  let tops params () =
    let kind = tops_kind_params params in
    let page, page_size = pagination_params params in
    Dbr.tops ?page ?page_size ?kind () >>= to_api

  let h24_stats _params () =
    Dbr.h24_stats () >>= to_api

  let supply params () =
    let level = Option.map int_of_string @@ EzAPI.find_param Service.param_level params in
    Dbr.supply ?level () >>= to_api

  let supply_coinmarketcap _params () =
    Dbr.supply () >>= fun {current_circulating_supply; _} ->
    EzAPIServerUtils.return_raw @@
    string_of_float @@
    Int64.to_float current_circulating_supply /. 1_000_000.

  let activated_balances _params () =
    Dbr.activated_balances () >>= to_api

  let balance_break_down (_params, hash) () =
    Dbr.balance_break_down hash >>= to_api

  let market_prices params () =
    let origin = EzAPI.find_param Service.param_origin params in
    Dbr.market_prices ?origin () >>= fun rows ->
    to_api @@ Array.of_list rows


  (* Search *)

  let search_block (_params, hash) () =
    Dbr.search_block hash >>= to_api

  let search_operation (_params, hash) () =
    Dbr.search_operation hash >>= to_api

  let search_account (_params, hash) () =
    Dbr.search_account (EzUrl.decode hash) >>= to_api

  let nb_search_block (_params, hash) () =
    Dbr.nb_search_block hash >>= to_api

  let nb_search_operation (_params, hash) () =
    Dbr.nb_search_operation hash >>= to_api

  let nb_search_account (_params, hash) () =
    Dbr.nb_search_account (EzUrl.decode hash) >>= to_api

  let alias (_params, hash) () =
    to_api (Alias.to_name hash).alias

  let find_param_tr tr t params =
    Option.map tr @@ EzAPI.find_param t params

  let find_constraints tr t params = {
    cstt_e = find_param_tr tr (t "e") params;
    cstt_g = find_param_tr tr (t "g") params;
    cstt_l = find_param_tr tr (t "l") params }

  let find_hash_type t params =
    List.fold_left (fun acc x ->
        if acc <> None then acc
        else match EzAPI.find_param (t x) params, x with
          | None, _ -> None
          | Some s, "is" -> Some (Ast.Is s)
          | Some s, "isnot" -> Some (Ast.IsNot s)
          | Some s, "start" -> Some (Ast.StartsWith s)
          | Some s, "end" -> Some (Ast.EndsWith s)
          | Some s, _ -> Some (Ast.Contains s))
      None ["is"; "isnot"; "start"; "end"; "inc"]

  let cal_of_ast_string s =
    let fmt = match String.split_on_char '/' s |> List.map int_of_string with
      | _ :: [] -> "%Y"
      | _ :: _ :: [] -> "%Y/%m"
      | _ :: _ :: _ :: [] -> "%Y/%m/%d"
      | _ -> "" in
    CalendarLib.Printer.Calendar.from_fstring fmt s

  let block_params_to_options params =
    let hash = find_hash_type Service.param_bhash params in
    let pred = find_hash_type Service.param_predecessor params in
    let baker = find_hash_type Service.param_baker params in
    let network = find_hash_type Service.param_network params in
    let protocol = find_hash_type Service.param_proto params in
    let signature = find_hash_type Service.param_signature params in
    let fitness = find_constraints int_of_string Service.param_fitness params in
    let volume = find_constraints Int64.of_string Service.param_volume params in
    let fees = find_constraints Int64.of_string Service.param_fees params in
    let level = find_constraints int_of_string Service.param_level_sch params in
    let cycle = find_constraints int_of_string Service.param_cycle_sch params in
    let votingp = {cstt_e = None; cstt_g = None; cstt_l = None} in
    let priority = find_constraints int_of_string Service.param_priority params in
    let nop = find_constraints int_of_string Service.param_nop params in
    let tsp = find_constraints (fun s -> s) Service.param_date params in
    let valpass = find_constraints int_of_string Service.param_valpass params in
    let cycle_pos = find_constraints int_of_string Service.param_cycle_pos params in
    let nendo = find_constraints int_of_string Service.param_endorsements params in
    let distance_level : int option = None in
    (hash, pred, baker, network, protocol, signature, fitness, volume, fees,
     level, cycle, votingp, priority, nop, tsp, valpass, cycle_pos, nendo, distance_level)

  let global_operations_params_to_options params =
    let hash = find_hash_type Service.param_ohash params in
    let bhash = find_hash_type Service.param_block_sch params in
    let src = find_hash_type Service.param_from params in
    let level = find_constraints int_of_string Service.param_level_sch params in
    let tsp = find_constraints (fun s -> s) Service.param_date params in
    let distance_level : int option = None in
    let cycle = find_constraints int_of_string Service.param_cycle_sch params in
    (hash, src, bhash, level, tsp, distance_level, cycle)

  let manager_operations_params_to_options params =
    let (hash, src, bhash, level, tsp, distance_level, cycle) =
      global_operations_params_to_options params in
    let internal = find_param_tr bool_of_string Service.param_internal params in
    let fee = find_constraints Int64.of_string Service.param_fee params in
    let counter = find_constraints int_of_string Service.param_counter params in
    let failed = find_param_tr bool_of_string Service.param_failed params in
    (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee,
    counter)

  let transactions_params_to_options params =
    let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal,
         fee, counter) =
      manager_operations_params_to_options params in
    let dst = find_hash_type Service.param_to params in
    let amount = find_constraints Int64.of_string Service.param_amount params in
    let burn = find_constraints Int64.of_string Service.param_burn params in
    let hasparam = find_param_tr bool_of_string Service.param_hasparam params in
    let collect_call = find_param_tr bool_of_string Service.param_collect_call params in
    (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal,
     fee, counter, dst, amount, burn, hasparam, collect_call)

  let activations_params_to_options params =
    manager_operations_params_to_options params

  let delegations_params_to_options params =
    let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal,
         fee, counter) =
      manager_operations_params_to_options params in
    let dlg = find_hash_type Service.param_dlg params in
    (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee, counter, dlg)

  let originations_params_to_options params =
    let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee, counter) =
      manager_operations_params_to_options params in
    let dlg = find_hash_type Service.param_dlg params in
    let manager = EzAPI.find_param Service.param_manager params in
    let orikt1 = EzAPI.find_param Service.param_orikt1 params in
    let balance = find_constraints Int64.of_string Service.param_ori_bal params in
    let burn = find_constraints Int64.of_string Service.param_burn params in
    (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee,
     counter, dlg, manager, orikt1, balance, burn)

  let accounts_params_to_options params =
    let hash = find_hash_type Service.param_ahash params in
    let dlg = find_hash_type Service.param_dlg params in
    let bblock = find_hash_type Service.param_bakedblock params in
    let baked = find_constraints int_of_string Service.param_bakedlevel params in
    let balance = find_constraints Int64.of_string (Service.param_balance "sp") params in
    let sbalance = find_constraints Int64.of_string (Service.param_balance "stak") params in
    let fbalance = find_constraints Int64.of_string (Service.param_balance "fro") params in
    let rewards = find_constraints Int64.of_string (Service.param_balance "rew") params in
    let fees = find_constraints Int64.of_string (Service.param_balance "fee") params in
    let deposits = find_constraints Int64.of_string (Service.param_balance "dep") params in
    let delegated = find_param_tr bool_of_string Service.param_delegated params in
    let revealed = find_param_tr bool_of_string Service.param_revealed params in
    let originated = find_param_tr bool_of_string Service.param_originated params in
    (hash, dlg, bblock, baked, balance, sbalance, fbalance, rewards, fees, deposits,
     delegated, revealed, originated)

  let nb_adv_search (params, search_kind) () =
    match search_kind with
    | "search_blocks" ->
      let (hash, pred, baker, network, protocol, signature, fitness, volume, fees,
           level, cycle, votingp, priority, nop, tsp, valpass, cycle_pos, nendo,
           distance_level) =
        block_params_to_options params in
      Dbr.nb_blocks_search ?distance_level
        ~hash ~pred ~baker ~network ~protocol ~signature ~fitness ~volume ~fees
        ~level ~cycle ~votingp ~priority ~nop ~tsp ~valpass ~cycle_pos ~nendo
    >>= to_api
    | "search_transactions" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee, counter,
           dst, amount, burn, hasparam, collect_call) = transactions_params_to_options params in
      Dbr.nb_transactions_search ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dst
        ~amount ~burn ~hasparam ~collect_call
      >>= to_api
    | "search_activations" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle)
        = global_operations_params_to_options params in
      Dbr.nb_activations_search ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp
      >>= to_api
    | "search_reveals" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee, counter) =
        manager_operations_params_to_options params in
      Dbr.nb_reveals_search ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp
        ~failed ~internal ~fee ~counter
      >>= to_api
    | "search_delegations" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee,
           counter, dlg) =  delegations_params_to_options params in
      Dbr.nb_delegations_search ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp
        ~failed ~internal ~fee ~counter ~dlg
      >>= to_api
    | "search_endorsements" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle) =
        global_operations_params_to_options params in
      Dbr.nb_endorsements_search ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp
      >>= to_api
    | "search_originations" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee,
           counter, dlg, manager, orikt1, balance, burn) =
        originations_params_to_options params in
      Dbr.nb_originations_search ?distance_level ~hash ~src ~bhash ~level ~cycle ~tsp
        ~failed ~internal ~fee ~counter ~dlg ~manager ~orikt1 ~balance ~burn
      >>= to_api
    | "search_accounts" ->
      let (hash, dlg, bblock, baked, balance, sbalance, fbalance, rewards, fees,
           deposits, delegated, revealed, originated) =
        accounts_params_to_options params in
      Dbr.nb_accounts_search
        ~hash ~dlg ~bblock ~baked ~balance ~sbalance ~fbalance ~rewards ~fees
        ~deposits ~delegated ~revealed ~originated
      >>= to_api
    | _ -> to_api 0

  let adv_search (params, search_kind) () =
    let page, page_size = pagination_params params in
    match search_kind with
    | "search_blocks" ->
      let (hash, pred, baker, network, protocol, signature, fitness, volume, fees,
           level, cycle, votingp, priority, nop, tsp, valpass, cycle_pos, nendo,
           distance_level) =
        block_params_to_options params in
      Dbr.blocks_search ?page ?page_size ?distance_level
      ~hash ~pred ~baker ~network ~protocol ~signature ~fitness ~volume ~fees
      ~level ~cycle ~votingp ~priority ~nop ~tsp ~valpass ~cycle_pos ~nendo
      >>= fun blocks -> to_api (Blocks_sch blocks)
    | "search_transactions" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee, counter,
           dst, amount, burn, hasparam, collect_call) = transactions_params_to_options params in
      Dbr.transactions_search ?page ?page_size ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dst
        ~amount ~burn ~hasparam ~collect_call
      >>= fun transacs -> to_api (Operations_sch (transacs, "transactions"))
    | "search_activations" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle) =
        global_operations_params_to_options params in
      Dbr.activations_search ?page ?page_size ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp
      >>= fun transacs -> to_api (Operations_sch (transacs, "activations"))
    | "search_reveals" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee,
           counter) = manager_operations_params_to_options params in
      Dbr.reveals_search ?page ?page_size ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter
      >>= fun reveals -> to_api (Operations_sch (reveals, "reveals"))
    | "search_delegations" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee,
           counter, dlg) =  delegations_params_to_options params in
      Dbr.delegations_search ?page ?page_size ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dlg
      >>= fun delegations -> to_api (Operations_sch (delegations, "delegations"))
    | "search_endorsements" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle) =
        global_operations_params_to_options params in
      Dbr.endorsements_search ?page ?page_size ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp
      >>= fun endorsements -> to_api (Operations_sch (endorsements, "endorsements"))
    | "search_originations" ->
      let (hash, src, bhash, level, tsp, distance_level, cycle, failed, internal, fee,
           counter, dlg, manager, orikt1, balance, burn) =
        originations_params_to_options params in
      Dbr.originations_search ?page ?page_size ?distance_level
        ~hash ~src ~bhash ~level ~cycle ~tsp ~failed ~internal ~fee ~counter ~dlg ~manager
        ~orikt1 ~balance ~burn
      >>= fun originations -> to_api (Operations_sch (originations, "originations"))
    | "search_accounts" ->
      let (hash, dlg, bblock, baked, balance, sbalance, fbalance, rewards, fees,
           deposits, delegated, revealed, originated) =
        accounts_params_to_options params in
      Dbr.accounts_search ?page ?page_size
        ~hash ~dlg ~bblock ~baked ~balance ~sbalance ~fbalance ~rewards ~fees ~deposits
        ~delegated ~revealed ~originated
      >>= fun accounts -> to_api (Accounts_sch accounts)
    | _ -> Lwt.fail EzAPI.ResultNotfound

  (* Node *)

  let return_result f = function
    | Error (code, content) ->
      Printf.printf "Error %d%s\n%!" code
        (match content with None -> "" | Some content -> ": " ^ content);
      to_api None
    | Ok x -> to_api @@ Some (f x)

  let node_timestamps _params () =
    Node_request.timestamps () >>= fun s ->
    to_api s

  let node_account (_params, hash) () =
    Node_request.account_dec hash >>=
    return_result (
      fun (acc_node_timestamp,
           {Dune_types.node_acc_balance; node_acc_delegate; node_acc_script;
            node_acc_storage; node_acc_counter; node_acc_manager; node_acc_spendable;
            node_acc_delegatable}) ->
        let acc_name = Alias.to_name hash in
        let acc_delegate = Option.map Alias.to_name node_acc_delegate in
        let acc_manager = Option.map Alias.to_name node_acc_manager in
        let acc_script = node_acc_script in
        let acc_storage = node_acc_storage in
        {acc_name; acc_balance = node_acc_balance; acc_delegate;
         acc_script; acc_storage; acc_counter = node_acc_counter;
         acc_node_timestamp; acc_manager; acc_spendable = node_acc_spendable;
         acc_delegatable = node_acc_delegatable})

  let node_delegate_details (_params, hash) () =
    Node_request.delegate_details hash
    >>= return_result (fun d -> d)

  let node_delegated_contracts (_params, hash) () =
    Node_request.delegate_details hash >>=
    return_result (fun details -> details.delegate_delegated_contracts)

  (* Server *)

  let version  _params () =
    to_api {
      server_version = DunscanConfig.version;
      server_build = DunscanConfig.en_date;
      server_commit = DunscanConfig.commit;
    }

  (* /date *)
  let date _params () =
    to_api (EzAPIServerUtils.req_time())

  let api_server_info _req () =
    let api_date = EzAPIServerUtils.req_time() in
    let api_versions = Infos.versions in
    let api_server_info = { Infos.api with
                            api_date ; api_versions } in
    to_api api_server_info


  (* Protocols *)

  let nb_protocol _params () =
    Dbr.nb_protocol () >>= to_api

  let protocols params () =
    let page, page_size = pagination_params params in
    Dbr.protocols ?page ?page_size () >>= to_api

  let constants params () =
    let protocol = protocol_params params in
    let level = level_param params in
    Dbr.constants ?level ?protocol () >>= function
    | [] -> to_api None
    | h :: _ -> to_api (Some h)

  (* Balance updates *)

  let balance_updates_number (params,hash) () =
    let from = cycle_param params in
    Dbr.nb_balance_updates ?from hash >>= to_api

  let balance (_params,hash) () =
    Dbr.balance hash >>= to_api

  let balance_updates (params,hash) () =
    let from = cycle_param params in
    let page, page_size = pagination_params params in
    Dbr.balance_updates ?page ?page_size ?from hash >>= to_api

  let active_balance_updates (params,hash) () =
    let cycle_opt = cycle_param params in
    match cycle_opt with
      Some cycle -> Dbr.active_balance_updates cycle hash >>= to_api
    | None -> to_api []

  let balance_from_balance_updates (_params,hash) () =
    Dbr.balance_from_balance_updates hash >>= to_api

  let balance_history (params,hash) () =
    match cycle_param params with
      None -> to_api []
    | Some cycle ->
      Dbr.balance_history cycle hash >>= to_api

  let cycle_frozen (params,hash) () =
     match cycle_param params with
       None -> to_api
                 {b_spendable = Int64.zero;
                  b_frozen =  Int64.zero;
                  b_rewards =  Int64.zero;
                  b_fees =  Int64.zero;
                  b_deposits =  Int64.zero}
     | Some cycle ->
        (Dbr.cycle_frozen cycle hash) >>= to_api

  let balance_number params () =
    let kind = tops_kind_params params in
    Dbr.nb_balance ?kind () >>= to_api

  let balance_ranking params () =
    let page, page_size = pagination_params params in
    let kind = tops_kind_params params in
    Dbr.balance_ranking ?page ?page_size ?kind () >>= to_api

  (* Services *)

  let nb_exchange _params () =
    Dbr.nb_exchange () >>= to_api

  let exchange_info params () =
    let page, page_size = pagination_params params in
    Dbr.exchange_info ?page ?page_size () >>= to_api

  let get_services _params () =
    Dbr.get_services () >>= to_api

  (* Tokens *)

  let account_tokens (_, hash) () =
    Dbr.account_tokens hash >>= to_api

  let token_balance (params, hash) () =
    let contract = token_contract_params params in
    Dbr.token_balance ?contract hash >>= to_api

  let nb_token_operations (params, hash) () =
    let contract = token_contract_params params in
    Dbr.nb_token_operations ?contract hash >>= to_api

  let token_operations (params, hash) () =
    let contract = token_contract_params params in
    let page, page_size = pagination_params params in
    Dbr.token_operations ?contract ?page ?page_size hash >>= to_api

  let token_info (_params, hash) () =
    Dbr.token_info hash >>= to_api

  let tokens _ () =
    Dbr.tokens () >>= to_api

  let baker_version (_, hash) () =
    Dbr.account_baker_version hash >>= to_api

end

module V2 = V1
module V3 = V1
module V4 = V1
