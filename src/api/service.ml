(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types
open EzAPI

let tup1_int = EzEncoding.tup1_int
let tup1_int64 = EzEncoding.tup1_int64
let tup1_string = EzEncoding.tup1_string

let int_enc = Api_encoding.int
let int64_enc = Api_encoding.int64

let param_number =
  Param.int
    ~name:"page_size"
    ~descr:"Number of replies"
    "number"

let param_operations =
  Param.bool
    ~name:"operations"
    ~descr:"Include operations"
    "operations"

let param_page =
  Param.int
    ~name:"page"
    ~descr:"Offset in number of pages"
    "p"

let param_contract =
  Param.bool
    ~name:"contract"
    ~descr:"Contracts if true, accounts if false, all in empty"
    "contract"

let param_type =
  Param.string
    ~required:true
    ~name:"type of operation"
    ~descr:"Limit the request to a particular kind of operations (`Transaction`, etc.)"
    "type"

let param_status =
  Param.string
    ~name:"status of operation"
    ~descr:"Status of an operation: Pending or Processed"
    "status"

let param_peers =
  Param.string
    ~name:"state of peers"
    ~descr:"'running' or 'disconnected' or any of them if let empty"
    "state"

let param_level =
  Param.int
    ~name:"level"
    ~descr:"Filtering by level"
    "level"

let param_cycle =
  Param.int
    ~name:"cycle"
    ~descr:"Filtering by cycle"
    "cycle"

let param_delegate =
  Param.bool
    ~name:"delegate"
    ~descr:"Originations as delegate if true, manager or account otherwise"
    "delegate"

let param_search_filter =
  Param.string
    ~name:"filter"
    ~descr:"Filter search results"
    "filter"

let param_future =
  Param.bool
    ~name:"future"
    ~descr:"Get future rights"
    "future"

let param_kind =
  Param.string
    ~name:"kind of tops"
    ~descr:"Limit the request to a particular kind of top accounts (`Frozen_deposits`, etc.)"
    "kind"

let param_prio =
  Param.int
    ~name:"priority"
    ~descr:"Filter by priority"
    "priority"

let param_block_hash =
  Param.string
    ~name:"block hash"
    ~descr:"Optional block hash containing the operation"
    "block_hash"

let param_token =
  Param.string
    ~name:"token"
    ~descr:"Token of recaptcha"
    "token"

let param_period =
  Param.int
    ~name:"period"
    ~descr:"Optional period for voting or other"
    "period"

let param_period_kind =
  Param.string
    ~name:"period kind"
    ~descr:"Optional period kind for voting or other"
    "period_kind"

let param_ballot =
  Param.string
    ~name:"ballot"
    ~descr:"Optional ballot"
    "ballot"

let param_protocol =
  Param.string ~name:"protocol" ~descr:"Optional protocol hash" "protocol"

let param_token_contract =
  Param.string ~name:"token contract" ~descr:"token contract hash" "contract"
let param_account =
  Param.string ~name:"account filter" ~descr:"account filter" "account"
let param_marketcap =
  Param.bool ~name:"marketcap" ~descr:"include marketcap" "marketcap"
let param_origin =
  Param.string ~name:"origin" ~descr:"filter from origin of info" "origin"
let param_length =
  Param.int ~name:"length" ~descr:"Optional length of chain" "length"

(* search params *)
let param_supp ?(param_kind=Param.string) ?(prefix="") ?(suffix="") name descr =
  let name = if prefix = "" then name else prefix ^ "_" ^ name in
  let name = if suffix = "" then name else name ^ "_" ^ suffix in
  param_kind ~name ~descr name

(* global filtering *)
let param_level_sch suffix =
  param_supp ~suffix ~param_kind:Param.int "level" "Optional level constraint"
let param_cycle_sch suffix =
  param_supp ~suffix ~param_kind:Param.int "cycle" "Optional cycle constraint"
let param_cycle_pos suffix =
  param_supp ~suffix ~param_kind:Param.int "cycle_pos" "Optional cycle position constraint"
let param_date suffix = param_supp ~suffix "date" "Optional date constraint"
(* operation filtering *)
let param_failed = param_supp "failed" "Optional operation failure"
let param_fee suffix = param_supp ~suffix "fee" "Optional fee constraint"
let param_amount suffix = param_supp ~suffix "amount" "Optional amount constraint"
let param_ohash suffix = param_supp ~suffix "ohash" "Optional operation hash"
let param_block_sch suffix = param_supp ~suffix "block" "Optional block parameter"
let param_from suffix = param_supp ~suffix "from" "Optional operation source"
let param_to suffix = param_supp ~suffix "to" "Optional operation destination"
let param_internal = param_supp ~param_kind:Param.bool "internal" "Optional internal parameter"
let param_counter suffix =
  param_supp ~suffix ~param_kind:Param.int "counter" "Optional counter constraint"
let param_burn suffix = param_supp ~suffix "burn" "Optional burn constraint"
let param_hasparam = param_supp ~param_kind:Param.bool "has_param" "Optional parameter boolean"
let param_collect_call = param_supp ~param_kind:Param.bool "collect_call" "Optional collect call boolean"
let param_manager = param_supp "manager" "Optional operation manager"
let param_orikt1 = param_supp "orikt1" "Optional operation kt1"
let param_ori_bal suffix =
  param_supp ~suffix ~param_kind:Param.int "oribal" "Optional origination balance constraint"
(* block filtering *)
let param_nop suffix =
  param_supp ~param_kind:Param.int ~suffix "nop" "Optional operation count constraint"
let param_fitness suffix =
  param_supp ~suffix ~param_kind:Param.int "fitness" "Optional fitness constraint"
let param_priority suffix =
  param_supp ~suffix ~param_kind:Param.int "priority" "Optional priority constraint"
let param_endorsements suffix =
  param_supp ~suffix ~param_kind:Param.int "endorsements" "Optional number of endorsement constraint"
let param_endorsedby suffix = param_supp ~suffix "endorsedby" "Optional endorser"
let param_volume suffix = param_supp ~suffix "volume" "Optional volume constraint"
let param_fees suffix = param_supp ~suffix "fees" "Optional fees constraint"
let param_baker suffix = param_supp ~suffix "baker" "Optional baker"
let param_bhash suffix = param_supp ~suffix "bhash" "Optional block hash"
let param_network suffix = param_supp ~suffix "network" "Optional network"
let param_predecessor suffix = param_supp ~suffix "predecessor" "Optional predecessor"
let param_successor suffix = param_supp ~suffix "successor" "Optional successor"
let param_proto suffix = param_supp ~suffix "protocol" "Optional protocol"
let param_signature suffix = param_supp ~suffix "signature" "Optional signature"
let param_valpass suffix =
  param_supp ~suffix ~param_kind:Param.int "valpass" "Optional validation pass constraint"
(* account filtering *)
let param_ahash suffix = param_supp ~suffix "ahash" "Optional account hash"
let param_balance prefix suffix =
  param_supp ~prefix ~suffix "bal" "Optional balance constraint"
(* let param_sbalance suffix = param_supp ~suffix "sbalance" "Optional staking balance constraint"
 * let param_fbalance suffix = param_supp ~suffix "fbalance" "Optional frozen balance constraint" *)
let param_dlg suffix = param_supp ~suffix "delegate" "Optional delegate"
let param_bakedblock suffix = param_supp ~suffix "baked_block" "Optional baked block"
let param_bakedlevel suffix =
  param_supp ~suffix ~param_kind:Param.int "baked_level" "Optional baked level constraint"
let param_delegated = param_supp ~param_kind:Param.bool "delegated" "Optional delegated boolean"
let param_revealed = param_supp ~param_kind:Param.bool "revealed" "Optional revealed boolean"
let param_originated = param_supp ~param_kind:Param.bool "originated" "Optional originated boolean"
(* balance filtering *)
let param_bal_kind = param_supp "bal_kind" "Optional balance kind"
let param_balblock suffix = param_supp ~suffix "bal_block" "Optional block hash for balance"
let param_balaccount suffix = param_supp ~suffix "bal_account" "Optional account hash for balance"

let pagination_params = [ param_page; param_number ]
let number_params = [ param_number ]
let operations_params = [ param_operations ]
let contract_params = [ param_contract ]
let type_params = [ param_type ]
let peers_params = [ param_peers ]
let cycle_params = [ param_cycle ]
let delegate_params = [ param_delegate ]
let search_filter_params = [ param_search_filter ]
let future_params = [ param_future ]
let prio_params = [ param_prio ]
let block_hash_params = [ param_block_hash ]
let token_params = [ param_token ]
let adv_search_params =
  [ param_page; param_number; param_failed; param_internal; param_bal_kind;
    param_hasparam; param_delegated; param_revealed; param_originated] @
  (List.flatten @@ (* with suffix for constraint *)
   List.map (fun p -> List.map (fun x -> p x) ["g"; "l"; "e"])
     [ param_level_sch; param_cycle_sch; param_cycle_pos; param_date; param_fee;
       param_amount; param_counter; param_nop; param_fitness; param_priority;
       param_endorsements; param_volume; param_fees; param_valpass;
       param_bakedlevel; param_burn ]) @
  (List.flatten @@ (* with suffix for hash_type *)
   List.map (fun p -> List.map (fun x -> p x) ["is"; "isnot"; "start"; "end"; "inc"])
     [ param_ohash; param_block_sch; param_from; param_to; param_endorsedby;
       param_baker; param_bhash; param_network; param_predecessor; param_successor;
       param_proto; param_signature; param_ahash; param_dlg; param_bakedblock;
       param_balblock; param_balaccount ]) @
  (List.flatten @@ (* with prefix and suffix *)
     List.map (fun x ->
         List.map (fun y -> param_balance y x) ["sp"; "stak"; "fro"; "rew"; "fee"; "dep"])
     ["g"; "l"; "e"])

let arg_block_hash =
  arg_string "block_hash" "BKyKuFqvCG7smSmXTewhigcVvHUTvecS8SAjsx7cjDzKe7js3HL"
let arg_account_hash =
  Resto1.Arg.make
    ~name:"account_hash"
    ~destruct:(fun s -> Ok (Crypto.tz_to_dn s))
    ~construct:(fun s -> s)
    ~example:"dn1MLnf3qjGsnaStSg1jMmsdgXKz9hteWE9i" (),
  "dn1MLnf3qjGsnaStSg1jMmsdgXKz9hteWE9i"
let arg_contract_hash =
  arg_string "contract_hash" "KT1QuofAgnsWffHzLA7D78rxytJruGHDe7XG"
let arg_op_hash =
  arg_string "op_hash" "oo5fwMjaLq8jzmKH1HJi9Qpg2VAfT3yMsMGtjnbHuCUAWAjiehV"
let arg_proposal_hash =
  arg_string "proposal_hash" "Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z"
let arg_hash =
  arg_string "hash" "BKyKuFqvCG7smSmXTewhigcVvHUTvecS8SAjsx7cjDzKe7js3HL"
let arg_period = arg_string "period" "minutes"
let arg_level = arg_int "level" 188418
let arg_cycle = arg_int "cycle" 42
let arg_node = arg_string "node" "balance"
let arg_alias = arg_string "alias" "Foundation Baker 1"
let arg_day = arg_string "day" "2018-10-01"
let arg_voting_period = arg_int "period" 8
let arg_search_kind = arg_string "search_kind" "search_blocks"

type nonrec 'a service0 = ('a, exn, no_security) service0
type nonrec ('a, 'b) service1 = ('a, 'b, exn, no_security) service1
type nonrec ('a, 'b) post_service0 = ('a, 'b, exn, no_security) post_service0

module type VERSION = sig
  val version_str : string
  module V : Api_encoding.Version_sig
end

module V (V : VERSION) = struct
  let version_v = V.version_str
  module V = V.V

  let section_blocks = EzAPI.section "Blocks Requests"
  let section_block = EzAPI.section "Block Requests"
  let section_accounts = EzAPI.section "Accounts Requests"
  let section_rewards = EzAPI.section "Rewards Requests"
  let section_bakings = EzAPI.section "Bakings Requests"
  let section_operations = EzAPI.section "Operations Requests"
  let section_level = EzAPI.section "Level Requests"
  let section_protocol = EzAPI.section "Protocol Requests"
  let section_balance_updates = EzAPI.section "Balance Updates Requests"
  let section_rights = EzAPI.section "Rights Requests"
  let section_token = EzAPI.section "Standard Token Requests"
  let section_adv_search = EzAPI.section "Search Requests"

  let section_server = EzAPI.section "Server Requests" (* Private *)
  let section_stats = EzAPI.section "Stats Requests" (* Private *)
  let section_search = EzAPI.section "Autocomplete Search Requests" (* Private *)
  let section_node = EzAPI.section "Node Requests" (* Private *)
  let section_website = EzAPI.section "Website Requests" (* Private *)

  let sections = [ (* order is important *)
    section_blocks;
    section_level;
    section_block;
    section_operations;
    section_accounts;
    section_rights;
    section_bakings;
    section_rewards;
    section_balance_updates;
    section_protocol;
    section_token;
    section_adv_search
  ]

  let other_sections = [
    section_server;
    section_stats;
    section_node;
    section_website;
    section_search;
  ]

  (*   SECTION BLOCKS *)

  (* A single block identified by its hash *)
  let block : (string, Data_types.block) service1  =
    service
      ~section:section_blocks
      ~name:"block"
      ~params:operations_params
      ~output:V.Block.encoding
      Path.(root // version_v // "block" /: arg_block_hash)

  (* Shortcut to last block *)
  let head : Data_types.block service0 =
    service
      ~name:"head"
      ~section:section_blocks
      ~output:V.Block.encoding
      Path.(root // version_v // "head")

  let head_cycle : int service0 =
    service
      ~name:"head_cycle"
      ~section:section_website
      ~output:tup1_int
      Path.(root // version_v // "head_cycle")

  (* Shortcut to first block *)
  let genesis : Data_types.block service0 =
    service
      ~name:"genesis"
      ~section:section_blocks
      ~output:V.Block.encoding
      Path.(root // version_v // "genesis")

  (* A list of blocks *)
  let blocks : Data_types.block list service0 =
    service
      ~section:section_blocks
      ~name:"blocks"
      ~params: (operations_params@pagination_params)
      ~output:(Json_encoding.list V.Block.encoding)
      Path.(root // version_v // "blocks")

  let blocks_with_pred_fitness : (Data_types.block * string) list service0 =
    service
      ~section:section_website
      ~name:"blocks_with_pred_fitness"
      ~params:pagination_params
      ~output:Json_encoding.(
          list (obj2
                  (req "block" V.Block.encoding)
                  (req "predecessor_fitness" string)))
      Path.(root // version_v // "blocks_with_pred_fitness")

  (* Alternative heads *)
  let heads : Data_types.block list service0 =
    service
      ~section:section_blocks
      ~name:"heads"
      ~params:(param_level :: pagination_params)
      ~output:(Json_encoding.list V.Block.encoding)
      Path.(root // version_v // "heads")

  let nb_heads : int service0 =
    service
      ~section:section_blocks
      ~name:"nb_heads"
      ~output:tup1_int
      Path.(root // version_v // "nb_heads")

  let heads_with_pred_fitness : (Data_types.block * string) list service0 =
    service
      ~section:section_website
      ~name:"heads_with_pred_fitness"
      ~params:(param_level :: pagination_params)
      ~output:Json_encoding.(
          list (obj2
                  (req "block" V.Block.encoding)
                  (req "predecessor_fitness" string)))
      Path.(root // version_v // "heads_with_pred_fitness")

    (* Count Alternative heads *)
  let nb_uncles : (int, int) service1 =
    service
      ~section:section_blocks
      ~name:"nb_uncles"
      ~output:tup1_int
      Path.(root // version_v // "nb_uncles" /: arg_level)

  let snapshot_levels : int list service0 =
    service
      ~section:section_blocks
      ~name:"snapshot_levels"
      ~output:(Json_encoding.list int_enc)
      Path.(root // version_v // "snapshot_levels")

  let snapshot_blocks : Data_types.snapshot list service0 =
    service
      ~section:section_blocks
      ~name:"snapshot_blocks"
      ~params:pagination_params
      ~output:V.Snapshot.encoding
      Path.(root // version_v // "snapshot_blocks")

  let nb_snapshot_blocks : int service0 =
    service
      ~section:section_blocks
      ~name:"nb_snapshot_blocks"
      ~output:tup1_int
      Path.(root // version_v // "nb_snapshot_blocks")

  let nb_cycle: int service0 =
    service
      ~section:section_website
      ~name:"nb_cycle"
      ~output:tup1_int
      Path.(root // version_v // "nb_cycle")

  (*    SECTION ACCOUNTS *)

  (* A list of accounts *)
  let accounts : Data_types.account_more list service0 =
    service
      ~name:"accounts"
      ~params:(pagination_params @ contract_params)
      ~section:section_accounts
      ~output:(Json_encoding.list V.Account_more.encoding)
      Path.(root // version_v // "accounts")

  (* The number of accounts *)
  let nb_accounts : int service0 =
    service
      ~section:section_accounts
      ~name:"nb_accounts"
      ~params:contract_params
      ~output:tup1_int
      Path.(root // version_v // "number_accounts")

  let account_bonds_rewards : (string, Data_types.account_bonds_rewards) service1 =
    service
      ~section:section_accounts
      ~name:"bonds_rewards"
      ~output:V.Bonds_rewards.encoding
      Path.(root // version_v // "bonds_rewards" /: arg_account_hash)

  let extra_bonds_rewards : (string, Data_types.account_extra_rewards) service1 =
    service
      ~section:section_accounts
      ~name:"extra_bonds_rewards"
      ~output:V.Bonds_rewards.extra
      Path.(root // version_v // "extra_bonds_rewards" /: arg_account_hash)

  let max_roll_cycle : int service0 =
    service
      ~section:section_stats
      ~name:"max_roll_cycle"
      ~output:tup1_int
      Path.(root // version_v // "max_roll_cycle")

  let rolls_distribution : (int, (account_name * int) list) service1 =
    service
      ~section:section_stats
      ~name:"roll_distribution"
      ~output:V.Rolls_distribution.encoding
      Path.(root // version_v // "rolls_distribution" /: arg_cycle)

  (* dn1 -> [ cycle, roll_count, total_roll_count ] *)
  let rolls_history : (account_hash, (int64 * int32 * int32) list) service1 =
    service
      ~params:pagination_params
      ~section:section_accounts
      ~name:"rolls_history"
      ~output:V.Rolls_distribution.rolls_history
      Path.(root // version_v // "rolls_history" /: arg_account_hash)

  (* dn1 -> roll_count *)
  let roll_number : (account_hash, int) service1 =
    service
      ~section:section_accounts
      ~name:"roll_number"
      ~output:tup1_int
      Path.(root // version_v // "roll_number" /: arg_account_hash)

  (* dn1 -> [ hash, reveal option, origination option ] *)
  let account_status : (account_hash, Data_types.account_status) service1 =
    service
      ~params:pagination_params
      ~section:section_accounts
      ~name:"account_status"
      ~output:V.Account_status.encoding
      Path.(root // version_v // "account_status" /: arg_account_hash)

  let account_from_alias : (string, account_hash option) service1 =
     service
      ~section:section_accounts
      ~name:"account_from_alias"
      ~output:Json_encoding.(tup1 (option string))
      Path.(root // version_v // "account_from_alias" /: arg_alias)

  let account_info : (account_hash, account_info option) service1 =
    service
      ~section:section_accounts
      ~name:"account_info"
      ~output:(Json_encoding.option V.Account_details.info_encoding)
      Path.(root // version_v // "account_info" /: arg_account_hash)

  (* SECTION REWARDS *)

  let rewards_split_cycles : (account_hash, all_rewards_split list) service1 =
    service
      ~params:pagination_params
      ~name:"rewards_split_cycles"
      ~section:section_rewards
      ~output: (Json_encoding.list V.Rewards_split.all_encoding)
      Path.(root // version_v // "rewards_split_cycles" /: arg_account_hash)

  let nb_cycle_rewards : (account_hash, int) service1 =
    service
      ~name:"nb_cycle_rewards"
      ~section:section_rewards
      ~output:tup1_int
      Path.(root // version_v // "nb_cycle_rewards" /: arg_account_hash)

  let rewards_split : (account_hash, rewards_split) service1 =
    service
      ~params:(pagination_params @ cycle_params)
      ~name:"rewards_split"
      ~section:section_rewards
      ~output:V.Rewards_split.encoding
      Path.(root // version_v // "rewards_split" /: arg_account_hash)

  let rewards_split_fast : (account_hash, (account_name * int64) list) service1 =
    service
      ~params: (pagination_params @ cycle_params)
      ~name:"rewards_split_fast"
      ~section:section_website
      ~output:Json_encoding.(
          list (tup2 V.account_name_encoding int64_enc))
      Path.(root // version_v // "rewards_split_fast" /: arg_account_hash)

  let nb_delegators : (account_hash, int) service1 =
    service
      ~params:cycle_params
      ~name:"nb_delegators"
      ~section:section_rewards
      ~output:tup1_int
      Path.(root // version_v // "nb_delegators" /: arg_account_hash)

  let delegator_rewards : (account_hash, delegator_reward list) service1 =
    service
      ~params:pagination_params
      ~name:"delegator_rewards"
      ~section:section_rewards
      ~output:V.Rewards_split.delegator_encodings
      Path.(root // version_v // "delegator_rewards" /: arg_contract_hash)

  let delegator_rewards_with_details :
    (account_hash, (delegator_reward * delegator_reward_details) list) service1 =
    service
      ~params:pagination_params
      ~name:"delegator_rewards_with_details"
      ~section:section_rewards
      ~output:(Json_encoding.list V.Rewards_split.delegator_rewards_all)
      Path.(root // version_v // "delegator_rewards_with_details" /: arg_contract_hash)

  let nb_cycle_delegator_rewards : (account_hash, int) service1 =
    service
      ~name:"nb_cycle_delegator_rewards"
      ~section:section_rewards
      ~output:tup1_int
      Path.(root // version_v // "nb_cycle_delegator_rewards" /: arg_contract_hash)


  (*   SECTION OPERATIONS *)

  (* A single operation *)
  let operation : (string, Data_types.operation) service1 =
    service
      ~params:block_hash_params
      ~name:"operation"
      ~section:section_operations
      ~output:(V.Op.operation true)
      Path.(root // version_v // "operation" /: arg_op_hash)

  let operation_nomic : (string, Data_types.operation) service1 =
    service
      ~params:block_hash_params
      ~name:"operation_nomic"
      ~section:section_website
      ~output:(V.Op.operation false)
      Path.(root // version_v // "operation_nomic" /: arg_op_hash)

  (* A list of operations *)
  let operations_bh : (string, Data_types.operation list) service1 =
    service
      ~params:(type_params @ pagination_params @ delegate_params)
      ~section:section_operations
      ~name:"operations_bh"
      ~output:(V.Op.operations true)
      Path.(root // version_v // "operations" /: arg_block_hash)

  let operations_bh_nomic : (string, Data_types.operation list) service1 =
    service
      ~params:(type_params @ pagination_params @ delegate_params)
      ~section:section_website
      ~name:"operations_bh_nomic"
      ~output:(V.Op.operations false)
      Path.(root // version_v // "operations_nomic" /: arg_block_hash)

  (* The number of operations for a block or an account *)
  let nb_operations_hash : (string, int) service1 =
    service
      ~params:(type_params @ delegate_params)
      ~name:"nb_operations_hash"
      ~section:section_operations
      ~output:tup1_int
      Path.(root // version_v // "number_operations" /: arg_hash)

  let operations : Data_types.operation list service0 =
    service
      ~params:(param_delegate :: type_params @ pagination_params)
      ~section:section_operations
      ~name:"operations"
      ~output:(V.Op.operations true)
      Path.(root // version_v // "operations")

  let operations_nomic : Data_types.operation list service0 =
    service
      ~params:(param_delegate :: type_params @ pagination_params)
      ~section:section_website
      ~name:"operations_nomic"
      ~output:(V.Op.operations false)
      Path.(root // version_v // "operations_nomic")

  (* The number of operations *)
  let nb_operations : int service0 =
    service
      ~params:[param_type]
      ~name:"nb_operations"
      ~section:section_operations
      ~output:tup1_int
      Path.(root // version_v // "number_operations")

  let nb_operations_block :
    (string, int * int * int * int * int * int * int * int * int * int * int * int * int) service1 =
    service
      ~name:"nb_operations_block"
      ~section:section_operations
      ~output:V.Counter.nb_operations_block
      Path.(root // version_v // "number_operations_block" /: arg_block_hash)

  let nb_operations_account :
    (string, int * int * int * int * int * int * int * int * int * int * int * int *
             int * int * int * int * int) service1 =
    service
      ~name:"nb_operations_account"
      ~section:section_operations
      ~output:V.Counter.nb_operations_account
      Path.(root // version_v // "number_operations_account" /: arg_account_hash)


  let endorsements_level : (int, Data_types.operation list) service1 =
    service
      ~section:section_operations
      ~name:"endorsements_level"
      ~output:(V.Op.operations true)
      Path.(root // version_v // "endorsements" /: arg_level)

  let nonces : (Data_types.nonces list) service0 =
    service
      ~params:pagination_params
      ~section:section_operations
      ~name:"nonces"
      ~output:(Json_encoding.list V.Nonce_hash.encoding)
      Path.(root // version_v // "nonces")

  let transaction_account_csv : (string, string) service1 =
    service
      ~params:token_params
      ~section:section_website
      ~name:"transaction_csv"
      ~output:tup1_string
      Path.(root // version_v // "transaction_account_csv" /: arg_account_hash)

  let nb_pending_operations : int service0 =
    service
      ~params:[param_type; param_account]
      ~section:section_operations
      ~name:"nb_pending_operations"
      ~output:tup1_int
      Path.(root // version_v // "nb_pending_operations")

  let pending_operations : pending_operation list service0 =
    service
      ~params:(param_type :: param_account :: pagination_params)
      ~section:section_operations
      ~name:"pending_operations"
      ~output:(Json_encoding.list V.Pending_operation.encoding)
      Path.(root // version_v // "pending_operations")


  (* SECTION BAKINGS *)

  (* List of baked blocks for a given account *)
  let bakings : (string, Data_types.baking list) service1 =
    service
      ~params:(cycle_params @ pagination_params)
      ~name:"bakings"
      ~section:section_bakings
      ~output:V.BakeOp.bakings
      Path.(root // version_v // "bakings" /: arg_account_hash)

  (* The number of baked blocks for an account *)
  let nb_bakings : (string, int) service1 =
    service
      ~params:cycle_params
      ~name:"nb_bakings"
      ~section:section_bakings
      ~output:tup1_int
      Path.(root // version_v // "number_bakings" /: arg_account_hash)

  let bakings_endorsement : (string, Data_types.baking_endorsement list) service1 =
    service
      ~params:(cycle_params @ pagination_params)
      ~name:"bakings_endorsement"
      ~section:section_bakings
      ~output:V.BakeEndorsementOp.bakings
      Path.(root // version_v // "bakings_endorsement" /: arg_account_hash)

  let nb_bakings_endorsement : (string, int) service1 =
    service
      ~params:cycle_params
      ~name:"nb_bakings_endorsement"
      ~section:section_bakings
      ~output:tup1_int
      Path.(root // version_v // "number_bakings_endorsement" /: arg_account_hash)

  let cycle_bakings : (string, Data_types.cycle_baking list) service1 =
    service
      ~params:pagination_params
      ~name:"cycle_bakings"
      ~section:section_bakings
      ~output:V.CycleBakeOp.bakings
      Path.(root // version_v // "cycle_bakings" /: arg_account_hash)

  let nb_cycle_bakings : (string, int) service1 =
    service
      ~name:"nb_cycle_bakings"
      ~section:section_bakings
      ~output:tup1_int
      Path.(root // version_v // "number_cycle_bakings" /: arg_account_hash)

  let cycle_endorsements : (string, Data_types.cycle_endorsement list) service1 =
    service
      ~params:pagination_params
      ~name:"cycle_endorsements"
      ~section:section_bakings
      ~output:V.CycleEndorsementOp.bakings
      Path.(root // version_v // "cycle_endorsements" /: arg_account_hash)

  let nb_cycle_endorsements : (string, int) service1 =
    service
      ~name:"nb_cycle_endorsements"
      ~section:section_bakings
      ~output:tup1_int
      Path.(root // version_v // "number_cycle_endorsements" /: arg_account_hash)

  let bakings_history :
    (string, Data_types.cycle_baking list * Data_types.cycle_rights list *
             Data_types.cycle_baking list) service1 =
    service
      ~params:pagination_params
      ~name:"bakings_history"
      ~section:section_website
      ~output:V.CycleRights.bakings_history
      Path.(root // version_v // "bakings_history" /: arg_account_hash)

  let nb_bakings_history : (string, int) service1 =
    service
      ~name:"nb_bakings_history"
      ~section:section_website
      ~output:tup1_int
      Path.(root // version_v // "number_bakings_history" /: arg_account_hash)

  let total_bakings : (string, Data_types.cycle_baking list) service1 =
    service
      ~name:"total_bakings"
      ~section:section_bakings
      ~output:V.CycleBakeOp.bakings
      Path.(root // version_v // "total_bakings" /: arg_account_hash)

  let endorsements_history :
    (string, Data_types.cycle_endorsement list * Data_types.cycle_rights list *
             Data_types.cycle_endorsement list) service1 =
    service
      ~params:pagination_params
      ~name:"endorsements_history"
      ~section:section_website
      ~output:V.CycleRights.endorsements_history
      Path.(root // version_v // "endorsements_history" /: arg_account_hash)

  let nb_endorsements_history : (string, int) service1 =
    service
      ~name:"nb_endorsements_history"
      ~section:section_website
      ~output:tup1_int
      Path.(root // version_v // "number_endorsements_history" /: arg_account_hash)

  let total_endorsements : (string, Data_types.cycle_endorsement list) service1 =
    service
      ~name:"total_endorsements"
      ~section:section_bakings
      ~output:V.CycleEndorsementOp.bakings
      Path.(root // version_v // "total_endorsements" /: arg_account_hash)

  let last_baking_and_endorsement :
    (string, baking list * baking_endorsement list *
             (int * int * string) * (int * int * string)) service1 =
    service
      ~section:section_bakings
      ~name:"last_baking_and_endorsement"
      ~output:V.LastNextBaking.last_baking
      Path.(root // version_v // "last_baking_and_endorsement" /: arg_account_hash)

  let next_baking_and_endorsement :
    (string, int * int * (int * int) * (int * int) * string) service1 =
    service
      ~section:section_bakings
      ~name:"next_baking_and_endorsement"
      ~output:V.LastNextBaking.next_baking
      Path.(root // version_v // "next_baking_and_endorsement" /: arg_account_hash)

  let baker_version : (string, string option) service1 =
    service
      ~section:section_bakings
      ~name:"baker_version"
      ~output:Api_encoding.(top_option tup1_string)
      Path.(root // version_v // "baker_version" /: arg_account_hash)

  (* SECTION RIGHTS *)

  let cycle_rights : Data_types.rights list service0 =
    service
      ~params:(pagination_params @ search_filter_params @ future_params)
      ~name:"cycle_rights"
      ~section:section_rights
      ~output:V.Rights.rights
      Path.(root // version_v // "baking_rights")

  let nb_cycle_rights : int service0 =
    service
      ~params:(search_filter_params @ future_params)
      ~name:"nb_cycle_rights"
      ~section:section_rights
      ~output:tup1_int
      Path.(root // version_v // "number_baking_rights")

  let baker_rights : (string, Data_types.baker_rights list) service1 =
    service
      ~params:(cycle_params @ pagination_params)
      ~name:"baker_rights"
      ~section:section_rights
      ~output:V.BakerRights.rights
      Path.(root // version_v // "baker_rights" /: arg_account_hash)

  let nb_baker_rights : (string, int) service1 =
    service
      ~params:cycle_params
      ~name:"nb_baker_rights"
      ~section:section_rights
      ~output:tup1_int
      Path.(root // version_v // "number_baker_rights" /: arg_account_hash)

  let cycle_all_rights : (string, int * int) service1 =
    service
      ~params:(cycle_params@prio_params)
      ~name:"cycle_all_rights"
      ~section:section_website
      ~output:V.CycleRights.nb_all_rights
      Path.(root // version_v // "cycle_all_rights" /: arg_account_hash)

  let cycle_baker_rights : (string, Data_types.cycle_rights list) service1 =
    service
      ~name:"cycle_baker_rights"
      ~section:section_rights
      ~output:V.CycleRights.rights
      Path.(root // version_v // "cycle_baker_rights" /: arg_account_hash)

  let nb_endorser_rights : (string, int) service1 =
    service
      ~params:cycle_params
      ~name:"nb_endorser_rights"
      ~section:section_rights
      ~output:tup1_int
      Path.(root // version_v // "number_endorser_rights" /: arg_account_hash)

  let endorser_rights : (string, Data_types.endorser_rights list) service1 =
    service
      ~params:(cycle_params @ pagination_params)
      ~name:"endorser_rights"
      ~section:section_rights
      ~output:(Json_encoding.list V.EndorserRights.encoding)
      Path.(root // version_v // "endorser_rights" /: arg_account_hash)

  let cycle_endorser_rights : (string, Data_types.cycle_rights list) service1 =
    service
      ~name:"cycle_endorser_rights"
      ~section:section_rights
      ~output:V.CycleRights.rights
      Path.(root // version_v // "cycle_endorser_rights" /: arg_account_hash)

  let required_balance : (string, (int * int64 * int64 * int64 * int * int) list) service1 =
    service
      ~name:"required_balance"
      ~section:section_rights
      ~output:V.Account.required_balance
      Path.(root // version_v // "required_balance" /: arg_account_hash)

  (*   SECTION BLOCK *)

  let block_succ : (string, block_hash option) service1 =
    service
      ~section:section_block
      ~name:"block_next"
      ~output:(Json_encoding.option tup1_string)
      Path.(root // version_v // "block_next" /: arg_block_hash)

  let block_pred : (string, block_hash) service1 =
    service
      ~section:section_block
      ~name:"block_prev"
      ~output:tup1_string
      Path.(root // version_v // "block_prev" /: arg_block_hash)

  let timestamp : (string, timestamp) service1 =
    service
      ~section:section_block
      ~name:"timestamp"
      ~output:tup1_string
      Path.(root // version_v // "timestamp" /: arg_block_hash)

  let level : (string, level) service1 =
    service
      ~section:section_block
      ~name:"level"
      ~output:V.Level.encoding
      Path.(root // version_v // "level" /: arg_block_hash)

  let network : (string, network_hash) service1 =
    service
      ~section:section_block
      ~name:"network"
      ~output:tup1_string
      Path.(root // version_v // "network" /: arg_block_hash)

  let priority : (string, int) service1 =
    service
      ~section:section_block
      ~name:"priority"
      ~output:tup1_int
      Path.(root // version_v // "priority" /: arg_block_hash)

  let volume : (string, int64) service1 =
    service
      ~section:section_block
      ~name:"volume"
      ~output:tup1_int64
      Path.(root // version_v // "volume" /: arg_block_hash)


  (*    SECTION LEVEL *)

  let block_level : (int, Data_types.block) service1 =
    service
      ~params:operations_params
      ~section:section_level
      ~name:"block_level"
      ~output:V.Block.encoding
      Path.(root // version_v // "block_level" /: arg_level)

  let block_hash_level : (int, block_hash) service1 =
    service
      ~section:section_level
      ~name:"block_hash_level"
      ~output:tup1_string
      Path.(root // version_v // "block_hash_level" /: arg_level)

  let chain : (int, block list) service1 =
    service
      ~name:"chain"
      ~params:[param_length]
      ~section:section_level
      ~output:(Json_encoding.list V.Block.encoding)
      Path.(root // version_v // "chain" /: arg_level)

  (* SECTION STATS *)

  let market_info : Data_types.gk_shell option service0 =
    service
      ~params:[param_marketcap; param_origin]
      ~section:section_stats
      ~name:"market_info"
      ~output:Api_encoding.(top_option Coingecko.encoding_full)
      Path.(root // version_v // "market_info" )

  (* The number of peers *)
  let nb_network_peers : int service0 =
    service
      ~params:peers_params
      ~section:section_stats
      ~name:"nb_network_peers"
      ~output:tup1_int
      Path.(root // version_v // "number_network_peers")

  let network_stats : Dune_types.network_stats list service0 =
    service
      ~params:(pagination_params @ peers_params)
      ~section:section_stats
      ~name:"network_stats"
      ~output:V.Network.encoding
      Path.(root // version_v // "network" )

  let country_stats : Data_types.country_stats list service0 =
    service
      ~params:peers_params
      ~section:section_stats
      ~name:"country_stats"
      ~output:V.Network.country_stats_encoding
      Path.(root // version_v // "country" )

  let baker : (string, Data_types.baker_stats) service1 =
    service
      ~section:section_stats
      ~name:"baker"
      ~output:V.Baker.encoding
      Path.(root // version_v // "baker_stats" /: arg_account_hash )

  let bakers : Data_types.baker_stats list service0 =
    service
      ~params:cycle_params
      ~section:section_stats
      ~name:"bakers"
      ~output:V.Baker.bakers_encoding
      Path.(root // version_v // "baker_stats" )

  let blocks_per_day : int Data_types.per_day service0 =
    service
      ~section:section_stats
      ~name:"blocks_per_day"
      ~output:V.Charts.int_per_day_encoding
      Path.(root // version_v // "blocks_per_day" )

  let bakers_per_day : int Data_types.per_day service0 =
    service
      ~section:section_stats
      ~name:"bakers_per_day"
      ~output:V.Charts.int_per_day_encoding
      Path.(root // version_v // "bakers_per_day" )

  let priorities_per_day : float Data_types.per_day service0 =
    service
      ~section:section_stats
      ~name:"priorities_per_day"
      ~output:V.Charts.float_per_day_encoding
      Path.(root // version_v // "priorities_per_day" )

  let operations_per_day : int Data_types.per_day service0 =
    service
      ~section:section_stats
      ~name:"operations_per_day"
      ~output:V.Charts.int_per_day_encoding
      Path.(root // version_v // "operations_per_day" )

  let operations_per_block_per_day : float Data_types.per_day service0 =
    service
      ~section:section_stats
      ~name:"operations_per_block_per_day"
      ~output:V.Charts.float_per_day_encoding
      Path.(root // version_v // "operations_per_block_per_day" )

  let fees_per_day : int64 Data_types.per_day service0 =
    service
      ~section:section_stats
      ~name:"fees_per_day"
      ~output:V.Charts.int64_per_day_encoding
      Path.(root // version_v // "fees_per_day" )

  let volume_per_day : int64 Data_types.per_day service0 =
    service
      ~section:section_stats
      ~name:"volume_per_day"
      ~output:V.Charts.int64_per_day_encoding
      Path.(root // version_v // "volume_per_day" )

  let mini_stats : mini_stats service0 =
    service
      ~name:"mini_stats"
      ~section:section_stats
      ~output:V.Charts.mini_stats
      Path.(root // version_v // "mini_stats" )

  let health_stats : (int, Data_types.health_stats) service1 =
    service
      ~section:section_stats
      ~name:"health"
      ~output:V.Health.encoding
      Path.(root // version_v // "health_stats" /: arg_cycle)

  let context_days : (string list) service0 =
    service
      ~section:section_stats
      ~name:"context_days"
      ~output:Json_encoding.(list string)
      Path.(root // version_v // "context_days")

  let context_stats : (string, Data_types.context_file_with_diff) service1 =
    service
      ~section:section_stats
      ~name:"context_stats"
      ~output:V.Context_stats.context_with_diff_encoding
      Path.(root // version_v // "context_stats" /: arg_day)

  let nb_tops : int service0 =
    service
      ~params:[ param_kind ]
      ~section:section_stats
      ~name:"nb_tops"
      ~output:tup1_int
      Path.(root // version_v // "nb_tops")

  let tops : top_accounts service0 =
    service
      ~params: (param_kind :: pagination_params)
      ~section:section_stats
      ~name:"tops"
      ~output:V.Tops.top_accounts_encoding
      Path.(root // version_v // "tops")

  let h24_stats : Data_types.h24_stats service0 =
    service
      ~section:section_stats
      ~name:"24h"
      ~output:V.H24_stats.encoding
      Path.(root // version_v // "24h_stats")

  let supply : supply_info service0 =
    service
      ~name:"supply"
      ~params:[param_level]
      ~section: section_stats
      ~output: V.Supply.encoding
      Path.(root // version_v // "supply")

  let supply_coinmarketcap : float service0 =
    service
      ~name:"supply_coinmarketcap"
      ~section:section_stats
      ~output:Json_encoding.float
      Path.(root // version_v // "supply_coinmarketcap")

  let balance_break_down : (string, balance_break_down) service1 =
    service
      ~name:"balance_break_down"
      ~section: section_stats
      ~output: V.Supply.h_encoding
      Path.(root // version_v // "balance_break_down" /: arg_account_hash)

  let activated_balances : int64 service0 =
    service
      ~name:"activated_balances"
      ~section: section_stats
      ~output:tup1_int64
      Path.(root // version_v // "activated_balances")


  let market_prices : (string * ( string * float ) array) array service0 =
    service
      ~params:[param_origin]
      ~section:section_stats
      ~name:"market_prices"
      ~output:Json_encoding.(array (tup2 string (array (tup2 string float))))
      Path.(root // version_v // "market_prices")

  (*   SECTION SEARCH   *)

  let search_block : (string, string list) service1 =
    service
      ~name:"search_block"
      ~section:section_search
      ~output:Json_encoding.(list string)
      Path.(root // version_v // "search_block" /: arg_block_hash)

  let search_operation : (string, string list) service1 =
    service
      ~name:"search_operation"
      ~section:section_search
      ~output:Json_encoding.(list string)
      Path.(root // version_v // "search_operation" /: arg_op_hash)

  let search_account : (string, (account_name * string) list) service1 =
    service
      ~name:"search_account"
      ~section:section_search
      ~output:V.Account.account_search
      Path.(root // version_v // "search_account" /: arg_account_hash)

  let nb_search_block : (string, int) service1 =
    service
      ~name:"nb_search_block"
      ~section:section_search
      ~output:tup1_int
      Path.(root // version_v // "nb_search_block" /: arg_block_hash)

  let nb_search_operation : (string, int) service1 =
    service
      ~name:"nb_search_operation"
      ~section:section_search
      ~output:tup1_int
      Path.(root // version_v // "nb_search_operation" /: arg_op_hash)

  let nb_search_account : (string, int) service1 =
    service
      ~name:"nb_search_account"
      ~section:section_search
      ~output:tup1_int
      Path.(root // version_v // "nb_search_account" /: arg_account_hash)

  let alias : (account_hash, string option) service1 =
    service
      ~section:section_search
      ~name:"alias"
      ~output:Json_encoding.(tup1 (option string))
      Path.(root // version_v // "alias" /: arg_account_hash)

  let adv_search : (string, adv_search) service1 =
    service
      ~params:adv_search_params
      ~section:section_search
      ~name:"adv_search"
      ~output:V.Adv_search.encoding
      Path.(root // version_v // "adv_search" /: arg_search_kind)

  let nb_adv_search : (string, int) service1 =
    service
      ~params:adv_search_params
      ~section:section_adv_search
      ~name:"nb_adv_search"
      ~output:tup1_int
      Path.(root // version_v // "nb_adv_search" /: arg_search_kind)

  (* SECTION NODE *)

  let node_timestamps: (string * string option) list service0 =
    service
      ~section:section_node
      ~name:"node_timestamps"
      ~output: Json_encoding.(list (tup2  string (option string)))
      Path.(root // version_v // "node_timestamps")

  let node_account: (string, account_details option) service1 =
    service
      ~section:section_node
      ~name:"node_account"
      ~output:(Api_encoding.top_option V.Account_details.encoding)
      Path.(root // version_v // "node_account" /: arg_account_hash)

  let node_delegate_details: (string, Dune_types.delegate_details option) service1 =
    service
      ~section:section_node
      ~name:"node_delegate_details"
      ~output:(Api_encoding.top_option @@ Dune_encoding.Delegate.encoding)
      Path.(root // version_v // "node_delegate_details" /: arg_account_hash)

  let node_delegated_contracts: (string, string list option) service1 =
    service
      ~params:pagination_params
      ~section:section_node
      ~name:"node_delegated_contracts"
      ~output:(Api_encoding.top_option Json_encoding.(list string))
      Path.(root // version_v // "delegated_contracts" /: arg_account_hash)

  (* SECTION SERVER *)

  let version : versions service0 =
    service
      ~section:section_server
      ~name:"version"
      ~output:V.Server.versions
      Path.(root // version_v // "version" )

  let api_server_info: api_server_info service0 =
    service
      ~section:section_server
      ~name:"info"
      ~output: V.Server.api_server_info
      Path.(root // version_v // "info")

  let date: (float) service0 =
    service
      ~section: section_server
      ~name:"server"
      ~output:(Json_encoding.tup1 Json_encoding.float)
      Path.(root // version_v // "date")

  (* SECTION PROTOCOL *)

  let protocols : proto_details list service0 =
    service
      ~section:section_protocol
      ~name:"protocols"
      ~params:pagination_params
      ~output:V.Proto_details.encoding
      Path.(root // version_v // "protocols")

  let nb_protocol : int service0 =
    service
      ~section:section_protocol
      ~name:"nb_protocol"
      ~output:tup1_int
      Path.(root // version_v // "nb_protocol")

  let constants : (int * Dune_types.constants) option service0 =
    service
      ~name:"constants"
      ~section:section_protocol
      ~params:[param_protocol; param_level]
      ~output:Json_encoding.(
          option
            (obj2 (req "cycle" int) (req "constants" Dune_encoding.constants)))
      Path.(root // version_v // "constants")

  (* SECTION BALANCE UPDATES *)

  let balance_updates : (string, balance_update_info list) service1 =
    service
      ~section:section_balance_updates
      ~name:"balance_updates"
      ~params:(pagination_params@cycle_params)
      ~output:V.Balance_update_info.encoding
      Path.(root // version_v // "balance_updates" /: arg_account_hash)

  let balance_updates_number : (string, int) service1 =
    service
      ~section:section_balance_updates
      ~name:"nb_balance_updates"
      ~params:cycle_params
      ~output:tup1_int
      Path.(root // version_v // "balance_updates_number" /: arg_account_hash)

  let active_balance_updates : (string, balance_update_info list) service1 =
    service
      ~section:section_balance_updates
      ~name:"active_balance_updates"
      ~params:cycle_params
      ~output:V.Balance_update_info.encoding
      Path.(root // version_v // "active_balance_updates" /: arg_account_hash)

  let balance : (string, Int64.t) service1 =
    service
      ~section:section_balance_updates
      ~name:"balance"
      ~output:tup1_int64
      Path.(root // version_v // "balance" /: arg_account_hash)

  let balance_from_balance_updates :
        (string, balance)
          service1 =
    service
      ~section:section_balance_updates
      ~name:"balance_from_balance_updates"
      ~output:(V.Balance.encoding)
      Path.(root // version_v // "balance_from_balance_updates" /: arg_account_hash)

  let balance_history : (string, (Int32.t * balance) list) service1 =
    service
      ~section:section_balance_updates
      ~name:"balance_history"
      ~output:V.Balance.balance_history
      Path.(root // version_v // "balance_history" /: arg_account_hash)

  let cycle_frozen : (string, balance) service1 =
    service
      ~section:section_balance_updates
      ~name:"cycle_frozen"
      ~params:[param_cycle]
      ~output:(V.Balance.encoding)
      Path.(root // version_v // "cycle_frozen" /: arg_account_hash)

  let balance_ranking : ((int * account_name * Int64.t) list) service0 =
    service
      ~section:section_balance_updates
      ~name:"balance_ranking"
      ~params:(param_kind :: pagination_params @ cycle_params)
      ~output:V.Balance.balance_ranking
      Path.(root // version_v // "balance_ranking")

  let balance_number : int service0 =
    service
      ~section:section_balance_updates
      ~name:"nb_balance"
      ~params:[param_kind]
      ~output:tup1_int
      Path.(root // version_v // "balance_number")

  (* SECTION SERVICES *)

  let nb_exchange: int service0 =
    service
      ~section:section_website
      ~name:"nb_exchange"
      ~output:Json_encoding.(tup1 int)
      Path.(root // version_v // "nb_exchange")

  let exchange_info: exchange_info list service0 =
    service
      ~params:pagination_params
      ~section:section_website
      ~name:"exchange_info"
      ~output:(Json_encoding.list Api_encoding.exchange_info_encoding)
      Path.(root // version_v // "exchange_info")

  let get_services: Data_types.service list service0 =
    service
      ~name:"services"
      ~section:section_website
      ~output:V.Services.encoding
      Path.(root // version_v // "services")

  (* SECTION TOKEN *)

  let tokens: token_info list service0 =
    service
      ~name:"tokens"
      ~section:section_token
      ~output:(Json_encoding.list V.Token.info)
      Path.(root // version_v // "tokens")

  let token_info: (string, token_info option) service1 =
    service
      ~name:"token_info"
      ~section:section_token
      ~output:(Api_encoding.top_option V.Token.info)
      Path.(root // version_v // "token_info" /: arg_contract_hash)

  let account_tokens: (string, token_info list) service1 =
    service
      ~name:"account_tokens"
      ~section:section_token
      ~output:Json_encoding.(list V.Token.info)
      Path.(root // version_v // "account_tokens" /: arg_account_hash)

  let token_balance: (string, (string * string) list) service1 =
    service
      ~name:"token_balance"
      ~params:[param_token_contract]
      ~section:section_token
      ~output:Json_encoding.(list (obj2 (req "contract" string) (req "balance" string)))
      Path.(root // version_v // "token_balance" /: arg_account_hash)

  let token_operations: (string, operation list) service1 =
    service
      ~name:"token_transfers"
      ~params:(param_token_contract :: pagination_params)
      ~section:section_token
      ~output:(V.Op.operations true)
      Path.(root // version_v // "token_transfers" /: arg_account_hash)

  let nb_token_operations: (string, int) service1 =
    service
      ~name:"nb_token_transfers"
      ~params:[param_token_contract]
      ~section:section_token
      ~output:tup1_int
      Path.(root // version_v // "nb_token_transfers" /: arg_account_hash)

end

let init () = ()

module V1 = V (struct
    let version_str = "v1"
    module V = Api_encoding.V1
  end)

module V2 = V (struct
    let version_str = "v2"
    module V = Api_encoding.V1
  end)

module V3 = V (struct
    let version_str = "v3"
    module V = Api_encoding.V3
    end)

module V4 = V (struct
    let version_str = "v4"
    module V = Api_encoding.V4
  end)

module Current = V4

module Node = struct

  let server_error = EzAPI.(ErrCase {
    code = 500; name = "Node Error";
    encoding = Json_encoding.def "Node Error" Dune_encoding_min.Error.encoding;
    select = (fun x -> Some x);
    deselect = (fun x -> x)
  })

  let head : (Dune_types.node_block, Dune_types.node_op_error, EzAPI.no_security) EzAPI.service0 =
    EzAPI.service
      ~register:false
      ~output:Dune_encoding.Block.encoding
      ~error_outputs:[server_error]
      EzAPI.Path.(root // "chains" // "main" // "blocks" // "head")

  let peers : (Dune_types.network_stats list, Dune_types.node_op_error, EzAPI.no_security) EzAPI.service0 = EzAPI.service
      ~register:false
      ~output:Dune_encoding.Network.encoding
      ~error_outputs:[server_error]
      EzAPI.Path.(root // "network" // "peers" )

  let head_shell : (Dune_types.block_shell, Dune_types.node_op_error, EzAPI.no_security) EzAPI.service0 =
    EzAPI.service
      ~output:Dune_encoding.Header.shell_encoding
      ~register:false
      ~error_outputs:[server_error]
      EzAPI.Path.(root // "chains" // "main" // "blocks" // "head" // "header" // "shell")

  let account : (string, Dune_types.node_account, Dune_types.node_op_error, EzAPI.no_security) EzAPI.service1 =
    EzAPI.service
      ~output:Dune_encoding.Account.encoding
      ~register:false
      ~error_outputs:[server_error]
      EzAPI.Path.(root // "chains" // "main" // "blocks" // "head" // "context" // "contracts" /: arg_account_hash)

  let delegate : (string, Dune_types.delegate_details, Dune_types.node_op_error, EzAPI.no_security) EzAPI.service1 =
    EzAPI.service
      ~output:Dune_encoding.Delegate.encoding
      ~register:false
      ~error_outputs:[server_error]
      EzAPI.Path.(root // "chains" // "main" // "blocks" // "head" // "context" // "delegates" /: arg_account_hash)

end
