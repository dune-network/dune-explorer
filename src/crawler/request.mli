(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types

exception NoCurlResult of string

(** [level url hash] returns the level of the block with the
    corresponding [hash]. *)
val level : Data_types.url -> block_hash -> node_level

(** [predecessor url hash] returns the hash of the predecessor of
    the block with the hash [hash]. *)
val predecessor : Data_types.url -> block_hash -> block_hash

(** [block url hash] returns a value [Data_types.block] which contains
    all the information about the block with the hash [hash]. *)
val block : Data_types.url -> block_hash -> node_block

(** [operations url block_hash] gets all the operations from the block
    with the hash [hash]. *)
val operations : Data_types.url -> string -> node_operation list list


(** Shortcuts on the block [head] *)
val genesis : Data_types.url -> node_block
val block_first : Data_types.url -> node_block
val head_block : ?block_hash:string -> Data_types.url -> node_block
val get_head_hash : ?block:string -> Data_types.url -> block_hash

val get_alternative_heads_hashes : Data_types.url -> block_hash list list

val current_level : ?block:string -> Data_types.url -> node_level
val pending_operations : Data_types.url -> mempool

val node_contracts :
  Data_types.url -> string -> (string * int64 * account_hash option) list

val request : ?post:bool -> ?headers:(string * string) list
  -> cachable:bool -> Data_types.url -> string -> string

val coingecko_tickers : unit -> Data_types.gk_ticker list
val network_stats : Data_types.url -> network_stats list
val prices : unit -> Data_types.gk_shell
val constants : Data_types.url -> string -> constants
val entrypoints : ?block:block_hash -> Data_types.url -> account_hash
  -> (string * Language.script_expr_t) list

val cmc : unit -> Data_types.cmc_quote * Data_types.cmc_quote
