module Compare = Compare

module MBytes = struct
  include Bigstring
  let to_hex b = Hex.of_bigstring b
  let of_hex h = Hex.to_bigstring h
end

let id s = s

module Signature = struct

  module Public_key = struct
    type t = string
    let compare = String.compare
    let encoding = (id, id)
    let pp ppf t = Format.pp_print_string ppf t
    let of_b58check_opt s = Some s
    let to_b58check s = s
  end

  module Public_key_hash = struct
    type t = string
    let compare = String.compare
    let encoding = (id, id)
    let pp ppf t = Format.pp_print_string ppf t
    let of_b58check_opt s = Some s
    let to_b58check s = s
  end

  type t = string
  let compare = String.compare
  let encoding = (id, id)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = Some s
  let to_b58check s = s
end

module Contract_repr = struct
  type t = string
  let compare = String.compare
  let encoding = (id, id)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = Some s
  let of_b58check s = Ok s
  let to_b58check s = s
end

module Script_expr_hash = struct
  type t = string
  let compare = String.compare
  let encoding = (id, id)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = try Some s with _ -> None
  let to_b58check s = s
end

module Script_timestamp_repr = struct
  type t = Z.t
  let compare = Z.compare
  let to_zint x = x
  let of_zint x = x
  let to_string t = Z.to_string t
  let of_string t = try Some (Z.of_string t) with _ -> None
end

module Tez_repr = struct
  type t = int64
  let compare = Int64.compare
  let to_mutez t = t
  let of_mutez t = Some t
  let of_mutez_exn t = t
  let to_string t = Int64.to_string t
  let of_string t = Int64.of_string_opt t
end

module Ztez_repr = struct
  type t = Z.t
  let compare = Z.compare
  let to_mutez t = t
  let of_mutez t = Some t
  let of_mutez_exn t = t
  let to_string t = Z.to_string t
  let of_string t = try Some (Z.of_string t) with _ -> None
end

module Dune_debug = struct
    module Array = Array
end
