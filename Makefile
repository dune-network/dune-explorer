DATABASE:=dune_mainnet
DESTSUBDIR:=dunscan
WITH_EXTRAS:=false
WITH_VERSION:=true
API_PORT:=
API_HOST:=
API_DOC_HOST:=https://api6.dunscan.io
AUTO_UPGRADE:=true
DEST_DIR ?= ${CURDIR}/www
COMMIT:=$(shell git rev-parse --short HEAD)

#If you want to host locally:
#API_PORT:=8000
#API_HOST:=localhost

-include config/Makefile.database

##########
## MAIN ##
##########

all: may-db-update
	PGDATABASE=$(DATABASE) dune build --profile release
	@mkdir -p bin
	@cp -f _build/default/src/crawler/main.exe bin/dunscan-crawler
	@cp -f _build/default/src/api/main.exe bin/dunscan-api-server
	@$(MAKE) website

########
## DB ##
########

may-db-update:
ifeq (${AUTO_UPGRADE},true)
	@$(MAKE) force-db-update
endif

config/db-version.txt:
	@echo 0 > config/db-version.txt


force-db-update: config/db-version.txt
	PGDATABASE=$(DATABASE) dune build src/db/db-update
	@_build/default/src/db/db-update/updater.exe --witness config/db-version.txt --database $(DATABASE)

########
## UI ##
########

website:
	@mkdir -p ${DEST_DIR}
	rsync -a static/* ${DEST_DIR}
ifeq (${API_HOST},)
	@sed -e 's|%%API%%|api1.dunscan.io|' \
		static/info.json > ${DEST_DIR}/info.json
else
	@sed -e 's|%%API%%|${API_HOST}:${API_PORT}|' \
		static/info.json > ${DEST_DIR}/info.json
endif
	@rsync -r static/local/* ${DEST_DIR}
	@cp -f _build/default/src/ui/main.bc.js ${DEST_DIR}/explorer-main.js
	@sed -i 's/%{commit}/$(COMMIT)/g' ${DEST_DIR}/index.html

client:
	PGDATABASE=$(DATABASE) dune build src/ui --profile release
	@$(MAKE) website

openapi:
	@_build/default/src/api/openapi_specs.exe \
		-o ${DEST_DIR}/dunscan_openapi.json \
	 	--license "GPL 3.0" "https://www.gnu.org/licenses/gpl-3.0.en.html" \
	 	--terms "https://dunscan.io/terms-of-services" \
	 	--version "4.0" \
	 	--servers $(API_DOC_HOST) "DunScan API Server" \
	 	--title "DunScan API" \
	 	--contact "contact@tzscan.io"

##########
## INIT ##
##########

submodule:
	@git submodule update

build-deps: dune-explorer.opam
	@opam install --deps-only .

init: submodule build-deps

###########
## UTILS ##
###########

release:
	sudo cp -r ${DEST_DIR}/* /var/www/${DESTSUBDIR}

clean:
	@dune clean
	@rm -f bin/* config/db-version.txt
	@rm -rf ${DEST_DIR}
